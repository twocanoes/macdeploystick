#!/bin/sh

dir=`/usr/bin/mktemp -d ~/.Trash/MDS.XXXXXX`
    /bin/chmod ugo+rx "${dir}"

if [ -d "/Applications/MDS.app" ] ; then
    mv "/Applications/MDS.app" "${dir}";
fi

if [ -e "/Library/LaunchDaemons/com.twocanoes.mdshelpertool.plist" ] ; then
	echo "removing prior MDS Helper LaunchDaemon"
	cd "/Library/LaunchDaemons"
	launchctl stop com.twocanoes.mdshelpertool
	launchctl unload com.twocanoes.mdshelpertool.plist
	rm com.twocanoes.mdshelpertool.plist
	echo "done removing prior MDS Helper LaunchDaemon"
	
fi
	
if [ -e "/Library/PrivilegedHelperTools/com.twocanoes.mdshelpertool" ] ; then
	echo "removing prior MDS Helper Tool"
	rm "/Library/PrivilegedHelperTools/com.twocanoes.mdshelpertool"
	echo "done removing prior MDS Helper Tool"
	
fi
