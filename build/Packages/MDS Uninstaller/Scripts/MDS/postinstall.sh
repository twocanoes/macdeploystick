#!/bin/sh

dir=`/usr/bin/mktemp -d ~/.Trash/MDS.XXXXXX`
    /bin/chmod ugo+rx "${dir}"

if [ -d "/Applications/MDS.app" ] ; then
    mv "/Applications/MDS.app" "${dir}";
fi

if [ -e "/Library/LaunchDaemons/com.twocanoes.mdshelpertool.plist" ] ; then
    echo "removing prior MDS Helper LaunchDaemon"
    cd "/Library/LaunchDaemons"
    launchctl stop com.twocanoes.mdshelpertool
    launchctl unload com.twocanoes.mdshelpertool.plist
    rm com.twocanoes.mdshelpertool.plist
    echo "done removing prior MDS Helper LaunchDaemon"

fi

if [ -e "/Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist" ] ; then
    echo "removing micromdm launchd"
    launchctl unload -w "/Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist"
    rm "/Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist"
    echo "done removing micromdm launchd"
fi

if [ -e "/usr/local/mds-micromdm" ]; then
    echo "putting /usr/local/mds-micromdm in trash"
    mv "/usr/local/mds-micromdm" "${dir}";
fi

if [ -e "/Library/PrivilegedHelperTools/com.twocanoes.mdshelpertool" ] ; then
    echo "removing prior MDS Helper Tool"
    rm "/Library/PrivilegedHelperTools/com.twocanoes.mdshelpertool"
    echo "done removing prior MDS Helper Tool"

fi
