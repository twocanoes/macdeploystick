//
//  SubscriptionManager.m
//  MDS
//
//  Created by Timothy Perfitt on 10/28/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "SubscriptionManager.h"
#import "SubscriptionViewController.h"
#import "AppDelegate.h"
#define kTCSIsSubscribed @"isSubscribed"
#define kTCSEdition @"edition"

@implementation SubscriptionManager
+ (instancetype)shared {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];
        [sharedMyManager readPrefs];

    });

    return sharedMyManager;
}

-(void)showSubcribeWithMessage:(NSString *)message{
    AppDelegate *delegate=(AppDelegate*)[NSApp delegate];

    SubscriptionViewController *subsecriptionViewController=[[SubscriptionViewController shared] selectionSubscriptionViewController];

    subsecriptionViewController.message=message;
    [[delegate.window contentViewController] presentViewControllerAsModalWindow:subsecriptionViewController];

}
-(BOOL)readPrefs{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    self.isSubscribed=[ud boolForKey:kTCSIsSubscribed];
    self.edition=[ud integerForKey:kTCSEdition];
    return YES;
}

-(BOOL)savePrefs{

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    if (self.edition==EDITION_FREE){
        [ud setBool:NO forKey:kTCSIsSubscribed];
    }
    else {
        [ud setBool:YES forKey:kTCSIsSubscribed];
    }
    [ud setInteger:self.edition forKey:kTCSEdition];
    return YES;
}


@end
