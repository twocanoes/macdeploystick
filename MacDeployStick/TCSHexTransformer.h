//
//  TCSHexTransformer.h
//  MDS
//
//  Created by Timothy Perfitt on 7/23/24.
//  Copyright © 2024 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSHexTransformer : NSValueTransformer

@end

NS_ASSUME_NONNULL_END
