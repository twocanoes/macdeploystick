//
//  TCSDefaultsManager.h
//  MDS
//
//  Created by Timothy Perfitt on 7/16/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <AppKit/AppKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSDefaultsManager : NSObject
+ (id)sharedManager;
-(NSString *)newTempFolder;
-(NSString *)tempFolder;
-(void)clearCache;
@end

NS_ASSUME_NONNULL_END
