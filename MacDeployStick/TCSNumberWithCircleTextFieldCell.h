//
//  TCSNumberWithCircleTextFieldCell.h
//  MDS
//
//  Created by Timothy Perfitt on 1/10/22.
//  Copyright © 2022 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSNumberWithCircleTextFieldCell : NSTextFieldCell

@end

NS_ASSUME_NONNULL_END
