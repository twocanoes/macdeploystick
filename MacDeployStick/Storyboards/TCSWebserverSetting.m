//
//  TCSWebserverSetting.m
//  MDS
//
//  Created by Timothy Perfitt on 3/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSWebserverSetting.h"

@implementation TCSWebserverSetting 
+ (BOOL)supportsSecureCoding {
    return YES;
}
- (nonnull id)copyWithZone:(nullable NSZone *)zone {
TCSWebserverSetting *newSetting=[[TCSWebserverSetting alloc] init];

    newSetting.port=_port;
    newSetting.path=[_path copy];
    newSetting.useTLS=_useTLS;
    newSetting.isActive=_isActive;
    newSetting.allowDirectoryListing=_allowDirectoryListing;
    newSetting.enablePHP7=_enablePHP7;
    newSetting.useForMunkiReport=_useForMunkiReport;
    newSetting.useForMunki=_useForMunki;

    return newSetting;

}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.port=[[decoder decodeObjectForKey:@"port"] integerValue];
    self.path=[decoder decodeObjectForKey:@"path"];
    self.useTLS=[decoder decodeBoolForKey:@"useTLS"];
    self.isActive=[decoder decodeBoolForKey:@"isActive"];
    self.allowDirectoryListing=[decoder decodeBoolForKey:@"allowDirectoryListing"];
    self.enablePHP7=[decoder decodeBoolForKey:@"enablePHP7"];
    self.useForMunkiReport=[decoder decodeBoolForKey:@"useForMunkiReport"];
    self.useForMunki=[decoder decodeBoolForKey:@"useForMunki"];

   return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {

    [encoder encodeObject:self.path forKey:@"path"];
    [encoder encodeObject:@(self.port) forKey:@"port"];
    [encoder encodeBool:self.isActive forKey:@"isActive"];
    [encoder encodeBool:self.useTLS forKey:@"useTLS"];
    [encoder encodeBool:self.allowDirectoryListing forKey:@"allowDirectoryListing"];
    [encoder encodeBool:self.enablePHP7 forKey:@"enablePHP7"];
    [encoder encodeBool:self.useForMunkiReport forKey:@"useForMunkiReport"];
    [encoder encodeBool:self.useForMunki forKey:@"useForMunki"];


}
@end
