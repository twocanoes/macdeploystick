//
//  TCSWebserverSetting.h
//  MDS
//
//  Created by Timothy Perfitt on 3/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSWebserverSetting : NSObject <NSCopying,NSSecureCoding>
@property (assign) NSInteger port;
@property (strong) NSString *path;
@property (assign) BOOL useTLS;
@property (assign) BOOL isActive;
@property (assign) BOOL allowDirectoryListing;
@property (assign) BOOL enablePHP7;
@property (assign) BOOL useForMunkiReport;
@property (assign) BOOL useForMunki;

@end

NS_ASSUME_NONNULL_END
