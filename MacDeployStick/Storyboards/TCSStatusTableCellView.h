//
//  TCSStatusTableCellView.h
//  MDS
//
//  Created by Timothy Perfitt on 3/11/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSStatusTableCellView : NSTableCellView
@property (nullable, assign) IBOutlet NSImageView *statusImageView;
@property (nullable, assign) IBOutlet NSImageView *badgeBackgroundImageView;
@property (nullable, assign) IBOutlet NSTextField *badgeTextField;
@property (nullable, assign) IBOutlet NSImageView *serviceStatusImageView;
@property (nullable, assign) IBOutlet NSProgressIndicator *serviceProgressIndicator;

@end

NS_ASSUME_NONNULL_END
