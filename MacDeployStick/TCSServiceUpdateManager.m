//
//  TCSServiceUpdateManager.m
//  MDS
//
//  Created by Timothy Perfitt on 3/11/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSServiceUpdateManager.h"
#import "TCWebConnection.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSConstants.h"
@interface TCSServiceUpdateManager()
@property (strong) TCTaskWrapperWithBlocks *munkiTask;
@property (strong) TCTaskWrapperWithBlocks *microMDMTask;
@end
@implementation TCSServiceUpdateManager
+ (instancetype)updateManager {
    static TCSServiceUpdateManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[TCSServiceUpdateManager alloc] init];
        sharedMyManager.updatesAvailable=[NSMutableDictionary dictionary];

    });
    return sharedMyManager;
}


@end
