//
//  NSData+PEM.m
//  Gate
//
//  Created by Timothy Perfitt on 9/29/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "NSData+PEM.h"
#import "NSData+Base64.h"
@implementation NSData (PEM)
+(NSData *)dataWithPEMString:(NSString *)inString{

   __block NSMutableString *endString=[NSMutableString string];
    __block BOOL foundHeader=NO;
    [[inString componentsSeparatedByString:@"\n"] enumerateObjectsUsingBlock:^(NSString * line, NSUInteger idx, BOOL * _Nonnull stop) {


        if ([line hasPrefix:@"----"]==YES && foundHeader==NO) {
            foundHeader=YES;
            return;
        }

        if ([line hasPrefix:@"----"]==NO && foundHeader==YES) {
            [endString appendFormat:@"%@\n",line];

        }
        if ([line hasPrefix:@"----"]==YES && foundHeader==YES) {
            *stop=YES;
            return;

        }


    }];


    NSData *finalData=[[NSData alloc] initWithBase64EncodedString:endString options:NSDataBase64DecodingIgnoreUnknownCharacters];
//    NSData *finalData=[NSData dataFromBase64String:endString];
    return finalData;

}

@end
