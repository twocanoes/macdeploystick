//
//  TCSButtonSelectFile.m
//  MDS
//
//  Created by Timothy Perfitt on 7/27/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSButtonSelectFile.h"
@interface TCSButtonSelectFile()

@property (strong) NSString *dialogMessage;
@property (strong) NSString *fileExtension;
@property (assign) BOOL canChooseFiles;
@property (assign) BOOL canChooseDirectories;
@property (assign) BOOL allowMultipleSelection;
@property (assign) BOOL isSaveDialog;

@end
@implementation TCSButtonSelectFile
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setAction:@selector(buttonPressed:)];
        [self setTarget:self];

    }
    return self;
}
-(void)buttonPressed:(id)sender{
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    if (self.fileExtension && self.fileExtension.length>0){
        openPanel.allowedFileTypes=[self.fileExtension componentsSeparatedByString:@","];


    }
    openPanel.canChooseFiles=self.canChooseFiles;
    openPanel.canChooseDirectories=self.canChooseDirectories;
    openPanel.allowsMultipleSelection=self.allowMultipleSelection;
    openPanel.message=self.dialogMessage;
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        NSURL *selectedFileURL=[openPanel URL];
        //update the value
        self.textField.stringValue=selectedFileURL.path;

        //update the binding
        NSDictionary *bindingInfo = [self.textField infoForBinding: NSValueBinding];
        [[bindingInfo valueForKey: NSObservedObjectKey] setValue: self.textField.stringValue
                                                      forKeyPath: [bindingInfo valueForKey: NSObservedKeyPathKey]];

    }
}


//- (void)drawRect:(NSRect)dirtyRect {
//    [super drawRect:dirtyRect];
//
//    // Drawing code here.
//}
@end
