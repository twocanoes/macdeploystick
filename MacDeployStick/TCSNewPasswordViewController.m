//
//  TCSNewPasswordViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 9/4/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSNewPasswordViewController.h"

@interface TCSNewPasswordViewController ()

@end

@implementation TCSNewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)okButtonPressed:(id)sender {

    if(self.updatedPassword && self.updatedPassword.length>0) {
        [self.delegate updatePassword:self.updatedPassword forUserGUID:self.userGUID];
        [self dismissViewController:self];
    }

}

@end
