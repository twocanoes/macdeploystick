//
//  TCSWebServiceController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/13/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//
#import "TCSWebServiceController.h"
#import"TCSWebserverSetting.h"
#import "MDSPrivHelperToolController.h"
#import "TCSConstants.h"
#import "TCSUtility.h"
#import <AppKit/AppKit.h>
@interface TCSWebServiceController()
@property (strong) NSArray *webserviceSettings;
@end
@implementation TCSWebServiceController

+ (instancetype)sharedController {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];
        
        [sharedMyManager loadWebserviceSettings];
        
    });
    return sharedMyManager;
}

-(BOOL)hasTLSCertificates:(id)sender{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *certPath=[ud objectForKey:CERTIFICATEPATH];
    NSString *keyPath=[ud objectForKey:INDENTITYKEYPATH];
    if(certPath && certPath.length>0 && keyPath && keyPath.length>0) {
        return YES;
    }
    return NO;
    
}
-(void)updateApacheSettings:(NSArray *)configs restartIfRunningWithCallback:(void (^)(BOOL success))updateCallback{
    //    NSArray *configs=[self.webSettingsArrayController arrangedObjects];
    __block BOOL hasSSL=NO;
    __block BOOL enablePHP7=NO;
    
    NSMutableArray *configArrayToSend=[NSMutableArray array];
    [configs enumerateObjectsUsingBlock:^(TCSWebserverSetting *setting, NSUInteger idx, BOOL * _Nonnull stop) {
        if (setting.isActive==NO) {
            return;
        }
        if (setting.useTLS==YES){
            hasSSL=YES;
        }
        if (setting.enablePHP7==YES){
            enablePHP7=YES;
        }
        [configArrayToSend addObject:setting];
        
    }];
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    
    dict[@"webserverSettings"]=[NSArray arrayWithArray:configArrayToSend];
    if(hasSSL==YES){
        
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        NSString *certPath=[ud objectForKey:CERTIFICATEPATH];
        NSString *keyPath=[ud objectForKey:INDENTITYKEYPATH];
        if(certPath && certPath.length>0 && keyPath && keyPath.length>0) {
            dict[@"certificatePath"]=certPath;
            dict[@"keyPath"]=keyPath;
        }
        dict[@"hasSSL"]=@(hasSSL);
        
    }
    if(enablePHP7==YES){
        

        dict[@"enablePHP7"]=@(enablePHP7);
        dict[@"phpLibPath"]=@"/usr/local/mds-php/lib/libphp7.so";
    }
    [[MDSPrivHelperToolController sharedHelper] updateWebserverWithConfigurations:[NSDictionary dictionaryWithDictionary:dict] withCallback:^(BOOL success) {
        if (success==YES){
            
            if ([self isRunning]==YES){
                
                [[MDSPrivHelperToolController sharedHelper]     restartWebserverWithCallback:^(BOOL success) {
                    if (success==NO) {
                        
                        updateCallback(NO);//[self showApacheError:@"There was an error restarting the web service. Please check the webserver log."];
                        return ;
                    }
                    
                    
                }];
//                [[TCSWebServiceController sharedController] restartServiceWithCallback:^(BOOL hadSuccess) {
//                    
//                }];
            }
            
        }
        else {
            if (success==NO) {
                updateCallback(NO);
                return;
            }//[self showApacheError:@"There was an error updating settings and restarting the web service. Please check the webserver log"];
            
        }
        updateCallback(success);
    }];
}

-(void)restartServiceWithCallback:(void (^)(BOOL hadSuccess))updateCallback{
    [[MDSPrivHelperToolController sharedHelper] restartWebserverWithCallback:^(BOOL success) {
        updateCallback(success);
        
        
    }];
    
}
-(BOOL)isRunning
{
    return [TCSUtility isProcessRunning:TCSPROCESSNAMEAPACHE];
    
}


-(void)showApacheError:(NSString *)errorMsg{
    
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Web Server Error";
    alert.informativeText=errorMsg;
    
    [alert addButtonWithTitle:@"OK"];
    [alert runModal];
    
}
-(void)loadWebserviceSettings{
    
    NSArray *newWebsites;
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSData *websiteData=[ud objectForKey:@"websites"];
    if (websiteData) {
        NSSet *set = [NSSet setWithArray:@[
            [TCSWebserverSetting class],
            [NSArray class],
            
        ]];
        NSError *err;
        if (@available(macOS 10.13, *)) {
            newWebsites=[NSKeyedUnarchiver unarchivedObjectOfClasses:set fromData:websiteData error: &err];
            if (err) {
                NSLog(@"%@",[err localizedDescription]);
            }
        }
    }
    else newWebsites=@[];
    
    self.webserviceSettings=newWebsites;
    
}
-(void)saveSettings:(NSArray <TCSWebserverSetting *> *)inSettings callback:(void (^)(BOOL hadSuccess))updateCallback{

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSError *err;
    NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:inSettings requiringSecureCoding:YES error:&err];
    
    [ud setObject:dataToSave forKey:@"websites"];
    
    [self loadWebserviceSettings];
    [[TCSWebServiceController sharedController] updateApacheSettings:inSettings restartIfRunningWithCallback:^(BOOL success) {
        
        updateCallback(success);
        
    }];
    
}
-(void)updateConfiguration:(TCSWebserverSetting *)inSetting atIndex:(NSInteger)idx{
    NSArray <TCSWebserverSetting* > *settingArray=[self webserviceSettings];
    if (idx==-1) {
        
        settingArray=[settingArray arrayByAddingObject:inSetting];
    }
    else {
        
        NSMutableArray *proxyArray=[self mutableArrayValueForKey:@"sharedWebsites"];
        [proxyArray replaceObjectAtIndex:idx withObject:inSetting];
        //        self.webSettingsArrayController.selectionIndex=self.editingWorkflowAtIndex;
        
    }
    //    [[TCSWebServiceController sharedController] saveSettings:settingArray callback:^(BOOL hadSuccess) {
    //        if (hadSuccess==NO) [self showApacheError:@"There was an error updating settings and restarting the web service. Please check the webserver log"];
    //
    //    }];
    
    
}
-(TCSWebserverSetting *)munkiWebserverSettings{
    NSArray *webSettings=[self webserviceSettings];
    
    __block TCSWebserverSetting *munkiSetting=nil;
    [webSettings enumerateObjectsUsingBlock:^(TCSWebserverSetting *setting, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (setting.useForMunki==YES){
            munkiSetting=setting;
            *stop=YES;
        }
    }];
    return munkiSetting;
}



-(void)stopWebServicesWithCallback:(void (^)(BOOL success, BOOL didCancel))callback{

    [[MDSPrivHelperToolController sharedHelper] stopWebserverWithCallback:^(BOOL success) {
        callback(success,NO);
    }];

};

-(void)startWebServicesWithCallback:(void (^)(BOOL success, BOOL didCancel))callback{
    dispatch_async(dispatch_get_main_queue(), ^{
        
//        NSAlert *alert=[[NSAlert alloc] init];
//        alert.messageText=@"Web Service Status";
//        if ([[TCSWebServiceController sharedController] isRunning]==NO){
//            
//            alert.informativeText=@"Do you want to start the web service?";
//            [alert addButtonWithTitle:@"Start Web Service"];
//        }
//        else {
//            alert.informativeText=@"Do you want to restart the web service?";
//            [alert addButtonWithTitle:@"Restart Web Service"];
//            
//        }
//        
//        [alert addButtonWithTitle:@"Close"];
//        
//        NSInteger res=[alert runModal];
//        
//        if (res==NSAlertSecondButtonReturn) {
//            callback(NO,YES);
//            return;
//        }
        [[MDSPrivHelperToolController sharedHelper] startWebserverWithCallback:^(BOOL success) {
            callback(success,NO);
            
        }];
    });
    
    
}
@end
