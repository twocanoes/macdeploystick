//
//  TCSSaveMasterViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 5/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSSaveMasterViewController.h"
#import "DeploySettings.h"
#import "TCDiskImageHelper.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSWorkflow.h"
#import "DeploySettings.h"
@interface TCSSaveMasterViewController ()
@property (strong) NSMutableArray *resourcePaths;
@property (assign) BOOL isRunning;
@property (assign) BOOL isStopping;
@property (strong) TCTaskWrapperWithBlocks *tw;
@end

@implementation TCSSaveMasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.preferredContentSize=self.view.frame.size;

}
- (IBAction)stopCopyingButtonPressed:(NSButton *)sender {
    self.status=@"Stopping...Please wait";

    self.isStopping=YES;
    if (self.isRunning==YES){

        [self.tw stopProcess];
    }
}
-(void)syncNow:(id)sender{

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    NSString *syncURLString=[ud objectForKey:@"syncURL"];
    if (!syncURLString || syncURLString.length==0){

        self.isRunning=NO;
        [self dismissViewController:self];

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Sync URL not found";
        alert.informativeText=@"The sync URL in preferences could not be found. Please open preferences and enter a sync URL to a master image.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        return ;

    }
    if (![syncURLString containsString:@"://"] && [syncURLString hasPrefix:@"/"]){

        syncURLString=[NSString stringWithFormat:@"file://%@",syncURLString];
    }

    TCDiskImageHelper *diskImageHelper=[[TCDiskImageHelper alloc] init];


    NSString *mountPoint=[diskImageHelper mountPointForImage:syncURLString];

    if (!mountPoint) {
        NSLog(@"The disk image could not be mounted: %@",syncURLString);
        self.isRunning=NO;
        [self dismissViewController:self];

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Mount Error";
        alert.informativeText=[NSString stringWithFormat:@"The disk image at \"%@\" could not be mounted. Check the Sync URL in preferences and your network connection and try again.",syncURLString];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        return ;

    }
    self.isRunning=YES;
    NSString *rsync=[[NSBundle mainBundle] pathForResource:@"rsync" ofType:nil];


    NSString *hash=[NSString stringWithFormat:@"%lx",[syncURLString hash]];
    NSString *syncFolder=[[DeploySettings syncFolder] stringByAppendingPathComponent:hash];

    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:syncFolder]==NO){

        NSError *err;
        if ([fm createDirectoryAtPath:syncFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return ;


        }
    }

    if (![[mountPoint substringFromIndex:mountPoint.length-1] isEqualToString:@"/"]){
        mountPoint=[mountPoint stringByAppendingString:@"/"];
    }
    self.tw=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

        dispatch_async(dispatch_get_main_queue(), ^{

            self.status=@"Starting Sync...";
        });
    } endBlock:^{

        [self importWorkflowsFromFolder:syncFolder];
        [self unmountAtPath:mountPoint];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.tw.terminationStatus!=0) {
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Sync Issue";
                alert.informativeText=@"There was an issue with syncing. Please check the disk and try again.";

                [alert addButtonWithTitle:@"OK"];
                [alert runModal];


            }
            self.isRunning=NO;
            [self dismissViewController:self];
        });

    } outputBlock:^(NSString *output) {

        dispatch_async(dispatch_get_main_queue(), ^{

            NSArray *logArray=[output componentsSeparatedByString:@"\n"];

            for (long i=logArray.count-1;i>=0;i--){

                if ([[logArray objectAtIndex:i] length]==0) {
                    continue;
                }
                NSArray *parts=[[logArray objectAtIndex:i] componentsSeparatedByString:@"/"];
                NSString *filename=[parts lastObject];
                if (parts.count<3) continue;

                self.status=[NSString stringWithFormat:@"Copying %@",filename];

                break;
            }



        });
    } errorOutputBlock:^(NSString *errorOutput) {

        NSLog(@"output2: %@",errorOutput);
    } arguments:@[rsync,@"-av",@"--delete",mountPoint,syncFolder]];
    [self.tw startProcess];



}

-(void)importWorkflowsFromFolder:(NSString *)folder{

    NSString *syncFolder=folder;

    NSString *workflowsPath=[syncFolder stringByAppendingPathComponent:@"master.mdsworkflows"];
    NSLog(@"-------Importing Workflows------- from %@",workflowsPath);

    NSData *data=[NSData dataWithContentsOfFile:workflowsPath];

   NSArray *workflows=[DeploySettings workflowArrayFromData:data];

    [workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
        currWorkflow.isReadonly=YES;
        NSLog(@"-------Importing Workflow: %@-------",currWorkflow.workflowName);
        if (currWorkflow.packagesFolderPath){
            currWorkflow.packagesFolderPath=[syncFolder stringByAppendingPathComponent:currWorkflow.packagesFolderPath];
        }
        if (currWorkflow.scriptFolderPath){
            currWorkflow.scriptFolderPath=[syncFolder stringByAppendingPathComponent:currWorkflow.scriptFolderPath];
        }
        if (currWorkflow.profilesFolderPath){
            currWorkflow.profilesFolderPath=[syncFolder stringByAppendingPathComponent:currWorkflow.profilesFolderPath];
        }

        if (currWorkflow.macOSFolderPath){
            currWorkflow.macOSFolderPath=[syncFolder stringByAppendingPathComponent:currWorkflow.macOSFolderPath];
        }
        if (currWorkflow.macOSImagePath){
            currWorkflow.macOSImagePath=[syncFolder stringByAppendingPathComponent:currWorkflow.macOSImagePath];
        }



        [[DeploySettings sharedSettings] addWorkflow:currWorkflow];

    }];


}
-(void)saveMasterToPath:(NSString *)saveVolPath{
    self.status=@"Creating Disk Image";
    self.isRunning=YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{


        TCDiskImageHelper *dih=[[TCDiskImageHelper alloc] init];
        NSFileManager *fm=[NSFileManager defaultManager];

        NSString *masterImagePath=[dih createSparseBundleDiskImageAtPath:saveVolPath volumeName:@"MDSMaster"];
        NSString *resourceDir=[masterImagePath stringByAppendingPathComponent:@"Resources"];

        NSError *err;
        if([fm createDirectoryAtPath:resourceDir withIntermediateDirectories:NO attributes:nil error:&err]==NO){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[NSAlert alertWithError:err] runModal];
                [self dismissViewController:self];
                
            });
            return;
        }
        
        NSMutableArray *newWorkflows=[NSMutableArray array];
        self.resourcePaths=[NSMutableArray array];
        NSArray *workflows=[[DeploySettings sharedSettings] workflows] ;

        [workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

            if (currWorkflow.isActive==NO) return;
            TCSWorkflow *newWorkflow=[currWorkflow copy];

            if (self.isStopping==NO && currWorkflow.shouldInstall==YES && currWorkflow.macOSFolderPath) {

                if ([fm fileExistsAtPath:currWorkflow.macOSFolderPath]==NO){
                    dispatch_async(dispatch_get_main_queue(), ^{

                        NSAlert *alert=[[NSAlert alloc] init];
                         alert.messageText=@"macOS Installer Not Found";
                         alert.informativeText=[NSString stringWithFormat:@"The macOS installer in the workflow %@ does not exist. Please verify and try again.",currWorkflow.workflowName];

                         [alert addButtonWithTitle:@"OK"];
                         [alert runModal];
                    });
                    *stop=self.isStopping=YES;
                    return;


                }
                newWorkflow.macOSFolderPath=[self copyIfNeeded:currWorkflow.macOSFolderPath destination:resourceDir];
                if (!newWorkflow.macOSFolderPath) {
                    *stop=self.isStopping=YES;
                    return;
                }
            }
            else if (self.isStopping==NO && currWorkflow.shouldImage==YES && currWorkflow.macOSImagePath) {

                if ([fm fileExistsAtPath:currWorkflow.macOSImagePath]==NO){
                    dispatch_async(dispatch_get_main_queue(), ^{

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"macOS ASR Image Not Found";
                        alert.informativeText=[NSString stringWithFormat:@"The macOS ASR Image in the workflow %@ does not exist. Please verify and try again.",currWorkflow.workflowName];

                        [alert addButtonWithTitle:@"OK"];
                        [alert runModal];
                    });
                    *stop=self.isStopping=YES;
                    return;


                }
                newWorkflow.macOSImagePath=[self copyIfNeeded:currWorkflow.macOSImagePath destination:resourceDir];
                if (!newWorkflow.macOSImagePath) {
                    *stop=self.isStopping=YES;
                    return;
                }
            }


            if (self.isStopping==NO && currWorkflow.usePackagesFolderPath==YES && currWorkflow.packagesFolderPath)  {

                if ([fm fileExistsAtPath:currWorkflow.packagesFolderPath]==NO){
                    dispatch_async(dispatch_get_main_queue(), ^{

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"macOS Installer Not Found";
                        alert.informativeText=[NSString stringWithFormat:@"The packages path in the workflow %@ does not exist. Please verify and try again.",currWorkflow.workflowName];

                        [alert addButtonWithTitle:@"OK"];
                        [alert runModal];
                    });
                    *stop=self.isStopping=YES;
                    return;


                }


                newWorkflow.packagesFolderPath=[self copyIfNeeded:currWorkflow.packagesFolderPath destination:resourceDir];
                if (!newWorkflow.packagesFolderPath) {
                    *stop=self.isStopping=YES;
                    return;
                }
            }
            if (self.isStopping==NO && currWorkflow.useScriptFolderPath==YES && currWorkflow.scriptFolderPath )  {
                if (self.isStopping==NO && currWorkflow.usePackagesFolderPath==YES && currWorkflow.packagesFolderPath)  {

                    if ([fm fileExistsAtPath:currWorkflow.scriptFolderPath]==NO){
                        dispatch_async(dispatch_get_main_queue(), ^{

                            NSAlert *alert=[[NSAlert alloc] init];
                            alert.messageText=@"Script Path Not Found";
                            alert.informativeText=[NSString stringWithFormat:@"The script folder path in the workflow %@ does not exist. Please verify and try again.",currWorkflow.workflowName];

                            [alert addButtonWithTitle:@"OK"];
                            [alert runModal];
                        });
                        *stop=self.isStopping=YES;
                        return;


                    }
                }
                newWorkflow.scriptFolderPath=[self copyIfNeeded:currWorkflow.scriptFolderPath destination:resourceDir];
                if (!newWorkflow.scriptFolderPath) {
                    *stop=self.isStopping=YES;
                    return;
                }
            }
            if (self.isStopping==NO && currWorkflow.useProfilesFolderPath==YES && currWorkflow.profilesFolderPath) {
                if (self.isStopping==NO && currWorkflow.usePackagesFolderPath==YES && currWorkflow.packagesFolderPath)  {

                    if ([fm fileExistsAtPath:currWorkflow.profilesFolderPath]==NO){
                        dispatch_async(dispatch_get_main_queue(), ^{

                            NSAlert *alert=[[NSAlert alloc] init];
                            alert.messageText=@"Profile Path Not Found";
                            alert.informativeText=[NSString stringWithFormat:@"The profiles folder path in the workflow %@ does not exist. Please verify and try again.",currWorkflow.workflowName];

                            [alert addButtonWithTitle:@"OK"];
                            [alert runModal];

                        });
                        *stop=self.isStopping=YES;
                        return;


                    }
                }
                newWorkflow.profilesFolderPath=[self copyIfNeeded:currWorkflow.profilesFolderPath destination:resourceDir];
                if (!newWorkflow.profilesFolderPath) {
                    *stop=self.isStopping=YES;
                    return;
                }
            }

            if (self.isStopping==NO ) [newWorkflows addObject:newWorkflow];
        }];

        if (newWorkflows.count>0) {
            DeploySettings *ds=[[DeploySettings alloc] initWithWorkflows:newWorkflows];

            if(self.isStopping==NO && [ds.workflowData writeToFile:[masterImagePath stringByAppendingPathComponent:@"master.mdsworkflows"] atomically:NO]==NO){
                NSLog(@"Could not save data!");
            }
        }
        else if (self.isStopping==NO){
            dispatch_async(dispatch_get_main_queue(), ^{

                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"No Workflows Exported";
                alert.informativeText=@"No Active Workflows found so no resources were exported to the master disk image";

                [alert addButtonWithTitle:@"OK"];
                [alert runModal];
            });
            self.isStopping=YES; // set this so image is removed.

        }
        [self unmountAtPath:masterImagePath];
        if (self.isStopping==YES){
            NSError *err;
            if([fm removeItemAtPath:saveVolPath error:&err]==NO){
                dispatch_async(dispatch_get_main_queue(), ^{

                    [[NSAlert alertWithError:err] runModal];
                });

            }
        }
        self.isRunning=NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewController:self];
        });

    });
}
-(void)unmountAtPath:(NSString *)inPath{
    NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/diskutil" arguments:@[@"unmount",inPath]];
    [task waitUntilExit];

}
- (BOOL)fileManager:(NSFileManager *)fileManager shouldCopyItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath{

    if (self.isStopping==YES) {
        return NO;

    }
    NSLog(@"copying %@ to %@",srcPath,dstPath);

    dispatch_async(dispatch_get_main_queue(), ^{
        self.status=[NSString stringWithFormat:@"Copying %@",srcPath.lastPathComponent];

    });
    return YES;

}
-(NSString *)copyIfNeeded:(NSString *)inPath destination:(NSString *)resourceDestinationPath{
    //    currWorkflow.packagesFolderPath
    NSFileManager *fm=[NSFileManager defaultManager];
    fm.delegate=self;

    NSString *destPath=[resourceDestinationPath stringByAppendingPathComponent:inPath.lastPathComponent];

    if ([self.resourcePaths containsObject:inPath]==NO) {

        [self.resourcePaths addObject:inPath];
        if (inPath && [fm fileExistsAtPath:inPath]==YES){

            if([fm fileExistsAtPath:[resourceDestinationPath stringByAppendingPathComponent:inPath.lastPathComponent]]==YES){

                while ([fm fileExistsAtPath:destPath]==YES){
                    destPath=[destPath stringByAppendingString:@"-1"];
                }
            }
            NSError *err;

            if([fm copyItemAtPath:inPath toPath:destPath error:&err]==NO) {

                dispatch_async(dispatch_get_main_queue(), ^{

                    [[NSAlert alertWithError:err] runModal];

                });


                NSLog(@"Error copying file: %@",err.localizedDescription);
                return nil;

            }

        }
    }
    NSString *relativeDestPath=[NSString stringWithFormat:@"Resources/%@",destPath.lastPathComponent];

    return relativeDestPath;
}




@end
