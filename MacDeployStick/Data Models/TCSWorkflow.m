//
//  Workflow.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/23/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "TCSWorkflow.h"

@implementation TCSWorkflowUser 


+ (BOOL)supportsSecureCoding {
    return YES;
}
- (instancetype)init
    {
        self = [super init];
        if (self) {
//            self.uid=@"501";
            self.shell=@"/bin/zsh";
        }
        return self;
    }

- (nonnull id)copyWithZone:(nullable NSZone *)zone {

    TCSWorkflowUser *newWorkflowUser=[[TCSWorkflowUser alloc] init];

    newWorkflowUser.userUUID=_userUUID;
    newWorkflowUser.isAdmin=_isAdmin;
    newWorkflowUser.shouldAutologin=_shouldAutologin;
    newWorkflowUser.isHidden=_isHidden;
    newWorkflowUser.fullName=[_fullName copy];
    newWorkflowUser.shortName=[_shortName copy];
    newWorkflowUser.sshKey=[_sshKey copy];
    newWorkflowUser.shell=[_shell copy];

    newWorkflowUser.jpegPhoto=[_jpegPhoto copy];
    newWorkflowUser.passwordHint=[_passwordHint copy];
    newWorkflowUser.uid=[_uid copy];
    newWorkflowUser.password=[_password copy];


    return newWorkflowUser;
}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }

    self.userUUID=[decoder decodeObjectForKey:@"userUUID"];
    self.isAdmin=[decoder decodeBoolForKey:@"isAdmin"];
    self.shouldAutologin=[decoder decodeBoolForKey:@"shouldAutologin"];
    self.isHidden=[decoder decodeBoolForKey:@"isHidden"];

    self.fullName=[decoder decodeObjectForKey:@"fullName"];
    self.shortName=[decoder decodeObjectForKey:@"shortName"];
    self.sshKey=[decoder decodeObjectForKey:@"sshKey"];
    self.shell=[decoder decodeObjectForKey:@"shell"];

    self.jpegPhoto=[decoder decodeObjectForKey:@"jpegPhoto"];
    self.passwordHint=[decoder decodeObjectForKey:@"passwordHint"];
    self.uid=[decoder decodeObjectForKey:@"uid"];
    self.password=[decoder decodeObjectForKey:@"password"];


    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {

    [encoder encodeObject:self.userUUID forKey:@"userUUID"];
    [encoder encodeBool:self.isAdmin forKey:@"isAdmin"];
    [encoder encodeBool:self.shouldAutologin forKey:@"shouldAutologin"];
    [encoder encodeBool:self.isHidden forKey:@"isHidden"];

    [encoder encodeObject:self.fullName forKey:@"fullName"];
    [encoder encodeObject:self.shortName forKey:@"shortName"];
    [encoder encodeObject:self.sshKey forKey:@"sshKey"];
    [encoder encodeObject:self.shell forKey:@"shell"];

    [encoder encodeObject:self.jpegPhoto forKey:@"jpegPhoto"];
    [encoder encodeObject:self.passwordHint forKey:@"passwordHint"];
    [encoder encodeObject:self.uid forKey:@"uid"];
    [encoder encodeObject:self.password forKey:@"password"];

}
@end
@implementation TCSWorkflowLocalization

-(id)copyWithZone:(NSZone *)zone{
    TCSWorkflowLocalization *newWorkflowLocalization=[[TCSWorkflowLocalization alloc] init];
    newWorkflowLocalization.keyboardLayout=[_keyboardLayout copy];
    newWorkflowLocalization.language=[_language copy];
    newWorkflowLocalization.locale=[_locale copy];
    newWorkflowLocalization.timezone=[_timezone copy];
    return newWorkflowLocalization;
}


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.keyboardLayout = [decoder decodeObjectForKey:@"keyboardLayout"];
    self.language = [decoder decodeObjectForKey:@"language"];
    self.locale = [decoder decodeObjectForKey:@"locale"];
    self.timezone = [decoder decodeObjectForKey:@"timezone"];

    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {

    [encoder encodeObject:self.keyboardLayout forKey:@"keyboardLayout"];

    [encoder encodeObject:self.language forKey:@"language"];
    [encoder encodeObject:self.locale forKey:@"locale"];
    [encoder encodeObject:self.timezone forKey:@"timezone"];
}
+ (BOOL)supportsSecureCoding {
    return YES;
}
@end

@implementation TCSWorkflow
+ (BOOL)supportsSecureCoding {
    return YES;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.workflowLocalization=[[TCSWorkflowLocalization alloc] init];
        self.isActive=YES;
        self.users=[NSMutableArray array];
        self.firstRunPackages=[NSMutableArray array];
        self.disabledPackages=[NSMutableArray array];
        self.disabledScripts=[NSMutableArray array];
        self.disabledPackages=[NSMutableArray array];

        self.mdsVariables=[NSMutableArray array];
        for (int i=1;i<10;i++){
            TCSMDSVariable *newMDSVariable=[[TCSMDSVariable alloc] init];
            newMDSVariable.mdsVariableName=[NSString stringWithFormat:@"mds_var%i",i];

            [self.mdsVariables addObject:newMDSVariable];
        }
        self.workflowFirstRunScripts=[NSMutableArray array];
        self.shouldWaitForNetwork=YES;
        self.rebootAfterFirstBootResources=YES;
        self.shouldSetTargetAsStartupDisk=YES;
    }
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone {

    TCSWorkflow *newWorkflow=[[TCSWorkflow alloc] init];
    newWorkflow.workflowName=[_workflowName copy];
    newWorkflow.shouldConfigureWifi=_shouldConfigureWifi;
    newWorkflow.wifiSSID=[_wifiSSID copy];
    newWorkflow.wifiPassword=[_wifiPassword copy];
    newWorkflow.shouldWaitForNetwork=_shouldWaitForNetwork;

    newWorkflow.workflowDescription=[_workflowDescription copy];
    newWorkflow.workflowAllowedModels=[_workflowAllowedModels copy];
    newWorkflow.shouldLimitWorkflowsByModelType=_shouldLimitWorkflowsByModelType;

    newWorkflow.workflowUUID=[_workflowUUID copy];
    newWorkflow.useComputerName=_useComputerName ;
    newWorkflow.computerName=_computerName;

    newWorkflow.prePackageScripts=[_prePackageScripts copy];
    newWorkflow.preInstallPackages=[_preInstallPackages copy];
    newWorkflow.prePackageProfiles=[_prePackageProfiles copy];

    newWorkflow.firstRunProfiles=[_firstRunProfiles copy];
    newWorkflow.firstRunPackages=[_firstRunPackages copy];
    newWorkflow.disabledPackages=[_disabledPackages copy];
    newWorkflow.disabledProfiles=[_disabledProfiles copy];
    newWorkflow.disabledScripts=[_disabledScripts copy];
    newWorkflow.workflowFirstRunScripts=[_workflowFirstRunScripts copy];
//    if (!newWorkflow.firstRunPackages) newWorkflow.firstRunPackages=[NSMutableArray array];


    newWorkflow.useScriptFolderPath=_useScriptFolderPath ;
    newWorkflow.scriptFolderPath=[_scriptFolderPath copy];
    newWorkflow.scriptFolderOrder=[_scriptFolderOrder copy];

    newWorkflow.useProfilesFolderPath=_useProfilesFolderPath ;
    newWorkflow.profilesFolderPath=[_profilesFolderPath copy];
    newWorkflow.profilesFolderOrder=[_profilesFolderOrder copy];

    newWorkflow.shouldRebootAfterInstallingResources=_shouldRebootAfterInstallingResources;
    newWorkflow.shouldRebootAfterInstallingResourcesInstallOS=_shouldRebootAfterInstallingResourcesInstallOS;

    newWorkflow.rebootAfterFirstBootResources=_rebootAfterFirstBootResources;
    newWorkflow.waitForMacBuddyExitBeforeRebooting=_waitForMacBuddyExitBeforeRebooting;

    newWorkflow.restartOrShutdownIndex=_restartOrShutdownIndex;

    newWorkflow.restartOrShutdownIndexInstallOS=_restartOrShutdownIndexInstallOS;

//create user stuff
    newWorkflow.users=[_users copy];
    newWorkflow.shouldCreateUser=_shouldCreateUser;
    newWorkflow.shouldCreateUserAdmin=_shouldCreateUserAdmin;
    newWorkflow.shouldCreateUserAutologin=_shouldCreateUserAutologin;
    newWorkflow.shouldCreateUserHidden=_shouldCreateUserHidden;
    newWorkflow.createUserFullName=[_createUserFullName copy];
    newWorkflow.createUserShortName=[_createUserShortName copy];
    newWorkflow.createUserSSHKey=[_createUserSSHKey copy];
    newWorkflow.createUserShell=[_createUserShell copy];

    newWorkflow.passwordHint=[_passwordHint copy];

    newWorkflow.createUserUID=[_createUserUID copy];
    newWorkflow.createUserPassword=[_createUserPassword copy];

    newWorkflow.usePackagesFolderPath=_usePackagesFolderPath;
    newWorkflow.packagesFolderPath=[_packagesFolderPath copy];
    newWorkflow.packagesFolderOrder=[_packagesFolderOrder copy];

    newWorkflow.packagesFolderBookmark=[_packagesFolderBookmark copy];

    newWorkflow.macOSImagePath=[_macOSImagePath copy];

    newWorkflow.macOSFolderPath=[_macOSFolderPath copy];
    newWorkflow.macOSInstallerDMGURL=[_macOSInstallerDMGURL copy];
    newWorkflow.asrImageURL=[_asrImageURL copy];

    newWorkflow.discoverCredentialsScriptPath=[_discoverCredentialsScriptPath copy];

    newWorkflow.shouldUseScriptToDiscoverCredentials=_shouldUseScriptToDiscoverCredentials;
    newWorkflow.loginPromptForPassword=_loginPromptForPassword;

    newWorkflow.shouldEraseAndInstall=_shouldEraseAndInstall;
    newWorkflow.shouldRebootAndInstall=_shouldRebootAndInstall;
    newWorkflow.loginInstallUsername=[_loginInstallUsername copy];
    newWorkflow.loginInstallPassword=[_loginInstallPassword copy];

    newWorkflow.shouldRunSoftwareUpdate=_shouldRunSoftwareUpdate;

    newWorkflow.shouldSetComputerName=_shouldSetComputerName;
    newWorkflow.computerNameString=[_computerNameString copy];
    newWorkflow.shouldPromptForComputerName=_shouldPromptForComputerName;

    newWorkflow.shouldSkipSetupAssistant=_shouldSkipSetupAssistant;
    newWorkflow.computerNameTypeIndex=_computerNameTypeIndex;
    newWorkflow.shouldEnableLocationServices=_shouldEnableLocationServices;

    newWorkflow.shouldSetVolumeName=_shouldSetVolumeName;
    newWorkflow.volumeName=_volumeName;

    newWorkflow.shouldSkipPrivacySetup=_shouldSkipPrivacySetup;
    newWorkflow.shouldEnableSSH=_shouldEnableSSH;
    newWorkflow.shouldEnableMunkiReport=_shouldEnableMunkiReport;
    newWorkflow.shouldSetTargetAsStartupDisk=_shouldSetTargetAsStartupDisk;
    newWorkflow.shouldSkipRosetta=_shouldSkipRosetta;

    newWorkflow.munkiReportEnrollmentScript=_munkiReportEnrollmentScript;
    newWorkflow.shouldEnableScreenSharing=_shouldEnableScreenSharing;
    newWorkflow.shouldEnableARD=_shouldEnableARD;

    newWorkflow.shouldDisableSIP=_shouldDisableSIP;

    
    newWorkflow.shouldInstall=_shouldInstall;

    newWorkflow.shouldImage=_shouldImage;
    newWorkflow.shouldSkipMacOSInstall=_shouldSkipMacOSInstall;
    newWorkflow.shouldUseMacOSInstallerURL=_shouldUseMacOSInstallerURL;
    newWorkflow.shouldUseAsrDmgURL=_shouldUseAsrDmgURL;

    newWorkflow.shouldBlessTarget=_shouldBlessTarget;

    newWorkflow.isActive=_isActive;
    newWorkflow.isDefault=NO;
    newWorkflow.isReadonly=_isReadonly;

    newWorkflow.useWorkflowLocalization=_useWorkflowLocalization;
    newWorkflow.workflowLocalization=[_workflowLocalization copy];

    newWorkflow.shouldSetVariables=_shouldSetVariables;
    newWorkflow.mdsVar1=[_mdsVar1 copy];
    newWorkflow.mdsVar2=[_mdsVar2 copy];
    newWorkflow.mdsVar3=[_mdsVar3 copy];
    newWorkflow.mdsVar4=[_mdsVar4 copy];
    newWorkflow.mdsVar5=[_mdsVar5 copy];
    newWorkflow.mdsVar6=[_mdsVar6 copy];
    newWorkflow.mdsVar7=[_mdsVar7 copy];
    newWorkflow.mdsVar8=[_mdsVar8 copy];
    newWorkflow.mdsVar9=[_mdsVar9 copy];

    newWorkflow.mdsVariables=[_mdsVariables copy];

    newWorkflow.munkiWorkflowURL=[_munkiWorkflowURL copy];
    newWorkflow.munkiClientInstallerPath=[_munkiClientInstallerPath copy];
    newWorkflow.shouldConfigureMunkiClient=_shouldConfigureMunkiClient;
    newWorkflow.munkiClientCertificate=[_munkiClientCertificate copy];
    newWorkflow.shouldTrustMunkiClientCertificate=_shouldTrustMunkiClientCertificate;
    newWorkflow.shouldTrustServerCertificate=_shouldTrustServerCertificate;

    return newWorkflow;
}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.isActive = [decoder decodeBoolForKey:@"isActive"];
    self.isDefault = [decoder decodeBoolForKey:@"isDefault"];
    self.isReadonly = [decoder decodeBoolForKey:@"isReadonly"];

    self.workflowName = [decoder decodeObjectForKey:@"workflowName"];
    self.shouldConfigureWifi = [decoder decodeBoolForKey:@"shouldConfigureWifi"];
    self.wifiPassword = [decoder decodeObjectForKey:@"wifiPassword"];
    self.wifiSSID = [decoder decodeObjectForKey:@"wifiSSID"];
    self.shouldWaitForNetwork = [decoder decodeBoolForKey:@"shouldWaitForNetwork"];


    self.workflowDescription = [decoder decodeObjectForKey:@"workflowDescription"];
    self.workflowAllowedModels = [decoder decodeObjectForKey:@"workflowAllowedModels"];
    self.shouldLimitWorkflowsByModelType = [decoder decodeBoolForKey:@"shouldLimitWorkflowsByModelType"];

    self.workflowUUID = [decoder decodeObjectForKey:@"workflowUUID"];

    self.useComputerName = [decoder decodeBoolForKey:@"useComputerName"];

    self.computerName = [decoder decodeIntegerForKey:@"computerName"];
    self.useScriptFolderPath = [decoder decodeBoolForKey:@"useScriptFolderPath"];

    self.prePackageScripts = [decoder decodeObjectForKey:@"prePackageScripts"];
    self.preInstallPackages = [decoder decodeObjectForKey:@"preInstallPackages"];
    self.prePackageProfiles = [decoder decodeObjectForKey:@"prePackageProfiles"];

    self.firstRunProfiles = [decoder decodeObjectForKey:@"firstRunProfiles"];
    self.firstRunPackages = [decoder decodeObjectForKey:@"firstRunPackages"];
    self.disabledPackages = [decoder decodeObjectForKey:@"disabledPackages"];
    self.disabledProfiles = [decoder decodeObjectForKey:@"disabledProfiles"];
    self.disabledScripts = [decoder decodeObjectForKey:@"disabledScripts"];

    self.workflowFirstRunScripts = [decoder decodeObjectForKey:@"workflowFirstRunScripts"];

    if (!self.workflowFirstRunScripts) {

        self.workflowFirstRunScripts=[NSMutableArray array];
    }
    self.scriptFolderPath = [decoder decodeObjectForKey:@"scriptFolderPath"];
    self.scriptFolderOrder = [decoder decodeObjectForKey:@"scriptFolderOrder"];

    self.useProfilesFolderPath = [decoder decodeBoolForKey:@"useProfilesFolderPath"];
    self.profilesFolderPath = [decoder decodeObjectForKey:@"profilesFolderPath"];
    self.profilesFolderOrder = [decoder decodeObjectForKey:@"profilesFolderOrder"];

    self.shouldRebootAfterInstallingResources=[decoder decodeBoolForKey:@"shouldRebootAfterInstallingResources"];
    self.shouldRebootAfterInstallingResourcesInstallOS=[decoder decodeBoolForKey:@"shouldRebootAfterInstallingResourcesInstallOS"];

    if ([decoder containsValueForKey:@"rebootAfterFirstBootResources"]){
        self.rebootAfterFirstBootResources=[decoder decodeBoolForKey:@"rebootAfterFirstBootResources"];
    }
    else {
        self.rebootAfterFirstBootResources=YES;
    }

    self.waitForMacBuddyExitBeforeRebooting=[decoder decodeBoolForKey:@"waitForMacBuddyExitBeforeRebooting"];

    self.restartOrShutdownIndex=[decoder decodeIntegerForKey:@"restartOrShutdownIndex"];
    self.restartOrShutdownIndexInstallOS=[decoder decodeIntegerForKey:@"restartOrShutdownIndexInstallOS"];

//user stuff
    self.users=[[decoder decodeObjectForKey:@"users"] mutableCopy];
    self.shouldCreateUser=[decoder decodeBoolForKey:@"shouldCreateUser"];
    self.shouldCreateUserAdmin=[decoder decodeBoolForKey:@"shouldCreateUserAdmin"];
    self.shouldCreateUserAutologin=[decoder decodeBoolForKey:@"shouldCreateUserAutologin"];

    self.shouldCreateUserHidden=[decoder decodeBoolForKey:@"shouldCreateUserHidden"];
    self.createUserFullName=[decoder decodeObjectForKey:@"createUserFullName"];
    self.createUserShortName=[decoder decodeObjectForKey:@"createUserShortName"];
    self.createUserSSHKey=[decoder decodeObjectForKey:@"createUserSSHKey"];
    self.createUserShell=[decoder decodeObjectForKey:@"createUserShell"];

    self.passwordHint=[decoder decodeObjectForKey:@"passwordHint"];

    self.createUserUID=[decoder decodeObjectForKey:@"createUserUID"];
    self.createUserPassword=[decoder decodeObjectForKey:@"createUserPassword"];


    self.usePackagesFolderPath = [decoder decodeBoolForKey:@"usePackagesFolderPath"];
    self.packagesFolderPath = [decoder decodeObjectForKey:@"packagesFolderPath"];
    self.packagesFolderOrder = [decoder decodeObjectForKey:@"packagesFolderOrder"];

    self.macOSFolderPath=[decoder decodeObjectForKey:@"macOSFolderPath"];
    self.macOSInstallerDMGURL=[decoder decodeObjectForKey:@"macOSInstallerDMGURL"];
    self.asrImageURL=[decoder decodeObjectForKey:@"asrImageURL"];

    self.macOSImagePath=[decoder decodeObjectForKey:@"macOSImagePath"];

    self.discoverCredentialsScriptPath=[decoder decodeObjectForKey:@"discoverCredentialsScriptPath"];

    self.shouldUseScriptToDiscoverCredentials = [decoder decodeBoolForKey:@"shouldUseScriptToDiscoverCredentials"];
    self.loginPromptForPassword = [decoder decodeBoolForKey:@"loginPromptForPassword"];


    self.packagesFolderBookmark = [decoder decodeObjectForKey:@"packagesFolderBookmark"];

    self.shouldEraseAndInstall = [decoder decodeBoolForKey:@"shouldEraseAndInstall"];

    self.shouldRebootAndInstall = [decoder decodeBoolForKey:@"shouldRebootAndInstall"];
    self.loginInstallUsername=[decoder decodeObjectForKey:@"loginInstallUsername"];
    self.loginInstallPassword=[decoder decodeObjectForKey:@"loginInstallPassword"];




    self.shouldRunSoftwareUpdate = [decoder decodeBoolForKey:@"shouldRunSoftwareUpdate"];

    self.shouldSetComputerName = [decoder decodeBoolForKey:@"shouldSetComputerName"];
    self.computerNameString=[decoder decodeObjectForKey:@"computerNameString"];
    self.shouldPromptForComputerName = [decoder decodeBoolForKey:@"shouldPromptForComputerName"];


    self.computerNameTypeIndex=[decoder decodeIntegerForKey:@"computerNameTypeIndex"];
    self.shouldSkipSetupAssistant = [decoder decodeBoolForKey:@"shouldSkipSetupAssistant"];
    self.shouldEnableLocationServices = [decoder decodeBoolForKey:@"shouldEnableLocationServices"];


    self.shouldSetVolumeName = [decoder decodeBoolForKey:@"shouldSetVolumeName"];
    self.volumeName = [decoder decodeObjectForKey:@"volumeName"];

    self.shouldSkipPrivacySetup = [decoder decodeBoolForKey:@"shouldSkipPrivacySetup"];
    self.shouldEnableSSH = [decoder decodeBoolForKey:@"shouldEnableSSH"];
    self.shouldEnableMunkiReport = [decoder decodeBoolForKey:@"shouldEnableMunkiReport"];
    self.shouldSetTargetAsStartupDisk = [decoder decodeBoolForKey:@"shouldSetTargetAsStartupDisk"];

    self.shouldSkipRosetta = [decoder decodeBoolForKey:@"shouldSkipRosetta"];


    self.munkiReportEnrollmentScript=[decoder decodeObjectForKey:@"munkiReportEnrollmentScript"];
    self.shouldEnableScreenSharing = [decoder decodeBoolForKey:@"shouldEnableScreenSharing"];
    self.shouldEnableARD = [decoder decodeBoolForKey:@"shouldEnableARD"];


    self.shouldDisableSIP = [decoder decodeBoolForKey:@"shouldDisableSIP"];


    self.shouldInstall = [decoder decodeBoolForKey:@"shouldInstall"];
    self.shouldImage = [decoder decodeBoolForKey:@"shouldImage"];
    self.shouldSkipMacOSInstall = [decoder decodeBoolForKey:@"shouldSkipMacOSInstall"];
    self.shouldUseMacOSInstallerURL = [decoder decodeBoolForKey:@"shouldUseMacOSInstallerURL"];
    self.shouldUseAsrDmgURL = [decoder decodeBoolForKey:@"shouldUseAsrDmgURL"];

    self.shouldBlessTarget = [decoder decodeBoolForKey:@"shouldBlessTarget"];
    self.useWorkflowLocalization = [decoder decodeBoolForKey:@"useWorkflowLocalization"];
    self.workflowLocalization = [decoder decodeObjectForKey:@"workflowLocalization"];

    self.shouldSetVariables = [decoder decodeBoolForKey:@"shouldSetVariables"];

    self.mdsVar1 = [decoder decodeObjectForKey:@"mdsVar1"];
    self.mdsVar2 = [decoder decodeObjectForKey:@"mdsVar2"];
    self.mdsVar3 = [decoder decodeObjectForKey:@"mdsVar3"];
    self.mdsVar4 = [decoder decodeObjectForKey:@"mdsVar4"];
    self.mdsVar5 = [decoder decodeObjectForKey:@"mdsVar5"];
    self.mdsVar6 = [decoder decodeObjectForKey:@"mdsVar6"];
    self.mdsVar7 = [decoder decodeObjectForKey:@"mdsVar7"];
    self.mdsVar8 = [decoder decodeObjectForKey:@"mdsVar8"];
    self.mdsVar9 = [decoder decodeObjectForKey:@"mdsVar9"];

    self.mdsVariables = [decoder decodeObjectForKey:@"mdsVariables"];
    if (!self.mdsVariables) {
        NSMutableArray <TCSMDSVariable *> *newVariablesArray=[NSMutableArray array];

        TCSMDSVariable *newMDSVariable;
        NSString *currOldMDSVar;
        int i;
        for (i=1;i<10;i++){
            switch (i) {
                case 1:
                    currOldMDSVar=self.mdsVar1;
                    break;
                case 2:
                    currOldMDSVar=self.mdsVar2;
                    break;
                case 3:
                    currOldMDSVar=self.mdsVar3;
                    break;
                case 4:
                    currOldMDSVar=self.mdsVar4;
                    break;
                case 5:
                    currOldMDSVar=self.mdsVar5;
                    break;
                case 6:
                    currOldMDSVar=self.mdsVar6;
                    break;
                case 7:
                    currOldMDSVar=self.mdsVar7;
                    break;
                case 8:
                    currOldMDSVar=self.mdsVar8;
                    break;
                case 9:
                    currOldMDSVar=self.mdsVar9;
                    break;

                default:
                    break;
            }


            newMDSVariable=[[TCSMDSVariable alloc] init];
            newMDSVariable.mdsVariableName=[NSString stringWithFormat:@"mds_var%i",i];
            if (currOldMDSVar && currOldMDSVar>0){
                newMDSVariable.mdsVarPrompt=[currOldMDSVar copy];
                newMDSVariable.variableTypeIndex=0;
            }
            [newVariablesArray addObject:newMDSVariable];

        }
        self.mdsVariables=newVariablesArray;

    }
    self.munkiClientInstallerPath = [decoder decodeObjectForKey:@"munkiClientInstallerPath"];
    self.munkiWorkflowURL = [decoder decodeObjectForKey:@"munkiWorkflowURL"];
    self.shouldConfigureMunkiClient = [decoder decodeBoolForKey:@"shouldConfigureMunkiClient"];
    self.shouldTrustMunkiClientCertificate = [decoder decodeBoolForKey:@"shouldTrustMunkiClientCertificate"];
    //
    self.shouldTrustServerCertificate = [decoder decodeBoolForKey:@"shouldTrustServerCertificate"];

    self.munkiClientCertificate = [decoder decodeObjectForKey:@"munkiClientCertificate"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeBool:self.isActive forKey:@"isActive"];
    [encoder encodeBool:self.isDefault forKey:@"isDefault"];
    [encoder encodeBool:self.isReadonly forKey:@"isReadonly"];

    [encoder encodeObject:self.workflowName forKey:@"workflowName"];
    [encoder encodeBool:self.shouldConfigureWifi forKey:@"shouldConfigureWifi"];

    [encoder encodeObject:self.wifiPassword forKey:@"wifiPassword"];
    [encoder encodeObject:self.wifiSSID forKey:@"wifiSSID"];
    [encoder encodeBool:self.shouldWaitForNetwork forKey:@"shouldWaitForNetwork"];

    [encoder encodeObject:self.workflowDescription forKey:@"workflowDescription"];
    [encoder encodeObject:self.workflowAllowedModels forKey:@"workflowAllowedModels"];
    [encoder encodeBool:self.shouldLimitWorkflowsByModelType forKey:@"shouldLimitWorkflowsByModelType"];

    [encoder encodeObject:self.workflowUUID forKey:@"workflowUUID"];

    [encoder encodeBool:self.useComputerName forKey:@"useComputerName"];
    [encoder encodeInteger:self.computerName forKey:@"computerName"];
    [encoder encodeBool:self.useScriptFolderPath forKey:@"useScriptFolderPath"];
    [encoder encodeObject:self.scriptFolderPath forKey:@"scriptFolderPath"];
    [encoder encodeObject:self.scriptFolderOrder forKey:@"scriptFolderOrder"];

    [encoder encodeObject:self.prePackageScripts forKey:@"prePackageScripts"];
    [encoder encodeObject:self.preInstallPackages forKey:@"preInstallPackages"];
    [encoder encodeObject:self.prePackageProfiles forKey:@"prePackageProfiles"];

    [encoder encodeObject:self.firstRunProfiles forKey:@"firstRunProfiles"];
    [encoder encodeObject:self.firstRunPackages forKey:@"firstRunPackages"];
    [encoder encodeObject:self.disabledPackages forKey:@"disabledPackages"];
    [encoder encodeObject:self.disabledProfiles forKey:@"disabledProfiles"];
    [encoder encodeObject:self.disabledScripts forKey:@"disabledScripts"];

    [encoder encodeObject:self.workflowFirstRunScripts forKey:@"workflowFirstRunScripts"];


    [encoder encodeBool:self.useProfilesFolderPath forKey:@"useProfilesFolderPath"];
    [encoder encodeObject:self.profilesFolderPath forKey:@"profilesFolderPath"];
    [encoder encodeObject:self.profilesFolderOrder forKey:@"profilesFolderOrder"];

    [encoder encodeBool:self.shouldRebootAfterInstallingResources forKey:@"shouldRebootAfterInstallingResources"];
    [encoder encodeBool:self.shouldRebootAfterInstallingResourcesInstallOS forKey:@"shouldRebootAfterInstallingResourcesInstallOS"];

    [encoder encodeBool:self.rebootAfterFirstBootResources forKey:@"rebootAfterFirstBootResources"];
    [encoder encodeBool:self.waitForMacBuddyExitBeforeRebooting forKey:@"waitForMacBuddyExitBeforeRebooting"];


    [encoder encodeInteger:self.restartOrShutdownIndex forKey:@"restartOrShutdownIndex"];
    [encoder encodeInteger:self.restartOrShutdownIndexInstallOS forKey:@"restartOrShutdownIndexInstallOS"];

//create user stuff

    [encoder encodeObject:[NSArray arrayWithArray:self.users] forKey:@"users"];
    [encoder encodeBool:self.shouldCreateUser forKey:@"shouldCreateUser"];
    [encoder encodeBool:self.shouldCreateUserAdmin forKey:@"shouldCreateUserAdmin"];
    [encoder encodeBool:self.shouldCreateUserAutologin forKey:@"shouldCreateUserAutologin"];

    [encoder encodeBool:self.shouldCreateUserHidden forKey:@"shouldCreateUserHidden"];

    //create user stuff
    [encoder encodeObject:self.createUserFullName forKey:@"createUserFullName"];
    [encoder encodeObject:self.createUserShortName forKey:@"createUserShortName"];
    [encoder encodeObject:self.createUserSSHKey forKey:@"createUserSSHKey"];
    [encoder encodeObject:self.createUserShell forKey:@"createUserShell"];

    [encoder encodeObject:self.passwordHint forKey:@"passwordHint"];

    [encoder encodeObject:self.createUserUID forKey:@"createUserUID"];
    [encoder encodeObject:self.createUserPassword forKey:@"createUserPassword"];


   

    [encoder encodeBool:self.usePackagesFolderPath forKey:@"usePackagesFolderPath"];
    [encoder encodeObject:self.packagesFolderPath forKey:@"packagesFolderPath"];
    [encoder encodeObject:self.packagesFolderOrder forKey:@"packagesFolderOrder"];

    [encoder encodeObject:self.packagesFolderBookmark forKey:@"packagesFolderBookmark"];

    [encoder encodeObject:self.macOSFolderPath forKey:@"macOSFolderPath"];
    [encoder encodeObject:self.macOSInstallerDMGURL forKey:@"macOSInstallerDMGURL"];
    [encoder encodeObject:self.asrImageURL forKey:@"asrImageURL"];

    [encoder encodeObject:self.macOSImagePath forKey:@"macOSImagePath"];

    [encoder encodeBool:self.shouldEraseAndInstall forKey:@"shouldEraseAndInstall"];

    [encoder encodeObject:self.discoverCredentialsScriptPath forKey:@"discoverCredentialsScriptPath"];
    [encoder encodeBool:self.shouldUseScriptToDiscoverCredentials forKey:@"shouldUseScriptToDiscoverCredentials"];

    [encoder encodeBool:self.loginPromptForPassword forKey:@"loginPromptForPassword"];


    [encoder encodeBool:self.shouldRebootAndInstall forKey:@"shouldRebootAndInstall"];
    [encoder encodeObject:self.loginInstallUsername forKey:@"loginInstallUsername"];
    [encoder encodeObject:self.loginInstallPassword forKey:@"loginInstallPassword"];

    [encoder encodeBool:self.shouldRunSoftwareUpdate forKey:@"shouldRunSoftwareUpdate"];

    [encoder encodeBool:self.shouldSetComputerName forKey:@"shouldSetComputerName"];
    [encoder encodeObject:self.computerNameString forKey:@"computerNameString"];

    [encoder encodeInteger:self.computerNameTypeIndex forKey:@"computerNameTypeIndex"];
    [encoder encodeBool:self.shouldPromptForComputerName forKey:@"shouldPromptForComputerName"];

    [encoder encodeBool:self.shouldSetVolumeName forKey:@"shouldSetVolumeName"];
    [encoder encodeObject:self.volumeName forKey:@"volumeName"];


    [encoder encodeBool:self.shouldSkipSetupAssistant forKey:@"shouldSkipSetupAssistant"];
    [encoder encodeBool:self.shouldEnableLocationServices forKey:@"shouldEnableLocationServices"];

    [encoder encodeBool:self.shouldSkipPrivacySetup forKey:@"shouldSkipPrivacySetup"];
    [encoder encodeBool:self.shouldEnableSSH forKey:@"shouldEnableSSH"];
    [encoder encodeBool:self.shouldEnableMunkiReport forKey:@"shouldEnableMunkiReport"];
    [encoder encodeBool:self.shouldSetTargetAsStartupDisk forKey:@"shouldSetTargetAsStartupDisk"];
    [encoder encodeBool:self.shouldSkipRosetta forKey:@"shouldSkipRosetta"];

    [encoder encodeObject:self.munkiReportEnrollmentScript forKey:@"munkiReportEnrollmentScript"];
    [encoder encodeBool:self.shouldEnableScreenSharing forKey:@"shouldEnableScreenSharing"];
    [encoder encodeBool:self.shouldEnableARD forKey:@"shouldEnableARD"];


    [encoder encodeBool:self.shouldDisableSIP forKey:@"shouldDisableSIP"];



    [encoder encodeBool:self.shouldInstall forKey:@"shouldInstall"];
    [encoder encodeBool:self.shouldImage forKey:@"shouldImage"];
    [encoder encodeBool:self.shouldSkipMacOSInstall forKey:@"shouldSkipMacOSInstall"];
    [encoder encodeBool:self.shouldUseMacOSInstallerURL forKey:@"shouldUseMacOSInstallerURL"];
    [encoder encodeBool:self.shouldUseAsrDmgURL forKey:@"shouldUseAsrDmgURL"];

    [encoder encodeBool:self.shouldBlessTarget forKey:@"shouldBlessTarget"];
    [encoder encodeBool:self.useWorkflowLocalization forKey:@"useWorkflowLocalization"];
    [encoder encodeObject:self.workflowLocalization forKey:@"workflowLocalization"];

    [encoder encodeBool:self.shouldSetVariables forKey:@"shouldSetVariables"];

    [encoder encodeObject:self.mdsVar1 forKey:@"mdsVar1"];
    [encoder encodeObject:self.mdsVar2 forKey:@"mdsVar2"];
    [encoder encodeObject:self.mdsVar3 forKey:@"mdsVar3"];
    [encoder encodeObject:self.mdsVar4 forKey:@"mdsVar4"];
    [encoder encodeObject:self.mdsVar5 forKey:@"mdsVar5"];
    [encoder encodeObject:self.mdsVar6 forKey:@"mdsVar6"];
    [encoder encodeObject:self.mdsVar7 forKey:@"mdsVar7"];
    [encoder encodeObject:self.mdsVar8 forKey:@"mdsVar8"];
    [encoder encodeObject:self.mdsVar9 forKey:@"mdsVar9"];
    [encoder encodeObject:self.mdsVariables forKey:@"mdsVariables"];

    [encoder encodeObject:self.munkiClientInstallerPath forKey:@"munkiClientInstallerPath"];
    [encoder encodeObject:self.munkiWorkflowURL forKey:@"munkiWorkflowURL"];
    [encoder encodeBool:self.shouldConfigureMunkiClient forKey:@"shouldConfigureMunkiClient"];
    [encoder encodeBool:self.shouldTrustMunkiClientCertificate forKey:@"shouldTrustMunkiClientCertificate"];
    [encoder encodeBool:self.shouldTrustServerCertificate forKey:@"shouldTrustServerCertificate"];

    [encoder encodeObject:self.munkiClientCertificate forKey:@"munkiClientCertificate"];


}
-(NSString *)label{


    if (self.isReadonly==YES) return [NSString stringWithFormat:@"%@ (Read Only)",self.workflowName];

    return self.workflowName;
}
@end
