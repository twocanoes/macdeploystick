//
//  TCSWebServiceController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/13/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSWebserverSetting.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSWebServiceController : NSObject
+ (instancetype)sharedController ;
-(void)updateApacheSettings:(NSArray *)settings restartIfRunningWithCallback:(void (^)(BOOL success))updateCallback;
-(void)restartServiceWithCallback:(void (^)(BOOL hadSuccess))updateCallback;
-(NSArray *)webserviceSettings;
-(void)loadWebserviceSettings;
-(void)stopWebServicesWithCallback:(void (^)(BOOL success, BOOL didCancel))callback;
-(void)startWebServicesWithCallback:(void (^)(BOOL success, BOOL didCancel))callback;
-(void)saveSettings:(NSArray *)inSettings callback:(void (^)(BOOL hadSuccess))updateCallback;
-(BOOL)isRunning;
-(BOOL)hasTLSCertificates:(id)sender;
@end

NS_ASSUME_NONNULL_END
