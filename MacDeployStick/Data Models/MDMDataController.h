//
//  MDMDataController.h
//  MDS
//
//  Created by Timothy Perfitt on 8/31/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DeviceMO+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface MDMDataController : NSObject
@property (strong, nonatomic) NSPersistentContainer *persistentContainer;
+ (instancetype)dataController ;
-(void)addDevice:(NSDictionary *)deviceInfo;
-(DeviceMO *)deviceForUDID:(NSString *)uuid;
-(BOOL)addRequestForDevice:(NSString *)deviceUDID requestType:(NSString *)requestType commandUUID:(NSString *)uuid;
-(BOOL)addResponseForDevice:(NSString *)deviceUDID status:(NSString *)status commandUUID:(nullable NSString *)commandUUID dateCreated:(NSDate *)dateCreated payload:(NSString *)payload;
-(NSArray <CertificateMO *> *)certificatesForDeviceUDID:(NSString *)deviceUDID;
-(NSArray *)allDevices;
-(void)removeDeviceWithUDID:(NSString *)udid;
@end
NS_ASSUME_NONNULL_END
