//
//  DeploySettings.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "DeploySettings.h"
#import "TCSConfigHelper.h"
#import "TCSUtility.h"
#import "TCSCopyFileController.h"
#import <RepackageFramework/RepackageFramework.h>
#import "TCSDefaultsManager.h"
#import "TCSConfigHelper.h"
#import "TCSMDSVariable.h"
#import "NSError+EasyError.h"
#import "NSString+NonASCIICharacters.h"
#define COMPUTERNAMEPROMPT 0
#define COMPUTERNAMESERIALNUMBER 1

@interface DeploySettings()
@property (strong) TCSCopyFileController *fileCopyController;
@property (strong) NSMutableArray *osDMGToCopy;
@property (strong) NSString *macOSDMGDestinationFolder;
@property (nonatomic,strong) NSMutableArray *workflows;
@property (strong) NSMutableDictionary <NSString *,NSString *> *sourcePackagesCopied;

@end
@implementation DeploySettings
+ (BOOL)supportsSecureCoding {
    return YES;
}
+(NSData *)workflowDataFromArray:(NSArray *)inArray{
    NSError *err;
    if (@available(macOS 10.13, *)) {
        NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:inArray requiringSecureCoding:YES error:&err];
        return dataToSave;
    } else {

        NSLog(@"not available before 10.13");
    }
    return nil;



}

+(NSArray *)workflowArrayFromData:(NSData *)inData{
    NSSet *set = [NSSet setWithArray:@[
        [NSArray class],
        [NSDictionary class],
        [DeploySettings class],
        [TCSWorkflow class],
        [TCSWorkflowLocalization class],
        [NSURL class],
        [NSData class],
        [NSUUID class],
        [TCSWorkflowUser class],
        [NSMutableData class],
        [NSMutableString class],
        [TCSMDSVariable class]
    ]];
    NSError *err;
    NSArray *workflowArray=nil;
    if (@available(macOS 10.13, *)) {
        workflowArray=[NSKeyedUnarchiver unarchivedObjectOfClasses:set fromData:inData error: &err];

        if (err) {
            NSLog(@"%@",[err localizedDescription]);
        }
        return workflowArray;
    } else {
        // Fallback on earlier versions
    }
    return workflowArray;
}
+(NSString *)syncFolder{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *applicationSupportDirectory = [paths firstObject];

    NSString *syncFolder=[applicationSupportDirectory stringByAppendingPathComponent:@"com.twocanoes.mds.sync"];

    return syncFolder;
}

+ (id)sharedSettings {
    static DeploySettings *sharedDeploySettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"workflowInfo"]){

            NSError *err;
            NSData *inData=[[NSUserDefaults standardUserDefaults] objectForKey:@"workflowInfo"];
            NSSet *set = [NSSet setWithArray:@[
                [NSArray class],
                [NSString class],
                [NSDictionary class],
                [DeploySettings class],
                [TCSWorkflow class],
                [TCSWorkflowLocalization class],
                [NSURL class],
                [NSData class],
                [NSUUID class],
                [TCSWorkflowUser class],
                [NSMutableData class],
                [NSMutableString class],
                [NSMutableData class],
                [TCSMDSVariable class]

            ]];


            if (@available(macOS 10.13, *)) {
                sharedDeploySettings = [NSKeyedUnarchiver unarchivedObjectOfClasses:set fromData:inData error: &err];
                if (err) {
                    NSLog(@"error unarchiving data:%@",err.localizedDescription);
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Error Reading Workflows";
                    alert.informativeText=@"There was an error reading your current workflow settings. This can happen if the preferences were  corrupted or if you upgraded from a different version of the app.\n\nDo you want to create new workflow settings? This will remove all prior workflows permanently.";

                    [alert addButtonWithTitle:@"Quit"];
                    [alert addButtonWithTitle:@"Delete and Create New Settings"];
                    NSInteger res=[alert runModal];
                    if (res==NSAlertFirstButtonReturn) {
                        [NSApp terminate:self];
                    }

                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"workflowInfo"];
                    sharedDeploySettings = [[self alloc] init];
                }
                else {
                    [sharedDeploySettings upgradeSettingsIfNeeded];
                }

            }

        }
        else sharedDeploySettings = [[self alloc] init];

    });


    return sharedDeploySettings;
}
-(void)upgradeSettingsIfNeeded{
    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
        if (currWorkflow.workflowUUID==nil) {
            currWorkflow.workflowUUID=[NSUUID UUID];
        }
        if (currWorkflow.users.count==0){
            if (currWorkflow.createUserShortName && currWorkflow.createUserShortName.length>0 && currWorkflow.shouldCreateUser==YES){
                currWorkflow.shouldCreateUser=NO;
                TCSWorkflowUser *user=[[TCSWorkflowUser alloc] init];
                user.fullName=[currWorkflow.createUserFullName copy];
                currWorkflow.createUserFullName=@"";
                user.shortName=[currWorkflow.createUserShortName copy];
                currWorkflow.createUserShortName=@"";
                user.uid=[currWorkflow.createUserUID copy];
                user.isAdmin=currWorkflow.shouldCreateUserAdmin;
                user.shouldAutologin=currWorkflow.shouldCreateUserAutologin;
                user.isHidden=currWorkflow.shouldCreateUserHidden;
                user.sshKey=[currWorkflow.createUserSSHKey copy];
                user.shell=[currWorkflow.createUserShell copy];

                currWorkflow.createUserSSHKey=@"";
                user.passwordHint=[currWorkflow.passwordHint copy];
                user.userUUID=[[NSUUID UUID] UUIDString];
                currWorkflow.passwordHint=@"";

                user.password=[currWorkflow.createUserPassword copy];
                currWorkflow.createUserPassword=@"";

                if (currWorkflow.users==nil) currWorkflow.users=[NSMutableArray array];
                [currWorkflow.users addObject:user];
            }

        }

    }];
}
- (instancetype)initWithWorkflows:(NSArray *)workflows
{
    self = [self init];
    if (self) {
        [self.workflows addObjectsFromArray:workflows];
    }
    return self;
}

- (id)init {
    if (self = [super init]) {
        self.workflows=[NSMutableArray array];

    }
    return self;

}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.sourcePackagesCopied=[NSMutableDictionary dictionary];
    self.backgroundImageURL = [decoder decodeObjectForKey:@"backgroundImageURL"];
    self.workflows = [decoder decodeObjectForKey:@"workflows"];
    self.shouldSaveToDMG=NO;

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.backgroundImageURL forKey:@"backgroundImageURL"];
    [encoder encodeObject:self.workflows forKey:@"workflows"];
}

-(void)addWorkflow:(TCSWorkflow *)workflow{
    NSMutableArray *proxyArray=[self mutableArrayValueForKey:@"workflows"];

    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([workflow.workflowUUID isEqual:currWorkflow.workflowUUID]){

            [proxyArray removeObject:currWorkflow];
        }
        else if ([workflow.workflowName.lowercaseString isEqualToString:currWorkflow.workflowName.lowercaseString]){

            NSString *newName=[NSString stringWithFormat:@"%@ (Copy)",workflow.workflowName];
            while (![workflow.workflowName.lowercaseString isEqualToString:currWorkflow.workflowName.lowercaseString]){

                newName=[NSString stringWithFormat:@"%@ (Copy)",newName];

            }
            workflow.workflowName=newName;
        }

    }];
    [proxyArray addObject:workflow];
    [self saveToPrefs:self];

}
-(void)saveToPrefs:(id)sender{
    NSError *err;
    if (@available(macOS 10.13, *)) {
        NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:self requiringSecureCoding:YES error:&err];

        if (err) {
            [[NSAlert alertWithError:err] runModal];
        }


        else [[NSUserDefaults standardUserDefaults] setObject:dataToSave forKey:@"workflowInfo"];
    } else {

        NSLog(@"not available before 10.13");
    }
}
-(NSData *)workflowData{

    return [DeploySettings workflowDataFromArray:self.workflows];

}

-(BOOL)addWorkflowsFromData:(NSData *)inData{
    __block BOOL passwordMissing=NO;
    NSArray *workflowArray=[DeploySettings workflowArrayFromData:inData];
    if (workflowArray) {
        [workflowArray enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
            if (currWorkflow.shouldCreateUser==YES && [currWorkflow.createUserPassword isEqualToString:@""]){
                passwordMissing=YES;
            }
            if (currWorkflow.users.count>0){

                [currWorkflow.users enumerateObjectsUsingBlock:^(TCSWorkflowUser *currUser, NSUInteger idx, BOOL * _Nonnull stop) {

                    if (!currUser.password || [currUser.password isEqualToString:@""]) { passwordMissing=YES;
                    }
                }];
            }

            if (currWorkflow.shouldConfigureWifi==YES && [currWorkflow.wifiPassword isEqualToString:@""]){
                passwordMissing=YES;
            }
            [self addWorkflow:currWorkflow];
        }];
        [self upgradeSettingsIfNeeded];
    }

    return passwordMissing;
}
-(void)copyBinary:(NSString *)sourceNetworkSetupPath resourceURL:(NSURL *)inResourceURL{
    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *err;
    NSString *binFolder=[[inResourceURL.path stringByAppendingPathComponent:@"Deploy"] stringByAppendingPathComponent:@"bin"];
    NSString *destinationNetworkSetupPath=[binFolder stringByAppendingPathComponent:sourceNetworkSetupPath.lastPathComponent];

    if ([fm fileExistsAtPath:destinationNetworkSetupPath]==NO) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceNetworkSetupPath.lastPathComponent]}];
        if ([fm fileExistsAtPath:binFolder]==NO){
            
            if ([fm createDirectoryAtPath:binFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];
            }
            
        }

        if([fm copyItemAtPath:sourceNetworkSetupPath toPath:destinationNetworkSetupPath error:&err]==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];

        }
    }
}
-(void)createMDSPackagesAtPath:(NSString *)inMDSPackagesPath forWorkflow:(TCSWorkflow *)inWorkflow error:(NSError **)err{
    if (inWorkflow.users.count>0 ) {


        __block NSError *newError=nil;
        [inWorkflow.users enumerateObjectsUsingBlock:^(TCSWorkflowUser *currUser, NSUInteger idx, BOOL * _Nonnull stop) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Creating user \"%@\" and package",currUser.fullName]}];

            NSString *createUserOutput = [TCSUtility createUserPackageAtPath:inMDSPackagesPath
                                                                     withUID:currUser.uid
                                                                    fullname:currUser.fullName
                                                                   shortname:currUser.shortName
                                                                    password:currUser.password
                                                                     isAdmin:currUser.isAdmin
                                                                 isAutologin:currUser.shouldAutologin
                                                                    isHidden:currUser.isHidden
                                                                passwordHint:currUser.passwordHint
                                                                   jpegPhoto:currUser.jpegPhoto
                                                                       shell:currUser.shell==nil?@"/bin/zsh":@"/bin/bash"
                                                           outputPackageName:[NSString stringWithFormat:@"1%02lu-com.twocanoes.mds.createuser-%@.pkg",(unsigned long)idx,currUser.shortName]];

            if (![createUserOutput isEqualToString: @"SUCCESS"]) {
                newError=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Error creating user package in workflow \"%@\".",inWorkflow.workflowName],NSLocalizedRecoverySuggestionErrorKey:createUserOutput}];
                NSLog(@"Error creating user package.");
                return ;
            }
        }];
        *err=newError;


    }
}
-(NSString *)filePathWithSize:(NSString *)inFilePath error:(NSError **)error{
    NSFileManager *fm=[NSFileManager defaultManager];
    NSDictionary  *fileAttributes;
    BOOL isDir;
    BOOL isFolder=[fm fileExistsAtPath:inFilePath isDirectory:&isDir] && isDir==YES;
    NSString *fileSizeString;

    if (isFolder==YES){
        fileSizeString=[self folderSize:inFilePath];
    }
    else{
        if((fileAttributes=[fm attributesOfItemAtPath:inFilePath error:error])==nil){
            return nil;
        }

        fileSizeString=[NSString stringWithFormat:@"%lli",[fileAttributes fileSize]];
    }
    NSString *newFilePath=[[inFilePath stringByDeletingPathExtension] stringByAppendingFormat:@"-%@.pkg",fileSizeString];



    return newFilePath;
}
- (NSString *)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;

    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] fileAttributesAtPath:[folderPath stringByAppendingPathComponent:fileName] traverseLink:YES];
        fileSize += [fileDictionary fileSize];
    }

    return [NSString stringWithFormat:@"%llu",fileSize];
}
- (NSString *)copyItemAtPath:(NSString *)sourcePath toPath:(NSString *)destinationPath symbolicLinkPath:(NSString *)symbolicLinkPath shouldAddFilesize:(BOOL)shouldAddFilesize error:(NSError **)error{

    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *newFileName=sourcePath;
    if (shouldAddFilesize==YES){
        newFileName=[self filePathWithSize:sourcePath error:error];
        if (!newFileName) {
            return nil;
        }
    }
    newFileName=[newFileName lastPathComponent];

    NSString *uniqueDestination=[[destinationPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:newFileName];

    if ([[[self.sourcePackagesCopied objectForKey:sourcePath] lowercaseString] isEqualToString:[uniqueDestination lowercaseString]]==NO){
        if ([fm fileExistsAtPath:uniqueDestination]==NO){

            NSString *disabledPath=[[[uniqueDestination stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Disabled"] stringByAppendingPathComponent:uniqueDestination.lastPathComponent];
            if ([fm fileExistsAtPath:disabledPath]){
                //already exists in disabled..just move
                if([fm moveItemAtPath:disabledPath toPath:uniqueDestination error:error]==NO) {
                    return nil;
                }
            }
            else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourcePath.lastPathComponent]}];

                if ([fm copyItemAtPath:sourcePath toPath:uniqueDestination error:error]==NO) {
                    return nil;
                }
            }
        }

    }
    if (symbolicLinkPath){
        NSString *symPathDestination=[NSString stringWithFormat:@"../../../SharedPackages/%@",uniqueDestination.lastPathComponent];

        NSString *symLinkPath=[[symbolicLinkPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:symbolicLinkPath.lastPathComponent];

        if ([fm fileExistsAtPath:symLinkPath]){
            if([fm removeItemAtPath:symLinkPath error:error]==NO){
                return nil;
            }
        }
        if([fm createSymbolicLinkAtPath:symLinkPath withDestinationPath:symPathDestination error:error]==NO){
            return nil;
        }
    }
    [self.sourcePackagesCopied setObject:uniqueDestination forKey:sourcePath];
    return uniqueDestination;

}
-(NSString *)replaceExtensionWithPKGExtensionForFile:(NSString *)inFile{

    return [[inFile stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"];

}
-(BOOL)moveToSubfolderAndRepackage:(NSString *)fullPackagePathSource shouldForceCopy:(BOOL)shouldForceCopy withError:(NSError *__autoreleasing *)err{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *backupFolder=[[fullPackagePathSource stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-original-%@",fullPackagePathSource.lastPathComponent,[[NSUUID UUID] UUIDString]]];

    if([fm createDirectoryAtPath:backupFolder withIntermediateDirectories:YES attributes:nil error:err]==NO){
        return NO;
    }
    NSString *backupFullPackagePathSource=[backupFolder stringByAppendingPathComponent:fullPackagePathSource.lastPathComponent];
    if([fm moveItemAtPath:fullPackagePathSource toPath:backupFullPackagePathSource error:err]==NO){

        return NO;

    }
    NSString *packageFileWithPkgExtension=[self replaceExtensionWithPKGExtensionForFile:fullPackagePathSource];
    NSString *subFolderPathWithPkgExtension=[backupFolder stringByAppendingPathComponent:fullPackagePathSource.lastPathComponent];
    if ([fm fileExistsAtPath:packageFileWithPkgExtension]==YES){

        if([fm moveItemAtPath:packageFileWithPkgExtension toPath:subFolderPathWithPkgExtension error:err]==NO){
            return NO;

        }
    }

    TCSRepackage *repackage=[[TCSRepackage alloc] init];

    [repackage repackagePackageAtPath:subFolderPathWithPkgExtension destination:packageFileWithPkgExtension forceEmbed:NO forceCopy:shouldForceCopy status:^(NSString * status) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":status}];

    } completionBlock:^(NSError * _Nonnull err) {
        if (err) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];
            return;

        }
    }];

    return YES;
}

-(NSString *)repackageAndCopyIfNeeded:(NSString *)fullPackagePathSource toDestination:(NSString *)destinationPath symbolicLink:(NSString *)symbolicLinkPath shouldForceCopy:(BOOL)shouldForceCopy shouldAddFilesize:(BOOL)shouldAddFilesize confirmationBlock:(BOOL (^)(void))confirmBlock error:(NSError *__autoreleasing *)error{
    TCSRepackage *repackage=[[TCSRepackage alloc] init];
    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *isDistributionError;

    BOOL isPKG=[[[fullPackagePathSource pathExtension] lowercaseString] isEqualToString:@"pkg"];
    BOOL isApp=[[[fullPackagePathSource pathExtension] lowercaseString] isEqualToString:@"app"];

    BOOL isDir;
    BOOL isFolder=[fm fileExistsAtPath:fullPackagePathSource isDirectory:&isDir] && isDir==YES;

    BOOL isBundle=isPKG && isFolder;

    if ( (shouldForceCopy==NO && ([repackage isDistribution:fullPackagePathSource error:error]==NO || isBundle==YES|| isApp==YES)) ) {


        if (isDistributionError) {
            return nil;

        }
        BOOL shouldRepackage=confirmBlock();
        if (shouldRepackage==NO) {
            *error=[NSError errorWithDomain:@"TCSERROR" code:299 userInfo:nil];

            return nil;
        }
        if ([self moveToSubfolderAndRepackage:fullPackagePathSource shouldForceCopy:shouldForceCopy withError:error]==NO){
            return nil;
        }

    }// repackaged to fullPackagePathSource
    //now we copy

    NSString *sourcePackage=[self replaceExtensionWithPKGExtensionForFile:fullPackagePathSource];

    NSString *newDestinationFilePath;

    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Checking %@",sourcePackage.lastPathComponent]}];


    if((newDestinationFilePath=[self copyItemAtPath:sourcePackage toPath:[destinationPath stringByAppendingPathComponent:sourcePackage.lastPathComponent] symbolicLinkPath:symbolicLinkPath shouldAddFilesize:shouldAddFilesize error:error])==nil){

        return nil;
    }
    return newDestinationFilePath;
}
-(NSString *)sanatizedWorkflowNameForWorkflow:(NSString *)inWorkflow{


    NSUInteger hash=[inWorkflow hash];

    NSMutableCharacterSet *allowChars=[NSMutableCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"];

    NSCharacterSet *charactersToRemove = [allowChars invertedSet];
    
    NSString *strippedReplacement = [[inWorkflow componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];

    return [NSString stringWithFormat:@"%@-%lx",strippedReplacement,hash];



}
-(BOOL)shouldRepackage:(NSString *)inPackageName{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    __block NSInteger res;
    __block BOOL shouldSurpress=NO;
    if ([ud boolForKey:@"TCSSuppressRepackageWarning"]==NO){

        dispatch_sync(dispatch_get_main_queue(), ^{
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Repackage Package";
            alert.informativeText=[NSString stringWithFormat:@"The package %@ is not of type distribution and needs to be repackaged. When it is a repackaged, the original package in resources is moved to a subfolder. Repackage?",inPackageName];

            [alert addButtonWithTitle:@"Repackage"];
            [alert addButtonWithTitle:@"Cancel"];
            alert.showsSuppressionButton=YES;
            alert.suppressionButton.title = @"Always repackage and do not show this message";

            res=[alert runModal];
            if (alert.suppressionButton.state == NSOnState) {
                shouldSurpress=YES;
            }


        });
    }
    else return YES;
    if (res==NSAlertSecondButtonReturn) {

        return NO;
    }
    if (shouldSurpress==YES) {
        [ud setBool: YES forKey:@"TCSSuppressRepackageWarning"];
    }

    return YES;
}
- (NSString *)extracted:(NSError **)_error fullPackagePathSource:(NSString *)fullPackagePathSource preOSPackagePathDestination:(NSString *)preOSPackagePathDestination sharedPackagesURL:(NSURL *)sharedPackagesURL {
    return [self repackageAndCopyIfNeeded:fullPackagePathSource toDestination:sharedPackagesURL.path symbolicLink:preOSPackagePathDestination shouldForceCopy:YES shouldAddFilesize:YES
                        confirmationBlock:^BOOL{
        BOOL shouldRepackage=[self shouldRepackage:fullPackagePathSource.lastPathComponent];

        return shouldRepackage;
    }
                                    error:_error];
}

-(BOOL)saveResourcesToURL:(NSURL *)inResourceURL withPrefix:(NSURL * )inPrefix error:(NSError *  *)error{
    __block NSError *_error = nil;
    NSFileManager *fm=[NSFileManager defaultManager];
    self.stopCopying=NO;

    [self.sourcePackagesCopied removeAllObjects];
    NSURL *appURL=[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"Applications"];
    NSURL *sharedPackagesURL=[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"SharedPackages"];

    if ([fm fileExistsAtPath:sharedPackagesURL.path]==NO) {

        if([fm createDirectoryAtURL:sharedPackagesURL withIntermediateDirectories:YES attributes:nil error:error]==NO){
            return NO;

        }
    }
    NSMutableArray *packagesCopied=[NSMutableArray array];
    NSMutableArray *prePackagesCopied=[NSMutableArray array];

    NSMutableArray *sharedPackagesCopied=[NSMutableArray array];

    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull workflowsStop) {

        if (currWorkflow.isActive==YES) {
            NSURL *workflowURL=[[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:[self sanatizedWorkflowNameForWorkflow:currWorkflow.workflowName]];
            NSURL *packagesURL=[workflowURL URLByAppendingPathComponent:@"Packages"];
            NSURL *preOSpackagesURL=[workflowURL URLByAppendingPathComponent:@"PreOS-Packages"];
            NSURL *workflowScriptsURL=[workflowURL URLByAppendingPathComponent:@"WorkflowScripts"];

            NSURL *customScriptsURL=[workflowURL URLByAppendingPathComponent:@"CustomScripts"];

            if (![fm fileExistsAtPath:customScriptsURL.path]){
                if([fm createDirectoryAtPath:customScriptsURL.path withIntermediateDirectories:YES attributes:nil error:&_error]==NO) {
                    *workflowsStop=YES;
                    return;
                }
            }

            if (currWorkflow.shouldUseScriptToDiscoverCredentials==YES){

                if (currWorkflow.discoverCredentialsScriptPath && currWorkflow.discoverCredentialsScriptPath.length>0){

                    NSString *discoverCredentialsDestinationPath=[customScriptsURL.path stringByAppendingPathComponent:@"discover_credentials.sh"];
                    if ([fm fileExistsAtPath:discoverCredentialsDestinationPath]==YES){
                        if([fm removeItemAtPath:discoverCredentialsDestinationPath error:&_error]==NO){
                            *workflowsStop=YES;
                            return;

                        }
                    }
                    if([fm copyItemAtPath:currWorkflow.discoverCredentialsScriptPath toPath:discoverCredentialsDestinationPath error:&_error]==NO){
                        *workflowsStop=YES;
                        return;
                    }

                }
            }



            if (![fm fileExistsAtPath:workflowScriptsURL.path]){
                if([fm createDirectoryAtPath:workflowScriptsURL.path withIntermediateDirectories:YES attributes:nil error:&_error]==NO) {
                    *workflowsStop=YES;
                    return;
                }
            }

            if (![fm fileExistsAtPath:packagesURL.path]){
                if([fm createDirectoryAtPath:packagesURL.path withIntermediateDirectories:YES attributes:nil error:&_error]==NO) {
                    *workflowsStop=YES;
                    return;
                }
            }

            if (![fm fileExistsAtPath:preOSpackagesURL.path]){
                if([fm createDirectoryAtPath:preOSpackagesURL.path withIntermediateDirectories:YES attributes:nil error:&_error]==NO) {
                    *workflowsStop=YES;
                    return;
                }

            }
            NSArray *folderContents=[fm contentsOfDirectoryAtPath:packagesURL.path error:&_error];
            if (folderContents==nil){
                *workflowsStop=YES;
                return;
            }
            __block BOOL hadError=NO;
            [folderContents enumerateObjectsUsingBlock:^(NSString *currFile, NSUInteger idx, BOOL * _Nonnull stop) {

                if([currFile hasNonASCIICharacters]==YES) {

                    dispatch_sync(dispatch_get_main_queue(), ^{

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Package Name";
                        alert.informativeText=[NSString stringWithFormat:@"The package %@ contains a alphanumeric character that may cause issue during installation",currFile];

                        [alert addButtonWithTitle:@"Cancel"];
                        [alert addButtonWithTitle:@"Continue"];
                        NSInteger res=[alert runModal];
                        if (res==NSAlertFirstButtonReturn) {

                            hadError=YES;
                            *stop=YES;
                            return;
                        }
                    });
                }
                NSString *fullPath=[packagesURL.path stringByAppendingPathComponent:currFile];
                if ([currFile containsString:@"com.twocanoes.mds"]){
                    if([fm removeItemAtPath:fullPath error:&_error]==NO){
                        *workflowsStop=YES;
                        return;
                    }
                }
            }];
            if (hadError==YES) {
                _error=[NSError errorWithDomain:@"TCS" code:299 userInfo:nil];

                *workflowsStop=YES;
                return;
            }

            //remove this folder since it needs to be created fresh each time
            if([fm createDirectoryAtPath:packagesURL.path withIntermediateDirectories:YES attributes:nil error:&_error]==NO) {
                NSLog(@"error creating user package: %@",(_error).localizedDescription);
            }

            [self createMDSPackagesAtPath:packagesURL.path forWorkflow:currWorkflow error:&_error];

            if (_error){
                *workflowsStop=YES;
                return;
            }

            NSArray *packagesArray=[self packagesPathsInFolder:currWorkflow.packagesFolderPath ignoreItems:currWorkflow.disabledPackages orderingArray:currWorkflow.packagesFolderOrder];

            if(currWorkflow.isActive==YES && currWorkflow.usePackagesFolderPath==YES && (!packagesArray || packagesArray.count==0)){

                *workflowsStop=YES;

                _error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedRecoverySuggestionErrorKey:@"Please specify a valid resources folder and try again.",NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The resource folder \"%@\"  in the workflow \"%@\" doesn\'t contain any packages/apps, all items have been disabled or no longer exists.",currWorkflow.packagesFolderPath,currWorkflow.workflowName]}];
                *workflowsStop=YES;
                return;
            }
            NSString *firstLoginInstallerFinalPath=[packagesURL.path stringByAppendingPathComponent:@"MDS First Login Installer.pkg"];

            if ([fm fileExistsAtPath:firstLoginInstallerFinalPath]==YES){

                if([fm removeItemAtPath:firstLoginInstallerFinalPath error:&_error]==NO){

                    NSLog(@"Could not remove firstLoginInstallerFinalPath");
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":_error}];
                    *workflowsStop=YES;
                    return;

                }
            }
            NSString *firstLoginInstaller=[[NSBundle mainBundle] pathForResource:@"MDS First Login Installer" ofType:@"pkg"];
//
            [packagesArray enumerateObjectsUsingBlock:^(NSString *currPackageName, NSUInteger idx, BOOL * _Nonnull stop) {

                NSString *fullPackagePathSource=[currWorkflow.packagesFolderPath stringByAppendingPathComponent:currPackageName];

                if (currWorkflow.usePackagesFolderPath==YES) { // option selected to use this workflow
                    //preinstall packages
                    if ([currWorkflow.preInstallPackages containsObject:currPackageName]){ //we have a pre install package

                        NSString *preOSPackagePathDestination=[preOSpackagesURL.path stringByAppendingPathComponent:[NSString stringWithFormat:@"0%02lu-%@",(unsigned long)idx,currPackageName]];

                        NSString *newPackagePath;
                        if((newPackagePath=[self extracted:&_error fullPackagePathSource:fullPackagePathSource preOSPackagePathDestination:preOSPackagePathDestination sharedPackagesURL:sharedPackagesURL])==nil){
                            *stop=YES;
                            *workflowsStop=YES;
                            return;
                        }
                        [prePackagesCopied addObject:preOSPackagePathDestination.lastPathComponent]; //add to list of completed
                        [sharedPackagesCopied addObject:newPackagePath.lastPathComponent]; //add to list of completed

                    }
                    else { // post install packages
                        NSString *fullPackagePathDestination=[sharedPackagesURL.path stringByAppendingPathComponent:currPackageName];

//                        NSString *fullPackagePathDestination=[sharedPackagesURL.path stringByAppendingPathComponent:currPackageName];




                        NSString *appPackageDestinationPath=[self replaceExtensionWithPKGExtensionForFile:fullPackagePathDestination];

                        BOOL shouldForceCopy=NO;
                        NSError *err;
                        //First Login
                        NSDictionary *fileAttributes=[fm attributesOfItemAtPath:fullPackagePathSource error:&err];
                        if(!fileAttributes) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                *stop=YES;
                                *workflowsStop=YES;

                                [[NSAlert alertWithError:err] runModal];
                            });
                            return;

                        }
                        NSNumber *size=fileAttributes[NSFileSize];

                        NSString *finalPackageName=[[NSString stringWithFormat:@"FirstLogin-%@-%@",appPackageDestinationPath.lastPathComponent.stringByDeletingPathExtension,size] stringByAppendingPathExtension:@"pkg"];

                        NSString* finalPackageNameWithSortOrder=[NSString stringWithFormat:@"2%02lu-%@",(unsigned long)idx,finalPackageName];
                        NSString *destinationFolder=[appPackageDestinationPath stringByDeletingLastPathComponent];
                        NSString *finalPackageNamePath=[destinationFolder stringByAppendingPathComponent:finalPackageName];

                        if ([currWorkflow.firstRunPackages containsObject:currPackageName]) {

                            NSArray *destinationFolderContents=[fm contentsOfDirectoryFullPathsAtPath:destinationFolder error:&_error];
                            if (!destinationFolderContents){
                                *stop=YES;
                                *workflowsStop=YES;
                                return;
                            }

                            NSArray *disabledFolderContents=[fm contentsOfDirectoryFullPathsAtPath:[destinationFolder stringByAppendingPathComponent:@"Disabled"] error:nil];

                            if (disabledFolderContents && disabledFolderContents.count>0){
                                destinationFolderContents=[destinationFolderContents arrayByAddingObjectsFromArray:disabledFolderContents];
                            }

                            __block BOOL foundExisting=NO;
                            [destinationFolderContents enumerateObjectsUsingBlock:^(NSString *currItemPath, NSUInteger idx, BOOL * _Nonnull stopDFC) {

                                if ([currItemPath.lastPathComponent containsString:finalPackageName]){

                                    //sort order has been changed. so we rename
                                    if ([fm fileExistsAtPath:[destinationFolder stringByAppendingPathComponent:finalPackageNameWithSortOrder]]==NO){

                                        if([fm moveItemAtPath:currItemPath toPath:[destinationFolder stringByAppendingPathComponent:finalPackageName] error:&_error]==NO){
                                            *stopDFC=YES;
                                            *stop=YES;
                                            *workflowsStop=YES;
                                            return;
                                        }
                                    }
                                    foundExisting=YES;
                                }
                            }];
                            if (![fm fileExistsAtPath:firstLoginInstallerFinalPath]){
                                if([fm copyItemAtPath:firstLoginInstaller toPath:firstLoginInstallerFinalPath error:&_error]==NO){
                                    *stop=YES;
                                    *workflowsStop=YES;
                                    return;
                                }

                                [packagesCopied addObject:firstLoginInstallerFinalPath.lastPathComponent]; //add to list of completed

                            }
                            if (foundExisting==NO){
                                //check if MDS First Login Installer.pkg is already there. this installs the first install packages.

                                NSString *tempFolder=[[TCSDefaultsManager sharedManager] newTempFolder];
                                if (!tempFolder) {
                                    *stop=YES;
                                    *workflowsStop=YES;
                                    return;
                                }
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Creating First Login pkg for %@",fullPackagePathSource.lastPathComponent]}];

                                if([fm copyItemAtPath:fullPackagePathSource toPath:[tempFolder  stringByAppendingPathComponent:finalPackageNameWithSortOrder] error:&_error]==NO){
                                    *stop=YES;
                                    *workflowsStop=YES;
                                    return;
                                }
                                TCSRepackage *repackage=[[TCSRepackage alloc] init];
                                NSString *finalPackagePath=[self replaceExtensionWithPKGExtensionForFile:finalPackageNamePath];
                                [repackage packageResourcesAtPath:tempFolder installLocation:@"/var/com.twocanoes.mds/packages" version:@"1.0" identifer:[[NSUUID UUID] UUIDString] outputPackagePath:finalPackagePath completionBlock:^(NSError * _Nonnull err) {
                                    if (err) {
                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];

                                        _error=err;
                                        *stop=YES;
                                        *workflowsStop=YES;
                                        return;

                                    }
                                }];
                            }
                            NSString *symPathDestination=[NSString stringWithFormat:@"../../../SharedPackages/%@",finalPackageNamePath.lastPathComponent];


                            NSString *symPathSource=[packagesURL.path stringByAppendingPathComponent:finalPackageNameWithSortOrder.lastPathComponent];

                             if ([fm fileExistsAtPath:symPathSource]){
                                 if([fm removeItemAtPath:symPathSource error:&_error]==NO){
                                     return ;
                                 }
                             }
                             if([fm createSymbolicLinkAtPath:symPathSource withDestinationPath:symPathDestination error:&_error]==NO){
                                 return ;
                             }

                            [sharedPackagesCopied addObject:[self replaceExtensionWithPKGExtensionForFile:finalPackageName]];

                            [packagesCopied addObject:[self replaceExtensionWithPKGExtensionForFile:finalPackageNameWithSortOrder]];

                        }
                        else {//post install packages
                            NSString *newPackagePath;

                            NSString *destPath=[self replaceExtensionWithPKGExtensionForFile:appPackageDestinationPath];
                            NSString *destPathLink=[packagesURL.path stringByAppendingPathComponent:[NSString stringWithFormat:@"2%02lu-%@",(unsigned long)idx,destPath.lastPathComponent]];

                            if((newPackagePath=[self repackageAndCopyIfNeeded:fullPackagePathSource toDestination:sharedPackagesURL.path symbolicLink:destPathLink shouldForceCopy:shouldForceCopy shouldAddFilesize:YES                                                         confirmationBlock:^BOOL{

                                BOOL shouldRepackage=[self shouldRepackage:fullPackagePathSource.lastPathComponent];

                                return shouldRepackage;
                            } error:&_error])==NO){
                                *stop=YES;
                                *workflowsStop=YES;
                                return;
                            }
                            [sharedPackagesCopied addObject:[self replaceExtensionWithPKGExtensionForFile:newPackagePath.lastPathComponent]];

                            [packagesCopied addObject:[self replaceExtensionWithPKGExtensionForFile:destPathLink.lastPathComponent]];


                        }
                    }
                }

                if (self.stopCopying==YES) *stop=YES;
            }];

            BOOL hasUserScripts=(currWorkflow.useScriptFolderPath==YES &&
                                 currWorkflow.scriptFolderPath &&
                                 currWorkflow.scriptFolderPath.length>0);
            BOOL hasUserProfiles=(currWorkflow.useProfilesFolderPath==YES &&
                                  currWorkflow.profilesFolderPath &&
                                  currWorkflow.profilesFolderPath.length>0);


            BOOL hasOptionsSet=(currWorkflow.shouldSetComputerName==YES ||
                                currWorkflow.shouldSkipSetupAssistant==YES||
                                currWorkflow.shouldSkipPrivacySetup ==YES||
                                currWorkflow.shouldConfigureWifi==YES||
                                currWorkflow.shouldEnableSSH==YES||
                                currWorkflow.shouldEnableMunkiReport==YES||
                                currWorkflow.shouldSetTargetAsStartupDisk==YES||
                                currWorkflow.passwordHint!=nil ||
                                currWorkflow.shouldEnableScreenSharing==YES ||
                                currWorkflow.shouldEnableLocationServices==YES||
                                currWorkflow.shouldWaitForNetwork==YES||
                                currWorkflow.shouldConfigureMunkiClient==YES||
                                currWorkflow.shouldTrustServerCertificate==YES||
                                (currWorkflow.shouldTrustMunkiClientCertificate==YES && currWorkflow.munkiClientCertificate) ||
                                currWorkflow.shouldRunSoftwareUpdate==YES ||
                                (currWorkflow.shouldInstall==NO && currWorkflow.shouldRebootAfterInstallingResources ==YES ) ||
                                (currWorkflow.shouldInstall==YES && currWorkflow.shouldRebootAfterInstallingResourcesInstallOS ==YES )

                                );

            if (*workflowsStop==NO && (hasUserProfiles || hasUserScripts || hasOptionsSet==YES || currWorkflow.users.count>0)){

                BOOL isDir;
                NSArray *scriptArray;
                NSArray *profilesArray;
                NSMutableArray *fullPathScriptsArray=[NSMutableArray array];
                NSMutableArray *fullPathPreScriptsArray=[NSMutableArray array];
                NSMutableArray *fullPathWorkflowScriptsArray=[NSMutableArray array];

                NSMutableArray *fullPathProfilesArray=[NSMutableArray array];
                NSMutableArray *fullPathPreProfilesArray=[NSMutableArray array];

                if(hasUserScripts==YES && [fm fileExistsAtPath:currWorkflow.scriptFolderPath isDirectory:&isDir] && isDir==YES) {
                    scriptArray=[fm contentsOfDirectoryAtPath:currWorkflow.scriptFolderPath error:&_error];
                    if(_error || !scriptArray) {
                        *workflowsStop=YES;
                        return;
                    }
                    NSArray *orderArray=currWorkflow.scriptFolderOrder;

                    NSMutableArray *scriptArrayToProcess=[NSMutableArray array];
                    NSMutableArray *scriptLeftOvers=[NSMutableArray arrayWithArray:scriptArray];

                    if (orderArray){
                        [orderArray enumerateObjectsUsingBlock:^(NSString *currentScript, NSUInteger idx, BOOL * _Nonnull stop) {

                            if ([scriptArray containsObject:currentScript]){
                                [scriptArrayToProcess addObject:currentScript];
                                [scriptLeftOvers removeObject:currentScript];
                            };
                        }];
                        [scriptArrayToProcess addObjectsFromArray:scriptLeftOvers];
                    }
                    else{
                        [scriptArrayToProcess addObjectsFromArray:scriptArray];
                    }
                    [scriptArrayToProcess enumerateObjectsUsingBlock:^(NSString *scriptName, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([currWorkflow.disabledScripts containsObject:scriptName]){
                            return;
                        }
                        if ([currWorkflow.prePackageScripts containsObject:scriptName]){
                            [fullPathPreScriptsArray addObject:[currWorkflow.scriptFolderPath stringByAppendingPathComponent:scriptName]];

                        }
                        else if ([currWorkflow.workflowFirstRunScripts containsObject:scriptName]){
                            [fullPathWorkflowScriptsArray addObject:[currWorkflow.scriptFolderPath stringByAppendingPathComponent:scriptName]];

                        }
                        else {
                            [fullPathScriptsArray addObject:[currWorkflow.scriptFolderPath stringByAppendingPathComponent:scriptName]];
                        }
                    }];

                    if (_error){
                        NSLog(@"error saving Scripts");
                        *workflowsStop=YES;
                        return;

                    }

                }

                if(hasUserProfiles==YES && [fm fileExistsAtPath:currWorkflow.profilesFolderPath isDirectory:&isDir] && isDir==YES) {
                    profilesArray=[fm contentsOfDirectoryAtPath:currWorkflow.profilesFolderPath error:&_error];
                    if(_error || !profilesArray) {
                        *workflowsStop=YES;
                        return;
                    }

                    NSArray *profilesOrderArray=currWorkflow.profilesFolderOrder;

                    NSMutableArray *profileArrayToProcess=[NSMutableArray array];
                    NSMutableArray *profileLeftOvers=[NSMutableArray arrayWithArray:profilesArray];

                    if (profilesOrderArray){
                        [profilesOrderArray enumerateObjectsUsingBlock:^(NSString *currProfile, NSUInteger idx, BOOL * _Nonnull stop) {

                            if ([profilesArray containsObject:currProfile]){
                                [profileArrayToProcess addObject:currProfile];
                                [profileLeftOvers removeObject:currProfile];
                            };
                        }];
                        [profileArrayToProcess addObjectsFromArray:profileLeftOvers];
                    }
                    else{
                        [profileArrayToProcess addObjectsFromArray:profilesArray];
                    }


                    [profileArrayToProcess enumerateObjectsUsingBlock:^(NSString *profileName, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([currWorkflow.disabledProfiles containsObject:profileName]){
                            return;
                        }

                        if ([currWorkflow.prePackageProfiles containsObject:profileName]){
                            [fullPathPreProfilesArray addObject:[currWorkflow.profilesFolderPath stringByAppendingPathComponent:profileName]];

                        }
                        else {
                            [fullPathProfilesArray addObject:[currWorkflow.profilesFolderPath stringByAppendingPathComponent:profileName]];
                        }
                    }];

                    if (_error){
                        NSLog(@"error saving profiles");
                        *workflowsStop=YES;
                        return;

                    }

                }



                NSString *scriptsPrePackagePath;

                NSString *scriptsPrePackagePathInstallOS=[packagesURL.path stringByAppendingPathComponent:@"001-com.twocanoes.mds.pre-scripts.pkg"];

                if([fm fileExistsAtPath:scriptsPrePackagePathInstallOS]){
                    if([fm removeItemAtPath:scriptsPrePackagePathInstallOS error:&_error]==NO){
                        *workflowsStop=YES;
                        return;
                    }
                }

                NSString *scriptsPrePackagePathNoInstallOS=[preOSpackagesURL.path stringByAppendingPathComponent:@"000-com.twocanoes.mds.pre-scripts.pkg"];

                if([fm fileExistsAtPath:scriptsPrePackagePathNoInstallOS]){
                    if([fm removeItemAtPath:scriptsPrePackagePathNoInstallOS error:&_error]==NO){
                        *workflowsStop=YES;
                        return;
                    }
                }


                if ([fm fileExistsAtPath:scriptsPrePackagePathInstallOS]){
                    if( [fm removeItemAtPath:scriptsPrePackagePathInstallOS error:&_error]==NO){
                        NSLog(@"error saving scriptsPrePackagePathInstallOS");
                        *workflowsStop=YES;
                        return;

                    }
                }
                if ([fm fileExistsAtPath:scriptsPrePackagePathNoInstallOS]){
                    if( [fm removeItemAtPath:scriptsPrePackagePathNoInstallOS error:&_error]==NO){
                        NSLog(@"error saving scriptsPrePackagePathNoInstallOS");
                        *workflowsStop=YES;
                        return;

                    }
                }



                if (currWorkflow.shouldInstall==YES) {
                    scriptsPrePackagePath=scriptsPrePackagePathInstallOS;
                }
                else {
                    scriptsPrePackagePath=scriptsPrePackagePathNoInstallOS;
                }


                BOOL wifiAlreadySet=NO;
                if (currWorkflow.shouldInstall==YES || (currWorkflow.shouldInstall==NO && fullPathPreScriptsArray.count>0)){
                    if( [TCSConfigHelper saveScripts:[NSArray arrayWithArray:fullPathPreScriptsArray] toPath:scriptsPrePackagePath
                                            profiles:currWorkflow.useProfilesFolderPath==YES?fullPathPreProfilesArray:@[]
                                        resourcePath:[currWorkflow.scriptFolderPath stringByAppendingPathComponent:@"Resources"]
                                   waitForNetworking:currWorkflow.shouldWaitForNetwork
                               shouldSetComputerName:currWorkflow.shouldInstall==NO?NO:currWorkflow.shouldSetComputerName //if this is a "run with workflow" script, then don't set computer name since it would be set in recovery.
                                            workflow:currWorkflow
                            shouldSetOneTimeSettings:NO
                               shouldSetPasswordHint:NO
                                      wifiAlreadySet:NO
                             shouldRunSoftwareUpdate:NO
                                        rebootAction:TCSSHUTDOWNOPTIONNONE
                                   shouldCreateUsers:YES
                                               error:&_error]==NO) {

                        if (_error) {
                            *workflowsStop=YES;
                            return;
                        }
                    }
                    else {
                        wifiAlreadySet=YES;
                    }
                    [prePackagesCopied addObject:scriptsPrePackagePath.lastPathComponent];
                    [sharedPackagesCopied addObject:scriptsPrePackagePath.lastPathComponent];

                }

                //                [fullPathScriptsArray sortUsingSelector:@selector(caseInsensitiveCompare:)];
                //                [fullPathWorkflowScriptsArray sortUsingSelector:@selector(caseInsensitiveCompare:)];

                NSString *workflowScriptsPath=[workflowURL.path stringByAppendingPathComponent:@"WorkflowScripts"];

                if ([fm fileExistsAtPath:workflowScriptsPath]==NO){

                    if([fm createDirectoryAtPath:workflowScriptsPath withIntermediateDirectories:YES attributes:nil error:&_error]==NO){
                        *workflowsStop=YES;
                        return;
                    }

                }
                [fullPathWorkflowScriptsArray enumerateObjectsUsingBlock:^(NSString *filePath, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString *destinationPath=[workflowScriptsPath stringByAppendingPathComponent:filePath.lastPathComponent];
                    if ([fm fileExistsAtPath:destinationPath]) {

                        if([fm removeItemAtPath:destinationPath error:&_error]==NO){
                            *workflowsStop=YES;
                            return;
                        }
                    }
                    if ([fm copyItemAtPath:filePath toPath:destinationPath error:&_error]==NO){
                        *workflowsStop=YES;
                        return;

                    }
                    if ([fm isExecutableFileAtPath:destinationPath]==NO){

                        if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:destinationPath error:&_error]==NO) {
                            *workflowsStop=YES;
                            return;

                        }
                    }
                }];

                NSString *scriptsPackagePath=[packagesURL.path stringByAppendingPathComponent:@"999-com.twocanoes.mds.scripts.pkg"];


                if (currWorkflow.shouldEnableMunkiReport==YES && currWorkflow.munkiReportEnrollmentScript && currWorkflow.munkiReportEnrollmentScript.length>0){

                    NSString *tempFolder=[[TCSDefaultsManager sharedManager] newTempFolder];

                    NSString *munkiReportScriptPath=[tempFolder stringByAppendingPathComponent:@"com.twocanoes.munkireport.sh"];


                    if([currWorkflow.munkiReportEnrollmentScript writeToFile:munkiReportScriptPath atomically:NO encoding:NSUTF8StringEncoding error:&_error]==NO){
                        *workflowsStop=YES;
                        return;

                    }
                    [fullPathScriptsArray addObject:munkiReportScriptPath];
                }
                if([TCSConfigHelper saveScripts:[NSArray arrayWithArray:fullPathScriptsArray]
                                         toPath:scriptsPackagePath
                                       profiles:currWorkflow.useProfilesFolderPath==YES?fullPathProfilesArray:@[]
                                   resourcePath:[currWorkflow.scriptFolderPath stringByAppendingPathComponent:@"Resources"]
                              waitForNetworking:currWorkflow.shouldWaitForNetwork
                          shouldSetComputerName:currWorkflow.shouldSetComputerName workflow:currWorkflow
                       shouldSetOneTimeSettings:YES
                          shouldSetPasswordHint:currWorkflow.users.count>0?YES:NO
                                 wifiAlreadySet:wifiAlreadySet
                        shouldRunSoftwareUpdate:NO
                    //                    shouldRunSoftwareUpdate:currWorkflow.shouldInstall==YES?currWorkflow.shouldRunSoftwareUpdate:NO
                                   rebootAction:currWorkflow.shouldInstall==YES && currWorkflow.shouldRebootAfterInstallingResourcesInstallOS==YES?(int)currWorkflow.restartOrShutdownIndexInstallOS:TCSSHUTDOWNOPTIONNONE
                              shouldCreateUsers:NO
                                          error:&_error]==NO) {


                    if (_error) {
                        *workflowsStop=YES;
                        return;
                    }
                }
                [packagesCopied addObject:scriptsPackagePath.lastPathComponent];
                [sharedPackagesCopied addObject:scriptsPackagePath.lastPathComponent];



            }
            if (_error) {
                *workflowsStop=YES;
                return;
            }
            [self cleanupFolder:packagesURL packagesCopied:packagesCopied ignore:packagesCopied workflowName:currWorkflow.workflowName];
            [self cleanupFolder:preOSpackagesURL packagesCopied:prePackagesCopied ignore:nil workflowName:currWorkflow.workflowName];
        }
        if (self.stopCopying==YES) *workflowsStop=YES;

    }];
    [self cleanupFolder:sharedPackagesURL packagesCopied:sharedPackagesCopied ignore:nil workflowName:nil];

    if (_error) {
        *error=_error;
        return NO;
    }
    if (self.stopCopying==YES) {
        return NO;
    }
    if (![fm fileExistsAtPath:appURL.path]){
        if([fm createDirectoryAtPath:appURL.path withIntermediateDirectories:YES attributes:nil error:error]==NO ){

            return NO;
        }
    }
    NSLog(@"copying Imagr.app");
    NSString *sourceImagrPath=[TCSConfigHelper imagrPath];

    if (!sourceImagrPath){
        NSLog(@"%@ does not exist!",sourceImagrPath);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"imagr app does not exist"}]}];

        return NO;
    }

    NSString *destinationImagrPath=[appURL.path stringByAppendingPathComponent:@"Imagr.app"];

    if ([fm fileExistsAtPath:destinationImagrPath]) {

        NSString *currentImagrVersion=[TCSConfigHelper imagrVersion];
        NSString *destinationImagrVersion=[TCSConfigHelper imagrVersionAtPath:destinationImagrPath];

        if (currentImagrVersion && destinationImagrVersion && ![destinationImagrVersion isEqualToString:currentImagrVersion])
        {


            NSLog(@"Imagr Version mismatch! Downloaded version : %@ destination version: %@. Removing from destination",currentImagrVersion, destinationImagrVersion);
            if([fm removeItemAtPath:destinationImagrPath error:error]==NO){

                NSLog(@"Could not remove imagr at destination.");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

                return NO;

            }
        }

    }
    if (![fm fileExistsAtPath:destinationImagrPath]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceImagrPath.lastPathComponent]}];

        if([fm copyItemAtPath:sourceImagrPath toPath:destinationImagrPath error:error]==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

        }
    }

    if (self.stopCopying==YES) {
        return NO;
    }

    if (![fm fileExistsAtPath:destinationImagrPath]) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceImagrPath.lastPathComponent]}];

        if([fm copyItemAtPath:sourceImagrPath toPath:destinationImagrPath error:error]==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];
        }

    }

    NSString *sourceMDSDeployPath=[[NSBundle mainBundle]  pathForResource:@"MDS Deploy" ofType:@"app"];
    NSString *destinationMDSDeployPath=[appURL.path stringByAppendingPathComponent:@"MDS Deploy.app"];

    if ([fm fileExistsAtPath:destinationMDSDeployPath]) {
        if([fm removeItemAtPath:destinationMDSDeployPath error:error]==NO){

            NSLog(@"Could not remove MDS Deploy at destination.");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

            return NO;

        }

    }

    if([fm copyItemAtPath:sourceMDSDeployPath toPath:destinationMDSDeployPath error:error]==NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];
    }


    NSString *sourceRunPath=[[NSBundle mainBundle] pathForResource:@"run" ofType:@""];
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *customRunPath=[ud objectForKey:@"alternateRunCommandPath"];

    if (customRunPath && customRunPath.length>0&&[fm fileExistsAtPath:customRunPath]){
        sourceRunPath=customRunPath;
    }

    NSString *destinationRunPath=[inResourceURL.path stringByAppendingPathComponent:@"run"];

    if ([fm fileExistsAtPath:destinationRunPath]==YES) {

        if([fm removeItemAtPath:destinationRunPath error:error]==NO) {
            return NO;

        }

    }

    NSString *settingsSourcePath=[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"sh"];
    NSString *settingsDestinationPath=[[inResourceURL.path stringByAppendingPathComponent:@"Deploy"] stringByAppendingPathComponent:@"settings.sh"];

    if ([fm fileExistsAtPath:settingsDestinationPath]==YES) {

        if([fm removeItemAtPath:settingsDestinationPath error:error]==NO) {

            return NO;

        }

    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceRunPath.lastPathComponent]}];

    if([fm copyItemAtPath:sourceRunPath toPath:destinationRunPath error:error]==NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

    }
    NSMutableString *settings=[NSMutableString stringWithContentsOfFile:settingsSourcePath encoding:NSUTF8StringEncoding error:error];
    if (!settings){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

    }

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldWaitForNetwork"]==YES) {
        [settings appendString:[NSString stringWithFormat:@"wait_for_network=1\n"]];

    }


    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldConnectToWiFi"]==YES) {


        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        NSString *recoveryWiFiSSID=[ud objectForKey:@"recoveryWiFiSSID"];
        NSString *recoveryWiFiPassword=[ud objectForKey:@"recoveryWiFiPassword"];
        if (recoveryWiFiSSID) [settings appendString:[NSString stringWithFormat:@"wifi_ssid=\"%@\"\nwifi_password=\"%@\"\n",recoveryWiFiSSID,recoveryWiFiPassword]];

    }
    if ([settings writeToFile:settingsDestinationPath atomically:NO encoding:NSUTF8StringEncoding error:error]==NO){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];
    }

    if (self.stopCopying==YES) {
        return NO;
    }

    if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:destinationRunPath error:error]==NO) {

    }

    NSString *sourceQREncode=[[NSBundle mainBundle]  pathForResource:@"qrencode" ofType:@""];

    NSString *sourceNetworkSetupPath=@"/usr/sbin/networksetup";
    NSString *sourceCurlPath=@"/usr/bin/curl";

    [self copyBinary:sourceNetworkSetupPath resourceURL:inResourceURL];
    [self copyBinary:sourceCurlPath resourceURL:inResourceURL];
    [self copyBinary:sourceQREncode resourceURL:inResourceURL];


    NSURL *prefix=[NSURL URLWithString:@"/Deploy"];
    if (!prefix) prefix=[inResourceURL URLByAppendingPathComponent:@"Deploy"];

    NSURL *config=[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"Config"];

    NSURL *serverConfigURL=[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"serverurl"]];
    NSMutableDictionary *configDict;

    if (!serverConfigURL) {
        serverConfigURL=[[prefix URLByAppendingPathComponent:@"Config"] URLByAppendingPathComponent:@"config.plist"];
        configDict=[@{@"serverurl":[TCSUtility variableStringFromURL:serverConfigURL]} mutableCopy];
    }
    else {
        configDict=[@{@"serverurl":serverConfigURL.absoluteString} mutableCopy];

    }
    NSURL *destinationURL=[config URLByAppendingPathComponent:@"workflow_options.sh"];

    if ([fm fileExistsAtPath:destinationURL.path]){

        if ([fm removeItemAtPath:destinationURL.path error:error]==NO){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

            return NO;

        }
    }
    if (![fm fileExistsAtPath:config.path]){
        if([fm createDirectoryAtPath:config.path withIntermediateDirectories:YES attributes:nil error:error]==NO ){

            return NO;
        }
    }

    if ([ud boolForKey:@"shouldOverrideWorkflowOptions"]==YES &&  [ud objectForKey:@"workflowOptionsScriptPath"] &&  [[ud objectForKey:@"workflowOptionsScriptPath"] length]>0){

        if([fm copyItemAtPath:[ud objectForKey:@"workflowOptionsScriptPath"] toPath:destinationURL.path error:error]==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":*error}];

            return NO;
        }

    }



    NSString *loggingURLString;
    if([ud boolForKey:@"shouldSendLoggingInfo"]==YES && [ud objectForKey:@"loggingInfoURLString"] && [[ud objectForKey:@"loggingInfoURLString"] length]>0){

        loggingURLString=[ud objectForKey:@"loggingInfoURLString"];

    }

    if (loggingURLString) [configDict setObject:loggingURLString forKey:@"reporturl"];
    NSString *destinationConfigPath=[appURL URLByAppendingPathComponent:@"com.grahamgilbert.Imagr.plist"].path;

    NSString *destinationConfigPath2=[appURL URLByAppendingPathComponent:@"config.plist"].path;


    if ([fm fileExistsAtPath:destinationConfigPath]) {

        if ([fm removeItemAtPath:destinationConfigPath error:error]==NO) {
            return NO;
        }

    }
    if ([fm fileExistsAtPath:destinationConfigPath2]) {

        if ([fm removeItemAtPath:destinationConfigPath2 error:error]==NO) {
            return NO;
        }

    }

    if([configDict writeToFile:destinationConfigPath atomically:NO]==NO) {
        *error=[NSError easyErrorWithTitle:@"Error writing to file"
                                      body:[NSString stringWithFormat:@"An error occurred when file %@.",destinationConfigPath]
                                      line:__LINE__
                                      file:@__FILE__];
        return NO;
    }

    if([configDict writeToFile:destinationConfigPath2 atomically:NO]==NO) {
        *error=[NSError easyErrorWithTitle:@"Error writing to file"
                                      body:[NSString stringWithFormat:@"An error occurred when file %@.",destinationConfigPath2]
                                      line:__LINE__
                                      file:@__FILE__];
        return NO;
    }
    if (self.stopCopying==YES) {
        *error=[NSError easyErrorWithTitle:@"Operation Cancelled"
                                      body:@"Saving resources was cancelled"
                                      line:__LINE__
                                      file:@__FILE__];

        return NO;
    }



    NSDictionary *settingsDict=[self imagrDictionaryWithError:error];

    if (!settingsDict){
        return NO;
    }


    NSString *destinationWorkflowConfigPath=[[config URLByAppendingPathComponent:@"config.plist"] path];

    if([settingsDict writeToFile:destinationWorkflowConfigPath atomically:NO]==NO){
        NSString *failureReason=[NSString stringWithFormat:@"The configuration file %@ could not be written. Please check the folder and try again.",destinationWorkflowConfigPath];

        *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"The configuration file could not be written.",NSLocalizedRecoverySuggestionErrorKey:failureReason}];

        return NO;
    }

    return YES;
}

-(int)cleanupFolder:(NSURL *)packagesURL packagesCopied:(NSArray *)packagesCopied ignore:(NSArray*)ignore workflowName:(NSString *)workflowName{
    NSFileManager *fm=[NSFileManager defaultManager];
    __block int count=0;
    __block int ignoreCount=0;
    __block NSError *err;
    NSArray *destinationContents=[fm contentsOfDirectoryAtPath:packagesURL.path error:&err];

    __block NSMutableArray *leftOvers=[NSMutableArray arrayWithArray:destinationContents];
    [packagesCopied enumerateObjectsUsingBlock:^(NSString *currFile, NSUInteger idx, BOOL * _Nonnull stop) {


        if ([leftOvers containsObject:currFile]) [leftOvers removeObject:currFile];
    }];

    if ([leftOvers containsObject:@".DS_Store"]) [leftOvers removeObject:@".DS_Store"];

    if (leftOvers.count>0) {

        NSString *disabledDir=[packagesURL.path stringByAppendingPathComponent:@"Disabled"];
        if (![fm fileExistsAtPath:disabledDir]) {

            if([fm createDirectoryAtPath:disabledDir withIntermediateDirectories:YES attributes:nil error:&err]==NO) {
                //                *workflowsStop=YES;
                NSLog(@"%@",err.localizedDescription);
                return -1;
            }
        }
        NSMutableString *packagelist=[NSMutableString string];
        [leftOvers enumerateObjectsUsingBlock:^(NSString *currLeftOver, NSUInteger idx, BOOL * _Nonnull stop) {

            BOOL isDir=NO;
            if (
                ([fm fileExistsAtPath:[packagesURL.path stringByAppendingPathComponent:currLeftOver] isDirectory:&isDir] && (isDir==YES && ![currLeftOver.pathExtension containsString:@"pkg"]))||
                ([currLeftOver containsString:@"com.twocanoes.mds"])
                )

            {
                return;
            }


            count++;
            NSString *saveFilename=currLeftOver;
            NSString *uuidString=[NSUUID UUID].UUIDString;
            if ([fm fileExistsAtPath:[disabledDir stringByAppendingPathComponent:currLeftOver]]){
                saveFilename=[NSString stringWithFormat:@"%@_%@",saveFilename,uuidString];
            }
            NSLog(@"extra package found:%@. moving to disabled folder or removing symlinks.",currLeftOver);
            if ([ignore containsObject:currLeftOver ]){
                ignoreCount++;
            }
            NSString *pathToGetRidOf=[packagesURL.path stringByAppendingPathComponent:currLeftOver];
            NSDictionary *attributes=[fm attributesOfItemAtPath:pathToGetRidOf error:nil];

            if (attributes && [attributes objectForKey:NSFileType]==NSFileTypeSymbolicLink){
                if([fm removeItemAtPath:pathToGetRidOf error:&err]==NO){
                    NSLog(@"%@",err);
                    return;
                }
                ignoreCount++;

            }
            else {
                if([fm moveItemAtPath:[packagesURL.path stringByAppendingPathComponent:currLeftOver] toPath:[disabledDir stringByAppendingPathComponent:saveFilename] error:&err]==NO){
                    NSLog(@"%@",err);
                    return;
                }
                [packagelist appendFormat:@"%@\n",currLeftOver];

            }
        }];
        if (count-ignoreCount>0) {
            NSString *failureReason=[NSString stringWithFormat:@"There were %i files in the package resources for that were not in the source. They were moved to a disabled folder in the SharedPackages for that workflow.\n\n%@",count,packagelist];

            NSError *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Extra packages found",NSLocalizedRecoverySuggestionErrorKey:failureReason}];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSWarning" object:self userInfo:@{@"error":error}];
        }

    }
    return count;
}
-(void)replaceWorkflow:(TCSWorkflow *)oldWorkflow withWorkflow:(TCSWorkflow *)workflow{
    NSMutableArray *proxyArray=[self mutableArrayValueForKey:@"workflows"];

    NSInteger index=[proxyArray indexOfObjectIdenticalTo:oldWorkflow];
    if (index!=NSNotFound) {
        [proxyArray replaceObjectAtIndex:index withObject:workflow];


    }
    else {
        [proxyArray addObject:workflow];

    }
}
-(NSArray *)macOSInstallers{
    __block NSMutableSet *set=[NSMutableSet set];

    //    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldShowInstallMacOSButton"]==YES &&
    //        [[[NSUserDefaults standardUserDefaults] valueForKey:@"launchMacOSInstallerPath"] length]>0 ) {
    //
    //        NSString *installerPath=[[NSUserDefaults standardUserDefaults] valueForKey:@"launchMacOSInstallerPath"];
    //        [set addObject:installerPath];
    //
    //    }

    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {


        if (currWorkflow.isActive==YES && currWorkflow.shouldInstall==YES && currWorkflow.macOSFolderPath && currWorkflow.shouldUseMacOSInstallerURL==NO) [set addObject:currWorkflow.macOSFolderPath];

        else if (currWorkflow.isActive==YES && currWorkflow.shouldImage==YES && currWorkflow.macOSImagePath && currWorkflow.shouldUseAsrDmgURL==NO){
            [set addObject:currWorkflow.macOSImagePath];
        }
    }];

    return [NSArray arrayWithArray:[set allObjects]];

}
-(BOOL)copyOSesToDestination:(NSString *)inDestinationFolder error:(NSError **)error{
    self.osDMGToCopy=[[self macOSInstallers] mutableCopy];

    if (self.osDMGToCopy.count==0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyCompleted" object:self];

        return YES;
    }
    __block NSError *returnError;
    [self.osDMGToCopy enumerateObjectsUsingBlock:^(NSString *currPath, NSUInteger idx, BOOL * _Nonnull stop) {
        NSFileManager *fm=[NSFileManager defaultManager];
        if (![fm fileExistsAtPath:currPath]) {

            returnError=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The OS Image at %@ cannot be found",currPath],NSLocalizedRecoverySuggestionErrorKey:@"Please select a different macOS disk image or return the macOS disk image back to this location"}];
            *stop=YES;


        }
    }];
    if (returnError) {
        *error=returnError;

        return NO;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.osDMGToCopy.count>0) [self copyOS:[self.osDMGToCopy objectAtIndex:0] destination:inDestinationFolder];
    });

    return YES;
}

-(void)copyOS:(NSString *)inSourcePath destination:(NSString *)inDestinationFolder{


    self.macOSDMGDestinationFolder=inDestinationFolder;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *error;

    if (![fm fileExistsAtPath:inSourcePath]) {
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The macOS disk image could not be found at \"%@\"",inSourcePath],NSLocalizedRecoverySuggestionErrorKey:@"Please select a valid macOS disk image and try again."}];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];
        //        [self copyCompleted:self withError:err];


    }
    //    self.status=[NSString stringWithFormat:@"Copying %@",inOSURL.path.lastPathComponent];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",inSourcePath.lastPathComponent]}];


    //    NSString *osDMGDestination=inDestination;

    if (![fm fileExistsAtPath:inDestinationFolder]){
        if([fm createDirectoryAtPath:inDestinationFolder  withIntermediateDirectories:YES
                          attributes:nil error:&error]==NO ){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":error}];
        }
    }

    if ([fm fileExistsAtPath:[inDestinationFolder stringByAppendingPathComponent:inSourcePath.lastPathComponent]]==NO){
        self.fileCopyController=[[TCSCopyFileController alloc] init];

        [self.fileCopyController copyFile:inSourcePath toPath:[inDestinationFolder stringByAppendingPathComponent:inSourcePath.lastPathComponent] delegate:self];
    }
    else {
        [self copyCompleted:self withError:nil];


    }


}
-(void)copyCompleted:(id)sender withError:(NSError *)error{
    [self.osDMGToCopy removeObjectAtIndex:0];
    if (error) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":error}];
        self.osDMGToCopy=0;
    }
    else if (self.osDMGToCopy.count==0){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyCompleted" object:self];
    }
    else {
        [self copyOS:[self.osDMGToCopy objectAtIndex:0] destination:self.macOSDMGDestinationFolder];

    }

}

-(void)percentCompleted:(float)percentCompleted{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"percent":@(percentCompleted/10)}];

    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyCompleted" object:self];
    //
    //    if (percentCompleted>self.percentComplete) {
    //        self.percentComplete=percentCompleted;
    //    }
}

-(void)cancelCopyOperation{
    if (self.fileCopyController) [self.fileCopyController cancelCopyOperation];

}
-(NSDictionary *)imagrDictionaryWithError:(NSError **)err{

    NSURL *resourcePrefix=[NSURL fileURLWithPath:@"/Deploy"];


    NSURL *OSURL=[resourcePrefix URLByAppendingPathComponent:@"macOS"];

    NSMutableDictionary *imagrDict=[NSMutableDictionary dictionary];

    NSMutableArray *imagrWorkflows=[NSMutableArray array];

    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *workflow, NSUInteger idx, BOOL * _Nonnull stop) {

        NSMutableDictionary *imagrWorkflow=[NSMutableDictionary dictionary];
        if (workflow.isActive==YES) {

            imagrWorkflow[@"name"]=workflow.workflowName;
            imagrWorkflow[@"description"]=workflow.workflowDescription;
            if (workflow.shouldLimitWorkflowsByModelType==YES &&
                workflow.workflowAllowedModels &&
                [workflow.workflowAllowedModels length]>0

                )
            {
                imagrWorkflow[@"allowed_models"]=workflow.workflowAllowedModels;

            }

            NSMutableArray *imagrComponents=[NSMutableArray array];

            NSString *sanatizedWorkflowName=[self sanatizedWorkflowNameForWorkflow:workflow.workflowName];
            NSURL *currWorkflowPackageURL=[[[resourcePrefix URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:sanatizedWorkflowName]  URLByAppendingPathComponent:@"Packages"];

            NSURL *currWorkflowPrePackageURL=[[[resourcePrefix URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:sanatizedWorkflowName]  URLByAppendingPathComponent:@"PreOS-Packages"];


            NSURL *currentWorkflowScripts=[[[resourcePrefix URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:sanatizedWorkflowName]  URLByAppendingPathComponent:@"WorkflowScripts"];

            NSArray *filePathsOfPackages=[self packagesPathsInFolder:workflow.packagesFolderPath ignoreItems:workflow.disabledPackages orderingArray:workflow.packagesFolderOrder];

            NSURL *currWorkflowDiscoverCredsScriptURL=[[[[resourcePrefix URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:sanatizedWorkflowName]  URLByAppendingPathComponent:@"CustomScripts"] URLByAppendingPathComponent:@"discover_credentials.sh"];

            [imagrComponents addObject:@{@"type":@"script_folder",
                                         @"url":[TCSUtility variableStringFromURL:currentWorkflowScripts]

            }];


            if (workflow.shouldDisableSIP==YES){

                [imagrComponents addObject:@{@"type":@"script",
                                             @"content":@"#!/bin/bash\n/usr/bin/csrutil enable --without nvram",
                                             @"first_boot":@NO,@"imagr_only":@YES
                }];

            }

            if (workflow.shouldInstall==YES && workflow.shouldEraseAndInstall==YES) {


                NSMutableDictionary *eraseVolumeDict=[@{@"type":@"eraseVolume",@"format":@"auto_hfs_or_apfs"} mutableCopy];
                if (workflow.shouldSetVolumeName==YES && workflow.volumeName.length>0){

                    [eraseVolumeDict setObject:workflow.volumeName forKey:@"name"];
                }
                [imagrComponents addObject:[NSDictionary dictionaryWithDictionary:eraseVolumeDict]];

            }
            if (workflow.shouldSetVariables==YES) {
                NSMutableArray*variableArray=[NSMutableArray arrayWithCapacity:9];

                if (workflow.mdsVariables){

                    [workflow.mdsVariables enumerateObjectsUsingBlock:^(TCSMDSVariable *currMDSVariable, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSString *currVariableLabel=[NSString stringWithFormat:@"com.twocanoes.mds.var%li",idx+1];
                        if (currMDSVariable.variableTypeIndex==MDSVARIABLETEXT){
                            if (currMDSVariable.mdsVarPrompt) [variableArray addObject:@{currVariableLabel:currMDSVariable.mdsVarPrompt}];
                        }
                        else {
                            if (currMDSVariable.mdsVarPrompt && currMDSVariable.variableOptionsArray && currMDSVariable.variableOptionsArray.count>0) {
                                NSMutableArray *values=[NSMutableArray array];
                                [currMDSVariable.variableOptionsArray enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    if ([obj objectForKey:@"option"]) [values addObject:[obj objectForKey:@"option"]];

                                }];
                                NSDictionary *popupOptions=@{
                                    @"label":currVariableLabel,
                                    @"prompt":currMDSVariable.mdsVarPrompt,
                                    @"options":values,
                                    @"add_other_option":@(currMDSVariable.variableOtherOption)
                                };
                                [variableArray addObject:popupOptions];
                            }
                        }

                    }];
                    if (variableArray && variableArray.count>0) {
                        NSDictionary *setVariables=@{@"type":@"extended_variables",@"variable_selections":variableArray};
                        [imagrComponents addObject:setVariables];
                    }
                }
            }
            if (workflow.shouldSetComputerName==YES) {
                
                NSMutableDictionary *computerNameOptions=[NSMutableDictionary dictionary];
                computerNameOptions[@"type"]=@"computer_name";
                computerNameOptions[@"nvram"]=@YES;
                if (workflow.computerNameString && workflow.computerNameString.length>0){


                    [computerNameOptions setValuesForKeysWithDictionary:@{@"computer_name":workflow.computerNameString}];
                }
                if (workflow.shouldPromptForComputerName==NO) {

                    computerNameOptions[@"auto"]=@YES;

                }
                if (computerNameOptions) {
                    [imagrComponents addObject:computerNameOptions];
                    NSDictionary *setComputerName=@{@"type":@"script",
                                                    @"content":@"#!/bin/bash\nnvram com.twocanoes.mds.ComputerName=\"{{computer_name}}\"",
                                                    @"first_boot":@NO,
                                                    @"imagr_only":@YES
                    };
                    [imagrComponents addObject:setComputerName];
                }


            }
            [imagrComponents addObject:@{@"type":@"package_folder",
                                         @"url":[TCSUtility variableStringFromURL:currWorkflowPrePackageURL],
                                         @"first_boot":@NO}];


            if ( workflow.shouldImage==YES) {

                NSMutableDictionary *installImageDictionary;

                if (workflow.shouldUseAsrDmgURL==NO){
                    if (workflow.macOSImagePath) {
                        installImageDictionary=[@{@"type":@"image",
                                                  @"ramdisk":@NO,
                                                  @"url":
                                                      [TCSUtility variableStringFromURL:[OSURL URLByAppendingPathComponent:[workflow.macOSImagePath lastPathComponent]]]} mutableCopy];
                    }
                }
                else if (workflow.asrImageURL) {
                    installImageDictionary=[@{@"type":@"image",
                                              @"ramdisk":@YES,
                                              @"url":
                                                  workflow.asrImageURL} mutableCopy];


                }



                [imagrComponents addObject:[NSDictionary dictionaryWithDictionary:installImageDictionary]];

                if (workflow.shouldSkipSetupAssistant==YES){

                    [imagrComponents addObject:@{@"type":@"script",
                                                 @"content":@"#!/bin/bash\n/usr/bin/touch \"{{target_volume}}/private/var/db/.AppleSetupDone\"\nexit 0\n",
                                                 @"first_boot":@NO
                    }];

                }

            }
            else if (workflow.shouldInstall==YES ) {
                NSMutableDictionary *installOSDictionary;


                if (workflow.shouldUseMacOSInstallerURL==NO && workflow.macOSFolderPath) {
                    installOSDictionary=[@{@"type":@"startosinstall",
                                           @"ramdisk":@NO,
                                           @"url":
                                               [TCSUtility variableStringFromURL:[OSURL URLByAppendingPathComponent:[workflow.macOSFolderPath lastPathComponent]]]} mutableCopy];
                    if (workflow.shouldSkipSetupAssistant==YES){ installOSDictionary[@"skip_setup_assistant"]=@YES; }




                    [installOSDictionary setObject:@(workflow.rebootAfterFirstBootResources) forKey:@"first_boot_reboot"];
                    [installOSDictionary setObject:@(workflow.waitForMacBuddyExitBeforeRebooting) forKey:@"wait_for_mac_buddy_exit"];




                }
                else if (workflow.macOSInstallerDMGURL) {
                    installOSDictionary=[@{@"type":@"startosinstall",
                                           @"ramdisk":@NO,
                                           @"url":
                                               workflow.macOSInstallerDMGURL} mutableCopy];

                    if (workflow.shouldSkipSetupAssistant==YES){ installOSDictionary[@"skip_setup_assistant"]=@YES; }

                }
                if (workflow.shouldUseScriptToDiscoverCredentials==YES){

                    if (workflow.discoverCredentialsScriptPath && workflow.discoverCredentialsScriptPath.length>0){



                        [installOSDictionary setObject:[TCSUtility variableStringFromURL:currWorkflowDiscoverCredsScriptURL] forKey:@"credentials_script_url"];

                    }
                }
                else if (workflow.loginPromptForPassword==YES){
                    if (workflow.loginInstallUsername) [installOSDictionary setObject:workflow.loginInstallUsername forKey:@"username"];

                    [installOSDictionary setObject:@(YES) forKey:@"prompt_os_installer_credentials"];
                }

                else if (workflow.loginInstallUsername && workflow.loginInstallUsername.length>0 && workflow.loginInstallPassword && workflow.loginInstallPassword.length>0){
                    if (workflow.loginInstallUsername) [installOSDictionary setObject:workflow.loginInstallUsername forKey:@"username"];
                    if (workflow.loginInstallPassword)  [installOSDictionary setObject:workflow.loginInstallPassword forKey:@"password"];

                }
                if (workflow.shouldSetVolumeName==YES && workflow.volumeName && workflow.volumeName.length>0) {
                    [installOSDictionary setObject:workflow.volumeName forKey:@"volume_name"];

                }
                if (workflow.shouldEraseAndInstall==YES) {
                    [installOSDictionary setObject:@(YES) forKey:@"erase"];

                }
                NSMutableArray *packageURLs=[NSMutableArray array];

                [packageURLs addObject:[TCSUtility variableStringFromURL:currWorkflowPackageURL]];
                installOSDictionary[@"additional_package_urls"]=[NSArray arrayWithArray:packageURLs];


                [imagrComponents addObject:[NSDictionary dictionaryWithDictionary:installOSDictionary]];

            }
            else if (workflow.shouldSkipMacOSInstall==YES ) {
                if (workflow.shouldSkipSetupAssistant==YES){

                    [imagrComponents addObject:@{@"type":@"script",
                                                 @"content":@"#!/bin/bash\n/usr/bin/touch \"{{target_volume}}/private/var/db/.AppleSetupDone\"\nexit 0\n",
                                                 @"first_boot":@NO
                    }];
                }

            }


            NSMutableArray *packagesPaths=[NSMutableArray arrayWithCapacity:filePathsOfPackages.count];

            if (workflow.usePackagesFolderPath==YES){


                [filePathsOfPackages enumerateObjectsUsingBlock:^(NSString *packageName, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString *pathExt=packageName.lowercaseString.pathExtension;
                    if ([pathExt isEqualToString:@"app"]){
                        NSString *appPackagePath=[[packageName stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"];

                        [packagesPaths addObject:[TCSUtility variableStringFromURL:[currWorkflowPackageURL URLByAppendingPathComponent:appPackagePath] ]];

                    }
                    else if ([pathExt isEqualToString:@"mpkg"]){
                        NSString *appPackagePath=[[packageName stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"];

                        [packagesPaths addObject:[TCSUtility variableStringFromURL:[currWorkflowPackageURL URLByAppendingPathComponent:appPackagePath] ]];

                    }

                    else {
                        [packagesPaths addObject:[TCSUtility variableStringFromURL:[currWorkflowPackageURL URLByAppendingPathComponent:packageName]]];
                    }
                }];

            }




            if (workflow.shouldInstall==NO ) {



                [imagrComponents addObject:@{@"type":@"package_folder",
                                             @"url":[TCSUtility variableStringFromURL:currWorkflowPackageURL],
                                             @"first_boot":@YES}];

            }
            if(workflow.shouldInstall==NO && workflow.shouldRebootAfterInstallingResources==YES){
                [imagrComponents addObject:@{@"type":@"restart_action",
                                             @"action":workflow.restartOrShutdownIndex==0?@"restart":@"shutdown"}];
            }

            if (workflow.shouldSetTargetAsStartupDisk) {
                imagrWorkflow[@"bless_target"]=[NSNumber numberWithBool:workflow.shouldSetTargetAsStartupDisk];
            }

            if (workflow.rebootAfterFirstBootResources) {
                imagrWorkflow[@"first_boot_reboot"]=[NSNumber numberWithBool:workflow.rebootAfterFirstBootResources];
            }

            if (workflow.shouldSkipRosetta){
                imagrWorkflow[@"workflow_should_skip_rosetta"]=[NSNumber numberWithBool:workflow.shouldSkipRosetta];

            }
            if (workflow.shouldConfigureWifi) {
                imagrWorkflow[@"workflow_should_configure_wifi"]=[NSNumber numberWithBool:workflow.shouldConfigureWifi];
                
                imagrWorkflow[@"workflow_wifi_ssid"]=workflow.wifiSSID;
                imagrWorkflow[@"workflow_wifi_password"]=workflow.wifiPassword;
                
            }
            if (imagrComponents && imagrComponents.count>0) [imagrWorkflow setObject:imagrComponents forKey:@"components"];


        } //workflow is active

        //have to having something in components to add workflow to workflows.
        if (imagrWorkflow && [[imagrWorkflow objectForKey:@"components"] count]>0) [imagrWorkflows addObject:imagrWorkflow];


        //    }

    }];

    [imagrWorkflows sortUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary * obj2) {
        return [obj1[@"name"] caseInsensitiveCompare:obj2[@"name"]];
    }];
    if (imagrWorkflows) [imagrDict setObject:imagrWorkflows forKey:@"workflows"];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *selectedAutorunWorkflow=[ud objectForKey:@"autorunWorkflowName"];

    if ([ud boolForKey:HASMDSSUPPORT]==YES){
        [imagrDict setObject:@(YES) forKey:@"has_mds_support"];


    }
    if ([ud boolForKey:@"shouldShowInstallMacOSButton"]==YES &&
        [[ud valueForKey:@"launchMacOSInstallerPath"] length]>0 ) {

        NSString *installerPath=[ud valueForKey:@"launchMacOSInstallerPath"];
        [imagrDict setObject:installerPath.lastPathComponent forKey:@"launch_macos_installer_name"];

    }

    if ([ud boolForKey:@"shouldAutomaticallyRunWorkflow"]==YES && selectedAutorunWorkflow) {

        NSInteger selectedAutorunTimeout=[ud integerForKey:@"autorunWorkflowTimeout"];


        [imagrDict setObject:selectedAutorunWorkflow forKey:@"autorun"];
        [imagrDict setObject:@(selectedAutorunTimeout) forKey:@"autorun_time"];


        if ([ud boolForKey:@"shouldSelectTargetVolumeWithName"]==YES && [ud objectForKey:@"autoTargetVolumeName"] && [[ud objectForKey:@"autoTargetVolumeName"] length]>0 ) {

            [imagrDict setObject:[ud objectForKey:@"autoTargetVolumeName"] forKey:@"target_volume_name"];
        }
    }
    //    [imagrDict setObject:@([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldWaitForNetwork"]) forKey:@"wait_for_network"];
    // forced this to NO because we handle network detection in run script and in packages during first boot scripts. This key is only for the imagr settings but is passed to the plist for first run scripts as well.
    [imagrDict setObject:@(NO) forKey:@"wait_for_network"];

    return imagrDict;

}

-(NSArray *)packagesPathsInFolder:(NSString *)folderPath ignoreItems:(NSArray *)ignoreItems orderingArray:(NSArray *)orderingArray{
    NSError *err;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSMutableArray *arrayOfPaths=[NSMutableArray array];
    NSArray *directoryContents=[fm contentsOfDirectoryAtPath:folderPath error:&err];
    NSMutableArray *itemsToProcess=[NSMutableArray array];
    NSMutableArray *leftOverItems=[NSMutableArray arrayWithArray:directoryContents];
    if (directoryContents && directoryContents.count>0) {

        [orderingArray enumerateObjectsUsingBlock:^(NSString *name, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([directoryContents containsObject:name]) {
                [itemsToProcess addObject:name];
                [leftOverItems removeObject:name];


            }
        }];
        [itemsToProcess addObjectsFromArray:leftOverItems];
        [itemsToProcess enumerateObjectsUsingBlock:^(NSString *packagePath, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([ignoreItems containsObject:packagePath.lastPathComponent]){
                return;
            }
            NSString *pathExt=packagePath.pathExtension.lowercaseString;
            if ([pathExt isEqualToString:@"pkg"]||
                [pathExt isEqualToString:@"mpkg"]||
                [pathExt isEqualToString:@"app"]||
                [pathExt isEqualToString:@"sparsebundle"]||
                [pathExt isEqualToString:@"dmg"]) {

                [arrayOfPaths addObject:packagePath];
            }
            else {
                NSLog(@"non-package item found: %@. Ignoring.",packagePath);
            }
        }];
    }
    else {
        NSLog(@"package path does not exist: %@",err);
        return nil;
    }
    return [NSArray arrayWithArray:arrayOfPaths];
}
@end
