//
//  TCSMDSVariable.h
//  MDS
//
//  Created by Timothy Perfitt on 1/21/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSMDSVariable.h"
#define MDSVARIABLETEXT 0
#define MDSVARIABLELIST 1

NS_ASSUME_NONNULL_BEGIN

@interface TCSMDSVariable : NSObject <NSSecureCoding>
@property NSMutableArray <NSDictionary *> *variableOptionsArray;
@property NSString *mdsVarPrompt;
@property NSString *mdsVariableName;
@property NSInteger variableTypeIndex;
@property BOOL variableOtherOption;

@end

NS_ASSUME_NONNULL_END
