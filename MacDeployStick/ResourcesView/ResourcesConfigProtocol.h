//
//  MunkiConfigProtocol.h
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#ifndef ResourcesConfigProtocol_h
#define ResourcesConfigProtocol_h
#import "TCSWorkflow.h"
@protocol ResourcesConfigProtocol <NSObject>

-(NSString *)profileFolderLabel;
-(TCSWorkflow *)workflow;
- (IBAction)selectPackageFolder:(id)sender;
- (IBAction)selectScriptsFolder:(id)sender;
- (IBAction)selectProfilesFolder:(id)sender;
@end


#endif /* MunkiConfigProtocol_h */
