//
//  DepProfileDataSource.h
//  MDS
//
//  Created by Timothy Perfitt on 9/7/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DepProfileDataSource : NSObject

@end

NS_ASSUME_NONNULL_END
