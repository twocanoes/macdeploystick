//
//  TCSASCIITextFormatter.m
//  MDS
//
//  Created by Timothy Perfitt on 9/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSASCIITextFormatter.h"

@implementation TCSASCIITextFormatter
- (NSString *)stringForObjectValue:(id)obj
{
    if (![obj isKindOfClass:[NSString class]]){
        return nil;
    }
    return [obj copy];
}
- (BOOL)getObjectValue:(out __autoreleasing id *)obj forString:(NSString *)string errorDescription:(out NSString *__autoreleasing *)error
{
    *obj = [string copy];
    return YES;
}

- (BOOL)isPartialStringValid:(NSString *)partialString
            newEditingString:(NSString **)newString
            errorDescription:(NSString **)error{



    NSMutableString *asciiCharacters = [NSMutableString string];
    for (int i = 32; i < 127; i++)  {
        [asciiCharacters appendFormat:@"%c", i];
    }

    NSCharacterSet *nonAsciiCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:asciiCharacters] invertedSet];

    NSString *modified = [[partialString componentsSeparatedByCharactersInSet:nonAsciiCharacterSet] componentsJoinedByString:@""];

    if ([modified isEqualToString:partialString]==NO){
   
        *newString=modified;

        return NO;
    }
    return YES;
}
//- (NSString *)stringForObjectValue:(id)anObject
//{
//    if (![anObject isKindOfClass:[NSNumber class]]) {
//        return nil;
//    }
//    return [NSString stringWithFormat:@"$%.2f", [anObject floatValue]];
//}

@end
