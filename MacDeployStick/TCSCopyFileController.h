//
//  TCSCopyFileController.h
//  Winclone
//
//  Created by Tim Perfitt on 5/5/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol TCSFileCopyDelegateProtocol <NSObject>

-(void)percentCompleted:(float)percentCompleted;
-(void)copyCompleted:(id)sender withError:(NSError *)error;

@end

@interface TCSCopyFileController : NSObject  <TCSFileCopyDelegateProtocol> {
    
}
@property (strong) NSString *sourceFilePath;
//@property (strong)
@property (assign)  id<TCSFileCopyDelegateProtocol> delegate;
- (void)copyFile:(NSString *)sourceFilePath toPath:(NSString *)destinationFilePath delegate:(id)inDelegate;
-(int)cancelCopyOperation;
- (void)copyFile:(NSString *)sourceFilePath toPath:(NSString *)destinationFilePath updateBlock:(void (^)(NSInteger percentComplete, NSString * statusMessage))update completionBlock:(void (^)( NSError *err))completion;
@end
