//
//  DepProfileDataSource.m
//  MDS
//
//  Created by Timothy Perfitt on 9/7/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "DepProfileDataSource.h"
#import <AppKit/AppKit.h>
@interface DepProfileDataSource()
@property (strong) NSArray *tableItems;
@property (assign) NSInteger selectedRow;


@end
@implementation DepProfileDataSource
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableItems=@[
                          @{@"name":@"General",@"image":@"General_sidebar"},
                          @{@"name":@"Setup Assistant",@"image":@"MDM_SetupAssistant_sidebar"},
                          @{@"name":@"Resources",@"image":@"WF_Resources_sidebar"},
                          @{@"name":@"User Accounts",@"image":@"WF_UserAccount_sidebar"},
                          @{@"name":@"Munki",@"image":@"Munki_sidebar"},
                          @{@"name":@"Options",@"image":@"WF_Options_sidebar"},

                          ];


    }
    return self;
}



- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{


    return self.tableItems.count;
}
-(void)tableViewSelectionDidChange:(NSNotification *)inNot{

    NSTableView *tableView=[inNot object];
    self.selectedRow=tableView.selectedRow;
}
- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {

    // Retrieve to get the @"MyView" from the pool or,
    // if no version is available in the pool, load the Interface Builder version
    NSTableCellView *result = [tableView makeViewWithIdentifier:@"DeviceInfoView" owner:self];

    // Set the stringValue of the cell's text field to the nameArray value at row
    NSDictionary *currentObject=[self.tableItems objectAtIndex:row];
    result.textField.stringValue =currentObject[@"name"];
    NSImage *rowImage=[NSImage imageNamed:currentObject[@"image"]];

    [rowImage setTemplate:YES];
    [result.imageView setImage:rowImage];

    // Return the result
    return result;
}
@end
