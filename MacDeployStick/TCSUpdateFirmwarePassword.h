//
//  TCSUpdateFirmwarePassword.h
//  MDS
//
//  Created by Timothy Perfitt on 9/4/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TCSUpdateFirmwarePasswordProtocol <NSObject>

- (void)updatePassword:(nonnull NSString *)newPassword priorPassword:(NSString * _Nullable)priorPassword forDevices:(nonnull NSArray *)inDevices ;

@end

@interface TCSUpdateFirmwarePassword : NSViewController
@property (strong) NSString *updatedPassword;
@property (strong) NSString *priorPassword;
@property (strong) NSArray *devices;
@property (assign) id <TCSUpdateFirmwarePasswordProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
