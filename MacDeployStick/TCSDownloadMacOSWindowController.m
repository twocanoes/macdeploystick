//
//  TCSDownloadMacOSWindowController.m
//  MDS
//
//  Created by Timothy Perfitt on 10/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDownloadMacOSWindowController.h"

@interface TCSDownloadMacOSWindowController ()

@end

@implementation TCSDownloadMacOSWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (BOOL)windowShouldClose:(id)sender {
    [self.window orderOut:self];
    return NO;
}
@end
