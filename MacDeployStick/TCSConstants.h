//
//  Includes.h
//  Certificate Request
//
//  Created by Tim Perfitt on 3/4/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#ifndef Includes_h
#define Includes_h


#endif /* Includes_h */

#define TCSAPACHECONFIG @"/etc/apache2/other/com.twocanoes.mds.http.conf"
#define TCSPROCESSNAMEAPACHE @"httpd"
#define TCSPROCESSNAMEMICROMDM @"micromdm"
#define TCSERRORDOMAIN @"TCS"
#define TCSERRORMESSAGEKEY @"TCSErrorMessage"
#define TCSERRORINFORMCODE -3
#define TCSERRORWARNINGCODE -2
#define TCSERRORCRITICALCODE -1
#define TCSERRORNOERROR 0
#define TCSDEVICEKEYCHAIN 0
#define TCSDEVICEYUBIKEY 1
#define TCSSMARTCARDCHANGED @"TCSSmartCardChanged"
#define TCSMDMHTTPDEBUG @"TCSMDMHttpDebug"
#define TCSSMARTCARDTOKENCHANGED @"TCSSmartCardTokenChanged"
#define TCSNOTIFICATIONUPDATEAVAILABLE @"TCSUpdateAvailable"
#define MDMLOGFILE [@"/Library/Logs/mds-micromdm.log" stringByExpandingTildeInPath]
#define SUPPORTURLS [[[NSBundle mainBundle] infoDictionary] objectForKey:@"Support URLs"]
#define TCSSHUTDOWNOPTIONREBOOT 0
#define TCSSHUTDOWNOPTIONSHUTDOWN 1
#define TCSSHUTDOWNOPTIONNONE 2

#define SERVERHOSTNAME @"serverHostname"
#define HASMDSSUPPORT @"hasMDSSupport"

#define ANCHORCERTIFICATES @"anchorCertificates"
#define CERTIFICATEPATH @"certificatePath"
#define INDENTITYKEYPATH @"identityKeyPath"
#define SSLINFOCHANGED @"sslInfoChanged"
//#define APNSCERTIFICATEPATH @"apnsCertificatePath"
//#define APNSIDENTITYKEYPATH @"apnsKeyPath"
#define MDMSERVERPORT @"mdmServerPort"
#define MDMSERVERRAPIKEY @"mdmServerAPIKey"
#define MDMSERVERREPOPATH @"mdmServerRepoPath"
#define MDMSERVERFILEREPOPATH @"mdmServerFileRepoPath"
#define TLSCERTIFICATEFOLDER @"tlsCertificateFolder"


#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define TCSNOTIFICATIONMDMSERVICESTARTED @"TCSNotificationMDMServiceStarted"
#define TCSNOTIFICATIONMDMSERVICESTOPPED @"TCSNotificationMDMServiceStopped"
#define TCSNOTIFICATIONSIDEBARRELOAD @"TCSNotificationSidebarReload"
#define TCSNOTIFICATIONSERVICEUPDATEBUSY @"TCSNotificationServiceUpdateBusy"
#define TCSAUTHRIGHT "admin"
