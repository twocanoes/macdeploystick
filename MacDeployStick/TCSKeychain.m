//
//  TCSBleuStationPassword.m
//  Bleu Configurator
//
//  Created by Steve Brokaw on 7/21/14.
//  Copyright (c) 2014 Twocanoes. All rights reserved.
//

#import "TCSKeychain.h"

#define KEYCHAIN_ACCESS_GROUP @"com.twocanoes.mds.apns"
//#define KEYCHAIN_SERVICE @"BleuStation"

NSString * const TCSBleuStationKeychainService = @"com.twocanoes.mds.apns";

@implementation TCSKeychain

+ (NSString *)randomPasswordLength:(NSUInteger)length
{
    NSMutableString *password = [NSMutableString stringWithString:@""];
    for (int i = 0; i < length; i++) {
        [password appendString:[NSString stringWithFormat:@"%c", arc4random_uniform(94) + '!']];
    }
    return [NSString stringWithString:password];
}
+ (NSString *)randomPassword
{
    return [[self class] randomPasswordLength:15];
}

+ (NSString *)passwordForService:(NSString *)service account:(NSString *)account error:(NSError **)err{

    NSDictionary *itemQuery = @{(__bridge id)kSecClass: (__bridge id)kSecClassGenericPassword,
                                (__bridge id)kSecAttrAccount: account,
                                (__bridge id)kSecAttrService: service,
                                (__bridge id)kSecAttrAccessGroup: KEYCHAIN_ACCESS_GROUP,
                                (__bridge id)kSecMatchLimit: (__bridge id)kSecMatchLimitOne,
                                (__bridge id)kSecReturnAttributes: @YES,
                                (__bridge id)kSecReturnData: @YES};

    CFTypeRef result = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)itemQuery, &result);
    if (status == noErr) {
        CFDataRef data = CFDictionaryGetValue(result, kSecValueData);
        NSString *password = [[NSString alloc] initWithData:(__bridge NSData *)data encoding:NSASCIIStringEncoding];

        return password;
    }
    //nil without error means not found
    if (status==errSecItemNotFound){
        return nil;
    }
    *err=[NSError errorWithDomain:@"TCS" code:-128 userInfo:nil];
    return nil;
}

+ (NSString *)passwordForAccount:(NSString *)account error:(NSError **)err
{

    return [[self class] passwordForService:TCSBleuStationKeychainService account:account error:err];
}

//+ (NSString *)passwordForUUID:(NSString *)uuid major:(NSNumber *)major minor:(NSNumber *)minor
//{
//    NSString *account = [NSString stringWithFormat:@"%@:%@:%@", uuid, major, minor];
//    return [[self class] passwordForAccount:account];
//}

+ (BOOL)setPassword:(NSString *)password forService:(NSString *)service account:(NSString *)account
{
    NSDictionary *query = @{(__bridge id)kSecClass:             (__bridge id)kSecClassGenericPassword,
                            (__bridge id)kSecAttrAccount:       account,
                            (__bridge id)kSecAttrService:       service,
                            (__bridge id)kSecAttrAccessGroup:   KEYCHAIN_ACCESS_GROUP,
                            (__bridge id)kSecMatchLimit:        (__bridge id)kSecMatchLimitOne,
                            (__bridge id)kSecReturnAttributes:  @YES};

    if (password) {
        NSData *passwordData = [password dataUsingEncoding:NSASCIIStringEncoding];

        CFTypeRef result = NULL;
        OSStatus queryStatus = SecItemCopyMatching((__bridge CFDictionaryRef)query, &result);
        switch (queryStatus) {
            case noErr:{

                NSDictionary *updateQuery = @{(__bridge id)kSecClass: (__bridge id)kSecClassGenericPassword,
                                              (__bridge id)kSecAttrAccount: account,
                                              (__bridge id)kSecAttrService: service,
                                              (__bridge id)kSecAttrAccessGroup: KEYCHAIN_ACCESS_GROUP
                                              };

                NSDictionary *updateItem = @{(__bridge id)kSecValueData: passwordData};

                OSStatus updateStatus = SecItemUpdate((__bridge CFDictionaryRef)(updateQuery), (__bridge CFDictionaryRef)updateItem);
                if (updateStatus != noErr) {
                    NSLog(@"Update Error: %i", (int)updateStatus);
                     return NO;
                }

                break;
            }
            case errSecItemNotFound: {
                NSLog(@"Not found %@", query);
                NSDictionary *newItem = @{(__bridge id)kSecClass: (__bridge id)kSecClassGenericPassword,
                                          (__bridge id)kSecAttrAccount: account,
                                          (__bridge id)kSecAttrService: service,
                                          (__bridge id)kSecAttrAccessGroup: KEYCHAIN_ACCESS_GROUP,
                                          (__bridge id)kSecValueData: passwordData} ;

                CFTypeRef result = NULL;
                OSStatus addStatus = SecItemAdd((__bridge CFDictionaryRef)newItem, &result);
                if (addStatus == noErr) {
                    NSLog(@"%@", result);
                }
                else {
                    return NO;
                }
                break;
            }
            default: {
                NSLog(@"unknown result");
                return NO;
                break;
            }
        }
    } else {
        NSDictionary *deleteQuery =  @{(__bridge id)kSecClass:             (__bridge id)kSecClassGenericPassword,
                                       (__bridge id)kSecAttrAccount:       account,
                                       (__bridge id)kSecAttrService:       service,
                                       (__bridge id)kSecAttrAccessGroup:   KEYCHAIN_ACCESS_GROUP};

        OSStatus deleteStatus = SecItemDelete((__bridge CFDictionaryRef)deleteQuery);
        if (deleteStatus != noErr) {
            NSLog(@"Eror removing keychain %@: %i", account, (int)deleteStatus);
            return NO;
        }
    }
    return YES;
}

+ (BOOL)setPassword:(NSString *)password forAccount:(NSString *)account
{
    return [[self class] setPassword:password forService:TCSBleuStationKeychainService account:account];
}

+ (void)setPassword:(NSString *)password forUUID:(NSString *)uuid major:(NSNumber *)major minor:(NSNumber *)minor
{
    NSString *account = [NSString stringWithFormat:@"%@:%@:%@", uuid, major, minor];
    [[self class] setPassword:password forAccount:account];
}

+ (void)deletePasswordForService:(NSString *)service account:(NSString *)account
{
    [[self class] setPassword:nil forService:service account:account];
}
+ (void)deletePasswordForAccount:(NSString *)account
{
    [[self class] setPassword:nil forAccount:account];
}

+ (void)deletePasswordForUUID:(NSString *)uuid major:(NSNumber *)major minor:(NSNumber *)minor
{
    NSString *account = [NSString stringWithFormat:@"%@:%@:%@", uuid, major, minor];
    [[self class] deletePasswordForAccount:account];
}

@end

@interface TCSPassword ()
@property (nonatomic, strong) NSMutableDictionary *keychainInfo;
@end
@implementation TCSPassword

- (instancetype)initWithService:(NSString *)service account:(NSString *)account group:(NSString *)group
{
    self = [super init];
    if (self) {
        _keychainInfo = [NSMutableDictionary dictionary];
        if (service) {
            _keychainInfo[(__bridge NSString *)kSecAttrService] = service;
        }
        if (account) {
            _keychainInfo[(__bridge NSString *)kSecAttrAccount] = account;
        }
        if (group) {
            _keychainInfo[(__bridge NSString *)kSecAttrAccessGroup] = group;
        }
    }
    return self;
}

- (void)setPassword:(NSString *)password
{
    NSData *data = [password dataUsingEncoding:NSUTF8StringEncoding];
    self.keychainInfo[(__bridge NSString *)kSecValueData] = data;
}

- (NSString *)password
{
    NSData *data = self.keychainInfo[(__bridge NSString *)kSecValueData];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (void)query
{

}
@end
