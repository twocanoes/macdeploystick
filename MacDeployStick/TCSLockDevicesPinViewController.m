//
//  TCSLockDevicesPinViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 2/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSLockDevicesPinViewController.h"

@interface TCSLockDevicesPinViewController ()
@property (strong) NSString *pin;
@property (strong) NSString *message;
@property (strong) NSString *phoneNumber;

@end

@implementation TCSLockDevicesPinViewController
- (IBAction)lockDeviceButtonPressed:(id)sender {

    if (self.pin.length!=6){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Length Error";
        alert.informativeText=@"The PIN must be exactly 6 digits";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        return;
    }

    NSMutableArray <NSString *> *names=[NSMutableArray array];
    [self.devices enumerateObjectsUsingBlock:^(DeviceMO * _Nonnull device, NSUInteger idx, BOOL * _Nonnull stop) {
        [names addObject:device.hostName==nil?device.udid:device.hostName];
    }];
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Lock Confirmation";
    alert.informativeText=[NSString stringWithFormat:@"Are you sure you want to lock the %li selected devices (%@)?",self.devices.count,[names componentsJoinedByString:@","]];

    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    [self.delegate lockDevices:self.devices pin:self.pin message:self.message phoneNumber:self.phoneNumber];
    [self dismissController:self];
}
- (void)controlTextDidChange:(NSNotification *)aNotification {
    NSTextField *textfield = [aNotification object];
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];

    char *stringResult = malloc([textfield.stringValue length]);
    int cpt=0;
    for (int i = 0; i < [textfield.stringValue length]; i++) {
        unichar c = [textfield.stringValue characterAtIndex:i];

        if ([charSet characterIsMember:c] && i<63) {
            stringResult[cpt]=c;
            cpt++;
        }
    }
    stringResult[cpt]='\0';
    NSString *newString=[NSString stringWithUTF8String:stringResult];;

    if (newString.length>6) newString=[newString substringToIndex:6];
    textfield.stringValue = newString;
    free(stringResult);

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

@end
