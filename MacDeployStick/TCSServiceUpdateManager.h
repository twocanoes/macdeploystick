//
//  TCSServiceUpdateManager.h
//  MDS
//
//  Created by Timothy Perfitt on 3/11/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSServiceUpdateManager : NSObject
@property (strong) NSMutableDictionary *updatesAvailable;
+ (instancetype)updateManager ;
@end

NS_ASSUME_NONNULL_END
