//
//  TCSSyncDetailsViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSSyncDetailsViewController.h"

@interface TCSSyncDetailsViewController ()

@end

@implementation TCSSyncDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)syncDetailsOkButtonPressed:(id)sender {
    [self.presentingViewController dismissViewController:self];
    
}
- (IBAction)syncDetailsCancelButtonPressed:(id)sender {
    [self.representedObject removeAllObjects];
    [self.presentingViewController dismissViewController:self];
}

@end
