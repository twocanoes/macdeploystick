//
//  TCSResourceViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TCSResourceOrderDelegate <NSObject,NSTableViewDelegate>
- (void)updateFirstRunArray:(nonnull NSArray *)inFirstRunArray orderArray:(NSArray *)inOrderArray disabledArray:(NSArray *)disabledArray resourcesArray:(NSArray *)resources identifier:(nonnull NSString *)identifier;
@end

@interface TCSResourceViewController : NSViewController
@property (weak) id <TCSResourceOrderDelegate> delegate;

-(void)populateArray:(NSArray *)inArray firstRun:(NSArray *)inFirstRun prePackage:(NSArray *)inPrepackage identifier:(nonnull NSString *)identifier preLabel:(NSString *)preLabel postLabel:(NSString *)postLabel disabledItems:(NSArray *)disabledItems;
-(NSArray *)firstRunArray;
@end

NS_ASSUME_NONNULL_END
