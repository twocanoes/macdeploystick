//
//  TCSSyncDetailsViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSSyncDetailsViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
