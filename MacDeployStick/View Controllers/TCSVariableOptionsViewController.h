//
//  TCSVariableOptionsViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 1/21/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMDSVariable.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TCSVariableOptionsProtocol <NSObject>

-(void)variableUpdated:(TCSMDSVariable *)inVariable;


@end

@interface TCSVariableOptionsViewController : NSViewController
@property TCSMDSVariable *mdsvariable;
@property (assign) id delegate;

@end

NS_ASSUME_NONNULL_END
