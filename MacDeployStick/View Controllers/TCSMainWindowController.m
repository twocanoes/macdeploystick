//
//  TCSMainWindowController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/30/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMainWindowController.h"
#import "AppDelegate.h"
@interface TCSMainWindowController () <NSWindowDelegate>

@end

@implementation TCSMainWindowController
- (IBAction)automaton2ButtonPressed:(id)sender {
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Automaton.app"]){
        [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/Automaton.app"];

    }
    else {
        NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"download-automaton-app"]];
        if (url) [[NSWorkspace sharedWorkspace] openURL:url];
    }

}
- (IBAction)dfuBlasterButtonPressed:(id)sender {

    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/DFU Blaster Pro.app"]){
        [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/DFU Blaster Pro.app"];

    }
    else {
        NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"download-dfu-blaster"]];
        if (url) [[NSWorkspace sharedWorkspace] openURL:url];
    }

}
- (IBAction)mistButtonPressed:(id)sender {
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Mist.app"]){
        [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/Mist.app"];

    }
    else {
        NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mist"]];
        if (url) [[NSWorkspace sharedWorkspace] openURL:url];

    }
}

- (void)windowDidLoad {
    [super windowDidLoad];

    AppDelegate *delegate=(AppDelegate *)[NSApp delegate];
    delegate.window=self.window;
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}



@end
