//
//  TCSWebServerSettingViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWebserverSetting.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TCSWebServiceConfigurationDelegateProtocol <NSObject>

-(BOOL)updateConfiguration:(TCSWebserverSetting *)newSettings;
@end

@interface TCSWebServerSettingViewController : NSViewController
@property (weak) id <TCSWebServiceConfigurationDelegateProtocol> delegate;
@property (assign) NSInteger newWebserverPort;
@end

NS_ASSUME_NONNULL_END
