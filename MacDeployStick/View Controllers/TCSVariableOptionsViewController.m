//
//  TCSVariableOptionsViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 1/21/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSVariableOptionsViewController.h"
@interface TCSVariableOptionsViewController ()
@property (strong) IBOutlet NSArrayController *optionsArrayController;

@end

@implementation TCSVariableOptionsViewController 
- (IBAction)okButtonPressed:(id)sender {
    [self.delegate variableUpdated:self.mdsvariable];
    [self dismissController:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}



@end
