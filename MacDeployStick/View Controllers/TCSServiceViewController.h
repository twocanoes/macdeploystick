//
//  TCSServiceViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <SecurityInterface/SFAuthorizationView.h>
#import "TCSMainWindowRightViewController.h"

typedef NS_ENUM(NSUInteger, BadgeType) {
    BadgeTypeNone,
    BadgeTypeNumber,
    BadgeTypeSymbol
};

typedef NS_ENUM(NSUInteger, StatusType) {
    StatusTypeNone,
    StatusTypeOK,
    StatusTypeWarning,
    StatusTypeError
};

typedef NS_ENUM(NSUInteger, StatusMode) {
    StatusModeStopped,
    StatusModeRunning


};
NS_ASSUME_NONNULL_BEGIN
@protocol TCSServiceViewProtocol <NSObject>

-(void)serviceUpdateAvailable:(NSNotification *)not;
-(void)stopService:(id)sender;
-(void)startService:(id)sender;
-(void)updateServiceStatus:(id)sender;

@property (strong) NSString *errorMessage;

@property (assign) StatusType statusType;
@property (assign) BadgeType badgeType;
@property (assign) NSInteger badgeCount;
@property (assign) BOOL hasError;
@property (assign) BOOL isInstalled;
@property (assign) BOOL isUpdateAvailable;
@property (strong) NSString *logFilePath;
@property (assign) StatusMode statusMode;
@property (assign) NSControlStateValue serviceButtonState;
@property (strong) NSString *serviceStatus;

@optional
-(IBAction)showLogButtonPressed:(id)sender;

@end

@interface TCSServiceViewController : TCSMainWindowRightViewController
@property (assign) BOOL isRunning;
@property (weak) IBOutlet SFAuthorizationView *authenticationView;
@property (assign) BOOL isAuthorized;
-(IBAction)serviceStateChanged:(nonnull NSButton *)sender ;
-(void)updateServiceStatus:(id)sender;
-(void)updateSidebar:(id)sender;
@end

NS_ASSUME_NONNULL_END
