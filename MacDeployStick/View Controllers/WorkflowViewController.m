//
//  WorkflowViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "WorkflowViewController.h"
#import "DeploySettings.h"
#import "ViewController.h"
#import "TCSMDSVariable.h"
#import "TCSWebServiceController.h"
#import "TCWebConnection.h"
@interface WorkflowViewController () 
@property (strong) IBOutlet NSArrayController *mdsVariablesArrayController;
@property (weak) IBOutlet NSTableView *mdsVariablesTableView;
@property (assign) BOOL isMunkiReportEnabled;

@end

@implementation WorkflowViewController


- (IBAction)selectPackageFolder:(id)sender {
//
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=NO;
    openPanel.canChooseDirectories=YES;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Please select a packages folder:";
    if (self.workflow.packagesFolderPath && self.workflow.packagesFolderPath.length>0){
        openPanel.directoryURL=[NSURL fileURLWithPath:self.workflow.packagesFolderPath];

    }
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        [self updatePackagesURL:[openPanel URL]];
    }

}
- (IBAction)trustMunkiCertificateButtonPressed:(NSButton *)inButton {
    NSString *certPath=[[NSUserDefaults standardUserDefaults] objectForKey:CERTIFICATEPATH];

    if (inButton.state==NSOnState && certPath && !self.workflow.munkiClientCertificate) {

        self.workflow.munkiClientCertificate=certPath;


    }



}
- (IBAction)selectMunkiClientCertificate:(id)sender {
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Select certificate in PEM format:";
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        self.workflow.munkiClientCertificate=[openPanel URL].path;
    }
}
- (IBAction)selectMunkiToolsInstallerButtonPressed:(id)sender {
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.allowedFileTypes=@[@"pkg"];
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {

        self.workflow.munkiClientInstallerPath=[openPanel URL].path;

    }

}
-(void)viewDidAppear{

    [self setPreferredContentSize:self.view.frame.size];


}

- (IBAction)asrSourceTypeButtonSelected:(NSButton *)sender {
    switch (sender.tag) {
        case 0:
            self.workflow.shouldUseAsrDmgURL=NO;
            break;
        case 1:
            self.workflow.shouldUseAsrDmgURL=YES;
            break;

    }


}


- (IBAction)macOSInstallerSourceTypeButtonSelected:(NSButton *)sender {
    switch (sender.tag) {
        case 0:
            self.workflow.shouldUseMacOSInstallerURL=NO;
            break;
        case 1:
            self.workflow.shouldUseMacOSInstallerURL=YES;
            break;

    }


}
- (IBAction)installMacOSButtonSelected:(NSButton *)sender {

    switch (sender.tag) {
        case 0:
            self.workflow.shouldInstall=YES;
            self.workflow.shouldImage=NO;
            self.workflow.shouldSkipMacOSInstall=NO;
            break;
        case 1:
            self.workflow.shouldInstall=NO;
            self.workflow.shouldImage=YES;
            self.workflow.shouldSkipMacOSInstall=NO;

            break;
        case 2:
            self.workflow.shouldInstall=NO;
            self.workflow.shouldImage=NO;
            self.workflow.shouldSkipMacOSInstall=YES;

            break;

    }


}


-(void)updatePackagesURL:(NSURL *)inURL{
    NSError *err;
    self.workflow.packagesFolderPath=inURL.path;
    self.workflow.usePackagesFolderPath=YES;
    NSData *selectedFileURLBookmark=[inURL bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                              includingResourceValuesForKeys:nil relativeToURL:nil error:&err];

    self.workflow.packagesFolderBookmark=selectedFileURLBookmark;
    [self performSegueWithIdentifier:@"packages" sender:self];
}
- (IBAction)selectScriptsFolder:(id)sender {
    //
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=NO;
    openPanel.canChooseDirectories=YES;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Please select a scripts folder:";

    if (self.workflow.scriptFolderPath && self.workflow.scriptFolderPath.length>0){
        openPanel.directoryURL=[NSURL fileURLWithPath:self.workflow.scriptFolderPath];

    }

    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        [self updateScriptsURL:[openPanel URL]];
    }

}
- (IBAction)selectProfilesFolder:(id)sender {
    //
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=NO;
    openPanel.canChooseDirectories=YES;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Please select a profiles folder:";

    if (self.workflow.profilesFolderPath && self.workflow.profilesFolderPath.length>0){
        openPanel.directoryURL=[NSURL fileURLWithPath:self.workflow.profilesFolderPath];

    }
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        [self updateProfilesURL:[openPanel URL]];
    }

}

- (NSString *)profileFolderLabel {
    return @"Specify a folder containing configuration profiles to install on the target Mac (macOS 10.15 and earlier)";
}

-(void)updateProfilesURL:(NSURL *)inURL{
    NSError *err;
    self.workflow.profilesFolderPath=inURL.path;
    self.workflow.useProfilesFolderPath=YES;
    NSData *selectedFileURLBookmark=[inURL bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                    includingResourceValuesForKeys:nil relativeToURL:nil error:&err];

    self.workflow.profilesFolderBookmark=selectedFileURLBookmark;
    [self performSegueWithIdentifier:@"profiles" sender:self];

}
-(void)updateScriptsURL:(NSURL *)inURL{
    NSError *err;
    self.workflow.scriptFolderPath=inURL.path;
    self.workflow.useScriptFolderPath=YES;
    NSData *selectedFileURLBookmark=[inURL bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                    includingResourceValuesForKeys:nil relativeToURL:nil error:&err];

    self.workflow.scriptFolderBookmark=selectedFileURLBookmark;
    [self performSegueWithIdentifier:@"scripts" sender:self];

}

-(void)itemDropped:(NSString *)inPath{

    NSFileManager *fm=[NSFileManager defaultManager];
    BOOL isDir;
    if([fm fileExistsAtPath:inPath isDirectory:&isDir] && isDir==YES) {
        [self updatePackagesURL:[NSURL fileURLWithPath:inPath]];
    }
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.showAdvancedUserOptions=YES;
    [self.tableView setDoubleAction:@selector(editUserTableDoubleClick:)];
    [self.mdsVariablesTableView setDoubleAction:@selector(mdsVariablesTableDoubleClick:)];
    self.isMunkiReportEnabled=NO;
 


}
- (IBAction)mdsVariablesTableDoubleClick:(id)sender {
    if ([self.mdsVariablesTableView clickedRow]!=-1) {

        [self performSegueWithIdentifier:@"variableOptions" sender:self];
    }

}
- (IBAction)editUserTableDoubleClick:(id)sender {
    if ([self.tableView clickedRow]!=-1) {

        [self performSegueWithIdentifier:@"editUserDetailSegue" sender:self];
    }
}

-(void)awakeFromNib{
    if (!self.representedObject) {
        self.representedObject=[DeploySettings sharedSettings];
    }

}
- (IBAction)selectInstallerButtonPressed:(id)sender {

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.allowedFileTypes=@[@"app",@"dmg",@"sparsebundle",@"sparseimage"];
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        [self saveSelectedOSURL:[openPanel URL]];
    }

}


- (IBAction)selectImageButtonPressed:(id)sender {

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.allowedFileTypes=@[@"dmg",@"sparsebundle",@"sparseimage"];
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {


        self.workflow.macOSImagePath=openPanel.URL.path;

    }

}
-(void)userUpdated:(TCSWorkflowUser *)inUser{

    NSInteger selectionIndex=self.usersArrayController.selectionIndex;
    if (self.addUser) {

        [self.usersArrayController addObject:inUser];
        self.addUser=nil;

    }
    else if (self.editUser) {

        [self.usersArrayController  removeObjectAtArrangedObjectIndex:selectionIndex];
        [self.usersArrayController insertObject:inUser atArrangedObjectIndex:selectionIndex];

    }

}

-(void)saveSelectedOSURL:(NSURL *)inURL{


    self.workflow.macOSFolderPath=inURL.path;

}

- (IBAction)okButtonPressed:(id)sender {


    if (self.workflow.shouldSetComputerName==YES && (!self.workflow.computerNameString || self.workflow.computerNameString.length==0) && self.workflow.shouldPromptForComputerName==NO) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Empty Computer Name";
        alert.informativeText=@"Computer name is empty in workflow options and is not set to prompt when running the workflow. Please provide a computer name, select to prompt for a computer name, or deselect the Set Computer Name option";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;

    }

    [self processWorkflow:self];


}
-(void)processWorkflow:(id)sender{
    if (self.workflow.isReadonly==YES) {

        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"This workflow is read only and cannot be modified. Please duplicate and edit the copy."}]];
        [alert runModal];

        return;
    }

    if (self.workflow.shouldInstall==YES && !self.workflow.macOSFolderPath && !self.workflow.macOSInstallerDMGURL && self.workflow.macOSInstallerDMGURL.length==0 ){

        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Please select a macOS Installer or unselect the Install macOS checkbox."}]];
        [alert runModal];
        return;

    }
    if (self.workflow.usePackagesFolderPath && !self.workflow.packagesFolderPath){

        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Please select a Packages & Apps folder or unselect the Packages & Apps checkbox."}]];
        [alert runModal];
        return;

    }
    if (self.workflow.useScriptFolderPath && !self.workflow.scriptFolderPath){

        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Please select a Scripts folder or unselect the Scripts checkbox."}]];
        [alert runModal];
        return;

    }
    if (self.workflow.useScriptFolderPath && !self.workflow.scriptFolderPath &&
        self.workflow.profilesFolderPath){

        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Please select a Profiles folder or unselect the Profiles checkbox."}]];
        [alert runModal];
        return;

    }
    //The workflow name changed so we need to update the autorun name as needed.
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    if (self.priorWorkflow!=nil) {
        if ([ud boolForKey:@"shouldAutomaticallyRunWorkflow"]==YES &&
            [[ud objectForKey:@"autorunWorkflowName"] isEqualToString:self.priorWorkflow.workflowName] &&

            ![self.priorWorkflow.workflowName isEqualToString:self.workflow.workflowName]){

            [ud setObject:self.workflow.workflowName forKey:@"autorunWorkflowName"];
        }
        [self.representedObject replaceWorkflow:self.priorWorkflow withWorkflow:self.workflow];
    }
    else {
        [self.representedObject addWorkflow:self.workflow];
    }
    if ([self.presentingViewController respondsToSelector:@selector(selectWorkflow:)]) {
        [(ViewController *)self.presentingViewController selectWorkflow:self.workflow];
    }

    [[DeploySettings sharedSettings] saveToPrefs:self];

    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSIDEBARRELOAD object:self];

    [self dismissController:self];

}

- (BOOL)shouldPerformSegueWithIdentifier:(NSStoryboardSegueIdentifier)identifier sender:(id)sender{

    if ([identifier isEqualToString:@"editUserDetailSegue"] && [self.usersArrayController selectedObjects].count==0 ) return NO;

    return YES;
}
-(void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{

    if (!segue.identifier) return;

    if ([segue.identifier isEqualToString:@"editUserDetailSegue"]){

        TCSCreateUsersViewController *createUsersViewController=segue.destinationController;
        createUsersViewController.delegate=self;
        self.addUser=nil;
        self.editUser=[[[self.usersArrayController selectedObjects] lastObject] copy];
        if (!self.editUser.userUUID) { self.editUser.userUUID=[[NSUUID UUID] UUIDString]; }
        if (!self.editUser) return;;
        createUsersViewController.workflowUser=self.editUser;

    }
    else if ([segue.identifier isEqualToString:@"newUserDetailSegue"]){

        TCSCreateUsersViewController *createUsersViewController=segue.destinationController;
        createUsersViewController.delegate=self;
        self.addUser=[[TCSWorkflowUser alloc] init];
        self.editUser=nil;
        NSInteger uid=601;
        self.addUser.userUUID=[[NSUUID UUID] UUIDString];

        while ([self workflowContainsUID:uid]) uid++;
        self.addUser.uid=[NSString stringWithFormat:@"%lu",uid];
        createUsersViewController.workflowUser=self.addUser;

    }
    else if ([segue.identifier isEqualToString:@"variableOptions"]){
        TCSVariableOptionsViewController *variablesController=segue.destinationController;
        variablesController.delegate=self;
        variablesController.mdsvariable=[[self.mdsVariablesArrayController.selectedObjects objectAtIndex:0] copy];


    }


    else {  //popover for resources
        TCSResourceViewController *rvc=segue.destinationController;
        NSMutableArray *resourcesArray=[NSMutableArray array];
        NSFileManager *fm=[NSFileManager defaultManager];
        NSArray *folderContents;
        NSArray *firstRunArray;
        NSArray *preItemsArray;
        NSArray *disabledItemsArray;
        NSMutableArray *resourceFolderOrder=[NSMutableArray array];

        NSString *resourcePath;
        NSString *preLabel;
        NSString *postLabel;

        if (self.workflow.shouldInstall==YES || self.workflow.shouldEraseAndInstall==YES){
            preLabel=@"After macOS is installed, but before packages";
            postLabel=@"After macOS and packages are installed";

        }
        else{
            preLabel=@"Before packages are installed when running workflow";
            postLabel=@"After first boot packages are installed";

        }

        if ([segue.identifier isEqualToString:@"scripts"]){
            folderContents=[fm contentsOfDirectoryAtPath:self.workflow.scriptFolderPath error:nil];
            preItemsArray=self.workflow.prePackageScripts;
            resourcePath=self.workflow.scriptFolderPath;
            firstRunArray=self.workflow.workflowFirstRunScripts;
            disabledItemsArray=self.workflow.disabledScripts;
            if(self.workflow.scriptFolderOrder) [resourceFolderOrder addObjectsFromArray:self.workflow.scriptFolderOrder];

        }
        else if ([segue.identifier isEqualToString:@"profiles"]){
            folderContents=[fm contentsOfDirectoryAtPath:self.workflow.profilesFolderPath error:nil];
            preItemsArray=self.workflow.prePackageProfiles;
            resourcePath=self.workflow.profilesFolderPath;
            firstRunArray=self.workflow.firstRunProfiles;
            disabledItemsArray=self.workflow.disabledProfiles;
            if(self.workflow.profilesFolderOrder) [resourceFolderOrder addObjectsFromArray:self.workflow.profilesFolderOrder];

        }
        else if ([segue.identifier isEqualToString:@"packages"]){
            folderContents=[fm contentsOfDirectoryAtPath:self.workflow.packagesFolderPath error:nil];
            if (folderContents && folderContents.count>0){

                folderContents=[folderContents sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
            }
            preItemsArray=self.workflow.preInstallPackages;
            resourcePath=self.workflow.packagesFolderPath;

            firstRunArray=self.workflow.firstRunPackages;
            disabledItemsArray=self.workflow.disabledPackages;
            if (self.workflow.packagesFolderOrder) [resourceFolderOrder addObjectsFromArray:self.workflow.packagesFolderOrder];

            if (self.workflow.shouldInstall==YES ){
                preLabel=@"When Running Workflow";
                postLabel=@"After macOS Installation";

            }
            else{
                preLabel=@"When Running Workflow";
                postLabel=@"On First Boot";
            }
        }

        [resourceFolderOrder enumerateObjectsUsingBlock:^(NSString *orderObject, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([folderContents containsObject:orderObject]){
                [resourcesArray addObject:orderObject];
            }
        }];
        [folderContents enumerateObjectsUsingBlock:^(NSString *itemName, NSUInteger idx, BOOL * _Nonnull stop) {
            BOOL isFolder;
            if ([fm fileExistsAtPath:[resourcePath stringByAppendingPathComponent:itemName] isDirectory:&isFolder] &&
                ((isFolder==NO) || (isFolder==YES&& [itemName hasSuffix:@".pkg"])) &&
                ![itemName hasPrefix:@"."]){
                if([resourcesArray containsObject:itemName]==NO) [resourcesArray addObject:itemName];

            }
        }];
//        NSArray *sortedResourceArray=[resourcesArray sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        [rvc populateArray:resourcesArray firstRun:[segue.identifier isEqualToString:@"packages"]||[segue.identifier isEqualToString:@"scripts"]||[segue.identifier isEqualToString:@"profiles"]?firstRunArray:nil prePackage:preItemsArray identifier:segue.identifier preLabel:([segue.identifier isEqualToString:@"profiles"]==NO || self.workflow.shouldInstall==YES)?preLabel:nil postLabel:postLabel disabledItems:disabledItemsArray];

        rvc.delegate=self;
    }

}
-(BOOL)workflowContainsUID:(NSInteger )inUID{
    __block BOOL found=NO;
    [self.workflow.users enumerateObjectsUsingBlock:^(TCSWorkflowUser *currUser, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([currUser.uid integerValue]==inUID) {
            found=YES;
            *stop =YES;
        }
    }];
    return found;
}
- (void)updateFirstRunArray:(nonnull NSArray *)inFirstRunArray orderArray:(NSArray *)inOrderArray disabledArray:(NSArray *)disabledArray resourcesArray:(NSArray *)resourcesArray identifier:(nonnull NSString *)identifier {
    if ([identifier isEqualToString:@"scripts"]){
        self.workflow.prePackageScripts=inOrderArray;
        self.workflow.workflowFirstRunScripts=inFirstRunArray;
        self.workflow.disabledScripts=disabledArray;
        self.workflow.scriptFolderOrder=resourcesArray;
    }
    else if ([identifier isEqualToString:@"profiles"]){
        self.workflow.prePackageProfiles=inOrderArray;
        self.workflow.firstRunProfiles=inFirstRunArray;
        self.workflow.disabledProfiles=disabledArray;
        self.workflow.profilesFolderOrder=resourcesArray;

    }
    else if ([identifier isEqualToString:@"packages"]){
        self.workflow.preInstallPackages=inOrderArray;
        self.workflow.firstRunPackages=inFirstRunArray;
        self.workflow.disabledPackages=disabledArray;
        self.workflow.packagesFolderOrder=resourcesArray;
    }

}


- (void)variableUpdated:(nonnull TCSMDSVariable *)inVariable {

    NSInteger selectedIndex=[[self.mdsVariablesArrayController selectionIndexes] firstIndex];

    NSMutableArray *arrangedObjects=[[self.mdsVariablesArrayController arrangedObjects] mutableCopy];
    [arrangedObjects replaceObjectAtIndex:selectedIndex withObject:inVariable];
    [self.mdsVariablesArrayController  removeObjects:[self.mdsVariablesArrayController arrangedObjects]];
    [self.mdsVariablesArrayController addObjects:arrangedObjects];

    [self.mdsVariablesArrayController setSelectionIndex:selectedIndex];
}





@end
