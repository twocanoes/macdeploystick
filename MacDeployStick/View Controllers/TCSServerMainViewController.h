//
//  TCSServerMainViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/7/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TCSItemSelectionProtocol <NSObject>

-(void)selectedViewControllerWithStoryboardName:(NSString *)sbName storyboardID:(NSString *)storyboardID;
-(NSViewController *)viewControllerForStoryboardName:(nonnull NSString *)sbName storyboardID:(nonnull NSString *)storyboardID;
@end
@interface TCSServerMainViewController : NSViewController
@property (weak) id <TCSItemSelectionProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
