//
//  TCSResourceViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSResourceViewController.h"
NSString * const TCSRESOURCESINTERNALPASTEBOARDTYPE=@"com.twocanoes.resources.pbtype-array";

@interface TCSResourceViewController ()
@property (weak) IBOutlet NSTableView *resourceTableView;
@property (strong) IBOutlet NSArrayController *arrayController;
@property (strong) NSMutableArray *resourceArray;
@property (strong) NSIndexSet *internalDragSet;
@end

@implementation TCSResourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.preferredContentSize=self.view.frame.size;

    [self.resourceTableView registerForDraggedTypes:@[TCSRESOURCESINTERNALPASTEBOARDTYPE]];

}
//-(BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(nonnull NSIndexSet *)rowIndexes toPasteboard:(nonnull NSPasteboard *)pboard{
//
//}


- (IBAction)enableAllItems:(id)sender {
    [[self.arrayController arrangedObjects] enumerateObjectsUsingBlock:^(NSMutableDictionary *currItem, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([currItem objectForKey:@"enabled"]){
            [currItem setObject:@(YES) forKey:@"enabled"];
        }

    }];

}
- (IBAction)disableAllItems:(id)sender {
    [[self.arrayController arrangedObjects] enumerateObjectsUsingBlock:^(NSMutableDictionary *currItem, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([currItem objectForKey:@"enabled"]){
            [currItem setObject:@(NO) forKey:@"enabled"];
        }

    }];

}

-(void)viewWillDisappear{

    NSMutableArray *orderArray=[NSMutableArray array];
    NSMutableArray *firstRunArray=[NSMutableArray array];
    NSMutableArray *disabledArray=[NSMutableArray array];
    NSMutableArray *resourcesArray=[NSMutableArray array];

    [[self.arrayController arrangedObjects] enumerateObjectsUsingBlock:^(NSDictionary *currentDict, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([currentDict objectForKey:@"header"]) return;
        [resourcesArray addObject:[currentDict objectForKey:@"name"]];

        if ([[currentDict objectForKey:@"order"] integerValue]==0){
            [orderArray addObject:[currentDict objectForKey:@"name"]];
        }
        else if ([[currentDict objectForKey:@"order"] integerValue]==2) {
            [firstRunArray addObject:[currentDict objectForKey:@"name"]];

        }

        if ([[currentDict objectForKey:@"enabled"] integerValue]==0){
                 [disabledArray addObject:[currentDict objectForKey:@"name"]];
        }
    }];

    [self.delegate updateFirstRunArray:firstRunArray orderArray:[NSArray arrayWithArray:orderArray] disabledArray:[NSArray arrayWithArray:disabledArray] resourcesArray:[NSArray arrayWithArray:resourcesArray] identifier:self.identifier];


}

-(NSArray *)firstRunArray{
    NSMutableArray *firstRunArray=[NSMutableArray array];
    [[self.arrayController arrangedObjects] enumerateObjectsUsingBlock:^(NSDictionary *currDict, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([[currDict objectForKey:@"order"] integerValue]==0) {

            NSString *name=[currDict objectForKey:@"name"];
            [firstRunArray addObject:name];
        }
    }];


    return [NSArray arrayWithArray:firstRunArray];




}

- (BOOL)tableView:(NSTableView*)tv
       acceptDrop:(id <NSDraggingInfo>)info
          row:(int)row
    dropOperation:(NSTableViewDropOperation)op
{

    NSPasteboard * draggingPasteboard=[info draggingPasteboard];

    if ([draggingPasteboard availableTypeFromArray:@[TCSRESOURCESINTERNALPASTEBOARDTYPE]]!=nil && [info draggingSource]==self.resourceTableView)
    {

        NSDictionary *aboveItem=[[self.arrayController arrangedObjects] objectAtIndex:row-1];


        NSMutableDictionary * currObject=[[[[self.arrayController arrangedObjects] objectsAtIndexes:self.internalDragSet] firstObject] mutableCopy];
        [currObject setObject:[aboveItem objectForKey:@"order"] forKey:@"order"];
        [self.arrayController removeObjectAtArrangedObjectIndex:self.internalDragSet.firstIndex];
        if (self.internalDragSet.firstIndex<row){
            [self.arrayController insertObject:currObject atArrangedObjectIndex:row-1];

        }
        else {

            [self.arrayController insertObject:currObject atArrangedObjectIndex:row];
        }
//        [self.arrayController insertObject:currObject atArrangedObjectIndex:row];;

        return YES;
    }

    return NO;

}
- (BOOL)tableView:(NSTableView *)tv writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard {
    if ([[[self.arrayController arrangedObjects] objectAtIndex:rowIndexes.firstIndex] objectForKey:@"header"]){

        return NO;
    }

    self.internalDragSet=rowIndexes;
    [pboard declareTypes:@[TCSRESOURCESINTERNALPASTEBOARDTYPE] owner:self];

    [pboard setData:[NSData data] forType:TCSRESOURCESINTERNALPASTEBOARDTYPE];

    return YES;

}

- (NSDragOperation)tableView:(NSTableView*)tv
        validateDrop:(id <NSDraggingInfo>)info
         proposedRow:(int)row
       proposedDropOperation:(NSTableViewDropOperation)op
{
    if (op==NSTableViewDropOn)
        return NSDragOperationNone;

    NSPasteboard * currentPasteboard=[info draggingPasteboard];
    if ([currentPasteboard availableTypeFromArray:@[TCSRESOURCESINTERNALPASTEBOARDTYPE]]!=nil && [info draggingSource]==self.resourceTableView)
    {

            if (row>=self.internalDragSet.firstIndex && row<=(self.internalDragSet.lastIndex+1))
                return NSDragOperationNone;


        return NSDragOperationMove;
    }

    return NSDragOperationNone;


}

-(void)populateArray:(NSArray *)inArray firstRun:(NSArray *)group2ItemsHints prePackage:(NSArray *)group0ItemsHints identifier:(nonnull NSString *)identifier preLabel:(NSString *)group0Label postLabel:(NSString *)group1Label
      disabledItems:(NSArray *)disabledItems{

    NSString *firstRunLabel=@"When first user logs in";

    if ([identifier isEqualToString:@"scripts"]){
        firstRunLabel=@"When running workflow";
    }

    self.identifier=identifier;
    self.resourceArray=[NSMutableArray array];

    NSMutableArray *viewArray=[NSMutableArray arrayWithCapacity:inArray.count];
    NSMutableArray *group0=[NSMutableArray array];
    NSMutableArray *group1=[NSMutableArray array];
    NSMutableArray *group2=[NSMutableArray array];

    NSMutableArray *orderedArray=[inArray mutableCopy];

    NSMutableArray *combinedHints=[NSMutableArray array];
    if (group0ItemsHints && group0ItemsHints.count>0) {
        [combinedHints addObjectsFromArray:group0ItemsHints];
    }
    if (group2ItemsHints && group2ItemsHints.count>0) {
        [combinedHints addObjectsFromArray:group2ItemsHints];
    }


    [combinedHints enumerateObjectsUsingBlock:^(NSArray *currentHint, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([orderedArray containsObject:currentHint]){
                [orderedArray removeObject:currentHint];
                //move to the end so they are in order
                [orderedArray insertObject:currentHint atIndex:orderedArray.count];

            }

    }];




    [orderedArray enumerateObjectsUsingBlock:^(NSString *currGroup1ItemName, NSUInteger idx, BOOL * _Nonnull stop) {
//order is the selected object.
        NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithCapacity:2];
        [dict setObject:currGroup1ItemName forKey:@"name"];

        if ([disabledItems containsObject:currGroup1ItemName]){
            [dict setObject:@(NO) forKey:@"enabled"];
        }
        else {
            [dict setObject:@(YES) forKey:@"enabled"];

        }

        if ([group0ItemsHints containsObject:currGroup1ItemName]){
            [dict setObject:@(0) forKey:@"order"];
        }
        else if ([group2ItemsHints containsObject:currGroup1ItemName]){
            [dict setObject:@(2) forKey:@"order"];
        }
        else {
            [dict setObject:@(1) forKey:@"order"];
        }

         [dict setObject:group1Label forKey:@"postLabel"];
        if (group0Label) [dict setObject:group0Label forKey:@"preLabel"];
        else   {
            [dict setObject:@"" forKey:@"preLabel"];
        }

        if (group0ItemsHints) [dict setObject:firstRunLabel forKey:@"firstRunLabel"];
        else  [dict setObject:@"" forKey:@"firstRunLabel"];

        [viewArray addObject:dict];

        if ([group0ItemsHints containsObject:currGroup1ItemName]){
            [group0 addObject:dict];
        }
        else if ([group2ItemsHints containsObject:currGroup1ItemName]){
            [group2 addObject:dict];
        }
        else {
            [group1 addObject:dict];
        }

    }];
    if (group0Label){
        [self.arrayController addObject:@{@"header":group0Label,@"order":@(0)}];
        [self.arrayController addObjects:group0];
    }
    if (group1Label) {
        [self.arrayController addObject:@{@"header":group1Label,@"order":@(1)}];
        [self.arrayController addObjects:group1];
    }
    if (firstRunLabel) {
        [self.arrayController addObject:@{@"header":firstRunLabel,@"order":@(2)}];
        [self.arrayController addObjects:group2];
    }

}
- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{

    if ([[[self.arrayController arrangedObjects] objectAtIndex:row] objectForKey:@"header"]){

       NSTableCellView *cellView=[tableView makeViewWithIdentifier:@"headerCellView" owner:[tableView delegate]];
        cellView.textField.stringValue=[[[self.arrayController arrangedObjects] objectAtIndex:row] objectForKey:@"header"];
        return cellView;

    }
  return [tableView makeViewWithIdentifier:tableColumn.identifier owner:[tableView delegate]];


}
- (BOOL)tableView:(NSTableView *)tableView isGroupRow:(NSInteger)row{

    if ([[[self.arrayController arrangedObjects] objectAtIndex:row] objectForKey:@"header"]){
        return YES;
    }
    return NO;

}

@end
