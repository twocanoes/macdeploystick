//
//  ExportMediaViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "ExportMediaViewController.h"
#import "DeploySettings.h"
#import "TCSUtility.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSDefaultsManager.h"

@interface ExportMediaViewController ()

@property (nonatomic,strong) NSArray *listOfMedia;
@property (assign) BOOL isRunning;
@property (assign) BOOL isInderminateProgress;
@property (assign) BOOL isCancelling;
@property (assign) BOOL savingToDMG;
@property (assign) float percentComplete;
//@property (strong) TCSCopyFileController *fileCopyController;
@property (strong) NSString *tempFolderPath;
@property (strong) NSString *status;
//@property (strong) (id)notificationRef;
@property (nonatomic, strong) NSString *resourceFolderURLPrefix;
@property (strong) TCTaskWrapperWithBlocks *createDiskImageTaskWrapper;
@property (strong) TCTaskWrapperWithBlocks *convertWrapper;

@property (strong)NSURL *installMediaBookmarkResolved;

@end

@implementation ExportMediaViewController

-(void)viewWillAppear{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorMessage:) name:@"TCSCopyFailed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateStatusItem:) name:@"TCSCopyItemStatus" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copyCompleted:) name:@"TCSCopyCompleted" object:nil];


    self.listOfMedia =[TCSUtility listOfMedia];
    if ([[DeploySettings sharedSettings]
          shouldSaveToDMG]==NO) {


        [self saveResourcesToPath:[[DeploySettings sharedSettings] externalVolumeURL].path prefix:nil];
    }
    else {
        self.tempFolderPath=[[TCSDefaultsManager sharedManager] newTempFolder];
        if (!self.tempFolderPath) return ;
        self.isRunning=YES;



        self.savingToDMG=YES;
        NSString *diskImageVolumeName=[[NSUserDefaults standardUserDefaults] objectForKey:@"diskImageVolumeName"];

        [self saveResourcesToPath:self.tempFolderPath prefix:[NSURL fileURLWithPath:[@"/Volumes" stringByAppendingPathComponent:diskImageVolumeName]]];


    }
}
-(void)viewWillDisappear{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)updateStatusItem:(NSNotification *)not{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.percentComplete+=2;

        if (not.userInfo[@"message"]) self.status=not.userInfo[@"message"];
    });


}
-(void)showErrorMessage:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.isRunning=NO;
        [self dismissController:self];

        NSError *err=notification.userInfo[@"error"];
        if (err.code!=299){
            [[NSAlert alertWithError:notification.userInfo[@"error"]] runModal];
        }
    });

}
-(void)saveResourcesToPath:(NSString *)inPath prefix:(NSURL *)inPrefix{
    self.isRunning=YES;
    __block NSError *error;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        BOOL stop=NO;
        NSDictionary *userInfo=nil;
        if([[DeploySettings sharedSettings] saveResourcesToURL:[NSURL fileURLWithPath:inPath] withPrefix:inPrefix error:&error]==NO && self.isCancelling==NO) {

            if (error){
                userInfo=@{@"error":error};
            }

            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:userInfo];
            stop=YES;
        }


        if (stop==NO && self.isCancelling==NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *externalVolume= [[[DeploySettings sharedSettings] externalVolumeURL] path];
                    NSString *osDestinationPath;
                    if (self.savingToDMG==NO) {
                        osDestinationPath=[[externalVolume stringByAppendingPathComponent:@"Deploy"] stringByAppendingPathComponent:@"macOS"];
                    }
                    else {
                        osDestinationPath=[[self.tempFolderPath stringByAppendingPathComponent:@"Deploy"] stringByAppendingPathComponent:@"macOS"];


                    }
                NSError *error;
                if([[DeploySettings sharedSettings] copyOSesToDestination:osDestinationPath error:&error]==NO) {
                    self.isRunning=NO;
                    [self dismissController:self];
                    [[NSAlert alertWithError:error] runModal];

                    return;

                }
            });

        }
         if (self.isCancelling==YES) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissController:self];

            self.isRunning=NO;
            });

        }
    });


}
-(void)percentCompleted:(float)percentCompleted{

    if (percentCompleted>self.percentComplete) {
        self.percentComplete=percentCompleted;
    }
}

- (void)copyCompleted:(id)sender withError:(NSError *)error {

    NSLog(@"copy completed");
    if (error!=nil) NSLog(@"%@",error.localizedDescription);
}

-(void)copyCompleted:(NSNotification *)note{
    NSError *error=[[note userInfo] objectForKey:@"error"];

    if (self.savingToDMG==YES) {
        NSFileManager *fm=[NSFileManager defaultManager];

        NSString *saveFilename=[[[DeploySettings sharedSettings] saveFilePath] lastPathComponent];

        NSString *tempFolder=[[TCSDefaultsManager sharedManager] newTempFolder];

        if (!tempFolder) return;
        NSString *tempSaveFilePath=[tempFolder stringByAppendingPathComponent:saveFilename];

        NSString *tempSaveFilePathConverted=[tempFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_converted.dmg",saveFilename]];

        self.percentComplete=self.percentComplete+5;
        NSString *diskImageVolumeName=[[NSUserDefaults standardUserDefaults] objectForKey:@"diskImageVolumeName"];

        if (!diskImageVolumeName || diskImageVolumeName.length==0) {
            diskImageVolumeName=@"mdsresources";
        }
        self.status=@"Saving to Disk Image";
        self.createDiskImageTaskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{


        } endBlock:^{

            if (self.isCancelling==NO && self.createDiskImageTaskWrapper.terminationStatus==0) {
                self.convertWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{
                    self.status=@"Converting Disk Image";
                    self.percentComplete=self.percentComplete+25;

                } endBlock:^{
                    if (self.convertWrapper.terminationStatus!=0) {


                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Error Converting Disk Image";
                        alert.informativeText=@"There was an error converting the disk image from ReadWrite to ReadOnly. Please verify you have enough free space (usually 2x the size of the resources is recommended).";

                        [alert addButtonWithTitle:@"OK"];
                        [alert runModal];
                        if ([fm fileExistsAtPath:tempSaveFilePathConverted]){
                            [fm removeItemAtPath:tempSaveFilePathConverted error:nil];
                        }
                        if ([fm fileExistsAtPath:tempSaveFilePath]){
                            [fm removeItemAtPath:tempSaveFilePath error:nil];
                        }

                        [self dismissController:self];

                        return;

                    }
                    NSError *err;
                    sleep(1);
                    if([fm moveItemAtPath:tempSaveFilePathConverted toPath:[[DeploySettings sharedSettings] saveFilePath] error:&err]==NO){

                        [[NSAlert alertWithError:err] runModal];
                    }
                    [[TCSDefaultsManager sharedManager] clearCache];
                    if (self.isCancelling==NO) {
                        self.savingToDMG=NO;
                        self.isRunning=NO;
                        [self dismissController:self];

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Disk Image Saved Successfully";
                        alert.informativeText=@"The disk image was saved successfully.";
                        [alert runModal];
                    }
                } outputBlock:^(NSString *output) {
                    NSLog(@"%@",output);
                } errorOutputBlock:^(NSString *errorOutput) {

                    NSLog(@"%@",errorOutput);
                } arguments:@[@"/usr/bin/hdiutil",@"convert",@"-format",@"UDRO",@"-ov",@"-o",tempSaveFilePathConverted,tempSaveFilePath]];
                [self.convertWrapper startProcess];
            }
            else if (self.createDiskImageTaskWrapper.terminationStatus!=0){
                if (self.convertWrapper.terminationStatus!=0) {


                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Error Creating Disk Image";
                    alert.informativeText=@"There was an error creating the disk image. Please verify you have enough free space (usually 2x the size of the resources is recommended).";

                    [alert addButtonWithTitle:@"OK"];
                    [alert runModal];
                    if ([fm fileExistsAtPath:tempSaveFilePathConverted]){
                        [fm removeItemAtPath:tempSaveFilePathConverted error:nil];
                    }
                    if ([fm fileExistsAtPath:tempSaveFilePath]){
                        [fm removeItemAtPath:tempSaveFilePath error:nil];
                    }

                    [self dismissController:self];
                    return;

                }

            }
            else{
                self.savingToDMG=NO;
                self.isRunning=NO;
            }


        } outputBlock:^(NSString *output) {
            if([output containsString:@"PERCENT:"]) {


                NSString *percentString=[[output componentsSeparatedByString:@"PERCENT:"] lastObject];

                if ([percentString floatValue]>0) self.percentComplete=[percentString floatValue];
            }
            NSLog(@"%@",output);
        } errorOutputBlock:^(NSString *errorOutput) {


            NSLog(@"%@",errorOutput);

        } arguments:@[@"/usr/bin/hdiutil",@"makehybrid",@"-hfs",@"-default-volume-name",diskImageVolumeName,@"-o",tempSaveFilePath,self.tempFolderPath]];


        [self.createDiskImageTaskWrapper startProcess];
        return;
    }
    else {
        self.isRunning=NO;
        if (self.isCancelling==NO && error) {
            [self dismissController:self];

            [[NSAlert alertWithError:error] runModal];
            return;

        }
        if (self.isCancelling==NO && error==nil) {
            [self showSuccess];
        }
        self.isCancelling=NO;
    }




}
-(void)showSuccess{

    [self dismissController:self];
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Resources Saved Successfully";
    alert.informativeText=@"The resources were exported successfully.";
    [alert runModal];
    [[TCSDefaultsManager sharedManager] clearCache];


}

- (IBAction)stopRunning:(id)sender {
    self.isCancelling=YES;
    if (self.isRunning==YES){

        if (self.createDiskImageTaskWrapper) [self.createDiskImageTaskWrapper stopProcess];
        [[DeploySettings sharedSettings] cancelCopyOperation];
        //        if (self.fileCopyController) [self.fileCopyController cancelCopyOperation];
        if (self.convertWrapper) [self.convertWrapper stopProcess];
        [[DeploySettings sharedSettings] setStopCopying:YES];

        [self dismissController:self];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
}



@end
