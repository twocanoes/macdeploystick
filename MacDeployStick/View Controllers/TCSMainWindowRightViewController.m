//
//  TCSMainWindowRightViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 4/1/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMainWindowRightViewController.h"
@interface TCSMainWindowRightViewController ()

@end

@implementation TCSMainWindowRightViewController

- (void)viewDidLoad {
    //    self.titleImage.frame=NSMakeRect(self.titleImage.frame.origin.x, self.titleImage.frame.origin.y, 154,154);
    self.titleImageWidthConstaint.constant=54;
    self.titleImageHeightConstaint.constant=54;
    self.titleImageTopConstaint.constant=18;
    self.titleImageLeadingConstaint.constant=24;

}
-(void)activateRunning:(BOOL)isRunning{

    self.isRunning=isRunning;
    id <TCSRegisterAppQuitProtocol> appDelegate=(id)[NSApp delegate];
    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSERVICEUPDATEBUSY object:self userInfo:@{@"isRunning":@(isRunning)}];

    if (isRunning==YES){
        [appDelegate registerForTerminateNotification:self];

    }
    else {
        [appDelegate unregisterForTerminateNotification:self];
    }

}



@end
