//
//  TCSDownloadResourcesViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/5/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDownloadResourcesViewController.h"
#import "TCTaskWrapperWithBlocks.h"

@interface TCSDownloadResourcesViewController ()
@property (strong) TCTaskWrapperWithBlocks *unzipWrapper;
@property (strong)  NSURLSessionDataTask *downloadTask;
@property (assign) BOOL isRunning;
@property (assign) BOOL shouldCancel;

@end

@implementation TCSDownloadResourcesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.preferredContentSize=self.view.frame.size;
    [self downloadResources];

    // Do view setup here.
}
-(void)viewDidAppear{
}
- (IBAction)cancelDownloadResourcesButtonPressed:(id)sender {
    self.shouldCancel=YES;
    [self.downloadTask cancel];
    self.downloadTask=nil;
    [self.delegate resourceDownloadCompleteWithError:YES];

}

-(void)downloadResources{
    self.shouldCancel=NO;
    self.isRunning=YES;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    NSString *imagrDataPath=[appSupportDir stringByAppendingPathComponent:@"Imagr.zip"];


    NSError *err;
    if( [fm createDirectoryAtPath:appSupportDir withIntermediateDirectories:YES attributes:nil error:&err]==NO) {
        [[NSAlert alertWithError:err] runModal];

    }

    NSString *dataUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"imagr_url"];

    NSURL *url = [NSURL URLWithString:dataUrl];
    if (!url){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Invalid URL";
        alert.informativeText=[NSString stringWithFormat:@"The URL specified in preferences, %@, is invalid. Please fix and try again",dataUrl];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        self.isRunning=NO;
        [self.delegate resourceDownloadCompleteWithError:YES];
        [self dismissViewController:self];


    }

    if ([fm fileExistsAtPath:imagrDataPath]) {
        if ([fm removeItemAtPath:imagrDataPath error:&err]==NO) {

            [[NSAlert alertWithError:err] runModal];
            self.isRunning=NO;
            [self.delegate resourceDownloadCompleteWithError:YES];

            [self dismissViewController:self];
        }
    }
    self.downloadTask = [[NSURLSession sharedSession]
                         dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{

               if (self.shouldCancel==NO) [[NSAlert alertWithError:error] runModal];
                self.isRunning=NO;
                [self.delegate resourceDownloadCompleteWithError:YES];

                [self dismissViewController:self];

            });
            return;

        }

        [data writeToFile:imagrDataPath atomically:YES];

        dispatch_async(dispatch_get_main_queue(), ^{


            self.unzipWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

            } endBlock:^{

                if ([self.unzipWrapper terminationStatus]!=0) {
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Unzip Error";
                    alert.informativeText=[NSString stringWithFormat:@"The file %@ could not be unzipped. Please try again.",imagrDataPath];
                    [alert runModal];

                    NSError *err;

                    if ([fm removeItemAtPath:appSupportDir error:nil]==NO) {

                        [[NSAlert alertWithError:err] runModal];

                    }
                    self.isRunning=NO;
                    [self.delegate resourceDownloadCompleteWithError:YES];

                    [self dismissViewController:self];
                    return;
                }
                else {
                    NSError *err;
                    if ([fm fileExistsAtPath:imagrDataPath]) {
                        if ([fm removeItemAtPath:imagrDataPath error:&err]==NO) {

                            [[NSAlert alertWithError:err] runModal];
                            self.isRunning=NO;
                            [self.delegate resourceDownloadCompleteWithError:YES];

                            [self dismissViewController:self];
                            return;

                        }
                    }
                    self.isRunning=NO;
                    [self.delegate resourceDownloadCompleteWithError:NO];

                    [self dismissViewController:self];



                }
            } outputBlock:^(NSString *output) {
                NSLog(@"#zip: %@",output);

            } errorOutputBlock:^(NSString *errorOutput) {
                NSLog(@"#zip: %@",errorOutput);

            } arguments:@[@"/usr/bin/unzip",@"-d",appSupportDir,@"-o",imagrDataPath]];


            [self.unzipWrapper startProcess];
        });


    }];


    [self.downloadTask resume];


}

@end
