//
//  TCSDownloadMacOSViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMainWindowRightViewController.h"
#import "AppDelegate.h"
#import "TCSMDSHelperReplyProtocol.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSDownloadMacOSViewController : TCSMainWindowRightViewController <TCSShouldQuitAppProtocol,TCSMDSHelperReplyProtocol>

@end

NS_ASSUME_NONNULL_END
