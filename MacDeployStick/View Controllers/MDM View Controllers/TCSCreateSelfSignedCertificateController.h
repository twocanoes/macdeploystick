//
//  TCSCreateSelfSignedCertificateController.h
//  MDS
//
//  Created by Timothy Perfitt on 7/29/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TCSIdentityCreatedProtocol <NSObject>

-(void)identityCreatedAtURL:(nullable NSURL *)certificatePath privateKeyURL:(nullable NSURL *)privateKeyPathURL;
@end
@interface TCSCreateSelfSignedCertificateController : NSViewController
@property (assign) id delegate;
@end

NS_ASSUME_NONNULL_END
