//
//  TCSServiceViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSServiceViewController.h"
#import "MDSPrivHelperToolController.h"
@interface TCSServiceViewController ()
@property (strong) NSString *startStopTooltip;

@end

@implementation TCSServiceViewController



-(void)stopService:(id)sender{
    [[NSException exceptionWithName:@"Unimplemented" reason:@"implement in superclass" userInfo:nil] raise];

}
-(void)startService:(id)sender{
    [[NSException exceptionWithName:@"Unimplemented" reason:@"implement in superclass" userInfo:nil] raise];
}

-(void)updateServiceStatus:(id)sender{
    [[NSException exceptionWithName:@"Unimplemented" reason:@"implement in superclass" userInfo:nil] raise];

}
-(IBAction)serviceStateChanged:(nonnull NSButton *)sender {
    if (sender.state==NSControlStateValueOn) {

        [self startService:self];

    }

    else {
        [self stopService:self];

    }
}

- (void)restartService:(nonnull id)sender {
    if (self.isRunning) {

        [self stopService:self];
    }
    [self startService:self];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.startStopTooltip=@"Unlock to change";
    [self.authenticationView setString:TCSAUTHRIGHT];

    self.authenticationView.delegate=self;
    [self.authenticationView updateStatus:self];

    [[NSNotificationCenter defaultCenter] addObserverForName:NSApplicationWillTerminateNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [self.authenticationView deauthorize:self];
    }];
}

- (void)authorizationViewDidAuthorize:(SFAuthorizationView *)view{
    self.isAuthorized=YES;
    SFAuthorization *auth=view.authorization;
    [MDSPrivHelperToolController sharedHelper].authorization=auth;

}
- (void)authorizationViewDidDeauthorize:(SFAuthorizationView *)view{
    self.isAuthorized=NO;
}
-(void)dealloc{
    [self.authenticationView setAutoupdate:NO];
    self.authenticationView.delegate=nil;

}
-(void)updateSidebar:(id)sender{
    [self updateSidebarActual:self];
    [self performSelector:@selector(updateSidebarActual:) withObject:self afterDelay:5];

}
-(void)updateSidebarActual:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSIDEBARRELOAD object:self];

}

@end
