//
//  ViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"
#import "TCSSaveMasterViewController.h"
#import "WorkflowViewController.h"
#import "TCSConfigHelper.h"
#import "AppDelegate.h"
#import "TCSDownloadResourcesViewController.h"
#import "TCSUtility.h"
#import "SubscriptionManager.h"
#import "SubscriptionViewController.h"
#include <sys/mount.h>
#include <sys/types.h>

//#ifdef APPSTORE
//#import <TCSStore/TCSStore.h>
//#endif

#define kNotificationPurchaseComplete @"TCSPurchaseComplete"
#define kProductID @"100"
#define TRIALDAYS 0

typedef void (^CheckResourcesCompletionBlock)(BOOL res);

typedef NS_ENUM(NSUInteger, TCSSAVEOPERATION) {
    SAVETOVOLUME,
    SAVETODMG,
};
@interface ViewController() <TCSDownloadResourcesProtocol>
@property (nonatomic, strong) DeploySettings *deploySettings;
//@property (nonnull,strong) NSURL *installMediaURL;
//@property (strong) id droppedFileNotification;
@property (strong) id keyPressedObserver;
@property (weak) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSArrayController *arrayController;
//@property (nonatomic,strong) NSMutableArray *workflows;
@property (nonatomic,strong) NSImage *installerImage;
@property (assign) BOOL workflowsExist;
//@property (strong) GCDWebServer* munkiServer;
@property (assign) NSIndexSet *selectedWorkflowIndexes;
@property (strong) CheckResourcesCompletionBlock completionBlock;

@end
//
@implementation ViewController


-(IBAction)duplicateSelectedWorkflows:(id)sender{



    NSArray *newWorkflows=[self duplicateWorkflows:self.arrayController.selectedObjects stripPasswords:NO];

    [newWorkflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
        [[DeploySettings sharedSettings] addWorkflow:currWorkflow];
        [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSIDEBARRELOAD object:self];

    }];
}

-(NSArray *)duplicateWorkflows:(NSArray *)inWorkflows stripPasswords:(BOOL)inStripPasswords{
    NSMutableArray *newWorkflows=[NSMutableArray array];
    if (self.arrayController.selectedObjects.count==0) return nil;

    [self.arrayController.selectedObjects enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
        TCSWorkflow *newWorkflow=[currWorkflow copy];
        newWorkflow.workflowUUID=[NSUUID UUID];
        newWorkflow.isReadonly=NO;
        if (inStripPasswords==YES){

            newWorkflow.wifiPassword=@"";
            newWorkflow.createUserPassword=@"";
            if (newWorkflow.users.count>0){

                [newWorkflow.users enumerateObjectsUsingBlock:^(TCSWorkflowUser *currUser, NSUInteger idx, BOOL * _Nonnull stop) {

                    currUser.password=@"";
                }];
            }

        }
        [newWorkflows addObject:newWorkflow];

    }];
    return [NSArray arrayWithArray:newWorkflows];

}
- (IBAction)loggingInfoCheckBoxChanged:(NSButton *)sender {

    if (sender.state==NSOnState && ![[NSUserDefaults standardUserDefaults] objectForKey:@"loggingInfoURLString"]) {
        NSString *hostname=[[NSUserDefaults standardUserDefaults] objectForKey:SERVERHOSTNAME];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"http://%@:8080",hostname] forKey:@"loggingInfoURLString"];

    }
}
-(IBAction)exportSelectWorkflows:(id)sender{

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Password Removal";

    alert.informativeText=@"Workflows may contain user and WiFi passwords. Do you want the exported file to contain the passwords? The passwords will need to be re-entered when importing the workflows.";

    [alert addButtonWithTitle:@"Include Passwords"];
    [alert addButtonWithTitle:@"Remove Passwords"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    BOOL stripPasswords=NO;
    if (res==NSAlertSecondButtonReturn) {
        stripPasswords=YES;
    }
    else if (res==NSAlertThirdButtonReturn) {
        return;
    }

    NSArray *newWorkflows=[self duplicateWorkflows:self.arrayController.selectedObjects stripPasswords:stripPasswords];

    if (self.arrayController.selectedObjects.count==0) return;

    NSSavePanel *savePanel=[NSSavePanel savePanel];
    savePanel.allowsOtherFileTypes=NO;
    savePanel.allowedFileTypes=@[@"mdsworkflows"];

    savePanel.message=[NSString stringWithFormat:@"Specify a name and location to export the %li workflow(s)",newWorkflows.count];
    NSModalResponse res2=[savePanel runModal];

    if (res2!=NSModalResponseOK) {

        return;
    }
    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *err;
    if ([fm fileExistsAtPath:[savePanel URL].path]) {
        if ([fm removeItemAtPath:[savePanel URL].path error:&err]==NO) {

            [[NSAlert alertWithError:err] runModal];
            return;
        }
    }

    NSString *savePath=[savePanel URL].path;

    NSData *workflowData=[DeploySettings workflowDataFromArray:newWorkflows];
    if (!workflowData) {
        NSLog(@"no data!");
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Error";
        alert.informativeText=@"No data was found to export. Please inspect the workflows and try again.";
        [alert runModal];

        return ;

    }

    if([workflowData writeToFile:savePath atomically:NO]==NO){
        NSLog(@"Could not save data!");
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Error";
        alert.informativeText=@"The data could not be saved. Please verify disk space, permissions, and possible termporal anomolies and try again.";
        [alert runModal];

    }
    NSAlert *successAlert=[[NSAlert alloc] init];
    successAlert.messageText=@"Workflow data exported";
    successAlert.informativeText=[NSString stringWithFormat:@"The %li workflow(s) have been exported.",newWorkflows.count];
    [successAlert runModal];

}
-(void)restartMDMIfNeeded:(id)sender{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    if ([ud boolForKey:TCSMDMRUNNING]==YES){
        [[self.view.window windowController] performSegueWithIdentifier:@"MDMConfigSegue" sender:self] ;
    }


}
- (void)viewDidLoad {
    [super viewDidLoad];

    [self performSelector:@selector(restartMDMIfNeeded:) withObject:self afterDelay:0.5];




    if (!self.representedObject) {
        self.representedObject=[DeploySettings sharedSettings];
    }
    //    if ([[DeploySettings sharedSettings] installMediaURL].path) self.installerImage=[[NSWorkspace sharedWorkspace] iconForFile:[[DeploySettings sharedSettings] installMediaURL].path];

    //    if ([[TCSConfigHelper imagrVersion] isEqualToString:@""];
    [self.tableView setDoubleAction:@selector(tableDoubleClick:)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldTurnOnWebServer"]==YES){

//        [self startWebServer:self];

    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldTurnOnMunkiRepo"]==YES){

//        [self performSelector:@selector(startMunkiRepo:) withObject:self afterDelay:1];
        

    }

}

- (IBAction)removeWorkflow:(id)sender {

    if (self.arrayController.selectedObjects.count==0) {
        NSBeep();
        return;
    }
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Delete Workflow?";

    alert.informativeText=@"This will permanently delete the workflow(s). Do you want to delete the selected workflow(s)?";

    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Delete"];
    NSInteger res=[alert runModal];

    if (res==NSAlertFirstButtonReturn) return;


    [self.arrayController removeObjects:self.arrayController.selectedObjects];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:@"shouldAutomaticallyRunWorkflow"]==YES) {
        NSArray *workflows= [[DeploySettings sharedSettings] workflows];
        NSString *selectedAutorunName=[ud objectForKey:@"autorunWorkflowName"];

        __block TCSWorkflow *foundWorkflow=nil;
        [workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([currWorkflow.workflowName isEqualToString:selectedAutorunName]){

                *stop=YES;
                foundWorkflow=currWorkflow;
            }

        }];
        if (!foundWorkflow) {
            [ud removeObjectForKey:@"shouldAutomaticallyRunWorkflow"];
            [ud removeObjectForKey:@"autorunWorkflowName"];


        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSIDEBARRELOAD object:self];



}


-(void)viewDidDisappear{

    [[NSNotificationCenter defaultCenter] removeObserver:self.keyPressedObserver];
}

- (IBAction)saveResourcesButtonPressed:(id)sender {
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    __block BOOL noOSSelected=NO;
    NSArray *workflows= [[DeploySettings sharedSettings] workflows];
    NSString *selectedAutorunName=[ud objectForKey:@"autorunWorkflowName"];
    __block TCSWorkflow *foundWorkflow=nil;


    [workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([currWorkflow.workflowName isEqualToString:selectedAutorunName]){
            foundWorkflow=currWorkflow;
        }
        if (currWorkflow.shouldInstall==NO&& currWorkflow.shouldImage==NO && currWorkflow.shouldSkipMacOSInstall==NO){
            noOSSelected=YES;
            foundWorkflow=currWorkflow;
        }


    }];
    if (noOSSelected==YES){

        NSLog(@"No OS selected");
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No macOS option selected";
        alert.informativeText=[NSString stringWithFormat:@"The workflow \"%@\" does not have a macOS option selected. Please select a macOS install option for this workflow and try again.",foundWorkflow.workflowName];
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;
    }
    if ([ud boolForKey:@"shouldAutomaticallyRunWorkflow"]==YES) {


        if (!foundWorkflow) {
            NSLog(@"workflow not found");
            NSAlert *alert=[[NSAlert alloc] init];
            alert.informativeText=@"Please select a workflow to run automatically.";
            alert.messageText=@"The automatically run workflow checkbox is selected but no workflow has been specified. Please select a workflow or unselect the checkbox.";
            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
            return;


        }

        BOOL isActive=[foundWorkflow isActive];

        if (isActive==NO) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.informativeText=@"Please select a different workflow to run automatically or uncheck the checkbox to automatically run the workflow.";
            alert.messageText=[NSString stringWithFormat:@"The workflow \"%@\" is set to run automatically but is not active.", [foundWorkflow workflowName]];

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
            return;


        }


    }
    __block NSString *badWorkflow=nil;
    __block BOOL hasActiveWorkflow=NO;
    __block BOOL hasNoUserAndSkippngSetupAssistant=NO;
    __block NSInteger activeWorkflowCount=0;
    [[[DeploySettings sharedSettings] workflows] enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

        if (currWorkflow.isActive==YES) {
            hasActiveWorkflow=YES;
            activeWorkflowCount++;
        }
        if (currWorkflow.users.count==0 && currWorkflow.shouldSkipSetupAssistant==YES){
            hasNoUserAndSkippngSetupAssistant=YES;

            badWorkflow=currWorkflow.workflowName;
        }

    }];

//    Edition edition=[[SubscriptionManager shared] edition];
////    AppDelegate *delegate=(AppDelegate*)[NSApp delegate];
//
//    if (activeWorkflowCount>3 && edition != EDITION_ENTERPRISE) {
//        NSString *message=@"More than 3 active workflows requires the Enterprise Edition. Please upgrade or deselect active workflows";
//
//        [[SubscriptionManager shared] showSubcribeWithMessage:message];
//
//
//
//        return;
//    }
//    else if (activeWorkflowCount>1 && edition == EDITION_FREE){
//        NSString *message=@"More than one active workflows requires  Enterprise or Pro edition. Please upgrade or deselect active workflows";
//        [[SubscriptionManager shared] showSubcribeWithMessage:message];
//
//        return;
//    }

    if (hasActiveWorkflow==NO) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.informativeText=@"There are no active workflows. Make sure at least one workflow is marked as active.";
        alert.messageText=[NSString stringWithFormat:@"No active workflows."];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;


    }
    if (hasNoUserAndSkippngSetupAssistant==YES) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.informativeText=[NSString stringWithFormat:@"The workflow \"%@\" specifies that Setup Assistant is skipped but the workflow does not have settings to create a user. A user may not be available to log in after setup is complete. To resolve this issue, open this workflow and provide settings for creating a user, or deselect the Skip Setup Assistant checkbox in Options. Are you sure that you want to save resources? ",badWorkflow];
        alert.messageText=[NSString stringWithFormat:@"Possible Issue with Skipping Setup Assistant"];

        [alert addButtonWithTitle:@"Cancel"];
        [alert addButtonWithTitle:@"Continue"];


        NSInteger res=[alert runModal];

        if (res==NSAlertFirstButtonReturn) return;



    }
    switch ([sender tag]) {
        case SAVETOVOLUME:
        {


            NSOpenPanel *openPanel=[NSOpenPanel openPanel];

            openPanel.canChooseFiles=NO;
            openPanel.canChooseDirectories=YES;
            openPanel.allowsMultipleSelection=NO;

            openPanel.message=@"Select a volume to save resources:";
            NSModalResponse res=[openPanel runModal];

            if (res!=NSModalResponseOK) {

                return;
            }

            NSString *openVolumePath=[openPanel URL].path;
            struct statfs output;
            statfs([openVolumePath UTF8String], &output);
            NSString *filesystemType=[NSString stringWithUTF8String:output.f_fstypename];
            NSLog(@"%@",filesystemType);
            if ([[filesystemType lowercaseString] containsString:@"hfs"]==NO) { //skip root
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Volume is not HFS+";
                alert.informativeText=@"The selected volume is not HFS+. This can cause issues. Are you sure you want to continue?";

                [alert addButtonWithTitle:@"Cancel"];
                [alert addButtonWithTitle:@"Continue"];
                NSInteger res=[alert runModal];

                if (res==NSAlertFirstButtonReturn) return;

            }

            [self downloadResourcesWithCompletedBlock:^(BOOL hadError) {
                if (hadError==YES) return;


                [[DeploySettings sharedSettings] setExternalVolumeURL:[NSURL fileURLWithPath:openVolumePath]];
                [[DeploySettings sharedSettings] setShouldSaveToDMG:NO];

                [self performSegueWithIdentifier:@"TCSExportMediaSegue" sender:self];
            }];
        }

            break;
        case SAVETODMG:
        {


                __block NSString *saveName;
                [[[DeploySettings sharedSettings] workflows] enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

                    if (currWorkflow.isActive==YES) {
                        saveName=currWorkflow.workflowName;
                        *stop=YES;
                    }

                }];
                NSSavePanel *savePanel=[NSSavePanel savePanel];
                savePanel.canCreateDirectories=YES;
                savePanel.allowedFileTypes=@[@"dmg"];
                savePanel.nameFieldStringValue=saveName;
                savePanel.message=@"Specify a name and location for the Disk Image:";
                NSModalResponse res=[savePanel runModal];

                if (res!=NSModalResponseOK) {

                    return;
                }
                NSFileManager *fm=[NSFileManager defaultManager];
                NSError *err;
                if ([fm fileExistsAtPath:[savePanel URL].path]) {
                    if ([fm removeItemAtPath:[savePanel URL].path error:&err]==NO) {

                        [[NSAlert alertWithError:err] runModal];
                        return;
                    }
                }
            [self downloadResourcesWithCompletedBlock:^(BOOL hadError) {
                if (hadError==YES) return;
                [[DeploySettings sharedSettings] setSaveFilePath:[savePanel URL].path];

                [[DeploySettings sharedSettings] setShouldSaveToDMG:YES];

                [self performSegueWithIdentifier:@"TCSExportMediaSegue" sender:self];


            }];
        }

            break;
        default:
            break;
    }

}
- (IBAction)tableDoubleClick:(id)sender {
    if ([self.tableView clickedRow]!=-1) {

        [self performSegueWithIdentifier:@"editWorkflow" sender:self];
    }
}
- (void)dismissViewController:(WorkflowViewController *)viewController{
    [super dismissViewController:viewController];

    if ([viewController isKindOfClass:[WorkflowViewController class]]) {
        [self.arrayController rearrangeObjects];

    }
}

-(void)selectWorkflow:(TCSWorkflow *)workflow{

            if (workflow) self.arrayController.selectedObjects=@[workflow];

}
- (IBAction)backgroundImageButtonPressed:(id)sender {
    self.deploySettings=[DeploySettings sharedSettings];

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {


        [[DeploySettings sharedSettings] setBackgroundImageURL:[openPanel URL]];

    }

}

-(BOOL)resourcesNeeded{
    return [TCSConfigHelper checkIfResourcesNeeded];
}
- (IBAction)connectToWifiButtonPressed:(NSButton *)sender {
    if (sender.state==NSOnState){
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        [ud setBool:YES forKey:@"shouldWaitForNetwork"];
    }
}

-(IBAction)checkForResources:(id)sender{




    [self downloadResourcesWithCompletedBlock:^(BOOL hadError) {

    }];


}



-(void)downloadResourcesWithCompletedBlock:(void (^)(BOOL hadError))completedBlock{


    BOOL resourcesNeeded=[TCSConfigHelper checkIfResourcesNeeded];

    if (resourcesNeeded==YES) {
        self.completionBlock=completedBlock;

        [self performSegueWithIdentifier:@"TCSDownloadResourcesSegue" sender:self];

    }
    else {

        completedBlock(NO);
    }
//    NSAlert *alert=[[NSAlert alloc] init];
//    alert.messageText=@"Resource Needed";
//    alert.informativeText=@"In order to function properly, resources need to be downloaded. Download now?";
//
//    [alert addButtonWithTitle:@"OK"];
//    [alert addButtonWithTitle:@"Cancel"];
//    NSInteger res=[alert runModal];
//
//    if (res==NSAlertSecondButtonReturn) return;
//



//

}

#ifdef APPSTORE

- (IBAction)buyButtonPressed:(id)sender {
    [self buy:self];
}

-(void)buy:(id)sender{

//    [[TCSStoreManager sharedManager] setMainWindow:[NSApp mainWindow]];
//
//
//    [[TCSStoreManager sharedManager] setTrialDays:TRIALDAYS];
//    //    [[TCSStoreManager sharedManager] setMoreInfoLink:@"https://twocanoes.com"];
//    [[TCSStoreManager sharedManager] showTrialBlocker:self];


}
#endif
//-(void)itemDropped:(NSString *)inPath{
//
//    if ([inPath.pathExtension isEqualToString:@"dmg"]){
//            [self saveSelectedDMGURL:[NSURL fileURLWithPath:inPath]];
//    }
//}


-(void)viewDidAppear{


//#ifdef APPSTORE
//
//    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationPurchaseComplete object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
//        NSLog(@"Purchase completed");
//    }];
//    [[TCSStoreManager sharedManager] setMoreInfoLink:@"https://twocanoes.com/mac/in-app/mds-purchasing-help"];
//    [[TCSStoreManager sharedManager] setTrialDays:14];
//
//    [[TCSStoreManager sharedManager] startWithProductIdentifer:@"MDS100"];
//    if ([[TCSStoreManager sharedManager] hasPurchased]==NO) {
//        [self buy:self];
//    }
//    //    [[TCSStoreManager sharedManager] setNewsletterSignupLaunches:1];
//    [[TCSStoreManager sharedManager] setNewsletterURL:@"https://twocanoes.com/subscribe/"];
//    [[TCSStoreManager sharedManager] showNewsletterIfNeeded];
//
//#endif

    self.keyPressedObserver=[[NSNotificationCenter defaultCenter] addObserverForName:TCSSelectKeyPressed object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        if (self.arrayController.selectedObjects.count==0) return;

        NSArray *selectedWorkflows=self.arrayController.selectedObjects;

        TCSWorkflow *workflow=[selectedWorkflows firstObject];
        BOOL selectState=!workflow.isActive;

        [selectedWorkflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
            currWorkflow.isActive=selectState;

        }];

    }];

}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(NSButton *)sender{
    WorkflowViewController *workflowViewController=segue.destinationController;
//
    if ([segue.identifier isEqualToString:@"newWorkflow"]) {

        workflowViewController.workflow=[[TCSWorkflow alloc] init];
//        workflowViewController.workflow.createUserUID=@"501";

        workflowViewController.priorWorkflow=nil;
        workflowViewController.workflow.shouldRebootAfterInstallingResources=NO;
        workflowViewController.workflow.shouldRebootAfterInstallingResourcesInstallOS=NO;

        workflowViewController.workflow.workflowUUID=[NSUUID UUID];
    }
    else if ([segue.identifier isEqualToString:@"TCSDownloadResourcesSegue"]) {

        TCSDownloadResourcesViewController *resourcesViewController=segue.destinationController;
        resourcesViewController.delegate=self;
    }
    else if ([segue.identifier isEqualToString:@"editWorkflow"]) {
        workflowViewController.workflow=[[[self.arrayController selectedObjects] firstObject] copy];
        workflowViewController.priorWorkflow=[[self.arrayController selectedObjects] firstObject];

//ComputerNameRemoved means we moved to the new style rather than an index. this is to migrate over the old settings to the new settings.
        switch (workflowViewController.workflow.computerNameTypeIndex) {
            case ComputerNameSerialAsk:
                workflowViewController.workflow.shouldPromptForComputerName=YES;
                workflowViewController.workflow.computerNameString=@"{{serial_number}}";
                workflowViewController.workflow.computerNameTypeIndex=ComputerNameRemoved;

                break;
            case ComputerNameSerialForce:
                workflowViewController.workflow.shouldPromptForComputerName=NO;
                workflowViewController.workflow.computerNameString=@"{{serial_number}}";
                workflowViewController.workflow.computerNameTypeIndex=ComputerNameRemoved;
                break;

            case ComputerNameAsk:
                workflowViewController.workflow.computerNameTypeIndex=ComputerNameRemoved;

                break;

            default:
                break;
        }
    }

}
-(IBAction)editWorkflow:(id)sender{

    [self performSegueWithIdentifier:@"editWorkflow" sender:self];

}
-(IBAction)newWorkflow:(id)sender{

    [self performSegueWithIdentifier:@"newWorkflow" sender:self];

}

-(IBAction)saveMaster:(id)sender{

//
//    [vc saveMaster:self];

    NSSavePanel *savePanel=[NSSavePanel savePanel];

    savePanel.allowedFileTypes=@[@"sparseimage"];
    savePanel.message=@"Select a filename and location to save the master resources:";
    NSModalResponse res=[savePanel runModal];

    if (res!=NSModalResponseOK) {

        return;
    }
    NSString *saveVolPath=[savePanel URL].path;

    NSStoryboard *mainStoryboard = [NSStoryboard storyboardWithName:@"Main" bundle: nil];

    TCSSaveMasterViewController *vc= [mainStoryboard instantiateControllerWithIdentifier:@"savemasterviewcontroller"];

    [vc saveMasterToPath:saveVolPath];
    [self presentViewControllerAsSheet:vc];

    return;
}
   

- (void)resourceDownloadCompleteWithError:(BOOL)hadError {
    self.completionBlock(hadError);
}


@end
