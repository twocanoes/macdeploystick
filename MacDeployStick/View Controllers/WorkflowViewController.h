//
//  WorkflowViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWorkflow.h"
#import "TCSResourceViewController.h"
#import "TCSCreateUsersViewController.h"
#import "TCSCreateUsersViewController.h"
#import "TCSWorkflowTableViewDatasource.h"
#import "ResourcesConfigProtocol.h"
#import "TCSVariableOptionsViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, MODE) {
    modeExisting,
    modeNew
};
@interface WorkflowViewController : NSViewController  <TCSResourceOrderDelegate,TCSWorkflowUserProtocol,ResourcesConfigProtocol,TCSVariableOptionsProtocol>

@property (nonatomic, strong) TCSWorkflow *workflow;
@property  (nonatomic, strong) TCSWorkflow * _Nullable  priorWorkflow;
@property (strong) IBOutlet NSArrayController *usersArrayController;

@property (strong)  TCSWorkflowUser  * _Nullable addUser;
@property (strong) TCSWorkflowUser  * _Nullable editUser;
@property (strong) NSIndexSet *selectedUserIndexes;
@property (weak) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSUserDefaultsController *sharedUserDefaultsController;
@property (strong) IBOutlet NSObjectController *objectController;
@property (strong) IBOutlet TCSWorkflowTableViewDatasource *workflowTableViewDatasource;
@property (assign) BOOL showAdvancedUserOptions;

@property (assign) MODE mode;
@end

NS_ASSUME_NONNULL_END
