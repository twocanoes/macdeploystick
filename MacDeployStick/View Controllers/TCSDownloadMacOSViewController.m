//
//  TCSDownloadMacOSViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDownloadMacOSViewController.h"
#import "TCTaskWrapperWithBlocks.h"
#import "MDSPrivHelperToolController.h"
#import "TCSLogFileWindowController.h"
#import <IOKit/pwr_mgt/IOPMLib.h>
#import "TCSDefaultsManager.h"

@interface TCSDownloadMacOSViewController (){
    IOPMAssertionID assertionID;
}

@property (weak) IBOutlet NSProgressIndicator *progressIndictator;

@property (strong) NSArray *catalogNames;
@property (strong) NSMutableArray *catalog;
@property (assign) NSInteger selectedCatalogIndex;
@property (strong) TCTaskWrapperWithBlocks *wrapper;
@property (strong) IBOutlet NSArrayController *catalogArrayController;
@property (assign) BOOL isCancelling;
@property (strong) TCSLogFileWindowController *logWindowController;
@property (strong) NSMutableArray *selectedProductsQueue;
@property (strong) NSString *selectedCatalogName;
@property (strong) NSString *workingPath;
@property (strong) NSString *savePath;
@property (strong) NSMutableDictionary *catalogDict;
@property (strong) NSString *selectedCatalogURLString;
@property (assign) float percentComplete;
@property (strong) NSString *statusMessage;
@end

@implementation TCSDownloadMacOSViewController
-(void)turnOffSleep:(NSString *)reason{
    NSLog(@"Setting to not sleep....");

    IOPMAssertionCreateWithName(kIOPMAssertionTypePreventUserIdleSystemSleep,
                                kIOPMAssertionLevelOn, (__bridge CFStringRef)reason, &assertionID);


}
- (void)awakeFromNib{
    NSSortDescriptor *versionSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"version" ascending:YES];
    NSArray *arrayOfSortDescriptors = [NSArray arrayWithObject:versionSortDescriptor];
    [self.catalogArrayController setSortDescriptors:arrayOfSortDescriptors];

}
-(void)allowSleep{
    NSLog(@"Setting the system to allow sleep...");
    if (assertionID) IOPMAssertionRelease(assertionID);

}
- (IBAction)catalogChanged:(id)sender {
    [self loadCatalog:self];
    
}

- (IBAction)showLogButtonPressed:(id)sender {

    [[NSWorkspace sharedWorkspace] openFile:@"/Library/Logs/mdshelper.log" withApplication:@"Console"];

//    if (!self.logWindowController){
//        self.logWindowController=[[TCSLogFileWindowController alloc] initWithWindowNibName:@"TCSLogFileWindowController"];
//        self.logWindowController.logFile=@"/Library/Logs/mdshelper.log";
//
//    }
//    [self.logWindowController.window makeKeyAndOrderFront:self];

}

- (IBAction)cancelButtonPressed:(id)sender {

    [[MDSPrivHelperToolController sharedHelper] stopRunningProcessesWithCallback:^(BOOL success) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self activateRunning:NO];
            self.isCancelling=YES;

        });
    }];
}

- (void)viewDidLoad {

    [super viewDidLoad];
    self.isRunning=NO;
    self.isCancelling=NO;
    self.catalogDict=[NSMutableDictionary dictionaryWithContentsOfFile:@"/System/Library/PrivateFrameworks/Seeding.framework/Versions/Current/Resources/SeedCatalogs.plist"];


    self.catalogNames=@[@"Production"];
    self.catalogNames=[self.catalogNames arrayByAddingObjectsFromArray:self.catalogDict.allKeys];

    self.catalog=[NSMutableArray array];
    [self loadCatalog:self];

}

-(void)loadCatalog:(id)sender{
    self.statusMessage=@"Loading Available Installers...";
    [self.progressIndictator setIndeterminate:YES];
    [self.progressIndictator startAnimation:self];
    self.percentComplete=0.;
    [self.catalogArrayController removeObjects:[self.catalogArrayController arrangedObjects]];

    NSMutableString *catalogPlistString=[NSMutableString string];
    NSString *currSelectedCatalogName=[self.catalogNames objectAtIndex:self.selectedCatalogIndex];
    NSString *installInstallMacOSScriptPath=[[NSBundle mainBundle] pathForResource:@"mist" ofType:@""];
    NSFileManager *fm=[NSFileManager defaultManager];
    self.workingPath=[[[TCSDefaultsManager sharedManager] tempFolder] stringByAppendingPathComponent:@"downloadMacOS"];
    if ([fm fileExistsAtPath:self.workingPath]==NO){

        [fm createDirectoryAtPath:self.workingPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSMutableArray *arguments=[@[installInstallMacOSScriptPath,@"list",@"-b",@"-q",@"-o",@"plist"] mutableCopy];

    BOOL isIPSW=NO;
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    if ([[ud objectForKey:@"showIPWS"] boolValue]==YES){
        [arguments addObjectsFromArray:@[@"-p",@"apple"]];
        isIPSW=YES;
    }
    else {
        [arguments addObjectsFromArray:@[@"-p",@"intel"]];
    }

    self.selectedCatalogURLString=[self.catalogDict objectForKey:currSelectedCatalogName];
    if (self.selectedCatalogURLString){

        [arguments addObjectsFromArray:@[@"-c",self.selectedCatalogURLString]];

    }
    else {
        self.selectedCatalogURLString=@"";
    }

    //@"--worrkdir",self.workingPath,@"--list"

//    if ([selectedCatalogName isEqualToString:@"Production"]){
//        arguments=@[installInstallMacOSScriptPath,@"--workdir",self.workingPath,@"--list"];
//    }
    [[MDSPrivHelperToolController sharedHelper] setDelegate:self];
    
//    [[MDSPrivHelperToolController sharedHelper] updatePermissionsAtWorkingPath:self.workingPath withCallback:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{

            self.wrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

                [self activateRunning:YES];

            } endBlock:^{
                [self activateRunning:NO];

                NSError *err;

                NSPropertyListFormat format;
                NSArray *tempRetrievedCatalog=[NSPropertyListSerialization propertyListWithData:[catalogPlistString dataUsingEncoding:NSUTF8StringEncoding] options:0 format:&format error:&err];

                if (!tempRetrievedCatalog) {
                    NSLog(@"%@",err.localizedDescription);
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Issue loading catalog";
                    alert.informativeText=@"There was an error loading the catalog.";

                    [alert addButtonWithTitle:@"OK"];
                    [alert runModal];

                    return;
                }

                NSMutableArray *retrievedCatalog=[NSMutableArray arrayWithCapacity:tempRetrievedCatalog.count];
                [tempRetrievedCatalog enumerateObjectsUsingBlock:^(NSDictionary *currDict, NSUInteger idx, BOOL * _Nonnull stop) {

                    NSMutableDictionary *currDictMutable=[currDict mutableCopy];
                    currDictMutable[@"isIPSW"]=@(isIPSW);
                    [retrievedCatalog addObject:[NSDictionary dictionaryWithDictionary:currDictMutable]];
                }];

                [self updateCatalog:retrievedCatalog];

            } outputBlock:^(NSString *output) {
                [catalogPlistString appendString:output];
            } errorOutputBlock:^(NSString *errorOutput) {

                NSLog(@"%@",errorOutput);
            } arguments:arguments] ;

            [self.wrapper startProcess];
        });
//    }];



}

-(void)updateCatalog:(NSArray *)inArray{

    dispatch_async(dispatch_get_main_queue(), ^{

        [self.catalogArrayController removeObjects:[self.catalogArrayController arrangedObjects]];


        [inArray enumerateObjectsUsingBlock:^(NSDictionary *catalogDictionary, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.catalogArrayController addObject:catalogDictionary];
        }];
        [self.catalogArrayController rearrangeObjects];
        [self.catalogArrayController setSelectionIndex:[self.catalogArrayController.arrangedObjects count]-1];
    });
}

- (IBAction)downloadButtonPressed:(id)sender {

    NSString *scriptPath=[[NSBundle mainBundle] pathForResource:@"mist" ofType:@""];


    NSFileManager *fm=[NSFileManager defaultManager];

    NSDictionary *dict=[fm attributesOfItemAtPath:scriptPath error:nil];

    if (dict && [dict objectForKey: NSFilePosixPermissions] && [[dict objectForKey: NSFilePosixPermissions] intValue]==0755 && [dict objectForKey:NSFileOwnerAccountID] && [[dict objectForKey:NSFileOwnerAccountID] intValue]==0){

        NSOpenPanel *openPanel=[NSOpenPanel openPanel];

        openPanel.canChooseDirectories=YES;
        openPanel.canChooseFiles=NO;
        openPanel.message=@"Select an output folder:";
        NSModalResponse res=[openPanel runModal];

        if (res!=NSModalResponseOK) {
            return;
        }


        self.savePath=openPanel.URL.path;
        [self activateRunning:YES];
        self.isCancelling=NO;
        self.workingPath=[[[TCSDefaultsManager sharedManager] tempFolder] stringByAppendingPathComponent:@"downloadMacOS"];
        if ([fm fileExistsAtPath:self.workingPath]==NO){

            [fm createDirectoryAtPath:self.workingPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        self.selectedCatalogName=[self.catalogNames objectAtIndex:self.selectedCatalogIndex];
        self.selectedProductsQueue=[NSMutableArray arrayWithArray:[self.catalogArrayController selectedObjects]];
        
        [self kickoffNextProcess];

    }
    else {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Invalid Permissions";
        alert.informativeText=[NSString stringWithFormat:@"The script to create the disk image of macOS must be owned by root and only be writable by the owner (755).\n\nPlease verify it hasn't been tampered with and update to the correct permissions and then try again.\n\ncommand: sudo chown root %@",scriptPath];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

    }



}

-(void)kickoffNextProcess{
    self.percentComplete=0.;
    [self.progressIndictator setIndeterminate:NO];
    [self.progressIndictator stopAnimation:self];

    if (self.selectedProductsQueue.count>0){
        NSDictionary *currentProduct=[self.selectedProductsQueue firstObject];
        [self.selectedProductsQueue removeObject:currentProduct];

        [self turnOffSleep:@"Download and build macOS installer"];

        BOOL createDiskImage=NO;
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

        if ([[ud objectForKey:@"downloadMacOSCreateDiskImage"] boolValue]==YES){
            createDiskImage=YES;
        }

        [[MDSPrivHelperToolController sharedHelper] installInstallMacOSWithProduct:currentProduct catalog:self.selectedCatalogURLString addToDiskImage:createDiskImage workingPath:self.workingPath withCallback:^(BOOL success) {

            if (success==YES) {
                NSFileManager *fm=[NSFileManager defaultManager];

                [[fm contentsOfDirectoryAtPath:[self.workingPath stringByAppendingPathComponent:@"com.twocanoes.mds.downloadOS"] error:nil] enumerateObjectsUsingBlock:^(NSString *currPath, NSUInteger idx, BOOL * _Nonnull stop) {

                    if ([[currPath pathExtension] isEqualToString:@"app"] || [[currPath pathExtension] isEqualToString:@"dmg"] ||
                        [[currPath pathExtension] isEqualToString:@"ipsw"]){

                        [fm moveItemAtPath:[[self.workingPath stringByAppendingPathComponent:@"com.twocanoes.mds.downloadOS"] stringByAppendingPathComponent:currPath] toPath:[self.savePath stringByAppendingPathComponent:currPath] error:nil];
                    }
                }];



                if (self.isCancelling==NO) {
                    [self kickoffNextProcess];
                }
                else {
                    [self activateRunning:NO];

                }
            }
            else {
                [self showFinishMessageWasSuccessfull:NO];
                [self activateRunning:NO];

            }
        }];
    }
    else {
        [self showFinishMessageWasSuccessfull:YES];

    }
}
//-(void)activateRunning:(BOOL)isRunning{
//
//    self.isRunning=isRunning;
//    id <TCSRegisterAppQuitProtocol> appDelegate=(id)[NSApp delegate];
//    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSERVICEUPDATEBUSY object:self userInfo:@{@"isRunning":@(isRunning)}];
//
//    if (isRunning==YES){
//        [appDelegate registerForTerminateNotification:self];
//
//    }
//    else {
//        [appDelegate unregisterForTerminateNotification:self];
//    }
//
//}
-(void)showFinishMessageWasSuccessfull:(BOOL)success{

    [self allowSleep];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self activateRunning:NO];
        NSAlert *alert=[[NSAlert alloc] init];
        if (success==NO) {
            alert.messageText=@"Error";
            alert.informativeText=@"The OS Image was not created successfully. Please verify you have enough free space and try again.";

        }
        else {
            alert.messageText=@"Completed";
            alert.informativeText=@"The OS Image was completed successfully.";

            [self dismissController:self];
        }

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
    });

}
-(BOOL)stopAppFromTerminating:(id)sender{
    if (self.isRunning==YES){

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Download in Progress";
        alert.informativeText=@"macOS is still downloading. Select Download macOS and cancel or allow the download to complete.";
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return YES;
    }
    return NO;
}
- (void)helperToolDidComplete {

}

- (void)helperToolDidFailWithError:(NSError *)error {

}

- (void)helperToolDidUpdatePercentComplete:(float)percentComplete statusMessage:(NSString *)statusMessage {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.percentComplete=percentComplete;
        self.statusMessage=statusMessage;

    });
}


@end
