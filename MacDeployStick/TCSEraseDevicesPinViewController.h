//
//  TCSEraseDevicesPinViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 2/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeviceMO+CoreDataProperties.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TCSEraseDeviceConfirmationDelegateProtocol <NSObject>

-(void)eraseDevices:(NSArray *)inDevices pin:(NSString *)inPin;


@end

@interface TCSEraseDevicesPinViewController : NSViewController
@property (strong) NSArray <DeviceMO *> * devices;
@property (assign) id <TCSEraseDeviceConfirmationDelegateProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
