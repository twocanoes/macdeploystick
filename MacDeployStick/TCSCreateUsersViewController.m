//
//  TCSCreateUsersViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 5/16/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSCreateUsersViewController.h"

@interface TCSCreateUsersViewController ()

@end

@implementation TCSCreateUsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.workflowUser) self.workflowUser=[[TCSWorkflowUser alloc] init];


}
- (IBAction)okButtonPressed:(id)sender {
    [self.delegate userUpdated:self.workflowUser];
    [self dismissViewController:self];
}
- (void)viewWillAppear{
    self.view.window.styleMask=NSWindowStyleMaskTitled;
    [self.view.window setTitle:@""];
}


- (void)controlTextDidEndEditing:(NSNotification *)obj{

    if ([obj.object tag]==121 && self.workflowUser.shortName.length==0){

        NSString *shortName = [self.workflowUser.fullName stringByReplacingOccurrencesOfString:@"\\s"
        withString:@""
           options:NSRegularExpressionSearch
             range:NSMakeRange(0, self.workflowUser.fullName.length)];

        self.workflowUser.shortName=[shortName lowercaseString];


    }
    
}
-(void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{


}
@end
