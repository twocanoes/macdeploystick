//
//  MDSPrivHelperToolController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <SecurityInterface/SFAuthorizationView.h>
#import "TCSMDSHelperReplyProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface MDSPrivHelperToolController : NSObject <NSXPCListenerDelegate>
@property (strong) SFAuthorization *authorization;
@property (weak) id <TCSMDSHelperReplyProtocol> delegate;
- (void)installToolIfNecessary;
+ (instancetype)sharedHelper ;
- (void)installInstallMacOSWithProduct:(NSDictionary *)product catalog:(NSString *)catalog addToDiskImage:(BOOL)shouldAddToDiskImage workingPath:(NSString *)workingPath withCallback:(void (^)(BOOL success))callback;
-(void)stopRunningProcessesWithCallback:(void (^)(BOOL success))callback;
-(void)updateWebserverWithConfigurations:(NSDictionary *)configurations withCallback:(void (^)(BOOL success))callback;
-(void)stopWebserverWithCallback:(void (^)(BOOL success))callback;
-(void)startWebserverWithCallback:(void (^)(BOOL success))callback;
-(void)addCertificateAtPath:(NSString *)certPath withCallback:(void (^)(NSError *err))callback;

-(void)restartWebserverWithCallback:(void (^)(BOOL success))callback;
-(void)removeUsers:(NSArray *)users fromFolder:(NSString *)path callback:(void (^)(NSError *err))callback;
-(void)createMacOSInstallVolume:(NSString *)volumePath withInstaller:(NSString *)installerPath callback:(void (^)(BOOL isDone, NSString *statusMsg,NSError * _Nullable))callback;
-(NSData *)token;
-(void)migrateFiles:(NSArray *)files withCallback:(void (^)(NSError *err))callback;
-(void)reboot:(void (^)(NSInteger))callback;

//- (void)updatePermissionsAtWorkingPath:(NSString *)workingPath withCallback:(void (^)(BOOL success))callback;
@end

NS_ASSUME_NONNULL_END
