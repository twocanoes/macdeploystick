//
//  TCSTabViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 7/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSTabViewController : NSTabViewController

@end

NS_ASSUME_NONNULL_END
