//
//  TCSArrayControllerCountTransformer.m
//  MDS
//
//  Created by Timothy Perfitt on 10/7/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSArrayControllerCountTransformer.h"

@implementation TCSArrayControllerCountTransformer

+ (Class)transformedValueClass { return [NSNumber class]; }

+ (BOOL)allowsReverseTransformation { return NO; }


- (id)transformedValue:(NSIndexSet *)value {
    return [NSNumber numberWithBool:[value count] > 0];
    
}

@end
