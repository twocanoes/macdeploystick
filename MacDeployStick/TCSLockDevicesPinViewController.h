//
//  TCSLokcDevicesPinViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 2/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeviceMO+CoreDataProperties.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TCSLockDeviceConfirmationDelegateProtocol <NSObject>

-(void)lockDevices:(NSArray *)inDevices pin:(NSString * )inPin message:(NSString * _Nullable)message phoneNumber:(NSString * _Nullable)phoneNumber;


@end

@interface TCSLockDevicesPinViewController : NSViewController
@property (strong) NSArray <DeviceMO *> * devices;
@property (assign) id <TCSLockDeviceConfirmationDelegateProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
