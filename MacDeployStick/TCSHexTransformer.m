//
//  TCSHexTransformer.m
//  MDS
//
//  Created by Timothy Perfitt on 7/23/24.
//  Copyright © 2024 Twocanoes Software. All rights reserved.
//

#import "TCSHexTransformer.h"

@implementation TCSHexTransformer
+ (Class)transformedValueClass { return [NSString class]; }

+ (BOOL)allowsReverseTransformation { return NO; }


- (id)transformedValue:(NSNumber *)value {
    return [NSString stringWithFormat:@"%lX", [value integerValue]];

}

@end
