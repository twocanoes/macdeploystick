//
//  TCSFlashArduino.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/30/18.
//  Copyright © 2018-2019 Twocanoes Software. All rights reserved.
//

#import "TCSFlashArduino.h"
#include <Carbon/Carbon.h>
@interface TCSFlashArduino ()
@property (strong) NSTimer *checkForArduinoTimer;
@property (atomic,assign) BOOL waitingToProgram;
@property (atomic,assign) BOOL isProgramming;
@property (atomic,assign) BOOL isRunning;
@property (strong) NSArray *foundDevices;
@property (strong) NSString *selectedDevice;
@property (strong) NSString *statusText;
@property (strong) NSString *firmwarePath;
@property (strong) NSString *language;

@end

@implementation TCSFlashArduino

-(void)viewWillDisappear{
    [self.checkForArduinoTimer invalidate];
}
- (void)viewWillAppear{
    self.isRunning=NO;
    TISInputSourceRef source = TISCopyCurrentKeyboardInputSource();
    NSString *layoutID = (__bridge NSString *)TISGetInputSourceProperty(source, kTISPropertyInputSourceID);

    NSBundle *mainBundle=[NSBundle mainBundle];

     NSArray *firmwares=[mainBundle pathsForResourcesOfType:@"hex" inDirectory:nil];

     [firmwares enumerateObjectsUsingBlock:^(NSString *currentFirmwarePath, NSUInteger idx, BOOL * _Nonnull stop) {

         NSString *currentLangName=[[[[firmwares[0] lastPathComponent] stringByDeletingPathExtension] componentsSeparatedByString:@"_"] lastObject];

         if ([currentLangName isEqualToString:[[layoutID componentsSeparatedByString:@"."] lastObject]]){
             *stop=YES;
             self.firmwarePath=currentFirmwarePath;
             self.language=[[layoutID componentsSeparatedByString:@"."] lastObject];
         }

     }];

    if (!self.firmwarePath){
        self.firmwarePath=[mainBundle pathForResource:[self firmwareName] ofType:@"hex"];
        self.language=@"English";
     }


}
- (void)viewDidLoad {
    [super viewDidLoad];


}
- (IBAction)createAutomatonButtonPressed:(NSButton *)sender {
    self.isRunning=YES;
    self.foundDevices=@[];
    self.checkForArduinoTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForNewDevices:) userInfo:nil repeats:NO];
    self.statusText=@"Searching for Automaton...";

}
-(void)checkForNewDevices:(id)sender{


    if (self.isRunning==NO) return;
    NSLog(@"checking for new devices");


    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSFileManager *fm=[NSFileManager defaultManager];
        NSError *err;
        NSArray *devices=[fm contentsOfDirectoryAtPath:@"/dev" error:&err];


        __block NSString *devicePath=nil;
        __block BOOL done=NO;

        [devices enumerateObjectsUsingBlock:^(NSString *currDeviceName, NSUInteger idx, BOOL * _Nonnull stop) {
            devicePath=[@"/dev" stringByAppendingPathComponent:currDeviceName];

            if ([devicePath containsString:@"cu.usbmodem"] && ![self.foundDevices containsObject:@{@"devicePath":devicePath}]){

                NSTask *task=[[NSTask alloc] init];
                task.launchPath=@"/bin/stty";
                task.arguments=@[@"-f",devicePath,@"1200"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.isProgramming=YES;
                });
                self.statusText=@"Entering Programming Mode";
                NSLog(@"putting into programming mode>>>>>>>");
                [task launch];
                [task waitUntilExit];

                sleep(1);

                [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(timedOut:) userInfo:nil repeats:NO];
            }

        }];


        devices=[fm contentsOfDirectoryAtPath:@"/dev" error:&err];
        if (!devices) {
            NSLog(@"no devices!");
            self.isProgramming=NO;
            return;
        }


        [devices enumerateObjectsUsingBlock:^(NSString *currDeviceName, NSUInteger idx, BOOL * _Nonnull stop) {

            devicePath=[@"/dev" stringByAppendingPathComponent:currDeviceName];

            if ([devicePath containsString:@"cu.usbmodem"] && ![self.foundDevices containsObject:@{@"devicePath":devicePath}]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.statusText=@"Programming...";
                    self.isProgramming=YES;
                });
                NSBundle *mainBundle=[NSBundle mainBundle];

                NSString *avrdudeFolder=[mainBundle pathForResource:@"avrdude" ofType:@""];
                NSString *avrDude=[[[[mainBundle bundlePath] stringByAppendingPathComponent:@"Contents"] stringByAppendingPathComponent:@"macOS"] stringByAppendingPathComponent:@"avrdude"];
                NSString *avrDudeConf=[avrdudeFolder stringByAppendingPathComponent:@"avrdude.conf"];


                NSTask *task=[[NSTask alloc] init];
                task.launchPath=avrDude;
                NSString *operation=[NSString stringWithFormat:@"flash:w:%@",self.firmwarePath];
                task.arguments=@[@"-p",@"atmega32u4",@"-U",operation,@"-c",@"avr109",@"-P",devicePath,@"-C",avrDudeConf,@"-q",@"-q"];

                [task launch];
                [task waitUntilExit];

                if (task.terminationStatus==0){
                    done=YES;

                    dispatch_async(dispatch_get_main_queue(), ^{

                        self.waitingToProgram=NO;

                        if (task.terminationStatus==0){
                            self.isProgramming=NO;
                            self.isRunning=NO;
                            NSAlert *alert=[[NSAlert alloc] init];
                            alert.messageText=@"Flash successful";
                            alert.informativeText=@"The Arduino automaton was successfully flashed. Please disconnect from the USB port now.";
                            [alert runModal];
                            [self dismissController:self];
                            *stop=YES;
                        }
                        else {
                            self.isProgramming=NO;
                        }


                    });


                }
                else {
                    devicePath=nil;
                }
            }
            else {
                devicePath=nil;
            }
        }];
        if (done==NO) {
//            self.isProgramming=NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"scheduling new timer");
                self.checkForArduinoTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForNewDevices:) userInfo:nil repeats:NO];

            });
        }


    });
}

-(void)timedOut:(id)not{
    dispatch_async(dispatch_get_main_queue(), ^{


        self.isRunning=NO;
        self.isProgramming=NO;
        [self.checkForArduinoTimer invalidate];
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Error Creating Automaton";
        alert.informativeText=@"The programming timed out. Please unplug and plug in the automaton and try again";
        [alert runModal];
        [self dismissController:self];
    });

}
-(NSString *)firmwareName{
    return @"arduino_firmware";
}


@end
