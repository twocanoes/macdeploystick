//
//  TCSConfigureAutomaton.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/31/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSConfigureAutomaton : NSViewController
@property (assign) BOOL dataLoaded;
-(NSInteger)sendSettings:(void *)inSettings;
-(void)completSendSettingsWithSuccess:(BOOL)success;
@end

NS_ASSUME_NONNULL_END
