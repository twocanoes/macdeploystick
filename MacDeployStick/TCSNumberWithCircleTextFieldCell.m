//
//  TCSNumberWithCircleTextFieldCell.m
//  MDS
//
//  Created by Timothy Perfitt on 1/10/22.
//  Copyright © 2022 Twocanoes Software. All rights reserved.
//

#import "TCSNumberWithCircleTextFieldCell.h"

@implementation TCSNumberWithCircleTextFieldCell
- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView {
    NSImage *image=[NSImage imageNamed:@"badge"];

    [image setFlipped:YES]; //image need to be flipped
    NSRect imageRect=NSMakeRect(0, 0, image.size.width, image.size.height);
    int newWidth=image.size.height/image.size.width * cellFrame.size.width;
    [image drawInRect:NSMakeRect((cellFrame.size.width-newWidth)/2, 0,newWidth ,cellFrame.size.height) fromRect:imageRect operation:NSCompositeSourceOver fraction:1.0];
    [super drawWithFrame:cellFrame inView:controlView];



}
@end
