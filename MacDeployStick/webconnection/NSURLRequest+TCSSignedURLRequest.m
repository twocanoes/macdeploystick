//
//  NSURLRequest+TCSSignedURLRequest.m
//  webconnection
//
//  Created by Steve Brokaw on 3/28/14.
//  Copyright (c) 2014 Twocanoes Softoware, inc. All rights reserved.
//

#import "NSURLRequest+TCSSignedURLRequest.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSDate+RFC1123.h"
#import "NSData+Base64.h"

NSString *signatureForDataWithKey(NSData *data, NSString *key);
NSString *signatureForDataWithKey(NSData *data, NSString *key)
{
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data bytes];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, [data length], cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    NSString *hash = [HMAC base64EncodedString];
    return hash;

}

@implementation NSURLRequest (TCSSignedURLRequest)

+ (instancetype)signedRequestWithURL:(NSURL *)URL method:(NSString *)method payload:(NSDictionary *)payload authKey:(NSString *)authKey authId:(NSString *)authId
{
    NSMutableData *signatureData = [NSMutableData data];
    
    NSDate *today = [NSDate date];
    NSString *dateHeader = [today rfc1123String];
    NSData *dateHeaderData = [dateHeader dataUsingEncoding:NSUTF8StringEncoding];
    [signatureData appendData:dateHeaderData];
    
    NSData *typeData = [[method lowercaseString] dataUsingEncoding:NSUTF8StringEncoding];
    
    [signatureData appendData:typeData];
    
    NSString *path = [URL path];
    NSData *urlData = [[path lowercaseString] dataUsingEncoding:NSUTF8StringEncoding];
    [signatureData appendData:urlData];
    
    //self.webData = [NSMutableData data];
    
    NSError *error;
    NSString *httpMethod = [method uppercaseString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];
    
    NSSet *validMethods = [NSSet setWithObjects:@"GET", @"POST", @"PUT", @"DELETE", nil];
    
    if (![validMethods containsObject:httpMethod]) {
        return nil;
    }
    
    [request setHTTPMethod:httpMethod];
    if (payload && ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"])) {
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
        
        [signatureData appendData:jsonData];
        
        NSString *resultAsString = [jsonData base64EncodedString];
        
        NSData *requestData = [NSData dataWithBytes:[resultAsString UTF8String] length:strlen([resultAsString UTF8String])];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", (int)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
    }
    
    if (authId && authKey) {
        NSString *signature = signatureForDataWithKey(signatureData, authKey);
        
        if (signature==nil) {
            return nil;
        }
        
        NSString *authWithKey = [NSString stringWithFormat:@"TCAPI:%@:%@", authId, signature];
        [request setValue:authWithKey forHTTPHeaderField:@"Authorization"];
        [request setValue:dateHeader forHTTPHeaderField:@"Date"];
        
    }
    return request;
}

+ (instancetype)signedGETRequestWithURL:(NSURL *)URL authKey:(NSString *)authKey authId:(NSString *)authId
{
    return [[self class] signedRequestWithURL:URL method:@"GET" payload:nil authKey:authKey authId:authId];
}

+ (instancetype)signedPOSTRequestWithURL:(NSURL *)URL payload:(NSDictionary *)payload authKey:(NSString *)authKey authId:(NSString *)authId
{
    return [[self class] signedRequestWithURL:URL method:@"POST" payload:payload authKey:authKey authId:authId];
}
@end
