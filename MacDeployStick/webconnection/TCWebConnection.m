//
//  TCWebConnection.m
//  Locamotion
//
//  Created by Tim Perfitt on 9/15/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import "TCWebConnection.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSData+Base64.h"
#import "NSDate+RFC1123.h"
#import "NSURLRequest+TCSSignedURLRequest.h"
#import "NSError+EasyError.h"
NSString * const TCWebConnectionNotification = @"TCWebConnectionNotification";

@interface TCWebConnection ()

@property (strong) TCWebCompletionBlock completionBlock;
@property (strong) TCWebErrorBlock errorBlock;
@property (strong) NSMutableData *webData;
@property (strong) NSURLConnection *connection;
@property (assign) NSInteger lastHTTPCode;
@property (strong) NSURL *url;

@end

@implementation TCWebConnection

-(id)initWithBaseURL:(NSURL *)baseURL
{
    
    if (self=[super init]) {
        self.url = baseURL;
    }
    return self;
    
}

-(NSString *)signatureForData:(NSData *)data withKey:(NSString *)key
{
    
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data bytes];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, [data length], cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    NSString *hash = [HMAC base64EncodedString];
    return hash;
}



-(NSInteger)httpCode
{
    return self.lastHTTPCode;
}

- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
        basicAuthUsername:(NSString *)username
        basicAuthPassword:(NSString *)password
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock
{
    return [self sendRequestToPath:path type:method payload:payload authId:nil authKey:nil basicAuthUsername:username basicAuthPassword:password bearer:nil onCompletion:completionBlock onError:errorBlock];


}
- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
                   authId:(NSString *)authId
                  authKey:(NSString *)authKey
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock
{
    return [self sendRequestToPath:path type:method payload:payload authId:authId authKey:authKey basicAuthUsername:nil basicAuthPassword:nil bearer:nil onCompletion:completionBlock onError:errorBlock];


}
- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
                   authId:(NSString *)authId
                  authKey:(NSString *)authKey
        basicAuthUsername:(NSString *)username
        basicAuthPassword:(NSString *)password
                   bearer:(NSString *)bearerToken
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock
{
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    NSMutableData *signatureData = [NSMutableData data];
    
    NSDate *today = [NSDate date];
    NSString *dateHeader = [today rfc1123String];
    NSData *dateHeaderData = [dateHeader dataUsingEncoding:NSUTF8StringEncoding];
    [signatureData appendData:dateHeaderData];
    
    NSData *typeData = [[method lowercaseString] dataUsingEncoding:NSUTF8StringEncoding];
    
    [signatureData appendData:typeData];
    
    NSData *urlData = [[path lowercaseString] dataUsingEncoding:NSUTF8StringEncoding];
    [signatureData appendData:urlData];
    
    self.webData = [NSMutableData data];
    
    NSError *error;
    NSString *httpMethod = [method uppercaseString];
    NSURL *endpointURL = [NSURL URLWithString:path relativeToURL:self.url];
    NSString *urlString = [endpointURL absoluteString];
    
    NSURL *requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];
    
    NSSet *validMethods = [NSSet setWithObjects:@"GET", @"POST", @"PUT", @"DELETE", nil];
    if (![validMethods containsObject:httpMethod]) {
        return NO;
    }
    
    [request setHTTPMethod:httpMethod];
    if (payload && ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"] ||  [httpMethod isEqualToString:@"DELETE"])) {
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
        
//        [signatureData appendData:jsonData];
        
//        NSString *resultAsString = [jsonData base64EncodedString];

//        NSData *requestData = [NSData dataWithBytes:[resultAsString UTF8String] length:strlen([resultAsString UTF8String])];

        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", (int)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: jsonData];
    }
    
    if (authId && authKey) {
        NSString *signature = [self signatureForData:signatureData withKey:authKey];
        
        if (signature==nil) {
            return NO;
        }
        
        NSString *authWithKey = [NSString stringWithFormat:@"TCAPI:%@:%@", authId, signature];
        [request setValue:authWithKey forHTTPHeaderField:@"Authorization"];
        [request setValue:dateHeader forHTTPHeaderField:@"Date"];
        
    }
    if (bearerToken){

        NSString *authWithKey = [NSString stringWithFormat:@"bearer %@", bearerToken];
        [request setValue:authWithKey forHTTPHeaderField:@"Authorization"];

    }
    if (username && password){
        NSString *usernamePassword=[NSString stringWithFormat:@"%@:%@",username,password];
        NSData *userPasswordData=[usernamePassword dataUsingEncoding:NSUTF8StringEncoding];

        NSString *base64EncodedUsernamePassword=[userPasswordData base64EncodedString];

        NSString *basicAuthHeader = [NSString stringWithFormat:@"Basic %@",base64EncodedUsernamePassword];
        [request setValue:basicAuthHeader forHTTPHeaderField:@"Authorization"];
    }

    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [self.connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];

    [self.connection start];
    return YES;

}
-(void)dealloc{


}
- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection{


    return  NO;
}

- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
               useAPIAuth:(BOOL)authRequired
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock
{
    if (authRequired) {
        return NO;
    }
    return [self sendRequestToPath:path type:method payload:payload authId:nil authKey:nil onCompletion:completionBlock onError:errorBlock];
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.webData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    self.lastHTTPCode = (int)[(NSHTTPURLResponse*)httpResponse statusCode];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if (self.lastHTTPCode>399){

        NSError *error=[NSError easyErrorWithTitle:@"HTTP error returned"
                                              body:[NSString stringWithFormat:@"The remote webserver returned an error. Please check the log and try again. Error code: %li",self.lastHTTPCode]
                                              line:__LINE__
                                              file:@__FILE__];



        self.errorBlock(error);
    }
    else {
        self.completionBlock(self.lastHTTPCode, self.webData);
    }
    
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return cachedResponse;
    
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse{
    return request;
}

- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    
}



#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.errorBlock(error);
}


@end
