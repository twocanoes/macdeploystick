#!/bin/bash
#Copyright 2018-2020 Twocanoes Software, Inc
#loop over empty directories instead of weird "*"

shopt -s nullglob

if [ -e ./com_twocanoes_mds_workflow_script_config.sh  ]; then
    source ./com_twocanoes_mds_workflow_script_config.sh
fi

#thanks to Pico Mitchell for write_prefs feature
write_prefs(){

    local folder=$1

    if [ -z "${folder}" ];  then
        echo "you must specify a path to write prefs"
        exit -1
    fi
    darwin_major_version="$(uname -r | cut -d '.' -f 1)" # 17 = 10.13, 18 = 10.14, 19 = 10.15, 20 = 11.0, etc.

    # The following keys can be retrieved/verified with the command: strings '/System/Library/CoreServices/Setup Assistant.app/Contents/MacOS/Setup Assistant' | grep '^DidSee' | sort
    # These are the keys that are common to all macOS version 10.13 and newer. Other OS version specific keys will be added below.
    setupassistant_did_see_pref_keys=( 'DidSeeApplePaySetup' 'DidSeeAvatarSetup' 'DidSeeCloudSetup' 'DidSeeiCloudLoginForStorageServices' 'DidSeePrivacy' 'DidSeeSiriSetup' 'DidSeeSyncSetup' 'DidSeeSyncSetup2' 'DidSeeTouchIDSetup' )

    if (( darwin_major_version == 17 )); then
        setupassistant_did_see_pref_keys+=( 'DidSeeTrueTonePrivacy' ) # Only exists in macOS 10.13, I believe it was a typo and replaced with "DidSeeTrueTone" in macOS 10.14.
    elif (( darwin_major_version >= 18 )); then
        setupassistant_did_see_pref_keys+=( 'DidSeeAppearanceSetup' 'DidSeeTrueTone' ) # Added in macOS 10.14.

        if (( darwin_major_version >= 19 )); then
            setupassistant_did_see_pref_keys+=( 'DidSeeActivationLock' 'DidSeeScreenTime' ) # Added in macOS 10.15.

            if (( darwin_major_version >= 20 )); then
                setupassistant_did_see_pref_keys+=( 'DidSeeAccessibility' 'DidSeeAppStore' ) # Added in macOS 11.

                if (( darwin_major_version >= 24 )); then
                    setupassistant_did_see_pref_keys+=( 'DidSeeIntelligence' ) # Added in macOS 15.
                fi
            fi

        fi
    fi

    for this_setupassistant_did_see_pref_key in "${setupassistant_did_see_pref_keys[@]}"; do
        defaults write "${folder}/Library/Preferences/com.apple.SetupAssistant" "${this_setupassistant_did_see_pref_key}" -bool true
    done

    # The following keys can be retrieved/verified with the command: strings '/System/Library/CoreServices/Setup Assistant.app/Contents/MacOS/Setup Assistant' | grep '^LastSeen' | sort
    defaults write "${folder}/Library/Preferences/com.apple.SetupAssistant" LastSeenBuddyBuildVersion -string "$(sw_vers -buildVersion)"

    # These are the keys that are common to all macOS version 10.13 and newer. Other OS version specific keys will be added below.
    setupassistant_last_seen_pref_keys=( 'LastSeenCloudProductVersion' 'LastSeeniCloudStorageServicesProductVersion' 'LastSeenSyncProductVersion' )

    if (( darwin_major_version >= 19 )); then
        setupassistant_last_seen_pref_keys+=( 'LastSeenDiagnosticsProductVersion' 'LastSeenSiriProductVersion' ) # Added in macOS 10.15.
    fi

    os_version="$(sw_vers -productVersion)"

    for this_setupassistant_last_seen_pref_key in "${setupassistant_last_seen_pref_keys[@]}"; do
        defaults write "${folder}/Library/Preferences/com.apple.SetupAssistant" "${this_setupassistant_last_seen_pref_key}" -string "${os_version}"
    done

}

report(){
    local message=$1
    local status=$2

    echo $message:$status
    if [ ${network_available} ]; then
        /usr/bin/curl -X POST  --data-urlencode "message=${message}" --data-urlencode "serial=${serial}" --data-urlencode "status=${status}" "${logging_url_string}"
    fi

}
##
# Determine if the network is up by looking for any non-loopback
# internet network interfaces.
##
CheckForNetwork()
{

    local test

    test=$(ifconfig -a inet 2>/dev/null | sed -n -e '/127.0.0.1/d' -e '/0.0.0.0/d' -e '/inet/p' | wc -l)
    if [ "${test}" -gt 0 ]; then
        NETWORKUP="YES"
    else
        NETWORKUP="NO"
    fi
}


SCRIPTDIR=$(/bin/pwd)

serial=$(ioreg -l | awk '/IOPlatformSerialNumber/ { print $4;}'|tr -d '\"')

if [ -n "${workflow_should_configure_wifi}" ] && [ -n "${workflow_wifi_ssid}" ] && [ -n "${workflow_wifi_password}" ] ; then

    network_setup_path="/usr/sbin/networksetup"

    for this_network_interface in $("${network_setup_path}" -listallhardwareports 2> /dev/null | sort | grep '^Device: ' | cut -c 9-); do
        if /usr/sbin/networksetup -getairportnetwork "${this_network_interface}" &> /dev/null; then
            /usr/sbin/networksetup -setairportnetwork "${this_network_interface}" "${workflow_wifi_ssid}" "${workflow_wifi_password}"
        fi
    done

    sleep 10
fi
if [ -n "${workflows_should_wait_for_network}" ] ; then

        for i in {1..36}; do

            CheckForNetwork
            if [ "${NETWORKUP}" == "YES" ]; then
                echo "Network is up"
                break
            else
                echo "Network not up. sleeping for 5 seconds."
                sleep 5
            fi

        done

fi
if [ -n "${workflow_should_create_user}" ] ; then


    report "adding ssh keys" "script"

    if [ -d "$2/sshkeys" ]; then
        /usr/sbin/createhomedir -c

        pushd "$2/sshkeys"
        for key_file_name in *; do
            echo "creating ssh key for ${key_file_name}"
            user_home="/Users/${key_file_name}"
            /bin/mkdir -p "${user_home}/.ssh"
            /bin/cp "${key_file_name}" "${user_home}"/.ssh/authorized_keys
            /bin/chmod 600 "${user_home}"/.ssh/authorized_keys
            /bin/chmod 700 "${user_home}"/.ssh
            /usr/sbin/chown -R "${key_file_name}:staff" "${user_home}"/.ssh
        done

        popd
        /bin/echo "allowing admin to reboot"
        /bin/echo "%admin ALL=(ALL) NOPASSWD:/sbin/reboot" >> /etc/sudoers.d/reboot

    fi


    if [ -n "${workflow_create_password_hint}" ]; then
        /usr/bin/dscl . -merge "${user_home}" hint "${workflow_create_password_hint}"
            /usr/bin/defaults write com.apple.loginwindow RetriesUntilHint -int 3
    fi
fi

/bin/echo "checking for scripts in $2/com.twocanoes.mds.scripts"

if [ -d "$2/com.twocanoes.mds.scripts" ]; then

    /bin/echo "folder exists. going into it"
    cd "$2/com.twocanoes.mds.scripts"

    /bin/echo "looping over scripts"

    for current_script in ./com.twocanoes.mds-pre* ; do
        /bin/echo "checking if ${current_script} is a file and is executable"
        if [ -f "${current_script}" ] && [ -x "${current_script}" ]; then
              report "running script ${current_script} $1 $2 $3" "script"
             "${current_script}" "$1" "$2" "$3"
        else
            /bin/echo "${current_script} is not a file or exectuable"
            /bin/ls -la "${current_script}"
        fi
    done
fi




if [ -n "${logging_url_string}" ]; then
    if curl --connect-timeout 5 "${logging_url_string}" ; then
        network_available=1
    fi
fi


if [ -n "${worksflows_should_enable_password_hints}" ]; then

    /usr/bin/defaults write com.apple.loginwindow RetriesUntilHint -int 3

fi
if [ -n "${workflows_should_trust_munki_client_certificate}" ]; then

    /usr/bin/security add-trusted-cert -r trustRoot -d -k "/Library/Keychains/System.keychain" -e hostnameMismatch "$2/munki_certificate.pem"

fi

if [ -n "${workflows_should_trust_server_certificate}" ]; then

    /usr/bin/security add-trusted-cert -r trustRoot -d -k "/Library/Keychains/System.keychain" -e hostnameMismatch "$2/server_certificate.pem"

fi

if [ -n "${workflows_munki_workflow_url}" ]; then

/usr/bin/defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL "${workflows_munki_workflow_url}"

fi
echo "Setting Variables from NVRAM if needed"
for num in 1 2 3 4 5 6 7 8 9; do
    VALUE=$(/usr/sbin/nvram "com.twocanoes.mds.var${num}"| sed $'s/com.twocanoes.mds.var.*\t//')
    if [ "${VALUE}" != "" ]; then
        declare -x "mds_var${num}"="${VALUE}"
    fi
done

echo "setting workflow name from NVRAM if needed"

WORKFLOWNAME_VALUE=$(/usr/sbin/nvram "com.twocanoes.mds.workflowname"| sed $'s/com.twocanoes.mds.workflowname\t//')
if [ "${WORKFLOWNAME_VALUE}" != "" ]; then
    declare -x "mds_workflowname"="${WORKFLOWNAME_VALUE}"
fi





if [ -n "${workflows_should_set_computer_name}" ]; then
    report "setting computer name" "configuration"

    NAME=$(/usr/sbin/nvram com.twocanoes.mds.ComputerName| sed $'s/com.twocanoes.mds.ComputerName\t//')

    RESOLVED_NAME=$(echo "$NAME")


    echo setting computer name to $NAME
    if [ -n "${RESOLVED_NAME}" ]; then
        /usr/sbin/scutil --set ComputerName "$RESOLVED_NAME"
        if [ "$?" -ne 0 ] ; then

            sleep 30
            /usr/sbin/scutil --set ComputerName "$RESOLVED_NAME"

        fi
        /usr/sbin/scutil --set HostName "$RESOLVED_NAME"
        /usr/sbin/scutil --set LocalHostName "$RESOLVED_NAME"
        /usr/bin/defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server.plist NetBIOSName "$RESOLVED_NAME"
    fi

    /usr/sbin/nvram -d com.twocanoes.mds.ComputerName

fi

if [ -d "$2/com.twocanoes.mds.scripts" ]; then


    cd "$2/com.twocanoes.mds.scripts"

    for current_script in ./* ; do
        if [[ "${current_script}" != "com.twocanoes.mds-pre"* ]]; then
            if [ -f "${current_script}" ] && [ -x "${current_script}" ]; then
                  report "running script ${current_script} $1 $2 $3" "script"
                 "${current_script}" "$1" "$2" "$3"
            fi
        fi
    done
fi
if [ -n "${workflows_remove_nvram_vars}" ]; then
    echo "removing NVRAM variables if needed"
    for num in 1 2 3 4 5 6 7 8 9; do
        /usr/sbin/nvram -d "com.twocanoes.mds.var${num}"
    done
fi

if [ -d "$2/com.twocanoes.mds.profiles" ]; then
    cd "$2/com.twocanoes.mds.profiles"
    for i in ./*.mobileconfig; do
        report "installing profile ${i}" "profile"

        profiles install -type configuration -path "$i"
    done
fi
if [ -n "${workflows_should_enable_ard}" ]; then
    report "enabling ARD" "configuration"

   /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -allowAccessFor -allUsers -privs -all -clientopts -setmenuextra -menuextra yes
    /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -restart -agent

fi


if [ -n "${workflows_should_enable_ssh}" ]; then
    report "enabling SSH" "configuration"

    /bin/launchctl load -w /System/Library/LaunchDaemons/ssh.plist

fi


if [ -n "${workflows_should_enable_screen_sharing}" ]; then
    report "enabling screen sharing" "configuration"

    /bin/launchctl enable system/com.apple.screensharing
    /bin/launchctl bootstrap system /System/Library/LaunchDaemons/com.apple.screensharing.plist
fi

if [ -n "${workflows_should_skip_setup_assistant}" ]; then
    report "touching AppleSetupDone to skip setup assistant" "configuration"
    /usr/bin/touch /private/var/db/.AppleSetupDone

        if [ -n "${workflows_should_enable_location_services}" ]; then
            report "Enabling location services" "configuration"

            defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd LocationServicesEnabled -int 1
    fi

fi



if [ -n "${workflows_should_skip_privacy_setup}" ]; then

darwin_major_version="$(uname -r | cut -d '.' -f 1)" # 17 = 10.13, 18 = 10.14, 19 = 10.15, 20 = 11.0, etc.

# The following keys can be retrieved/verified with the command: strings '/System/Library/CoreServices/Setup Assistant.app/Contents/MacOS/Setup Assistant' | grep '^DidSee' | sort
# These are the keys that are common to all macOS version 10.13 and newer. Other OS version specific keys will be added below.
setupassistant_did_see_pref_keys=( 'DidSeeApplePaySetup' 'DidSeeAvatarSetup' 'DidSeeCloudSetup' 'DidSeeiCloudLoginForStorageServices' 'DidSeePrivacy' 'DidSeeSiriSetup' 'DidSeeSyncSetup' 'DidSeeSyncSetup2' 'DidSeeTouchIDSetup' )

if (( darwin_major_version == 17 )); then
    setupassistant_did_see_pref_keys+=( 'DidSeeTrueTonePrivacy' ) # Only exists in macOS 10.13, I believe it was a typo and replaced with "DidSeeTrueTone" in macOS 10.14.
elif (( darwin_major_version >= 18 )); then
    setupassistant_did_see_pref_keys+=( 'DidSeeAppearanceSetup' 'DidSeeTrueTone' ) # Added in macOS 10.14.

    if (( darwin_major_version >= 19 )); then
        setupassistant_did_see_pref_keys+=( 'DidSeeActivationLock' 'DidSeeScreenTime' ) # Added in macOS 10.15.

        if (( darwin_major_version >= 20 )); then
            setupassistant_did_see_pref_keys+=( 'DidSeeAccessibility' 'DidSeeAppStore' ) # Added in macOS 11.

            if (( darwin_major_version >= 24 )); then
                setupassistant_did_see_pref_keys+=( 'DidSeeIntelligence' ) # Added in macOS 15.
            fi
        fi
    fi
fi


    report "writing preferences to skip privacy setup" "configuration"
    user_template_dir="$3/Library/User Template"

if [ ! -d "${user_template_dir}" ] ; then
    user_template_dir="$3/System/Library/User Template"

fi

    for USER_TEMPLATE in "${user_template_dir}"/*
    do
        write_prefs "${USER_TEMPLATE}"
    done

    # Checks first to see if the Mac is running 10.7.0 or higher.
    # If so, the script checks the existing user folders in /Users
    # for the presence of the Library/Preferences directory.
    #
    # If the directory is not found, it is created and then the
    # iCloud, Data & Privacy, Diagnostic and Siri pop-up settings
    # are set to be disabled.

    for USER_HOME in "$3/Users"/*
    do
        USER_UID=`basename "${USER_HOME}"`
        if [ ! "${USER_UID}" = "Shared" ]; then
            if [ ! -d "${USER_HOME}"/Library/Preferences ]; then
                for this_setupassistant_did_see_pref_key in "${setupassistant_did_see_pref_keys[@]}"; do
                    defaults write "${USER_HOME}/Library/Preferences/com.apple.SetupAssistant" "${this_setupassistant_did_see_pref_key}" -bool true
                done

                # The following keys can be retrieved/verified with the command: strings '/System/Library/CoreServices/Setup Assistant.app/Contents/MacOS/Setup Assistant' | grep '^LastSeen' | sort
                defaults write "${USER_HOME}/Library/Preferences/com.apple.SetupAssistant" LastSeenBuddyBuildVersion -string "$(sw_vers -buildVersion)"

                # These are the keys that are common to all macOS version 10.13 and newer. Other OS version specific keys will be added below.
                setupassistant_last_seen_pref_keys=( 'LastSeenCloudProductVersion' 'LastSeeniCloudStorageServicesProductVersion' 'LastSeenSyncProductVersion' )

                if (( darwin_major_version >= 19 )); then
                    setupassistant_last_seen_pref_keys+=( 'LastSeenDiagnosticsProductVersion' 'LastSeenSiriProductVersion' ) # Added in macOS 10.15.
                fi

                os_version="$(sw_vers -productVersion)"

                for this_setupassistant_last_seen_pref_key in "${setupassistant_last_seen_pref_keys[@]}"; do
                    defaults write "${USER_HOME}/Library/Preferences/com.apple.SetupAssistant" "${this_setupassistant_last_seen_pref_key}" -string "${os_version}"
                done
                /bin/mkdir -p "${USER_HOME}"/Library/Preferences
                /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library
                /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library/Preferences
            fi
            if [ -d "${USER_HOME}"/Library/Preferences ]; then

                write_prefs "${USER_HOME}"
                /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant.plist
            fi
        fi
    done
fi


arch=$(/usr/bin/arch)

if [ "${arch}" == "arm64" ]; then
    echo "Apple Silicon so checking if we should install rosetta..."
    if [ "${workflow_should_skip_rosetta}" == "1" ] ; then
            echo "Skipping rosetta install"

    else
            echo "arm64. Installing rosetta."

        for i in 1 2 3 4 5 6 7 8 9 10; do

            if  /usr/sbin/softwareupdate --install-rosetta --agree-to-license ; then
                break;
            fi
            echo "error installing rosetta. sleeping and then trying again (10 tries max)"
            sleep 1

        done


    fi
fi


if [ -n "${worksflows_should_run_software_update}" ]; then
    report "running software update -i -a"
    /usr/sbin/softwareupdate -i -a
fi


if [ -n "${worksflows_should_reboot}" ]; then
    report "script to install profiles and run scripts complete. Rebooting" "success"

    report "rebooting in 1 minute"
    /sbin/shutdown -r +1m
elif [ -n "${worksflows_should_shutdown}" ]; then
    report "script to install profiles and run scripts complete. Shutting down." "success"

    report "shutting down in 1 minute"
    /sbin/shutdown -h +1m

else
    report "script to install profiles and run scripts complete" "success"

fi


