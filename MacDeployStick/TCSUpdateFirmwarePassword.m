//
//  TCSUpdateFirmwarePassword.m
//  MDS
//
//  Created by Timothy Perfitt on 9/4/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSUpdateFirmwarePassword.h"

@interface TCSUpdateFirmwarePassword ()

@end

@implementation TCSUpdateFirmwarePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)okButtonPressed:(id)sender {

    [self.delegate updatePassword:self.updatedPassword priorPassword:self.priorPassword forDevices:self.devices];
    [self dismissViewController:self];


}

@end
