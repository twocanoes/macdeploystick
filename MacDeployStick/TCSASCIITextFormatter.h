//
//  TCSASCIITextFormatter.h
//  MDS
//
//  Created by Timothy Perfitt on 9/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSASCIITextFormatter : NSFormatter

@end

NS_ASSUME_NONNULL_END
