//
//  TCSNewPasswordViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 9/4/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TCSPasswordUpdatedProtocol <NSObject>

-(void)updatePassword:(NSString *)newPassword forUserGUID:(NSString *)userGUID;

@end
@interface TCSNewPasswordViewController : NSViewController
@property (strong) NSString *updatedPassword;
@property (strong) NSString *userGUID;
@property (assign) id <TCSPasswordUpdatedProtocol> delegate;
@end

NS_ASSUME_NONNULL_END
