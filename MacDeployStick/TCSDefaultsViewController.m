//
//  TCSDefaultsViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/5/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDefaultsViewController.h"
#import <SecurityInterface/SFChooseIdentityPanel.h>
#import <SecurityInterface/SFCertificatePanel.h>
#import "DeploySettings.h"
#import "TCSUtility.h"
#import "TCSDefaultsManager.h"
#import "NSData+Base64.h"
#import "TCSConfigHelper.h"
#import "TCSecurity.h"
#import "TCSWebServiceController.h"
#import "TCSServiceViewController.h"
#import "SubscriptionManager.h"
#import "MDSPrivHelperToolController.h"
#import "NSData+PEM.h"
#define WEBSERVICE @"webservice"
#import "MDSPrivHelperToolController.h"
#define SERVICES @[WEBSERVICE]
@interface TCSDefaultsViewController ()
@property (weak) IBOutlet NSTabView *defaultsTabView;
@property (assign) BOOL sslInfoChanged;
@property (strong) NSMutableArray *servicesToStart;
@property (strong) NSString *backupFolder;
@property (strong) NSString *exportFolder;
@property (weak) IBOutlet SFAuthorizationView *authenticationView;
@property (assign) BOOL isExporting;
@end

@implementation TCSDefaultsViewController 
- (IBAction)signPackagesButtonPressed:(NSButton*)button {

//    if (button.state == NSControlStateValueOn){
//        if ([[SubscriptionManager shared] isSubscribed]==NO){
//
//            NSString *message=@"Signing Packages requires Enterprise or Pro edition. Please upgrade or deselect active workflows";
//            [[SubscriptionManager shared] showSubcribeWithMessage:message];
//            NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
//            [ud setBool:NO forKey:@"shouldSignPackages"];
//
//        }
//    }

}

-(void)viewDidAppear{
    
    [self.view.window setMinSize:NSMakeSize(800,500)];
    
}

-(IBAction)clearTempFiles:(id)sender{
    
    [[TCSDefaultsManager sharedManager] clearCache];
}

-(void)viewWillAppear{
    self.sslInfoChanged=NO;
    [[NSUserDefaults standardUserDefaults] addObserver:self
                                            forKeyPath:@"serverHostname"
                                               options:NSKeyValueObservingOptionNew
                                               context:NULL];
    

    
    [[NSUserDefaults standardUserDefaults] addObserver:self
                                            forKeyPath:@"tlsCertificateFolder"
                                               options:NSKeyValueObservingOptionNew
                                               context:NULL];

}
-(void)viewWillDisappear{
    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"serverHostname"];
    
    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"tlsCertificateFolder"];

    
    if (self.sslInfoChanged) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SSLINFOCHANGED object:self];
    }
    
}
-(void)showAuthNeededError{

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Authentication Required";
    alert.informativeText=@"Authentication is required to stop services. Please click the lock below for authorization.";

    [alert addButtonWithTitle:@"OK"];
    [alert runModal];

    return;

}
- (IBAction)exportSettings:(id)sender {
    if (self.authenticationView.authorizationState==SFAuthorizationViewLockedState){

        [self showAuthNeededError];
        return;
    }


    self.isExporting=YES;
    self.servicesToStart=[NSMutableArray array];
    NSSavePanel *savePanel=[NSSavePanel savePanel];

    savePanel.message=@"Please provide a filename and location to save the export file:";
    NSModalResponse res=[savePanel runModal];

    if (res!=NSModalResponseOK) {

        return;
    }
    NSURL *saveURL=savePanel.URL;
    //save prefs
    //save application support
    //check for web, stop, make copy


    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *err;

    if ([fm fileExistsAtPath:saveURL.path]){

        if ([fm removeItemAtPath:saveURL.path error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];

            return;
        }
    }

    self.backupFolder=saveURL.path.stringByDeletingPathExtension;
    if([fm createDirectoryAtPath:self.backupFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

        [[NSAlert alertWithError:err] runModal];
        return;
    }

    [self stopServices:SERVICES];

}
-(void)stopServices:(NSArray *)services{

    if (services.count==0){

        if (self.isExporting==YES){
            [self backupFiles];
        }
        else {

            [self importItems];
        }
        return;
    }
    NSString *currService=[services objectAtIndex:0];

    NSMutableArray *remainingArray=[NSMutableArray arrayWithArray:services];
    [remainingArray removeObjectAtIndex:0];


    if ([currService isEqualToString:WEBSERVICE]){
        if ([[TCSWebServiceController sharedController] isRunning]==YES){
            [[TCSWebServiceController sharedController] stopWebServicesWithCallback:^(BOOL success, BOOL didCancel) {
                if (success==NO){
                    dispatch_async(dispatch_get_main_queue(), ^{

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Error Stopping Webserver";
                        alert.informativeText=@"There was an error stopping the web service.";

                        [alert addButtonWithTitle:@"OK"];
                        [alert runModal];
                    });
                    return;

                }
                else {
                    [self.servicesToStart addObject:WEBSERVICE];
                    [self stopServices:[NSArray arrayWithArray:remainingArray]];
                }

            }];
        }
        else {
            [self stopServices:[NSArray arrayWithArray:remainingArray]];
        }

    }

}
-(void)backupFiles{
    NSError *err;
    NSFileManager *fm=[NSFileManager defaultManager];
    [[TCSDefaultsManager sharedManager] clearCache];

    NSString *appSupport=[TCSConfigHelper applicationSupportPath];
    if (appSupport){
        if([fm copyItemAtPath:appSupport toPath:[self.backupFolder stringByAppendingPathComponent:@"Application Support"] error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;

        }
    }

    NSArray <NSString *> *keysToSave=@[@"autorunWorkflowName",@"depProfile",@"imagr_url",@"tlsCertificateFolder",@"diskImageVolumeName",@"autorunWorkflowTimeout",@"webserverPort",@"tempFolder",@"loggingInfoURLString",@"identityKeyPath",@"certificatePath", @"recoveryWiFiPassword",@"recoveryWiFiSSID",@"serverHostname", @"shouldAutomaticallyRunWorkflow", @"shouldConnectToWiFi",@"shouldSendLoggingInfo", @"shouldSignPackages", @"shouldSkipIfAlreadySigned", @"shouldWaitForNetwork",@"signingIdentity", @"syncURL", @"syncURLArray",@"websites",@"workflowInfo",@"shouldOverrideWorkflowOptions",@"workflowOptionsScriptPath"];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictionaryToSave=[NSMutableDictionary dictionaryWithCapacity:keysToSave.count];
    [keysToSave enumerateObjectsUsingBlock:^(NSString *currKey, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([ud objectForKey:currKey]){

            if ([[ud objectForKey:currKey] isKindOfClass:[NSString class]] && [[ud objectForKey:currKey] hasPrefix:NSHomeDirectory()]){

                NSMutableString *dataString=[[ud objectForKey:currKey] mutableCopy];

                [dataString replaceOccurrencesOfString:NSHomeDirectory() withString:@"%%HOME%%" options:NSCaseInsensitiveSearch range:NSMakeRange(0, NSHomeDirectory().length)];

                [dictionaryToSave setObject:dataString forKey:currKey];
            }
            else {
                [dictionaryToSave setObject:[ud objectForKey:currKey] forKey:currKey];
            }

        }
    }];

    if (dictionaryToSave.count>0){

        if([dictionaryToSave writeToFile:[self.backupFolder stringByAppendingPathComponent:@"prefs.plist"] atomically:NO]==NO){

            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Save Error";
            alert.informativeText=@"Could not save a copy of the prefs";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
            return;

        }
    }

    NSArray *webSettings=[[TCSWebServiceController sharedController] webserviceSettings];


    __block BOOL foundOther=NO;


    if (foundOther==YES){
        dispatch_async(dispatch_get_main_queue(), ^{

            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Additional Websites Found";
            alert.informativeText=@"There are custom websites defined in Web Service. The configuration was exported but the website files and folders were not exported. Please manually move these files and folders.";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        });

    }

    if (self.servicesToStart.count>0){
        [self startServices];
    }
    dispatch_async(dispatch_get_main_queue(), ^{

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Export Complete";
        alert.informativeText=@"The MDS migration file has been successfully exported.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
    });

}
-(void)importItems{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *plistPath=[self.exportFolder stringByAppendingPathComponent:@"prefs.plist"];
    if ([fm fileExistsAtPath:plistPath]){

        NSDictionary *prefs=[NSDictionary dictionaryWithContentsOfFile:plistPath];

        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        [prefs enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull currKey, id  _Nonnull obj, BOOL * _Nonnull stop) {


            if ([[prefs objectForKey:currKey] isKindOfClass:[NSString class]] && [[prefs objectForKey:currKey] hasPrefix:@"%%HOME%%"]){

                NSMutableString *dataString=[[prefs objectForKey:currKey] mutableCopy];

                [dataString replaceOccurrencesOfString:@"%%HOME%%" withString:NSHomeDirectory() options:NSCaseInsensitiveSearch range:NSMakeRange(0, @"%%HOME%%".length)];

                [ud setObject:dataString forKey:currKey];
            }

            else {
                [ud setObject:obj forKey:currKey];
            }
        }];
    }

    [[TCSWebServiceController sharedController] loadWebserviceSettings];
    NSString *appSupport=[TCSConfigHelper applicationSupportPath];

    NSString *exportedAppSupportFolder=[self.exportFolder stringByAppendingPathComponent:@"Application Support"];

    NSError *err;
    if ([fm fileExistsAtPath:exportedAppSupportFolder]){

        if ([fm fileExistsAtPath:appSupport]==YES){
            NSString *uuid=[[NSUUID UUID] UUIDString];

            if([fm moveItemAtPath:appSupport toPath:[appSupport stringByAppendingString:uuid]
                            error:&err]==NO){
                [[NSAlert alertWithError:err] runModal];
                return;
            }
        }

    }
    NSString *exportedAppSupport=[self.exportFolder stringByAppendingPathComponent:@"Application Support"];

    if (exportedAppSupport){
        if([fm copyItemAtPath:exportedAppSupport toPath:appSupport error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;

        }
    }
    NSString *tempFolder=[[TCSDefaultsManager sharedManager] newTempFolder];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    [self finishImport];

}
-(void)finishImport{
    if (self.servicesToStart.count>0){
        [self startServices];
    }
    dispatch_async(dispatch_get_main_queue(), ^{

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Import Complete";
        alert.informativeText=@"The MDS migration folder has been successfully imported.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
    });

}
-(void)startServices{

    if ([self.servicesToStart containsObject:WEBSERVICE]){
        [[TCSWebServiceController sharedController] startWebServicesWithCallback:^(BOOL success, BOOL didCancel) {

        }];

    }

}
- (IBAction)importSettings:(id)sender {
    if (self.authenticationView.authorizationState==SFAuthorizationViewLockedState){

        [self showAuthNeededError];
        return;
    }
    self.servicesToStart=[NSMutableArray array];

    self.isExporting=NO;
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=NO;
    openPanel.canChooseDirectories=YES;


    openPanel.message=@"Please select the folder that contains the exported data from MDS:";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {
        return;
    }
    self.exportFolder=openPanel.URL.path;

    [self stopServices:SERVICES];


}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"serverHostname"]) {
        self.sslInfoChanged=YES;
    }

    if ([keyPath isEqualToString:@"tlsCertificateFolder"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *identityFolderPath=[[NSUserDefaults standardUserDefaults] objectForKey:TLSCERTIFICATEFOLDER];
            
            NSFileManager *fm=[NSFileManager defaultManager];
            
            NSString *newKeyPath=[identityFolderPath stringByAppendingPathComponent:@"mds_identity.key"];
            NSString *newCertPath=[identityFolderPath stringByAppendingPathComponent:@"mds_identity.cer"];
            if ([fm fileExistsAtPath:newKeyPath]==NO || [fm fileExistsAtPath:newCertPath]==NO){
                
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Identity Not Found";
                alert.informativeText=@"The selected folder must contain a mds_identity.cer and mds_identity.key file. Please select a folder with those items or create a self signed certificate.";
                
                [alert addButtonWithTitle:@"OK"];
                [alert runModal];
                return;
            }
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[identityFolderPath stringByAppendingPathComponent:@"mds_identity.key"] forKey:INDENTITYKEYPATH];
            
            [[NSUserDefaults standardUserDefaults] setObject:[identityFolderPath stringByAppendingPathComponent:@"mds_identity.cer"] forKey:CERTIFICATEPATH];
            
            self.sslInfoChanged=YES;
        });
    }
    
}

- (IBAction)shouldUseTLSButtonPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:CERTIFICATEPATH] || ![[NSUserDefaults standardUserDefaults] objectForKey:INDENTITYKEYPATH]){
        
        
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No SSL Certificate Defined";
        alert.informativeText=@"In order to use SSL, please create or provide information for an SSL Certificate in the Security pane of preferences.";
        
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        
        return;
    }
    self.sslInfoChanged=YES;
}
- (IBAction)clearCustomRunCommandValue:(id)sender {
    
    
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    
    [ud removeObjectForKey:@"alternateRunCommandPath"];
}
/*
 
 */



- (IBAction)showAPNSIdentityButtonPressed:(id)sender {
    NSArray *returnIdentityArray;
    OSStatus sanityCheck;
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                        (id)kSecClassIdentity,           kSecClass,
                                                        kSecMatchLimitAll,      kSecMatchLimit,
                                                        kCFBooleanFalse,         kSecReturnRef,
                                                        kCFBooleanFalse,         kSecReturnAttributes,
                                                        nil
                                                        ] , (void *)&returnIdentityArray);
    
    
    if (sanityCheck!=noErr) {
        NSLog(@"SecIdentityCopyCertificate error");
        return ;
    }
    
    
    
    
    SFChooseIdentityPanel *panel=[SFChooseIdentityPanel sharedChooseIdentityPanel];
    
    [panel setAlternateButtonTitle:@"Cancel"];
    NSInteger ret=[panel runModalForIdentities:returnIdentityArray message:@"Select identity for Apple Push Notifications."];
    
    if (ret==NSModalResponseOK) {
        SecIdentityRef selectedIdentity=panel.identity;
        CFRetain(selectedIdentity);
        SecIdentityRef identity=panel.identity;
        OSStatus            err;
        SecCertificateRef   certificate;
        CFStringRef         summary;
        
        err = SecIdentityCopyCertificate(identity, &certificate);
        summary = SecCertificateCopySubjectSummary(certificate);
        NSString *identityCertificateText=(__bridge NSString *)(summary);
        
        [[NSUserDefaults standardUserDefaults] setObject:identityCertificateText forKey:@"APNSIdentity"];
    }
    
}
- (IBAction)showIdentityButtonPressed:(id)sender {
    NSArray *returnIdentityArray;
    OSStatus sanityCheck;
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                        (id)kSecClassIdentity,           kSecClass,
                                                        kSecMatchLimitAll,      kSecMatchLimit,
                                                        kCFBooleanTrue,         kSecReturnRef,
                                                        kCFBooleanFalse,         kSecReturnAttributes,
                                                        
                                                        nil
                                                        ] , (void *)&returnIdentityArray);
    
    
    if (sanityCheck!=noErr) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No Identities Found";
        alert.informativeText=@"There were no identities found in the keychain. Please import a developer identity and try again.";
        
        [alert addButtonWithTitle:@"OK"];
        
        [alert runModal];
        
        NSLog(@"SecIdentityCopyCertificate error");
        return ;
    }
    
    
    
    
    SFChooseIdentityPanel *panel=[SFChooseIdentityPanel sharedChooseIdentityPanel];
    
    [panel setAlternateButtonTitle:@"Cancel"];
    NSInteger ret=[panel runModalForIdentities:returnIdentityArray message:@"Select identity for signing macOS packages."];
    
    if (ret==NSModalResponseOK) {
        SecIdentityRef selectedIdentity=panel.identity;
        CFRetain(selectedIdentity);
        SecIdentityRef identity=panel.identity;
        OSStatus            err;
        SecCertificateRef   certificate;
        CFStringRef         summary;
        
        err = SecIdentityCopyCertificate(identity, &certificate);
        summary = SecCertificateCopySubjectSummary(certificate);
        NSString *identityCertificateText=(__bridge NSString *)(summary);
        
        [[NSUserDefaults standardUserDefaults] setObject:identityCertificateText forKey:@"signingIdentity"];
    }
    
}


- (IBAction)selectKeyButtonPressed:(id)sender {
    
    
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Select key in PEM format:";
    NSModalResponse res=[openPanel runModal];
    
    if (res==NSModalResponseOK) {
        [[NSUserDefaults standardUserDefaults] setObject:[openPanel URL].path forKey:INDENTITYKEYPATH];
            }
    
    
}
- (IBAction)selectCertificateButtonPressed:(id)sender {
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Select certificate in PEM format:";
    NSModalResponse res=[openPanel runModal];
    
    if (res==NSModalResponseOK) {
        
        [[NSUserDefaults standardUserDefaults] setObject:[openPanel URL].path forKey:CERTIFICATEPATH];
        
    }
}

- (IBAction)restoreUserDefaultsButtonPressed:(id)sender {
    
    //    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imagr_url"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"diskImageVolumeName"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"webserverPort"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tempFolder"];
    


    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CERTIFICATEPATH];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:INDENTITYKEYPATH];
    

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"alternateRunCommandPath"];
    
    
}
- (IBAction)restoreAlertPromptsButtonPressed:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TCSSuppressCreateSSLCertificate"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TCSSuppressRepackageWarning"];
    
}

-(IBAction)changeDefaultsTabView:(NSToolbarItem *)toolbarItem{
    
    NSInteger tag=toolbarItem.tag;
    
    [self.defaultsTabView selectTabViewItemAtIndex:tag];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.authenticationView setString:TCSAUTHRIGHT];

    self.authenticationView.delegate=self;
    [self.authenticationView updateStatus:self];

}
- (void)authorizationViewDidAuthorize:(SFAuthorizationView *)view{
    SFAuthorization *auth=view.authorization;
    [MDSPrivHelperToolController sharedHelper].authorization=auth;

}
- (void)authorizationViewDidDeauthorize:(SFAuthorizationView *)view{
}
@end


