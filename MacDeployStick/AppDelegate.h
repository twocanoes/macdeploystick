//
//  AppDelegate.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@protocol TCSShouldQuitAppProtocol <NSObject>

@optional

-(BOOL)stopAppFromTerminating:(id)sender;
@end
@protocol TCSRegisterAppQuitProtocol <NSObject>

- (void)registerForTerminateNotification:(id <TCSShouldQuitAppProtocol> )sender;
- (void)unregisterForTerminateNotification:(id <TCSShouldQuitAppProtocol> )sender;

@end
#ifdef APPSTORE
@interface AppDelegate : NSObject <NSApplicationDelegate,NSFileManagerDelegate>
#else
@interface AppDelegate : NSObject <NSApplicationDelegate,NSFileManagerDelegate,TCSRegisterAppQuitProtocol>
@property (weak) IBOutlet NSMenuItem *mdmMenuItem;

@property (weak) NSWindow *window;


#endif
-(void)showPaddleInfo;
-(BOOL)resourcesNeeded;


@end

