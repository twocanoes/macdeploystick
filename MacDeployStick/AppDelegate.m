//
//  AppDelegate.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//
//#define BETA 1
//#import "MDS-Swift.h"
#import "NSData+Base64.h"
#import "NSData+HexString.h"
#import "TCSecurity.h"
#import "NSData+HexString.h"
#import "TCSConfigHelper.h"
#import "AppDelegate.h"
#import "DeploySettings.h"
#import "TCSDefaultsManager.h"
#import "TCSArrayControllerCountTransformer.h"
#import "TCSUtility.h"
#import "TCSConstants.h"
#import "TCSServiceUpdateManager.h"
#import "SubscriptionViewController.h"
#import "SubscriptionManager.h"
#import <ProductLicense/ProductLicense.h>
#import "TCWebConnection.h"
#define MDSBUNDLEID @"com.twocanoes.MacDeployStick"
#define TRIALDAYS 14
//#import "GCDWebServerDataResponse.h"
@interface AppDelegate ()
@property (assign) BOOL isStoreVersion;
@property (strong) NSViewController *vc;
@property (strong) NSMutableArray *appTerminateObjectsToAskWhenTerminating;
@end

@implementation AppDelegate
- (instancetype)init
{
    self = [super init];
    if (self) {
        TCSArrayControllerCountTransformer *xfmr=[[TCSArrayControllerCountTransformer alloc] init];

        [NSValueTransformer setValueTransformer:xfmr forName:@"ArrayControllerCountTransformer"];
        NSString *path=[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist" inDirectory:nil];

        NSDictionary *defaultDict=[NSDictionary dictionaryWithContentsOfFile:path];
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

        [ud registerDefaults:defaultDict];

        NSError *err;
        NSDictionary *mdsinfo;
        if (@available(macOS 10.13, *)) {
            mdsinfo=[NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:[defaultDict objectForKey:@"mdsinfo_url"]] error:&err];

            if (!mdsinfo) {
                NSLog(@"could not load mdsinfo");
            }
        }

        NSDictionary *infoPlist=[[NSBundle mainBundle] infoDictionary];

        NSString *appVersion=[infoPlist objectForKey:@"CFBundleShortVersionString"];

        if (mdsinfo && mdsinfo[@"imgr_required_versions"][appVersion]) {

            if (![mdsinfo[@"imgr_required_versions"][appVersion] isEqualToString:[infoPlist objectForKey:@"CFBundleShortVersionString"]]){
                if (mdsinfo[@"imgr_required_versions"][appVersion]){
                    NSString *imagrRequiredVersion=mdsinfo[@"imgr_required_versions"][appVersion];

                    [defaultDict setValue:imagrRequiredVersion forKey:@"imagr_version_required"];

                    [defaultDict setValue:mdsinfo[@"imgr"][imagrRequiredVersion] forKey:@"imagr_url"];
                }
            }

        }
        if (mdsinfo && mdsinfo[@"munki_required_versions"][appVersion]) {

            if (mdsinfo[@"munki_required_versions"][appVersion]){
                NSString *munkiRequiredVersion=mdsinfo[@"munki_required_versions"][appVersion];

                [defaultDict setValue:munkiRequiredVersion forKey:@"munki_required_versions"];

                [defaultDict setValue:mdsinfo[@"munki"][munkiRequiredVersion] forKey:@"munki_url"];
            }


        }

        


    }
    return self;
}
-(BOOL)resourcesNeeded{
    NSString *currentImgrVersion=[TCSConfigHelper imagrVersion];
    if (currentImgrVersion) {
        //means we have imagr
        NSString *requiredImgrVersion=[[NSUserDefaults standardUserDefaults] objectForKey:@"imagr_version_required"];
        if (![currentImgrVersion isEqualToString:requiredImgrVersion]){


            if ([[NSApp delegate] respondsToSelector:@selector(clearResourceCache:)]){

                [[NSApp delegate] performSelector:@selector(clearResourceCache:) withObject:self];
            }

        }

    }
    
    return [TCSConfigHelper checkIfResourcesNeeded];
}


-(IBAction)deactivateLicense:(id)sender{


    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"License Deactive Confirmation";
    alert.informativeText=@"Are you sure you want to deactivate your license?";

    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Deactivate"];
    NSInteger res=[alert runModal];
    if (res==NSAlertFirstButtonReturn) return;



}
- (IBAction)clearResourceCache:(id)sender {
    NSFileManager *fm=[NSFileManager defaultManager];
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    if ([fm fileExistsAtPath:[appSupportDir stringByAppendingPathComponent:@"Imagr.app"]]){

        NSError *err;
        if([fm removeItemAtPath:[appSupportDir stringByAppendingPathComponent:@"Imagr.app"] error:&err]==NO) {

            [[NSAlert alertWithError:err] runModal];
        }
    }
}

-(BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender{
    return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    if ([ud boolForKey:@"beta"]==NO){


        if (![ud objectForKey:@"tts"]){ //setup start time for trial
            [ud setObject:[NSDate date] forKey:@"tts"];
        }
        NSDate *firstLaunchDate=(NSDate *)[ud objectForKey:@"tts"];
        NSInteger secondsPassed=[[NSDate date] timeIntervalSinceDate:firstLaunchDate];
        TCSLicenseCheck *check=[[TCSLicenseCheck alloc] init];
        TCSLicenseStatus s=[check checkLicenseStatus:MDSBUNDLEID withExtension:@""];

        if (s!=valid) {
            if (secondsPassed>24*60*60*TRIALDAYS) { //expired trial and no valid license file
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Trial Complete";
                    alert.informativeText=@"This version of MDS is compiled, notarized, and has automatic software update checking. It requires a license. Please purchase a license at https://twocanoes.com/ or download an existing license at https://profile.twocanoes.com";
                    [alert addButtonWithTitle:@"Quit"];
                    [alert runModal];
                    [NSApp terminate:self];
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"MDS Trial";
                    long trialDaysLeft=TRIALDAYS-(secondsPassed/(24*60*60));
                    alert.informativeText=[NSString stringWithFormat:@"Thank you for trying MDS. This version of MDS is compiled, notarized, and has automatic software update checking. It requires a license after the trial is complete. Please purchase a license at https://twocanoes.com/ or download an existing license at https://profile.twocanoes.com.\n\nYou have %li days remaining on your trial.",trialDaysLeft];
                    [alert runModal];
                });
            }
        }
        else if(s==valid) {

            NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"license_check"]];

            NSUserDefaults *appDefaults = [NSUserDefaults standardUserDefaults];
            NSString *base64LicenseFile=[appDefaults stringForKey:@"LicenseFileBase64"];
            NSData *licenseData = [base64LicenseFile dataUsingEncoding:NSUTF8StringEncoding];
            NSData *licenceDataHashed=[TCSecurity sha256:licenseData];
            TCWebConnection *wc=[[TCWebConnection alloc] initWithBaseURL:url];

            NSBundle *mainBundle=[NSBundle mainBundle];
            NSDictionary *infoPlist=[mainBundle infoDictionary];

            NSString *version = [infoPlist objectForKey:@"CFBundleShortVersionString"];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";

            NSString *formattedDateString = [dateFormatter stringFromDate:[NSDate date]];
            NSString *serial=[TCSUtility getSerialNumber];
            NSData *serialData=[serial dataUsingEncoding:NSUTF8StringEncoding];
            NSData *serialDataHash=[TCSecurity sha256:serialData];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      @"NA",@"user",
                                      [serialDataHash hexString],@"device",
                                      @"none",@"provider",
                                      formattedDateString,@"time",
                                      //            @"guid":@"1ee80e32-fa37-4340-be18-6440a339e10b",
                                      @"mds",@"activity_type",
                                      version,@"software_version",
                                      nil];
            //            @"huuid":@"8e87c5d3bcba0f3438582840516ebc675b5140589ecc1ba98b31674396346c23"


            NSDictionary *payload = @{@"tcs-auth":@[userInfo]};
            [wc sendRequestToPath:@"/api/license_activity" type:@"POST" payload:payload

                           authId:@"" authKey:@"" basicAuthUsername:nil basicAuthPassword:nil  bearer:licenceDataHashed.hexString onCompletion:^(NSInteger responseCode, NSData *responseData) {

            } onError:^(NSError *error) {
                NSLog(@"error :%@",error.localizedDescription);
            }];

        }
    }
    if (![ud objectForKey:SERVERHOSTNAME] ||
        [[ud objectForKey:SERVERHOSTNAME] length]==0){

        NSString *currHost=[[NSHost currentHost] name];
        if ([currHost containsString:@"."]==NO){
            currHost=[currHost stringByAppendingString:@".local"];
        }

        [ud setObject:currHost forKey:SERVERHOSTNAME];
    }
//    if ([ud boolForKey:HASMDSSUPPORT]==NO){
//        NSAlert *alert=[[NSAlert alloc] init];
//        alert.messageText=@"Support MDS";
//        alert.informativeText=@"Support MDS by purchasing support. This supports development of new features, new hardware, and new OS versions. It also provides you with high priority feature requests and escalated support. Additionally, it makes you feel good.";
//
//        [alert addButtonWithTitle:@"Learn More"];
//        [alert addButtonWithTitle:@"OK"];
//        alert.showsSuppressionButton=YES;
//        [alert.suppressionButton setTitle:@"Support already purchased. Do not show again."];
//        NSInteger res=[alert runModal];
//        if (res==NSAlertFirstButtonReturn) {
//
//            NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mdsBuySupport"]];
//            [[NSWorkspace sharedWorkspace]  openURL:url];
//
//        }
//        else {
//            if (alert.suppressionButton.state==YES) {
//
//                NSAlert *alert=[[NSAlert alloc] init];
//                alert.messageText=@"Support Purchased";
//                alert.informativeText=@"By clicking OK below, you will no longer be prompted about support. If you have not purchased support, please support the project by doing so. It helps. Seriously.";
//
//                [alert addButtonWithTitle:@"OK"];
//                [alert addButtonWithTitle:@"More Info"];
//
//                NSInteger res=[alert runModal];
//                if (res==NSAlertSecondButtonReturn) {
//                    NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mdsBuySupport"]];
//                    [[NSWorkspace sharedWorkspace]  openURL:url];
//
//                }
//                else {
//                    [ud setBool:YES forKey:HASMDSSUPPORT];
//                }
//
//            }
//        }


//    }

    NSFileManager *fm=[NSFileManager defaultManager];


    if ([ud objectForKey:@"syncURL"] && ![ud objectForKey:@"syncURLArray"]){

        NSError *err;
        if([fm fileExistsAtPath:[DeploySettings syncFolder]]==NO){
            if([fm createDirectoryAtPath:[DeploySettings syncFolder] withIntermediateDirectories:YES attributes:nil error:&err]==NO){

                [[NSAlert alertWithError:err] runModal];

            }

        }
        NSString *syncURLString=[ud objectForKey:@"syncURL"];

        NSString *hash=[NSString stringWithFormat:@"%lx",[syncURLString hash]];
        NSArray *itemsToMove=[fm contentsOfDirectoryAtPath:[DeploySettings syncFolder] error:&err];

        if (err){

            [[NSAlert alertWithError:err] runModal];

        }
        else {

            NSString *newSyncFolder=[[DeploySettings syncFolder] stringByAppendingPathComponent:hash];


            if([fm createDirectoryAtPath:newSyncFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

                [[NSAlert alertWithError:err] runModal];

            }

            else {

                [itemsToMove enumerateObjectsUsingBlock:^(NSString *itemName, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSError *err;
                    if([fm moveItemAtPath:[[DeploySettings syncFolder] stringByAppendingPathComponent:itemName] toPath:[newSyncFolder stringByAppendingPathComponent:itemName] error:&err]==NO){

                        *stop=YES;
                        [[NSAlert alertWithError:err] runModal];
                    }

                }];

                [ud setObject:@[@{@"name":@"migrated",@"url":syncURLString}] forKey:@"syncURLArray"];

            }
        }
    }
    NSArray *mdmCommands=[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MDMCommands" ofType:@"plist"]];
    NSMenu *mainMenu = [[NSApplication sharedApplication] mainMenu];

    NSMenuItem *commandMenuItem=[mainMenu itemWithTag:99];
    NSMenu *commandsMenu=commandMenuItem.submenu;
    [mdmCommands  enumerateObjectsUsingBlock:^(NSDictionary *info, NSUInteger idx, BOOL * _Nonnull stop) {

        NSString *title=[info objectForKey:@"title"];
        SEL selector = NSSelectorFromString(@"mdmCommand:");
        NSMenuItem *newMenuItem=[[NSMenuItem alloc] initWithTitle:title action:selector keyEquivalent:@""];
        newMenuItem.representedObject=info;

        newMenuItem.hidden=[[info objectForKey:@"isHidden"] boolValue];
        [commandsMenu insertItem:newMenuItem atIndex:0];

    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:@"TCSWarning" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [[NSAlert alertWithError:note.userInfo[@"error"]] runModal];
        });

    }];
#ifdef APPSTORE
        self.isStoreVersion=YES;
#else
    self.isStoreVersion=NO;
//    [self showPaddleInfo];
#endif
    NSDate *lastTimePromoShown=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTimePromoShown"] ;

    if (lastTimePromoShown==nil) {

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"]; lastTimePromoShown=[NSDate date]; }
    


    if ([ud boolForKey:@"beta"]==YES){

        NSString *compileDate = [NSString stringWithUTF8String:__DATE__];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MMM d yyyy"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [df setLocale:usLocale];
        NSDate *betaExpireDate = [[df dateFromString:compileDate] dateByAddingTimeInterval:60*60*24*14];

        NSLog(NSLocalizedString(@"Beta Product.  Expires on %@",@"log message"),[betaExpireDate description]);


        NSTimeInterval ti=[betaExpireDate timeIntervalSinceNow];
        NSInteger res;
        if (ti<0) {
            res=NSRunAlertPanel(NSLocalizedString(@"Beta Period Ended",@"Title for alert panel that beta expired"),NSLocalizedString(@"This beta has expired.  Please visit twocanoes.com to download an updated version.",@"body for alert panel that beta expired"), NSLocalizedString(@"Visit",@"button alert panel that beta expired"),NSLocalizedString(@"Quit", @"button for alert panel that beta expired"),nil);
            if (res==NSAlertDefaultReturn) {
                ;
                NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mainwebsite"]];
                [[NSWorkspace sharedWorkspace]  openURL:url];
            }
            [NSApp terminate:self];
        }
        else {

            NSAlert *alert=[NSAlert alertWithMessageText:@"Beta Product" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"This is a beta product and will expire on %@",[betaExpireDate description]];

            [alert runModal];
        }
    }


}


- (void)applicationWillTerminate:(NSNotification *)aNotification {

    [[DeploySettings sharedSettings] saveToPrefs:self];
    [[TCSDefaultsManager sharedManager] clearCache];


}
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    __block BOOL shouldTerminate=YES;

     if (self.appTerminateObjectsToAskWhenTerminating && [self.appTerminateObjectsToAskWhenTerminating count]>0){
        [self.appTerminateObjectsToAskWhenTerminating enumerateObjectsUsingBlock:^(id  <TCSShouldQuitAppProtocol> obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj stopAppFromTerminating:self]==YES){
                shouldTerminate=NO;
                *stop=YES;
            }
        }];
    }

    if (shouldTerminate==YES) return NSTerminateNow;

    return NSTerminateCancel;

}
-(IBAction)importWorkflows:(id)sender{

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];

    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.allowedFileTypes=@[@"mdsworklows",@"mdsworkflows"];
    openPanel.message=@"Select a volume to import workflows:";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {

        return;
    }

    NSString *openVolumePath=[openPanel URL].path;

    BOOL missingPassword=NO;
    NSData *workflowData=[NSData dataWithContentsOfFile:openVolumePath];
    if (workflowData) {
        if ([[DeploySettings sharedSettings] addWorkflowsFromData:workflowData]==YES){
            missingPassword=YES;
        }
    }

    if (missingPassword==YES) {
        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:0 userInfo:@{NSLocalizedDescriptionKey:@"One or more workflows have a blank user or WiFi password. Please open workflows and enter in passwords as necessary."}]];
        [alert runModal];
    }


}

-(BOOL)workflowsExist{
    if ([[[DeploySettings sharedSettings] workflows] count]>0) return YES;
    else return NO;
}
- (void)registerForTerminateNotification:(id <TCSShouldQuitAppProtocol> )sender {

    if (!self.appTerminateObjectsToAskWhenTerminating){

        self.appTerminateObjectsToAskWhenTerminating=[NSMutableArray array];
    }
    [self.appTerminateObjectsToAskWhenTerminating addObject:sender];

}

- (void)unregisterForTerminateNotification:(id<TCSShouldQuitAppProtocol>)sender {

    if (self.appTerminateObjectsToAskWhenTerminating && [self.appTerminateObjectsToAskWhenTerminating containsObject:sender]){

        [self.appTerminateObjectsToAskWhenTerminating removeObject:sender];
    }
}


@end
