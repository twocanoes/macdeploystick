//
//  NSData+PEM.h
//  Gate
//
//  Created by Timothy Perfitt on 9/29/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (PEM)
+(NSData *)dataWithPEMString:(NSString *)hex;

@end

NS_ASSUME_NONNULL_END
