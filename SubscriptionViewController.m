//
//  SubscriptionViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 10/28/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "SubscriptionViewController.h"
#import "SubscriptionManager.h"
@interface SubscriptionViewController ()

@end

@implementation SubscriptionViewController
+ (instancetype)shared {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[SubscriptionViewController alloc]  initWithNibName:@"SubscriptionViewController" bundle:nil];


    });
    return sharedMyManager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
}
- (IBAction)editionSelected:(NSButton *)button {
    switch (button.tag) {
        case 0:
            [[SubscriptionManager shared] setEdition:EDITION_FREE];
            break;
        case 1:
            [[SubscriptionManager shared] setEdition:EDITION_PRO];
            [[SubscriptionManager shared] setIsSubscribed:YES];
            break;
        case 2:
            [[SubscriptionManager shared] setEdition:EDITION_ENTERPRISE];
            [[SubscriptionManager shared] setIsSubscribed:YES];
            break;

        default:
            break;
    }
    [[SubscriptionManager shared] savePrefs];

    [self dismissViewController: self];
}
-(SubscriptionViewController *)selectionSubscriptionViewController{
    NSStoryboard *subscriptionStoryboard = [NSStoryboard storyboardWithName:@"Subscription" bundle: nil];

    SubscriptionViewController *subscriptionViewController= [subscriptionStoryboard instantiateControllerWithIdentifier:@"subscription_view_controller"];
    return subscriptionViewController;
}

- (IBAction)buySupportButtonPressed:(id)sender {
    NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"buy-mds"]];
    if (url) [[NSWorkspace sharedWorkspace] openURL:url];

}


@end
