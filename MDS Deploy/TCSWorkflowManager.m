//
//  TCSWorkflowManager.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 3/9/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSUtility.h"
#import "AppKit/AppKit.h"
#import "NSError+EasyError.h"
#import "TCSWorkflowManager.h"
#import "TCTaskHelper.h"
//#import <DiskDevice/DiskDevice.h>
#import "TCDeviceManager.h"
#import "TCSUtility.h"
#define TCSWORKFLOWKEY @"workflows"
#define TCSCOMPONENTIMAGE @"image"
#define TCSCOMPONENTSKEY @"components"
#define TCSCFIRSTBOOT @"first_boot"
#define TCSURLKEY @"url"
#define TCSCOMPONENTSTARTOSINSTALL @"startosinstall"
#define TCSCOMPONENTINSTALLER @"startosinstall"
#define TCSCOMPONENTPACKAGE @"package"
#define TCSCOMPONENTPACKAGEFOLDER @"package_folder"
#define TCSCOMPONENTSCRIPT @"script"
#define TCSCOMPONENTSCRIPTFOLDER @"script_folder"
#define TCSCOMPONENTPARTITION @"partition"
#define TCSCOMPONENTINCLUDEDWORKFLOW @"included_workflow"
#define TCSCOMPONENTCOMPUTERNAME @"computer_name"
#define TCSCOMPONENTLOCALIZE @"localize"
#define TCSCOMPONENTRESTARTACTION @"restart_action"
#define TCSCOMPONENTERASEVOLUMEACTION @"eraseVolume"
#define TCSCOMPONENTEXTENDEDVARIABLES @"extended_variables"

#define TCSFIRSTBOOTFOLDERLOCALPATH @"/private/var/mds/first-boot"

NS_OPTIONS(NSInteger, WorkflowComponentType){
    startOSInstall,
    package,
    packageFolder,
    script,
    script_folder,
    partition,
    includedWorkflow,
    computerName,
    localize,
    restartAction

};


@interface TCSWorkflowManager()
@property (strong) TCSCopyFileController *fileCopyController;

@property (strong) NSArray *workflows;
@property (strong) TCVolume *selectedVolume;
@property (strong) NSDictionary *selectedWorkflow;
@property (assign) NSInteger currentComponentIndex;
@property (assign) BOOL firstBootSetupDone;
@property (nonatomic, copy, nullable) void (^workflowCompletionBlock)(NSError *, NSString *installPath);
@property (nonatomic, copy, nullable) void (^workflowStatusBlock)(NSInteger percentComplete, NSString *statusMessage);
@property (nonatomic, copy, nullable) NSDictionary * (^simpleVariableBlock)(NSString *query,NSString *defaultAnswer);
@property (nonatomic, copy, nullable) NSDictionary * (^choiceVariableBlock)(NSString *query, NSArray *selections, BOOL addOther);
@property (nonatomic, copy, nullable) NSDictionary * (^usernamePasswordBlock)(NSString *query,NSString *defaultAnswer);
@property (assign) BOOL hasSupport;


@property (assign) BOOL shouldCancel;
@property (strong) NSURL *serverURL;

//@property (nonatomic, copy, nullability) returnType (^blockName)(parameterTypes);

@end
@implementation TCSWorkflowManager
+ (instancetype)shared {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadConfig];
    }
    return self;
}

-(void)cancelCurrentOperations:(id)sender{
    self.shouldCancel=YES;
    if (self.fileCopyController){
        [self.fileCopyController cancelCopyOperation];
    }

}
-(NSArray *)installers{

    NSURL *deployFolder=[[self.serverURL URLByDeletingLastPathComponent] URLByDeletingLastPathComponent];

    NSURL *macOSInstallers=[deployFolder URLByAppendingPathComponent:@"macOS"];

    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    NSArray *dirContents=[fm contentsOfDirectoryAtPath:macOSInstallers.path error:&err];
    if (!dirContents){
        return nil;
    }

    NSMutableArray *installerPaths=[NSMutableArray array];
    [dirContents enumerateObjectsUsingBlock:^(NSString *currentName, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[[currentName pathExtension] lowercaseString] isEqualToString:@"app"] ||
            [[[currentName pathExtension] lowercaseString] isEqualToString:@"dmg"] ){
            [installerPaths addObject:[macOSInstallers.path stringByAppendingPathComponent:currentName]];

        }
    }];
    return [NSArray arrayWithArray:installerPaths];
}
-(BOOL)runWorkflowAtIndex:(NSInteger)index withVolume:(TCVolume *)volume simpleVariableBlock:(NSDictionary * (^)(NSString *query, NSString *defaultAnswer))simpleVariableBlock choiceVariableBlock:(NSDictionary * (^)(NSString *query, NSArray *selections, BOOL addOther))choiceVariableBlock usernamePasswordBlock:(NSDictionary * (^)(NSString *query, NSString *defaultAnswer))usernamePasswordBlock updateBlock:(void (^)(NSInteger percentComplete, NSString * statusMessage))update completionBlock:(void (^)( NSError *err, NSString *installerPath))completion{

    self.firstBootSetupDone=NO;
    self.shouldCancel=NO;
    self.workflowStatusBlock=update;
    self.workflowCompletionBlock=completion;
    self.simpleVariableBlock=simpleVariableBlock;
    self.choiceVariableBlock = choiceVariableBlock;
    self.usernamePasswordBlock = usernamePasswordBlock;
    if (index>self.workflows.count) {
        NSLog(@"Invalid workflow index");
        return NO;
    }

    if (!volume){
        NSLog(@"invalid volume");
    }
    self.selectedVolume=volume;
    NSDictionary *workflow = [self.workflows objectAtIndex:index];
    if (!workflow || workflow.count==0) { return NO; }
    NSArray <NSDictionary <NSString *, id> *> *components = [workflow objectForKey:TCSCOMPONENTSKEY];
    if (!components) { return NO; }
    self.currentComponentIndex=-1;
    self.selectedWorkflow=workflow;
    [self processNextComponent:self];
    return YES;
}

-(void)processNextComponent:(id)sender{
    NSLog(@"processing next component");
    if (self.shouldCancel==YES) {
        self.shouldCancel=NO;
        NSLog(@"cancelling");
        NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Cancelled"]}];

        self.workflowCompletionBlock(err, nil);
        return;
    }
    NSArray *components=self.selectedWorkflow[TCSCOMPONENTSKEY];
    self.currentComponentIndex++;

    if (self.currentComponentIndex>components.count-1) {
        NSLog(@"done!");
        self.workflowCompletionBlock(nil,nil);
        return;
    }
    NSLog(@"kicking off next component: %li", self.currentComponentIndex);
    NSDictionary *currentComponent=[components objectAtIndex:self.currentComponentIndex];
    NSString *currentType=currentComponent[@"type"];
    if ([currentType isEqualToString:TCSCOMPONENTSTARTOSINSTALL]){
        NSLog(@"Component: StartOSInstall");
        self.workflowStatusBlock((self.currentComponentIndex+1)*10,[NSString stringWithFormat:@"Verifying target volume"]);

//        we are in MDS Deploy, so that means ARM. This is startosinstall, so must:copy
//        to a launch daemon and target volume and reboot.


        if ([TCSUtility isARM]==YES){
            [self prepareStartOSInstallForComponent:components[self.currentComponentIndex]];
        }
        else {
            [self prepareStartOSInstallForComponent:components[self.currentComponentIndex]];

        }


    }
    else if ([currentType isEqualToString:TCSCOMPONENTPACKAGE]){
        NSLog(@"   unsupported operation %@, skipping",currentType );

        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }
    else if ([currentType isEqualToString:TCSCOMPONENTPACKAGEFOLDER]){
        NSLog(@"Component: TCSCOMPONENTPACKAGEFOLDER");
        self.workflowStatusBlock((self.currentComponentIndex+1)*10,[NSString stringWithFormat:@"Copying first boot packages"]);


        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            NSMutableDictionary *mutableComponent = [components [self.currentComponentIndex] mutableCopy];

            BOOL firstBootReboot = [[self.selectedWorkflow  valueForKey:@"first_boot_reboot"] boolValue];
            mutableComponent[@"first_boot_reboot"] = @(firstBootReboot);


            BOOL workflow_should_skip_rosetta = [[self.selectedWorkflow  valueForKey:@"workflow_should_skip_rosetta"] boolValue];
            mutableComponent[@"workflow_should_skip_rosetta"] = @(workflow_should_skip_rosetta);


            BOOL workflow_should_configure_wifi = [[self.selectedWorkflow  valueForKey:@"workflow_should_configure_wifi"] boolValue];
            mutableComponent[@"workflow_should_configure_wifi"] = @(workflow_should_configure_wifi);


            if ([self.selectedWorkflow valueForKey:@"workflow_wifi_ssid"]) {

                mutableComponent[@"workflow_wifi_ssid"] = [self.selectedWorkflow valueForKey:@"workflow_wifi_ssid"];

            }
            if ([self.selectedWorkflow valueForKey:@"workflow_wifi_password"]) {

                mutableComponent[@"workflow_wifi_password"] = [self.selectedWorkflow valueForKey:@"workflow_wifi_password"];

            }


            NSLog(@"mutableComponent: %@",mutableComponent);
            [self installPackagesWithComponent:mutableComponent];
        });
    }
    else if ([currentType isEqualToString:TCSCOMPONENTSCRIPT]){
        NSLog(@"Component: TCSCOMPONENTSCRIPT");

        if ([currentComponent[@"imagr_only"] boolValue] == YES) {
            [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];
            return;
        }
        if (currentComponent[@"first_boot"] && [currentComponent[@"first_boot"] boolValue]==NO){
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                if ([self runScript:currentComponent[@"content"] target:self.selectedVolume.filePath]==NO){
                    return;
                }
            });

        }
        else {
            NSLog(@"   unsupported first boot script operation %@, skipping",currentType );
        }
    }
    else if ([currentType isEqualToString:TCSCOMPONENTSCRIPTFOLDER]){
        NSLog(@"Component: TCSCOMPONENTSCRIPTFOLDER");

        if (currentComponent[@"url"]){


            NSURL *folderURL=[NSURL URLWithString:currentComponent[@"url"]];
            NSFileManager *fm=[NSFileManager defaultManager];

            NSArray *contents=[fm contentsOfDirectoryAtPath:folderURL.path error:nil];

            if (contents && contents.count>0){
                NSLog(@"   unsupported operation %@, skipping",currentType );
            }
        }
        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }
    else if ([currentType isEqualToString:TCSCOMPONENTPARTITION]){
        NSLog(@"   unsupported operation %@, skipping",currentType );

        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }
    else if ([currentType isEqualToString:TCSCOMPONENTINCLUDEDWORKFLOW]){
        NSLog(@"   unsupported operation %@, skipping",currentType );

        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }
    else if ([currentType isEqualToString:TCSCOMPONENTCOMPUTERNAME]){
        NSLog(@"Setting computer name in NVRAM to apply at next reboot");

        NSString *computerName=@"";

        if (currentComponent[TCSCOMPONENTCOMPUTERNAME] && [currentComponent[TCSCOMPONENTCOMPUTERNAME] length]>0) computerName=[self replacePlaceholders:currentComponent[TCSCOMPONENTCOMPUTERNAME] target:self.selectedVolume.device];
        if (currentComponent[@"nvram"] && [currentComponent[@"nvram"] boolValue]==YES ){

            if (currentComponent[@"auto"]==nil || [currentComponent[@"auto"] boolValue]==NO){
                NSLog(@"prompting");

                NSString *prompt=@"Computer Name";
                NSDictionary *answerInfo=self.simpleVariableBlock(prompt,computerName);
                NSString *answer=[answerInfo objectForKey:@"response"];
                if (answer) {
                    computerName=answer;
                }
                else{
                    NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Cancelled"]}];
                    self.workflowCompletionBlock(err,nil);

                    return;

                }
            }
            if (!computerName || computerName.length==0){

                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"You did not specify a computer name. You must provide a computer name."}];
                self.workflowCompletionBlock(err,nil);

                return;



            }
            NSLog(@"writing to NVRAM");
            if([self writeToNVRAM:@{@"com.twocanoes.mds.ComputerName":computerName}]==NO){
                NSLog(@"Error");

                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Error setting computer name in nvram."]}];

                if (getuid()==0){
                    NSLog(@"%@",err.localizedDescription);
                    self.workflowCompletionBlock(err,nil);
                }
                else {
                    self.workflowStatusBlock(50, err.localizedDescription);
                }

            }
            [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

        }
        else {
            NSLog(@"   unsupported operation %@, skipping",currentType );
            [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];


        }


    }
    else if ([currentType isEqualToString:TCSCOMPONENTLOCALIZE]){
        NSLog(@"   unsupported operation %@, skipping",currentType );

        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }
    else if ([currentType isEqualToString:TCSCOMPONENTRESTARTACTION]){
        NSLog(@"Component: TCSCOMPONENTRESTARTACTION");

        if ([[currentComponent objectForKey:@"action"] isEqualToString:@"shutdown"]){
            NSLog(@"Shutting down");
            self.workflowStatusBlock(100, @"Shutting Down");
            NSTask *task= [NSTask launchedTaskWithLaunchPath:@"/sbin/shutdown" arguments:@[@"-h",@"now"]];

            [task waitUntilExit];

            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Error shutting Down."]}];

            self.workflowCompletionBlock(err,nil);

        }
        else {
            NSLog(@"Restarting");
            self.workflowStatusBlock(100, @"Restarting");
           NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/sbin/reboot" arguments:@[]];
            [task waitUntilExit];

            if (task.terminationStatus!=0){
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Error restarting."]}];

                self.workflowCompletionBlock(err,nil);

            }
        }


    }
    else if ([currentType isEqualToString:TCSCOMPONENTERASEVOLUMEACTION]){
        NSLog(@"Component: TCSCOMPONENTERASEVOLUMEACTION");
        NSString *devicePath=self.selectedVolume.device;
        NSString *volumeName=currentComponent[@"name"];

        if ([TCSUtility isARM]==NO){

            NSString *volumeUUID=[TCSUtility apfsVolumeUUIDFromDevice:self.selectedVolume.device];

            NSLog(@"volume UUID: %@",volumeUUID);
            if (!volumeUUID){
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"could not find volumeUUID from device"]}];
                self.workflowCompletionBlock(err,nil);


            }
             NSDictionary *apfsVolumeInfo=[TCSUtility containerAndVolumesFromVolumeUUID:volumeUUID];

            if (!apfsVolumeInfo){
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"could not find apfsVolumeInfo from volumeUUID"]}];
                self.workflowCompletionBlock(err,nil);


            }
            NSString *containerUUID=[apfsVolumeInfo objectForKey:@"container"];

            NSLog(@"containerUUID UUID: %@",containerUUID);
            NSArray *volumes=[apfsVolumeInfo objectForKey:@"volumes"];

            [volumes enumerateObjectsUsingBlock:^(NSArray *currVolumeUUID, NSUInteger idx, BOOL * _Nonnull stop) {
                NSLog(@"deleting: %@",currVolumeUUID);

                NSTask *task=[[NSTask alloc] init];
                task.launchPath=@"/usr/sbin/diskutil";
                task.arguments=@[@"apfs",@"deleteVolume",currVolumeUUID];
                task.standardOutput=[NSPipe pipe];
                task.standardError=[NSPipe pipe];
                [task launch];

                NSData *standardOutput=[[task.standardOutput fileHandleForReading] readDataToEndOfFile];
                if (standardOutput && standardOutput.length>0) {
                    NSLog(@"%@",[[NSString alloc] initWithData:standardOutput encoding:NSUTF8StringEncoding]);
                }

                NSData *errorOutput=[[task.standardError fileHandleForReading] readDataToEndOfFile];
                if (errorOutput && errorOutput.length>0) {
                    NSLog(@"%@",[[NSString alloc] initWithData:errorOutput encoding:NSUTF8StringEncoding]);

                }
            }];

            NSString *updatedVolumeName=@"Macintosh HD";
            if (volumeName){
                updatedVolumeName=volumeName;
            }
            NSLog(@"adding volume to : %@",containerUUID);

            NSTask *task=[[NSTask alloc] init];
            task.launchPath=@"/usr/sbin/diskutil";
            task.arguments=@[@"apfs",@"addVolume",containerUUID,@"apfs",updatedVolumeName];
            task.standardOutput=[NSPipe pipe];
            task.standardError=[NSPipe pipe];
            [task launch];

            NSData *standardOutput=[[task.standardOutput fileHandleForReading] readDataToEndOfFile];
            if (standardOutput && standardOutput.length>0) {
                NSLog(@"%@",[[NSString alloc] initWithData:standardOutput encoding:NSUTF8StringEncoding]);
            }

            NSData *errorOutput=[[task.standardError fileHandleForReading] readDataToEndOfFile];
            if (errorOutput && errorOutput.length>0) {
                NSLog(@"%@",[[NSString alloc] initWithData:errorOutput encoding:NSUTF8StringEncoding]);

            }
            NSLog(@"finding device for : %@",containerUUID);

            NSString *devicePath=[TCSUtility devicePathForFirstVolumeInContainer:containerUUID];

            if (!devicePath) {
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Could not resolve device path"}];
                self.workflowCompletionBlock(err,nil);

            }
            __block TCVolume *foundVolume=nil;
            [TCDeviceManager.sharedDeviceManager.attachedDisks enumerateObjectsUsingBlock:^(TCVolume *vol, NSUInteger idx, BOOL * _Nonnull stop) {
                NSLog(@"looking at %@ and comparing to %@",vol.device,devicePath );

                if ([vol.device isEqualToString:[@"/dev" stringByAppendingPathComponent:devicePath]]){
                    foundVolume=vol;
                    *stop=YES;
                    return;
                }
            }];
            if (!foundVolume){
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"could not find volume path"}];
                self.workflowCompletionBlock(err,nil);

            }
            self.selectedVolume=foundVolume;

//            NSLog(@"    Erasing volume %@",self.selectedVolume.filePath);


        }
        else {
            NSLog(@"   erase volume now included with installer, renaming drive if needed" );
        }


        if (volumeName){
            NSLog(@"   renaming volume" );

            NSTask *task=[[NSTask alloc] init];
            task.launchPath=@"/usr/sbin/diskutil";
            task.arguments=@[@"rename",self.selectedVolume.filePath,volumeName];
            task.standardOutput=[NSPipe pipe];
            task.standardError=[NSPipe pipe];
            [task launch];

            NSData *standardOutput=[[task.standardOutput fileHandleForReading] readDataToEndOfFile];
            if (standardOutput && standardOutput.length>0) {
                NSLog(@"%@",[[NSString alloc] initWithData:standardOutput encoding:NSUTF8StringEncoding]);
            }

            NSData *errorOutput=[[task.standardError fileHandleForReading] readDataToEndOfFile];
            if (errorOutput && errorOutput.length>0) {
                NSLog(@"%@",[[NSString alloc] initWithData:errorOutput encoding:NSUTF8StringEncoding]);

            }
            __block TCVolume *updatedSelectedVolume=nil;
            [[TCDeviceManager.sharedDeviceManager attachedDisks] enumerateObjectsUsingBlock:^(TCVolume *currVolume, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([currVolume.device.lowercaseString isEqualToString:devicePath.lowercaseString]) {
                    updatedSelectedVolume=currVolume;
                }

            }];

            if (!updatedSelectedVolume) {
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Could not find new volume after renaming"}];
                self.workflowCompletionBlock(err,nil);
                return;

            }
            self.selectedVolume=updatedSelectedVolume;
        }


        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }
    else if ([currentType isEqualToString:TCSCOMPONENTEXTENDEDVARIABLES]){
        NSLog(@"Component: TCSCOMPONENTEXTENDEDVARIABLES");

        NSArray *variableArray=[currentComponent objectForKey:@"variable_selections"];
        if (!variableArray) {
            NSLog(@"variable_selections is empty");
            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"invalid variable info"]}];

            self.workflowCompletionBlock(err,nil);

        }

        NSLog(@"variables: %@",variableArray);
        __block BOOL didCancel=NO;
        NSMutableDictionary *variablesToWrite=[NSMutableDictionary dictionary];
        [variableArray enumerateObjectsUsingBlock:^(NSDictionary *varInfo, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([varInfo objectForKey:@"label"]) {
                NSLog(@"label");
                BOOL addOtherOption=[[varInfo objectForKey:@"add_other_option"] boolValue];
                NSString *label=[varInfo objectForKey:@"label"];
                NSArray *options=[varInfo objectForKey:@"options"];
                NSString *prompt=[varInfo objectForKey:@"prompt"];

                if (!label ||
                    !options ||
                    !prompt){
                    *stop=YES;
                    NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"error parsing options"]}];
                    self.workflowCompletionBlock(err,nil);

                    return;
                }
                NSString *answer=[self.choiceVariableBlock(prompt,options, addOtherOption) objectForKey:@"response"];

                if (answer) {
                    NSLog(@"got answer");
                    [variablesToWrite setObject:answer forKey:label];
                }
                else{
                    NSLog(@"no answer");
                    didCancel=YES;
                    NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Cancelled"]}];
                    self.workflowCompletionBlock(err,nil);
                    didCancel=YES;
                    *stop=YES;
                    return;

                }

            }
            else {
                NSLog(@"not label");

                NSString *variableName=varInfo.allKeys.firstObject;
                NSString *prompt=varInfo.allValues.firstObject;

                NSString *answer=[self.simpleVariableBlock(prompt,@"") objectForKey:@"response"];

                if (answer) {
                    NSLog(@"got answer");

                    [variablesToWrite setObject:answer forKey:variableName];
                }
                else{
                    NSLog(@"cancelled...");

                    NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Cancelled"]}];
                    self.workflowCompletionBlock(err,nil);
                    didCancel=YES;
                    *stop=YES;

                    return;

                }



            }
        }];

        if (didCancel==YES) {
            NSLog(@"cancelled. returning.");
            return;
        }
        NSLog(@"writing variables to NVRAM");
        if([self writeToNVRAM:variablesToWrite]==NO){
            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"error writing nvram"]}];

            if (getuid()==0){

                self.workflowCompletionBlock(err,nil);
                return;
            }
        }
        NSLog(@"writing variables to NVRAM completed.");
        [self performSelector:@selector(processNextComponent:) withObject:self afterDelay:0.1];

    }

    else {
        NSLog(@"unknown operation %@",currentType );

    }

}


-(BOOL)runScript:(NSString *)script target:(NSString *)target{
    NSLog(@"running script");

    NSString *scriptToRun=[self replacePlaceholders:script target:target];
    NSString *folderName=[[NSUUID UUID] UUIDString];
    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    NSString *tempFolder=[NSTemporaryDirectory() stringByAppendingPathComponent:folderName];
    if([fm createDirectoryAtPath:tempFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

        self.workflowCompletionBlock(err,nil);
        return NO;
    }
    NSString *targetScriptFilePath=[tempFolder stringByAppendingPathComponent:@"mds_script.sh"];

    if([scriptToRun writeToFile:targetScriptFilePath atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){
        self.workflowCompletionBlock(err,nil);
        return NO;
    }


    if([fm setAttributes:@{@"NSFilePosixPermissions":[NSNumber numberWithUnsignedLong:0700U]} ofItemAtPath:targetScriptFilePath error:&err]==NO){
        self.workflowCompletionBlock(err,nil);
        return NO;

    }

    NSTask *task=[[NSTask alloc] init];
    task.launchPath=targetScriptFilePath;
    task.standardOutput=[NSPipe pipe];
    task.standardError=[NSPipe pipe];
    [task launch];

    NSData *standardOutput=[[task.standardOutput fileHandleForReading] readDataToEndOfFile];
    if (standardOutput && standardOutput.length>0) {
        NSLog(@"%@",[[NSString alloc] initWithData:standardOutput encoding:NSUTF8StringEncoding]);
    }

    NSData *errorOutput=[[task.standardError fileHandleForReading] readDataToEndOfFile];
    if (errorOutput && errorOutput.length>0) {
        NSLog(@"%@",[[NSString alloc] initWithData:errorOutput encoding:NSUTF8StringEncoding]);

    }

    [task waitUntilExit];
    if (task.terminationStatus!=0){
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Error running script. Please check log for error message."]}];

        if (getuid()==0){
            self.workflowCompletionBlock(err,nil);
                return NO;
        }
        else {
            self.workflowStatusBlock(50, err.localizedDescription);
        }


    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [self processNextComponent:self];

    });


    return YES;
}
-(NSString *)replacePlaceholders:(NSString *)script target:(NSString *)target{

    NSMutableString *mutableScript=[script mutableCopy];
    [mutableScript replaceOccurrencesOfString:@"{{target_volume}}" withString:target options:NSLiteralSearch range:NSMakeRange(0, mutableScript.length)];

    NSString *serialNumber=[TCSUtility getSerialNumber];
    NSString *modelIdentifier=[TCSUtility machineModel];
    if (serialNumber) {
        [mutableScript replaceOccurrencesOfString:@"{{serial_number}}" withString:serialNumber options:NSLiteralSearch range:NSMakeRange(0, mutableScript.length)];
    }
    if (modelIdentifier){
        [mutableScript replaceOccurrencesOfString:@"{{machine_model}}" withString:modelIdentifier options:NSLiteralSearch range:NSMakeRange(0, mutableScript.length)];
    }

    return [NSString stringWithString:mutableScript];

}
-(BOOL)writeToNVRAM:(NSDictionary *)nvramVariableDict{

    __block BOOL isSuccessful=NO;
    [nvramVariableDict enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL * _Nonnull stop) {

        NSTask *task=[[NSTask alloc] init];
        task.launchPath=@"/usr/sbin/nvram";
        NSString *arg=[NSString stringWithFormat:@"%@=%@",key,obj];
        task.standardOutput=[NSPipe pipe];
        task.standardError=[NSPipe pipe];
        task.arguments=@[arg];
        [task launch];
        NSData *standardOutput=[[task.standardOutput fileHandleForReading] readDataToEndOfFile];
        if (standardOutput && standardOutput.length>0) {
            NSLog(@"%@",[[NSString alloc] initWithData:standardOutput encoding:NSUTF8StringEncoding]);
        }
        NSData *errorOutput=[[task.standardError fileHandleForReading] readDataToEndOfFile];
        if (errorOutput && errorOutput.length>0) {
            NSLog(@"%@",[[NSString alloc] initWithData:errorOutput encoding:NSUTF8StringEncoding]);

        }
        [task waitUntilExit];
        if (task.terminationStatus!=0){
            isSuccessful=NO;
            *stop=YES;
            return;
        }
        isSuccessful=YES;
    }];
    return isSuccessful;
}

-(void)prepareStartOSInstallForComponent:(NSDictionary *)component{
    /*
     steps:
     1. if erase, then verify that the disk is blank. If not, then warn user and ask to open disk utility
     2. If disk is blank, then create file and folder and copy launchd items.
     3. Open macOS installer.
     4. Profit
     */

    
    NSFileManager *fm=[NSFileManager defaultManager];
    BOOL erase=[component[@"erase"] boolValue];

    //workflow marked as install macos and erase so check to see if it has a system
    //folder on it. if it does, throw a -99 error which will kick off disk utility
    //and user can erase.
    if (erase==YES){
        if ([fm fileExistsAtPath:[self.selectedVolume.filePath stringByAppendingPathComponent:@"System"]]){

            NSError *err=[NSError errorWithDomain:@"TCS" code:-99 userInfo:@{NSLocalizedDescriptionKey:@"Disk Not Erased"}];

            self.workflowCompletionBlock(err, nil);
            return;
        }
    }
/*
 2. If disk is blank, then create file and folder and copy launchd items.
 3. Open macOS installer.
 4. Profit

 */
    //if we are erasing the drive and we got this far, it means there is
    //no system folder so we need to create a few files to trigger
    //migrate install.

    NSError *err=nil;
    NSString *csPath=[self.selectedVolume.filePath stringByAppendingPathComponent:@"System/Library/CoreServices"];

    NSString *systemVersionFilePath=[csPath stringByAppendingPathComponent:@"SystemVersion.plist"];
    NSString *dsPath=[self.selectedVolume.filePath stringByAppendingPathComponent:@"private/var/db/dslocal/nodes/Default"];

    if ([fm fileExistsAtPath:csPath]==NO){
        if([fm createDirectoryAtPath:csPath withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0755} error:&err]==NO){
            self.workflowCompletionBlock(err, nil);

            return;
        }
        if([fm createFileAtPath:systemVersionFilePath contents:[NSData data] attributes:@{NSFilePosixPermissions:@0755}]==NO){

            NSError *error=[NSError easyErrorWithTitle:@"Error Creating File"
                                                  body:[NSString stringWithFormat:@"An error occurred when creating the System Version file at %@. Please check the volume and try again.",systemVersionFilePath]
                                                  line:__LINE__
                                                  file:@__FILE__];

            [[NSAlert alertWithError:error] runModal];
        }

    }
    if ([fm fileExistsAtPath:dsPath]==NO){
        if([fm createDirectoryAtPath:dsPath withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0755} error:&err]==NO){

            self.workflowCompletionBlock(err, nil);
            return;

        }
    }

    if (component[@"skip_setup_assistant"] && [component[@"skip_setup_assistant"] boolValue]==YES){

        NSString *appleSetupDonePath=[self.selectedVolume.filePath stringByAppendingPathComponent:@"private/var/db/.AppleSetupDone"];


        if([fm createFileAtPath:appleSetupDonePath contents:[NSData data] attributes:@{NSFilePosixPermissions:@0400,NSFileOwnerAccountID:@0, NSFileGroupOwnerAccountID:@0}]==NO){

            NSError *error=[NSError easyErrorWithTitle:@"Error Creating File"
                                                  body:[NSString stringWithFormat:@"An error occurred when creating the AppleSetupDone file at %@. Please check the volume and try again.",appleSetupDonePath]
                                                  line:__LINE__
                                                  file:@__FILE__];

            [[NSAlert alertWithError:error] runModal];
        }

    }

    if (component[TCSURLKEY]){
        if([self setupFirstBoot:self]==NO){

            return;
        }
        NSString *encodedString=[component[TCSURLKEY] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *installerPathURL=[NSURL URLWithString:encodedString];
        if (installerPathURL) {
            NSString *destination=[[self firstBootFolder] stringByAppendingPathComponent:installerPathURL.path.lastPathComponent];

            if ([encodedString hasPrefix:@"http"]){
            //we have a url, so just open and launch. no need to copy.
               NSString *mountPath=[TCSUtility mountDMGAtPath:encodedString];

                if (!mountPath){
                    NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Error mounting disk image at %@",destination]}];

                    self.workflowCompletionBlock(err, nil);

                    return;
                }

                [self addPackagesWithComponent:component installerPath:mountPath];

            }
            else {

                self.fileCopyController=[[TCSCopyFileController alloc] init];

                [self.fileCopyController copyFile:installerPathURL.path toPath:destination updateBlock:^(NSInteger percentComplete, NSString *statusMessage) {
                    self.workflowStatusBlock(percentComplete, [NSString stringWithFormat:@"Copying Installer (%li%%)",percentComplete]);

                } completionBlock:^(NSError *err) {
                    if (err) {
                        NSLog(@"error: %@",err.localizedDescription);

                        if (self.shouldCancel==YES) {
                            NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:@""}];

                            self.workflowCompletionBlock(err, nil);

                        }
                        else {
                            self.workflowCompletionBlock(err, nil);

                        }
                    }
                    else {
                        [self addPackagesWithComponent:component installerPath:destination];
                    }
                }];

            }
        }
        else {
            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"invalid URL for installer"}];
            self.workflowCompletionBlock(err, nil);
            return;
        }
    }
    else {
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"no URL for installer specified"}];
        self.workflowCompletionBlock(err, nil);
        return;

    }
}
-(void)addPackagesWithComponent:(NSDictionary *)component installerPath:(NSString *)installerPath{
    //create placeholder component for first boot packages
    NSMutableDictionary *firstBootComponent=[NSMutableDictionary dictionary];
    if (component[@"additional_package_urls"] && [component[@"additional_package_urls"] count] > 0) {
        firstBootComponent[TCSURLKEY]=component[@"additional_package_urls"][0];


    }

    self.workflowStatusBlock(-1, @"Preparing first boot scripts and packages");
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        if (component[@"first_boot_reboot"]){
            NSLog(@"first_boot_reboot set");
            firstBootComponent[@"first_boot_reboot"]=component[@"first_boot_reboot"];
        }
        else {
            NSLog(@"first_boot_reboot unset");
        }
        if (component[@"wait_for_mac_buddy_exit"]){
            firstBootComponent[@"wait_for_mac_buddy_exit"]=component[@"wait_for_mac_buddy_exit"];

        }
        firstBootComponent[TCSCOMPONENTINSTALLER]=installerPath;
        firstBootComponent[@"type"]=TCSCOMPONENTPACKAGEFOLDER;
        firstBootComponent[TCSCFIRSTBOOT]=@YES;
        [self installPackagesWithComponent:firstBootComponent];
    });

}
-(void)prepareStartOSInstallForComponentOld:(NSDictionary *)component{
    if ([self setupFirstBoot:self]==NO){
        return;
    }
    NSMutableArray *configSettings=[NSMutableArray array];
    [configSettings addObject:@"first_boot_startosinstall=1"];
    NSString *username=component[@"username"];
    NSString *password=component[@"password"];
    NSString *volumeName=component[@"volume_name"];
    NSString *credentialScriptURLString=component[@"credentials_script_url"];

    BOOL promptForCredentials=[component[@"prompt_os_installer_credentials"] boolValue];
    NSString *osInstallerURLString=component[@"url"];
    NSArray *additionalPackages=component[@"additional_package_urls"];
    NSString *firstBootItemsFolder=[[self firstBootFolder] stringByAppendingPathComponent:@"items"];
    NSString *firstBootItemsFolderLocalPath=[TCSFIRSTBOOTFOLDERLOCALPATH stringByAppendingPathComponent:@"items"];

    if (credentialScriptURLString){

        NSURL *credentialScriptURL=[NSURL URLWithString:credentialScriptURLString];


        NSFileManager *fm=[NSFileManager defaultManager];

        NSError *err;
        NSString *destinationCustomScriptPath=[[self firstBootFolder] stringByAppendingPathComponent:credentialScriptURL.lastPathComponent];
        if ([fm fileExistsAtPath:destinationCustomScriptPath]){

            if([fm removeItemAtPath:destinationCustomScriptPath error:&err]==NO){
                self.workflowCompletionBlock(err,nil);
                return;

            }
        }
        if([fm copyItemAtURL:credentialScriptURL toURL:[NSURL fileURLWithPath:destinationCustomScriptPath] error:&err]==NO){
            self.workflowCompletionBlock(err,nil);
            return;

        }
    }
    else if (promptForCredentials==YES){

        NSDictionary *credentialInfo=self.usernamePasswordBlock(@"Enter Credentials",username );
        if (!credentialInfo) {
            NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"Cancelled"]}];

            self.workflowCompletionBlock(err,nil);
            return;
        }
        username=credentialInfo[@"username"];
        password=credentialInfo[@"password"];
    }
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        if (additionalPackages && additionalPackages.count>0){

            NSURL *additionalPackagesURL=[NSURL URLWithString:additionalPackages[0]];
            [configSettings addObject:[NSString stringWithFormat:@"additional_packages=\"%@\"",firstBootItemsFolderLocalPath]];

            NSError *err;


            if([self copyItemsFromPath:additionalPackagesURL.path destinationPath:firstBootItemsFolder extension:@"pkg" error:&err]==NO){
                self.workflowCompletionBlock(err,nil);
                return;
            }

        }

        if (osInstallerURLString  && osInstallerURLString.length>0){

            NSLog(@"os installer: %@",osInstallerURLString);
            [configSettings addObject:[NSString stringWithFormat:@"os_installer=\"%@\"",[firstBootItemsFolderLocalPath stringByAppendingPathComponent:osInstallerURLString.lastPathComponent]]];

            NSFileManager *fm=[NSFileManager defaultManager];
            NSError *err;

            NSString *prefixToRemove = @"file://";
            NSString *osInstallerURLPath = [osInstallerURLString copy];
            if ([osInstallerURLString hasPrefix:prefixToRemove])
                osInstallerURLPath = [osInstallerURLString substringFromIndex:[prefixToRemove length]];



            if ([fm fileExistsAtPath:[firstBootItemsFolder stringByAppendingPathComponent:osInstallerURLString.lastPathComponent]]==YES){
                NSError *removeError;
                if([fm removeItemAtPath:[firstBootItemsFolder stringByAppendingPathComponent:osInstallerURLString.lastPathComponent] error:&removeError]==NO){
                    self.workflowCompletionBlock(removeError,nil);
                    return ;

                }
            }

            if([fm copyItemAtPath:osInstallerURLPath toPath:[firstBootItemsFolder stringByAppendingPathComponent:osInstallerURLString.lastPathComponent] error:&err]==NO){
                self.workflowCompletionBlock(err,nil);

                NSLog(@"%@",err.localizedDescription);
                return;
            }
            BOOL erase=[component[@"erase"] boolValue];
            if (username && username.length>0) [configSettings addObject:[NSString stringWithFormat:@"username=\"%@\"",username]];

            if (password && password.length>0) [configSettings addObject:[NSString stringWithFormat:@"password=\"%@\"",password]];

            if (volumeName && volumeName.length>0) [configSettings addObject:[NSString stringWithFormat:@"volume_name=\"%@\"",volumeName]];

            if (erase==YES) [configSettings addObject:@"erase=1"];


            BOOL rebootAfterFirstBootResources=[component[@"first_boot_reboot"] boolValue];
            if (rebootAfterFirstBootResources==YES) [configSettings addObject:@"first_boot_reboot=1"];

            BOOL waitForSetupAsssistant=[component[@"wait_for_mac_buddy_exit"] boolValue];
            if (waitForSetupAsssistant==YES) [configSettings addObject:@"wait_for_mac_buddy_exit=1"];



            if([[configSettings componentsJoinedByString:@"\n"] writeToFile:[[self firstBootFolder] stringByAppendingPathComponent:@"first-boot-config.sh"] atomically:NO]==NO){

                NSLog(@"error writing first boot config");
                NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"error writing first boot config"]}];
                self.workflowCompletionBlock(err,nil);

                return;
            }
        }
        else {
            NSLog(@"osInstaller not specified!");
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self processNextComponent:self];

        });
    });

}

-(BOOL)setupFirstBoot:(id)sender{

    if (self.firstBootSetupDone==YES){

        NSLog(@"firstboot setup already done");
        return YES;
    }

    self.firstBootSetupDone=YES;
    NSError *err;

    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *firstBootFolder=[self firstBootFolder];

    if ([fm fileExistsAtPath:firstBootFolder]==NO){
        if([fm createDirectoryAtPath:firstBootFolder withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0755} error:&err]==NO){

            self.workflowCompletionBlock(err, nil);
            return NO;

        }
    }
    NSString *libraryPath=[self.selectedVolume.filePath stringByAppendingPathComponent:@"Library"];

    NSString *launchDaemonDir=[libraryPath stringByAppendingPathComponent:@"LaunchDaemons"];
    if([fm createDirectoryAtPath:launchDaemonDir withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0755, NSFileOwnerAccountID:@0, NSFileGroupOwnerAccountID:@0} error:&err]==NO){

        self.workflowCompletionBlock(err,nil);
        return NO;


    }
    if(chflags(libraryPath.UTF8String, SF_NOUNLINK) != 0){
        perror("Error setting sunlnk\n");
    }

    NSBundle *mainBundle=NSBundle.mainBundle;
    NSString *scriptPath=[mainBundle pathForResource:@"firstboot" ofType:@"sh"];
    NSString *launchDaemonPlist=[mainBundle pathForResource:@"com.twocanoes.mds-first-boot" ofType:@"plist"];
    if ([fm fileExistsAtPath:[firstBootFolder stringByAppendingPathComponent:scriptPath.lastPathComponent]]==YES){

        if([fm removeItemAtPath:[firstBootFolder stringByAppendingPathComponent:scriptPath.lastPathComponent] error:&err]==NO){
            self.workflowCompletionBlock(err,nil);
            return NO;
        }
    }
    if([fm copyItemAtPath:scriptPath toPath:[firstBootFolder stringByAppendingPathComponent:scriptPath.lastPathComponent] error:&err]==NO){
        NSLog(@"%@",err.localizedDescription);
        self.workflowCompletionBlock(err,nil);
        return NO;
    }

    NSMutableDictionary *scriptFileProperties = [NSMutableDictionary dictionary];
    [scriptFileProperties setObject:[NSNumber numberWithInt:0] forKey:NSFileOwnerAccountID];
    [scriptFileProperties setObject:[NSNumber numberWithInt:0] forKey:NSFileGroupOwnerAccountID];

    [scriptFileProperties setObject:[NSNumber numberWithShort:0755] forKey:NSFilePosixPermissions];

    if([fm setAttributes:scriptFileProperties ofItemAtPath:[firstBootFolder stringByAppendingPathComponent:scriptPath.lastPathComponent] error:&err]==NO) {

        NSLog(@"%@",err.localizedDescription);

    }


    if ([fm fileExistsAtPath:[launchDaemonDir stringByAppendingPathComponent:launchDaemonPlist.lastPathComponent]]==YES){
        [fm removeItemAtPath:[launchDaemonDir stringByAppendingPathComponent:launchDaemonPlist.lastPathComponent] error:nil];
    }
    if([fm copyItemAtPath:launchDaemonPlist toPath:[launchDaemonDir stringByAppendingPathComponent:launchDaemonPlist.lastPathComponent] error:&err]==NO){
        NSLog(@"%@",err.localizedDescription);
        self.workflowCompletionBlock(err,nil);
        return NO;

    }
    NSMutableDictionary *newProperties = [NSMutableDictionary dictionary];
    [newProperties setObject:[NSNumber numberWithInt:0] forKey:NSFileOwnerAccountID];
    [newProperties setObject:[NSNumber numberWithInt:0] forKey:NSFileGroupOwnerAccountID];

    [newProperties setObject:[NSNumber numberWithShort:0644] forKey:NSFilePosixPermissions];


    if([fm setAttributes :newProperties ofItemAtPath:[launchDaemonDir stringByAppendingPathComponent:launchDaemonPlist.lastPathComponent] error:&err]==NO) {

                NSLog(@"%@",err.localizedDescription);
    }
    NSString *loginLogPath=[[NSBundle mainBundle] pathForResource:@"LoginLog" ofType:@"app"];

    NSString *asidePath = [self.selectedVolume.filePath stringByAppendingPathComponent:@"var"];

    NSLog(@"removing %@ if it exists",[asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent]);

    if ([fm fileExistsAtPath:[asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent]]==YES){
        NSLog(@"removing");
        if([fm removeItemAtPath:[asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent] error:&err]==NO){

            return NO;
        }

    }

    NSLog(@"moving loginlog to %@ to run xattr",asidePath);
    if([fm copyItemAtPath:loginLogPath toPath:[asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent] error:&err]==NO){
        NSLog(@"%@",err.localizedDescription);

    }
    NSLog(@"removing com.apple.quarantine at %@", [asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent]);

    [[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/bin/xattr" withOptions:@[@"-d",@"com.apple.quarantine", [asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent]]];

    
    NSString *privHelperToolPath= [self.selectedVolume.filePath stringByAppendingPathComponent:@"Library/PrivilegedHelperTools"];

    [fm createDirectoryAtPath:privHelperToolPath withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0755, NSFileOwnerAccountID:@0, NSFileGroupOwnerAccountID:@0} error:&err];

    if ([fm fileExistsAtPath:[privHelperToolPath stringByAppendingPathComponent:loginLogPath.lastPathComponent]]==YES){
        if([fm removeItemAtPath:[privHelperToolPath stringByAppendingPathComponent:loginLogPath.lastPathComponent] error:&err]==NO){

            return NO;
        }

    }


    NSLog(@"moving %@ to %@",[asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent],[privHelperToolPath stringByAppendingPathComponent:loginLogPath.lastPathComponent] );

    if([fm moveItemAtPath:[asidePath stringByAppendingPathComponent:loginLogPath.lastPathComponent] toPath:[privHelperToolPath stringByAppendingPathComponent:loginLogPath.lastPathComponent] error:&err]==NO){
        NSLog(@"%@",err.localizedDescription);
    }
    else {
        NSLog(@"successful");
    }




    NSString *sourcePlistPath=[[NSBundle mainBundle] pathForResource:@"se.gu.it.LoginLog" ofType:@"plist"];
    NSString *launchAgentFolder= [self.selectedVolume.filePath stringByAppendingPathComponent:@"Library/LaunchAgents"];

    if([fm createDirectoryAtPath:launchAgentFolder withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0755, NSFileOwnerAccountID:@0, NSFileGroupOwnerAccountID:@0} error:&err]==NO){
        self.workflowCompletionBlock(err,nil);
        return NO;
    }


    if ([fm fileExistsAtPath:[launchAgentFolder stringByAppendingPathComponent:sourcePlistPath.lastPathComponent]]==YES){
        if([fm removeItemAtPath:[launchAgentFolder stringByAppendingPathComponent:sourcePlistPath.lastPathComponent] error:&err]==NO){
            self.workflowCompletionBlock(err,nil);
            return NO;
        }
    }

    if([fm copyItemAtPath:sourcePlistPath toPath:[launchAgentFolder stringByAppendingPathComponent:sourcePlistPath.lastPathComponent] error:&err]==NO){
        NSLog(@"%@",err.localizedDescription);
    }

    if([fm setAttributes:scriptFileProperties ofItemAtPath:[launchAgentFolder stringByAppendingPathComponent:sourcePlistPath.lastPathComponent] error:&err]==NO) {
        NSLog(@"%@",err.localizedDescription);
    }

    
    return YES;
}
-(NSString *)firstBootFolder{

    NSString *firstBootFolder=[self.selectedVolume.filePath stringByAppendingPathComponent:@"private/var/mds/first-boot/"];

    return firstBootFolder;
}
-(void)installPackagesWithComponent:(NSDictionary *)component{

    NSError *err;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *firstBootFolder=[self firstBootFolder];
    NSString *firstBootItemsFolder=[firstBootFolder stringByAppendingPathComponent:@"items"];

    if([fm createDirectoryAtPath:firstBootItemsFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

        NSLog(@"%@",err.localizedDescription);
        self.workflowCompletionBlock(err,nil);
        return;

    }

    NSString *packageFolderPath = [component objectForKey:TCSURLKEY];
    if (!packageFolderPath ){
        NSLog(@"invalid package folder: %@",packageFolderPath);
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"invalid package folder: %@",packageFolderPath]}];

        self.workflowCompletionBlock(err,nil);
        return;

    }

    NSURL *packagesPathURL=[NSURL URLWithString:packageFolderPath];
    if ([fm fileExistsAtPath:packagesPathURL.path]==NO){

        NSLog(@"package folder not found package folder: %@",packagesPathURL.path);
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"package folder not found package folder: %@",packagesPathURL.path]}];

        self.workflowCompletionBlock(err,nil);

    }

    //        NSLog(@"Found first boot");
        if([self setupFirstBoot:self]==NO){
            NSLog(@"failed setting up first boot");

            return;
        }
         NSMutableArray *configSettings=[NSMutableArray array];

         BOOL rebootAfterFirstBootResources=[component[@"first_boot_reboot"] boolValue];
         if (rebootAfterFirstBootResources==YES) [configSettings addObject:@"first_boot_reboot=1"];

         BOOL waitForSetupAsssistant=[component[@"wait_for_mac_buddy_exit"] boolValue];
         if (waitForSetupAsssistant==YES) [configSettings addObject:@"wait_for_mac_buddy_exit=1"];

        BOOL workflowShouldConfigureWifi=[self.selectedWorkflow[@"workflow_should_configure_wifi"] boolValue];
        NSString *workflowWifiSSID=self.selectedWorkflow[@"workflow_wifi_ssid"];
        NSString *workflowWifiPassword=self.selectedWorkflow[@"workflow_wifi_password"];


        if (workflowShouldConfigureWifi==YES && workflowWifiSSID && workflowWifiSSID.length>0 && workflowWifiPassword && workflowWifiPassword.length>0) {

            [configSettings addObject:@"workflow_should_configure_wifi=1"];
            [configSettings addObject:[NSString stringWithFormat:@"workflow_wifi_ssid=\'%@\'",workflowWifiSSID]];
            [configSettings addObject:[NSString stringWithFormat:@"workflow_wifi_password=\'%@\'",workflowWifiPassword]];


        }
        BOOL workflowShouldSkipRosetta=[component[@"workflow_should_skip_rosetta"] boolValue];


        if (workflowShouldSkipRosetta==YES){
            NSLog(@"workflowShouldSkipRosetta");

            [configSettings addObject:@"workflow_should_skip_rosetta=1"];
        }
        else {
            NSLog(@"no workflowShouldSkipRosetta");
        }


        NSString *filePath = [[self firstBootFolder] stringByAppendingPathComponent:@"first-boot-config.sh"];
        if ([fm fileExistsAtPath:filePath]==false && [[configSettings componentsJoinedByString:@"\n"] writeToFile:filePath atomically:NO]==NO){

             NSLog(@"error writing first boot config");
             NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@",@"error writing first boot config"]}];
             self.workflowCompletionBlock(err,nil);

             return;
         }
        NSLog(@"writing first-boot-config.sh done");




//        NSError *err;
    NSLog(@"copying from %@ to %@",packagesPathURL.path, firstBootItemsFolder);
        if([self copyItemsFromPath:packagesPathURL.path destinationPath:firstBootItemsFolder extension:@"pkg"  error:&err]==NO){
            self.workflowCompletionBlock(err,nil);
            return;
        }

//    else {
//        NSLog(@"unimplemented not first boot package install");
//    }

    /*
     firstBootComponent[TCSCOMPONENTINSTALLER]=component[TCSURLKEY];

     */

    if (component[TCSCOMPONENTINSTALLER]) {
        self.workflowCompletionBlock(nil,component[TCSCOMPONENTINSTALLER]);

    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self processNextComponent:self];

        });
    }
}
-(BOOL)loadConfig{
    NSLog(@"loading config");
    NSURL *appFolderURL = [[NSBundle.mainBundle bundleURL] URLByDeletingLastPathComponent];

    NSLog(@"appFolderURL: %@",appFolderURL);
    NSURL *appConfigInfoURL =[appFolderURL URLByAppendingPathComponent:@"config.plist"];
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:appConfigInfoURL.path]==false){
        appConfigInfoURL=[NSURL fileURLWithPath:TESTAPPCONFIGPATH];
    }
    NSLog(@"loading configInfo");
    NSDictionary *configInfo = [self dictionaryForPathURL:appConfigInfoURL];
    NSString *serverURLString;

    if (!(serverURLString=configInfo[@"serverurl"])){
        return NO;
    }
    NSLog(@"serverURLString: %@",serverURLString);
    self.serverURL=[NSURL URLWithString:serverURLString];
    NSLog(@"serverURL: %@",self.serverURL);
    if (![fm fileExistsAtPath:self.serverURL.path]){
        NSLog(@"serverURL does not exist. checking for qa path");
        if([fm fileExistsAtPath:TESTCONFIGPATH]){
            NSLog(@"using qa path");
            self.serverURL=[NSURL fileURLWithPath:TESTCONFIGPATH];
        }
    }
    NSLog(@"serverURL: %@",self.serverURL);
    self.config=[self dictionaryForPathURL:self.serverURL];
    if (self.config) {
        self.workflows=[self.config objectForKey:TCSWORKFLOWKEY];
        self.hasSupport=[[self.config objectForKey:@"has_mds_support"] boolValue];
    }
    NSMutableArray *filteredWorkflows=[NSMutableArray array];
    NSString *thisModel=[TCSUtility machineModel];


    NSURL *deployFolder=[[self.serverURL URLByDeletingLastPathComponent] URLByDeletingLastPathComponent];

    NSURL *configFolderURL=[deployFolder URLByAppendingPathComponent:@"Config"];
    NSURL *workflowOptionsScript=[configFolderURL URLByAppendingPathComponent:@"workflow_options.sh"];

    if ([fm fileExistsAtPath:workflowOptionsScript.path]){

        NSTask *task=[[NSTask alloc] init];
        task.launchPath=workflowOptionsScript.path;
        task.arguments=@[];
        task.standardOutput=[NSPipe pipe];
        task.standardError=[NSPipe pipe];
        [task launch];

        NSData *standardOutput=[[task.standardOutput fileHandleForReading] readDataToEndOfFile];
        if (standardOutput && standardOutput.length>0) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:standardOutput options:0 error:nil];
            NSString *autorunName=[responseDict objectForKey:@"workflow_autorun_name"];
            NSString *autorunTimeout=[responseDict objectForKey:@"workflow_autorun_timeout"];
            NSString *workflowAutorunVolume=[responseDict objectForKey:@"workflow_autorun_volume"];
            NSMutableDictionary *mutableConfig=[self.config mutableCopy];
            mutableConfig[@"autorun"]=autorunName;

            if (autorunName){
                NSLog(@"replacing autorun workflow name to %@",autorunName);
                mutableConfig[@"autorun"]=autorunName;

            }
            if (autorunTimeout){
                NSLog(@"replacing autorun timeout %@",autorunTimeout);
                mutableConfig[@"autorun_time"]=@([autorunTimeout integerValue]);

            }
            if (workflowAutorunVolume){
                NSLog(@"replacing autorun workflow volume to %@",workflowAutorunVolume);
                mutableConfig[@"target_volume_name"]=workflowAutorunVolume;

            }
            self.config=[NSDictionary dictionaryWithDictionary:mutableConfig];

        }

        NSData *errorOutput=[[task.standardError fileHandleForReading] readDataToEndOfFile];
        if (errorOutput && errorOutput.length>0) {
            NSLog(@"%@",[[NSString alloc] initWithData:errorOutput encoding:NSUTF8StringEncoding]);

        }
        [task waitUntilExit];

    }


    [self.workflows enumerateObjectsUsingBlock:^(NSDictionary *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

        if (currWorkflow[@"allowed_models"]){

            NSArray *allowedModels=[currWorkflow[@"allowed_models"] componentsSeparatedByString:@"\n"];

            [allowedModels enumerateObjectsUsingBlock:^(NSString *currModel, NSUInteger idx, BOOL * _Nonnull stopAllowedModels) {
                NSString *strippedCurrentModel=[currModel stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                //skip empty line.
                if (strippedCurrentModel.length==0) {
                    return;
                }

                else if ([thisModel.lowercaseString containsString:strippedCurrentModel.lowercaseString]==YES){
                    [filteredWorkflows addObject:currWorkflow];
                    *stopAllowedModels=YES;
                    return;
                }
            }];


        }
        else {
            [filteredWorkflows addObject:currWorkflow];

        }

    }];
    self.workflows=filteredWorkflows;
    NSLog(@"config loaded");
    return YES;

}

-(NSString *)macOSInstallerForLaunch{

    NSString *launchMacOSInstaller=[self.config objectForKey:@"launch_macos_installer_name"];

    if (launchMacOSInstaller && launchMacOSInstaller.length>0){

        NSURL *deployFolder=[[self.serverURL URLByDeletingLastPathComponent] URLByDeletingLastPathComponent];

        NSURL *macOSInstallers=[deployFolder URLByAppendingPathComponent:@"macOS"];

        return [macOSInstallers.path stringByAppendingPathComponent:launchMacOSInstaller];

    }
    return nil;
}
-(BOOL)copyItemsFromPath:(NSString *)sourceFolder destinationPath:(NSString *)destinationFolder extension:(nullable NSString *)extension error:(NSError **)err{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSArray *packagesPaths=[fm contentsOfDirectoryAtPath:sourceFolder error:err];

    if (!packagesPaths){
        self.workflowCompletionBlock(*err,nil);
        return NO;
    }
    NSLog(@"copying package from %@",sourceFolder);
    __block BOOL isSuccessful=YES;
    __block NSError *copyErr;

    [packagesPaths enumerateObjectsUsingBlock:^(NSString *currPath, NSUInteger idx, BOOL * _Nonnull stop) {

        NSString *currentPackagePath=[destinationFolder stringByAppendingPathComponent:currPath.lastPathComponent];
        if ([fm fileExistsAtPath:currentPackagePath]==YES){
            NSLog(@"file exists:%@",currentPackagePath);
            return;
        }

        if (extension && extension.length>0 && [[[currPath pathExtension] lowercaseString] isEqualToString:extension.lowercaseString]==NO){
            NSLog(@"wrong extension: %@",extension.lowercaseString);
            return;
        }




        if ([fm fileExistsAtPath:currentPackagePath]==YES){
            NSLog(@"removing: %@",currentPackagePath);

            NSError *removeError;
            if([fm removeItemAtPath:currentPackagePath error:&removeError]==NO){
                self.workflowCompletionBlock(removeError,nil);
                return ;

            }
        }


        NSLog(@"copying %@ to %@",[[sourceFolder stringByAppendingPathComponent:currPath] stringByResolvingSymlinksInPath], currentPackagePath);

        if ([fm copyItemAtPath:[[sourceFolder stringByAppendingPathComponent:currPath] stringByResolvingSymlinksInPath] toPath:currentPackagePath error:&copyErr]==NO){

            NSLog(@"%@",copyErr.localizedDescription);
            isSuccessful=NO;
            *stop=YES;
        }

    }];

    if (isSuccessful==NO){
        *err=copyErr;

    }
    return isSuccessful;
}

-(NSArray <NSString *> *)workflowNames{
    NSLog(@"getting workflowNames");
    NSMutableArray <NSString *> *workflowNames = [NSMutableArray array];
    NSArray *workflowArray;
    if (!(workflowArray=self.workflows)){
        NSLog(@"No workflows found. that is weird.");

        return nil;
    }
    [workflowArray enumerateObjectsUsingBlock:^(NSDictionary *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {

        NSLog(@"adding %@",currWorkflow[@"name"]);
        [workflowNames addObject:currWorkflow[@"name"]];
    }];
    NSLog(@"workflow names: %@",workflowNames);

    return workflowNames;
}

-(NSArray <NSString *> *)workflowDescriptions{

    NSMutableArray <NSString *> *workflowDescriptions = [NSMutableArray array];
    NSArray *workflowArray;
    if (!(workflowArray=self.workflows)){
        return nil;
    }
    [workflowArray enumerateObjectsUsingBlock:^(NSDictionary *currWorkflow, NSUInteger idx, BOOL * _Nonnull stop) {
        if (currWorkflow[@"description"]){
            [workflowDescriptions addObject:currWorkflow[@"description"]];
        }
        else {
            [workflowDescriptions addObject:@"No description provided."];

        }
    }];

    return workflowDescriptions;
}
-(BOOL)hasSupportContract{
    return self.hasSupport;

}
-(NSURL *)currentVolumeURL{
    NSLog(@"getting currentVolumeURL");
    NSFileManager *fm=[NSFileManager defaultManager];
    if([fm fileExistsAtPath:TESTCONFIGPATH]){

        return [NSURL fileURLWithPath:@"/Users/Shared/mds-test"];
    }
    NSURL *pathURL=[NSBundle.mainBundle.bundleURL URLByDeletingLastPathComponent];
    NSArray <TCVolume *> *attachedDisks = [TCDeviceManager.sharedDeviceManager attachedDisks];
    __block BOOL isComplete = NO;
    while ([pathURL.path isEqualToString:@"/"]==NO && pathURL.path.length>0) {

        [attachedDisks enumerateObjectsUsingBlock:^(TCVolume * _Nonnull currVol, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([currVol.filePath.lowercaseString isEqualToString:pathURL.path.lowercaseString]) {
                isComplete=YES;
                return;
            }

        }];
        if (isComplete == true){

            break;
        }
        pathURL=[pathURL URLByDeletingLastPathComponent];
    }
    NSLog(@"currentVolumeURL:%@",pathURL);

    return pathURL;
}
-(NSDictionary <NSString *, id> *)dictionaryForPathURL:(NSURL *)pathURL{
    NSError *err;
    NSString *dictionaryString = [NSString stringWithContentsOfFile:pathURL.path encoding:NSUTF8StringEncoding error:&err];

    if (!dictionaryString) {
        return nil;
    }
    dictionaryString=[self replaceCurrentVolumePathPlaceholders:dictionaryString];


    NSData *plistData=[dictionaryString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *plistDict=[NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:nil error:&err];

    if (!plistDict){
        NSLog(@"%@",err.localizedDescription);
        return nil;
    }

    return plistDict;
}
-(NSString *)replaceCurrentVolumePathPlaceholders:(NSString *)inString{

    NSMutableString *updatedString = [inString mutableCopy];
    NSURL *currentVolURL = [self currentVolumeURL];
    NSDictionary <NSString *, NSString *> *placeholders=@{@"{{current_volume_path}}":currentVolURL.resourceSpecifier};


    [placeholders enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {

        [updatedString replaceOccurrencesOfString:key withString:obj options:NSLiteralSearch range:NSMakeRange(0, updatedString.length)];
    }];
    return [NSString stringWithString:updatedString];
}

- (void)copyCompleted:(id)sender withError:(NSError *)error {
    NSLog(@"copy completed");
    if (error){

        NSLog(@"error: %@",error.localizedDescription);
    }

}

- (void)percentCompleted:(float)percentCompleted {

    NSLog(@"percent completed: %2.2f",percentCompleted);
}

@end
