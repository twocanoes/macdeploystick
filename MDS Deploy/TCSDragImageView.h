//
//  TCSDragImageView.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/3/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSDragImageView : NSImageView

@end

NS_ASSUME_NONNULL_END
