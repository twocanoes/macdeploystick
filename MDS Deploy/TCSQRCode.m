//
//  TCSQRCode.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 10/27/22.
//  Copyright © 2022 Twocanoes Software. All rights reserved.
//

#import "TCSQRCode.h"
#import <CoreImage/CoreImage.h>
#import <AppKit/AppKit.h>

@implementation TCSQRCode
- (NSImage *)generateQRCode:(NSString *)text{

    NSLog(@"generateQRCode");
    CGImageRef cgImage = [self createQRImageForString:text size:CGSizeMake(512, 512)];

    NSImage *image = [[NSImage alloc] initWithCGImage:cgImage size:NSMakeSize(512, 512)];


    
    CGImageRelease(cgImage);
    return image;

}
- (CGImageRef)createQRImageForString:(NSString *)string size:(CGSize)size {
    NSLog(@"createQRImageForString");
  // Setup the QR filter with our string
  CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
  [filter setDefaults];

  NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
  [filter setValue:data forKey:@"inputMessage"];
  CIImage *image = [filter valueForKey:@"outputImage"];

  // Calculate the size of the generated image and the scale for the desired image size
  CGRect extent = CGRectIntegral(image.extent);
  CGFloat scale = MIN(size.width / CGRectGetWidth(extent), size.height / CGRectGetHeight(extent));

  // Since CoreImage nicely interpolates, we need to create a bitmap image that we'll draw into
  // a bitmap context at the desired size;
  size_t width = CGRectGetWidth(extent) * scale;
  size_t height = CGRectGetHeight(extent) * scale;
  CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
  CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);

#if TARGET_OS_IPHONE
  CIContext *context = [CIContext contextWithOptions:nil];
#else
  CIContext *context = [CIContext contextWithCGContext:bitmapRef options:nil];
#endif

  CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];

  CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
  CGContextScaleCTM(bitmapRef, scale, scale);
  CGContextDrawImage(bitmapRef, extent, bitmapImage);

  // Create an image with the contents of our bitmap
  CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);

  // Cleanup
  CGContextRelease(bitmapRef);
  CGImageRelease(bitmapImage);
    NSLog(@"scaledImage:%@",scaledImage);

  return scaledImage;
}
@end
