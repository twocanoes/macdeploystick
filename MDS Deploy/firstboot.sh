#!/bin/sh

#  firstboot.sh
#  MDS
#
#  Created by Timothy Perfitt on 3/10/21.
#  Copyright © 2021 Twocanoes Software. All rights reserved.

#
ConnectToWifi()
{
    echo ConnectToWifi
    if [ -n "${workflow_should_configure_wifi}" ] && [ -n "${workflow_wifi_ssid}" ] && [ -n "${workflow_wifi_password}" ] ; then
        echo "Wifi settings found. Attempting to apply to interface(s)"
        network_setup_path="/usr/sbin/networksetup"
        for this_network_interface in $("${network_setup_path}" -listallhardwareports 2> /dev/null | sort |  grep '^Device: ' | cut -c 9-); do

            echo "trying to setup wifi on ${this_network_interface}"
            if /usr/sbin/networksetup -getairportnetwork "${this_network_interface}" &> /dev/null; then
                /usr/sbin/networksetup -setairportnetwork "${this_network_interface}" "${workflow_wifi_ssid}" "${workflow_wifi_password}"
            fi
        done
    else
        echo "no wifi settings"
    fi
}
CleanUp()
{
    echo "cleaning up"
    if [ -e "/Library/LaunchDaemons/com.twocanoes.mds-first-boot.plist" ]; then
        rm "/Library/LaunchDaemons/com.twocanoes.mds-first-boot.plist"
        #do not unload com.twocanoes.mds-first-boot.plist because that will stop this script. ouch.
    fi

    if [ -e "/Library/LaunchAgents/se.gu.it.LoginLog.plist" ]; then
        rm "/Library/LaunchAgents/se.gu.it.LoginLog.plist"
        if [ -e "/Library/PrivilegedHelperTools/LoginLog.app" ]; then
            killall LoginLog
            rm -rf "/Library/PrivilegedHelperTools/LoginLog.app"
        fi

    fi
    if [ -d "/var/mds" ]; then
        rm -rf "/var/mds"
    fi

}
CheckForNetwork()
{
    echo CheckForNetwork
    local test

    test=$(ifconfig -a inet 2>/dev/null | sed -n -e '/127.0.0.1/d' -e '/0.0.0.0/d' -e '/inet/p' | wc -l)
    if [ "${test}" -gt 0 ]; then
        NETWORKUP="YES"
    else
        NETWORKUP="NO"
    fi
}

# don't loop if no values
shopt -s nullglob

script_dir=$(dirname $0)
if [ -e "${script_dir}/first-boot-config.sh" ]; then
    source "${script_dir}/first-boot-config.sh"
fi

echo "=========First boot script========="
echo "checking Wifi"
ConnectToWifi
CheckForNetwork

if [ "${NETWORKUP}" == "NO" ]; then
    echo
    echo "======================================================="
    echo "No network interface is up. Trying every 5 seconds for a minute. Please turn on WiFi, connect an ethernet cable, or add in a PhoneNet terminating resistor."
    echo "======================================================="
fi

for i in 1 2 3 4 5 6 7 8 9 10; do

    CheckForNetwork
    if [ "${NETWORKUP}" == "YES" ]; then
        echo "Network is up"
        break
    else
        echo "Network not up. Sleeping for 5 seconds"
        sleep 5
    fi

done



arch=$(/usr/bin/arch)

if [ "${arch}" == "arm64" ]; then
    echo "Apple Silicon so checking if we should install rosetta..."
    if [ "${workflow_should_skip_rosetta}" == "1" ] ; then 
        echo "Skipping rosetta install"
    else
        echo "arm64. Installing rosetta."

        for i in 1 2 3 4 5 6 7 8 9 10; do

            if  /usr/sbin/softwareupdate --install-rosetta --agree-to-license ; then
                break;
            fi
            echo "error installing rosetta. sleeping and then trying again (10 tries max)"
            sleep 1

        done
    fi
fi


if [ "${os_installer: -4}" == ".dmg" ]; then
    echo "using disk image"

    dmg_mount_point=$(hdiutil mount "${os_installer}" -plist | grep '/Volumes' | sed 's|.*<string>\(/Volumes/.*\)</string>.*|\1|g' )

    if [ $? -ne 0 ]; then
        echo "could not mount installer at ${os_installer}"
        exit -1
    fi

    if [ "${dmg_mount_point}" = "" ]; then
        echo "f get dmg mount point"
        exit -1
    fi
    start_os_installer=("${dmg_mount_point}"/Applications/*.app/Contents/Resources/startosinstall)
    
else
    start_os_installer="${os_installer}/Contents/Resources/startosinstall"

fi

if [ -e "/Library/PrivilegedHelperTools/LoginLog.app/Contents/MacOS/LoginLog" ]; then
    /Library/PrivilegedHelperTools/LoginLog.app/Contents/MacOS/LoginLog &
fi
if [ -n "${first_boot_startosinstall}" ] ; then

    if [ ! -e "${start_os_installer}" ]; then
        echo "cannot find os installer at ${start_os_installer}. Exiting."
        exit -1
    fi
    if  [ -n "${start_os_installer}" ]; then
        PARAMS=(--nointeraction --agreetolicense --forcequitapps)
        if [ -n "${erase}" ]; then
            PARAMS+=(--eraseinstall)
        fi
        if [ -e  "${script_dir}/discover_credentials.sh" ]; then 
            echo "discovering credentials"
            
            credentials=($("${script_dir}/discover_credentials.sh"))
            if [  $? ne 0 ]; then
            
                echo "could not discover credentials. exiting"
                exit -1
            fi
            
            username="${credentials[0]}"
            password="${credentials[1]}"
            
            PARAMS+=(--user "${username}")

        else
            if [ -n "${username}" ]; then
                PARAMS+=(--user "${username}")
            fi
        fi
        if [ -n "${volume_name}" ]; then
            PARAMS+=(--newvolumename "${volume_name}")
        fi

    # set install_package_list to blank.
        install_package_list=()
        for file in "${script_dir}/items"/*.pkg; do
            if [[ $file != *"/*.pkg" ]]; then
                PARAMS+=("--installpackage")
                PARAMS+=("$file")
            fi
        done

        if [ "${arch}" == "arm64" ]; then
            echo "arm64"
            if [ -n "${username}" ] && [ -n "${password}" ] ; then
                PARAMS+=(--stdinpass)

                echo "running echo '**********' | ${start_os_installer}" "${PARAMS[@]}"
                echo "${password}" |  "${start_os_installer}" "${PARAMS[@]}"
            else
            
                echo "username or password not defined and we are on ARM64."
                exit -1
            fi
        else
            echo "Intel"
            echo running "${start_os_installer}" "${PARAMS[@]}"
            "${start_os_installer}" "${PARAMS[@]}"
        fi
        #should never get here since startosinstaller reboots so exit with -1 just in case we ever do
        exit -1

    else
        echo "no username, password or os_installer specified"
        exit -1
    fi
else
    echo "installing packages"
    pushd "${script_dir}"/items

    for curr_file in *.pkg; do
        echo "installing package ${curr_file}"
        installer -pkg "${curr_file}" -target /
        echo "installation complete with exit code $?. deleting \"${curr_file}\""
        rm "${curr_file}"
    done
fi

killall -9 LoginLog


if [ -n "${first_boot_reboot}" ]; then
    if [ -n "${wait_for_mac_buddy_exit}" ]; then

        found=0
        i=0
        while true; do
            echo "Looking for Setup Assistant"
            i=$((i+1))
            if [ $i -eq 30 ]; then
                echo "timing out"
                break
            fi

            if pgrep "^Setup Assistant$" 2>1 >> /dev/null ; then
                found=1
                echo "found setup assistant; sleeping for 10 seconds before checking again"
                sleep 10
            else

                if [ $found -eq 1 ]; then
                    echo "setup assistant completed. Continuing on."
                    break
                fi
                echo "waiting for setup assistant"
                sleep 1
            fi
        done
    fi
    CleanUp
    reboot
else
    CleanUp
fi

