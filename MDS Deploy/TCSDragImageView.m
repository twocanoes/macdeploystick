//
//  TCSDragImageView.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/3/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSDragImageView.h"

@implementation TCSDragImageView

//- (void)drawRect:(NSRect)dirtyRect {
//    [super drawRect:dirtyRect];
//
//    // Drawing code here.
//}

- (void)mouseDown:(NSEvent *)event{
    [self.window performWindowDragWithEvent:event];
}
@end
