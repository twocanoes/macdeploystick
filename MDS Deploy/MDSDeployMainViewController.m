//
//  ViewController.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 3/9/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "MDSDeployMainViewController.h"
//#import <DiskDevice/DiskDevice.h>
#import "TCDeviceManager.h"
#import "TCSWorkflowManager.h"
#import "TCSSingleChoiceVariableWindow.h"
#import "TCSMultipleChoiceVariableWindow.h"
#import "TCSVariableWindow.h"
#import "TCSUsernamePasswordWindow.h"
#import "TCSUtility.h"
#import "TCSQRCode.h"
#import "TCTaskHelper.h"

@interface MDSDeployMainViewController()
@property (nonatomic, strong) NSArray <NSString *> *workflowNames;
@property (nonatomic, strong) NSArray <NSString *> *partitionNames;
@property (nonatomic, strong) NSArray <TCVolume *> *partitions;
@property (nonatomic, strong) NSString *workflowDescription;
@property (nonatomic, strong) NSString *macOSInstallerForLaunch;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSImage *qrCode;
@property (nonatomic, strong) NSString *qrCodeText;

@property (strong) NSTimer *countdownTimer;
@property (assign) NSInteger countdownSecondsRemaining;
@property (nonatomic, assign) BOOL isAutoRunning;

@property (nonatomic, assign) BOOL isRunning;
@property (weak) IBOutlet NSButton *runButton;
@property (nonatomic, strong) NSString *runButtonTitle;
@property (nonatomic, strong) NSString *progressText;
@property (strong) IBOutlet TCSSingleChoiceVariableWindow *singleChoiceVariableWindow;
@property (strong) IBOutlet TCSMultipleChoiceVariableWindow *multipleChoiceVariableWindow;
@property (strong) IBOutlet TCSUsernamePasswordWindow *usernamePasswordWindow;

@property (weak) IBOutlet NSPopUpButton *partitionPopupButton;
@property (weak) IBOutlet NSPopUpButton *workflowPopupButton;
@property (nonatomic, assign) NSInteger workflowSelectedIndex;
@end
@implementation MDSDeployMainViewController
- (IBAction)openInstallerButtonPressed:(id)sender {

    if ([self macOSInstallerForLaunch]){

        [self runInstallerAtPath:[self macOSInstallerForLaunch]];
    }
    
}
-(NSString *)pathForInstallerInPath:(NSString *)mountPoint{
    NSString *appPathDMG=[mountPoint stringByAppendingPathComponent:@"Applications"];

    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    NSArray *appDirContents=[fm contentsOfDirectoryAtPath:appPathDMG error:&err];
    if (!appDirContents){
        NSLog(@"%@",err.localizedDescription);
        return nil;
    }
    NSLog(@"searching for the installer...");
    __block NSString *appPath=nil;
    [appDirContents enumerateObjectsUsingBlock:^(NSString *currFilename, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([[[currFilename pathExtension] lowercaseString] isEqualToString:@"app"]){
            NSLog(@"found %@",currFilename);
            *stop=YES;
            appPath=[appPathDMG stringByAppendingPathComponent:currFilename];
        }
    }];
    return appPath;

}
-(void)runInstallerAtPath:(NSString *)path{
    NSFileManager *fm=[NSFileManager defaultManager];

    __block NSString *appPath=path;
    NSLog(@"preparing to launch installer at %@",path);
    if ([[[path pathExtension] lowercaseString] isEqualToString:@"dmg"]){
        NSLog(@"installer is in a dmg. Mounting");
        NSString *mountPoint=[TCSUtility mountDMGAtPath:path];
        if (mountPoint){
            NSLog(@"found mount point at %@",mountPoint);
            appPath=[self pathForInstallerInPath:mountPoint];
        }
    }
    else {
        BOOL isDir;
        if ([fm fileExistsAtPath:path isDirectory:&isDir] && isDir==YES && [[[path pathExtension] lowercaseString] isEqualToString:@"app"]){
            NSLog(@"found .app in app path. Using %@.",path);

            appPath=path;
        }
        else {
            NSLog(@"Searching for installer in %@",path);

            appPath=[self pathForInstallerInPath:path];
        }

    }
    if ([fm fileExistsAtPath:appPath]){
        NSLog(@"launching %@",appPath);

        NSLog(@"removing  com.apple.quarantine from install at %@",appPath);

        [[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/bin/xattr" withOptions:@[@"-d",@"com.apple.quarantine", appPath]];
        [[NSWorkspace sharedWorkspace] launchApplication:appPath];
        sleep(5);

        NSLog(@"seeing if we are autorunning and automaton is plugged in.");

        if ([TCSWorkflowManager.shared.config objectForKey:@"autorun"] ){
            NSError *err;
            NSString *automatonPath=[self automatonPath];
             if (!automatonPath) {
                 NSAlert *alert=[[NSAlert alloc] init];
                 alert.messageText=@"No Automaton";
                 alert.informativeText=@"No automaton was found plugged in. Continuing manually.";

                 [alert runModal];

             }

            [self sendAutomatonCommandAtPath:automatonPath command:@"installerm1" error:&err];

            if (err) {
                [[NSAlert alertWithError:err] runModal];
            }

        }

        [NSApp terminate:self];

    }
    else{

        NSLog(@"the app path does not exist at %@.", appPath);
    }

    return ;
}

- (IBAction)runWorkflowButtonPressed:(id)sender {
    if (self.isRunning==YES && self.isAutoRunning==NO){

        [[TCSWorkflowManager shared] cancelCurrentOperations:self];
        self.progressText=@"Canceling. Please wait....";
        return;
    }

    if (self.isAutoRunning && self.countdownTimer && [self.countdownTimer isValid]==YES){
        [self.countdownTimer invalidate];

        self.isRunning=NO;
        return;
    }
    if (self.isAutoRunning==NO){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Run Workflow?";
        alert.informativeText=@"Are you sure you want to run the selected workflow?";

        [alert addButtonWithTitle:@"OK"];
        [alert addButtonWithTitle:@"Cancel"];
        NSInteger res=[alert runModal];
        if (res==NSAlertSecondButtonReturn) return;
    }

    NSInteger selectedPartitionIndex=[self.partitionPopupButton indexOfSelectedItem];

    if (selectedPartitionIndex>self.partitions.count-1){
        NSLog(@"invalid partition index");
        return;
    }

    TCVolume *selectedVolume=[self.partitions objectAtIndex:selectedPartitionIndex];

    if (!selectedVolume){
        NSLog(@"invalid selected Volume");
    }
    NSInteger selectedWorkflowIndex=[self.workflowPopupButton indexOfSelectedItem];

    self.isRunning=YES;
    [[TCSWorkflowManager shared] runWorkflowAtIndex:selectedWorkflowIndex withVolume:selectedVolume simpleVariableBlock:^NSDictionary * _Nonnull(NSString * _Nonnull query, NSString *defaultAnswer) {
        [self.singleChoiceVariableWindow setPrompt:query];
        [self.singleChoiceVariableWindow setDefaultAnswer:defaultAnswer];
       NSModalResponse res=[NSApp runModalForWindow:self.singleChoiceVariableWindow];

        if (res==NSModalResponseCancel) return nil;
        return self.singleChoiceVariableWindow.answer;

    }
    choiceVariableBlock:^NSDictionary * _Nonnull(NSString * _Nonnull query, NSArray * _Nonnull selections, BOOL addOther) {
        [self.multipleChoiceVariableWindow setPrompt:query];
        self.multipleChoiceVariableWindow.showOther=addOther;

        [self.multipleChoiceVariableWindow setupPopupAnswers:selections];

       NSModalResponse res= [NSApp runModalForWindow:self.multipleChoiceVariableWindow];
        if (res==NSModalResponseCancel) return nil;

        return @{@"response":self.multipleChoiceVariableWindow.answer};

    }
     usernamePasswordBlock:^NSDictionary * _Nonnull(NSString * _Nonnull query, NSString *defaultUsername) {
        [self.usernamePasswordWindow setPrompt:query];


        if (defaultUsername) [self.usernamePasswordWindow setDefaultUsername:defaultUsername];

        
        NSModalResponse res= [NSApp runModalForWindow:self.usernamePasswordWindow];
        if (res==NSModalResponseCancel) return nil;

        return self.usernamePasswordWindow.answer;

    }

     updateBlock:^(NSInteger percentComplete, NSString * _Nonnull statusMsg) {

        dispatch_async(dispatch_get_main_queue(), ^{

            self.progressText=statusMsg;
            NSLog(@"%@",statusMsg);
        });
    }
    completionBlock:^(NSError * _Nonnull err, NSString *installerPath) {
        dispatch_async(dispatch_get_main_queue(), ^{

            self.isRunning=NO;
            if (!err) {

                if (installerPath) {

                    if (self.isAutoRunning==YES){
                        [self runInstallerAtPath:installerPath];

                    }
                    else {

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Workflow Complete";
                        alert.informativeText=@"The workflow has completed successfully and the target volume prepared for package installation after the installer is run. Any external drives can be disconnected after opening the installer. Click Open Installer to use the installer to select the target volume to complete the installation. After the installer completes, the Mac will be rebooted and any workflow customizations will be applied.";
                        [alert addButtonWithTitle:@"Open Installer"];
                        [alert addButtonWithTitle:@"Cancel"];

                        NSInteger res=[alert runModal];
                        if (res==NSAlertFirstButtonReturn){
                            [self runInstallerAtPath:installerPath];
                        }

                    }


                }
                else {
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Workflow Complete";
                    alert.informativeText=@"The workflow has completed successfully.";
                    [alert addButtonWithTitle:@"Restart"];
                    [alert addButtonWithTitle:@"OK"];
                    NSInteger res=[alert runModal];
                    if (res==NSAlertFirstButtonReturn) [self reboot];

                }


            }
            else {
                if (err.code==-99) {
                    NSLog(@"disk is not erased. checking to see if autorun is enabled");

                    if ([TCSWorkflowManager.shared.config objectForKey:@"autorun"] ){
                        if (getuid()==0){

                            NSFileManager *fm=[NSFileManager defaultManager];
                            NSString *recoveryApp=@"/System/Installation/CDIS/KeyRecoveryAssistant.app";
                            if ([fm fileExistsAtPath:recoveryApp]){

                                [[NSWorkspace sharedWorkspace] launchApplication:recoveryApp];
                                sleep(15);


                            }
                            else {
                                NSAlert *alert=[[NSAlert alloc] init];
                                alert.messageText=@"Launch failed!";
                                alert.informativeText=@"KeyRecoveryAssistant could not be launched.";

                                [alert runModal];
                                return;
                            }
                        }
                        else {

                            NSLog(@"skipping launching KeyRecoveryAssistant because we are not root, which probably means we are running as a test");
                        }
                        NSError *err;
                        NSString *automatonPath=[self automatonPath];
                         if (!automatonPath) {
                             NSAlert *alert=[[NSAlert alloc] init];
                             alert.messageText=@"No Automaton";
                             alert.informativeText=@"No automaton was found plugged in. Continuing manually.";

                             [alert runModal];

                         }

                        [self sendAutomatonCommandAtPath:automatonPath command:@"erasem1" error:&err];

                        if (err) {
                            [[NSAlert alertWithError:err] runModal];
                        }
                        return;



                    }
                    [self performSegueWithIdentifier:@"openDiskUtilitySegueID" sender:self];


                }
                else if (err.code==-100) {
                    return;
                }
                else {
                    [[NSAlert alertWithError:err] runModal];
                }

            }
        });
    }];
}

-(NSString *)automatonPath{
    NSLog(@"autorunning so verifying that we have an automaton plugged in");
    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *err;
    NSArray *dirContents;
    if(!(dirContents=[fm contentsOfDirectoryAtPath:@"/dev" error:&err])){

        [[NSAlert alertWithError:err] runModal];
        NSLog(@"weird. auto running but can't get contents of /dev. giving up.");

        return nil;

    }

    __block NSString *automatonPath;
    [dirContents enumerateObjectsUsingBlock:^(NSString *itemName, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([itemName.lowercaseString containsString:@"cu.usb"]){
            automatonPath=[@"/dev" stringByAppendingPathComponent:itemName];
            *stop=YES;
            return ;
        }
    }];
    return automatonPath;
}
-(BOOL)sendAutomatonCommandAtPath:(NSString *)automatonPath command:(NSString *)command error:(NSError **)err{


    if (automatonPath){

        NSLog(@"telling automaton to take over");
        int filedesc = open( automatonPath.UTF8String, O_RDWR );

        if (filedesc<0) {
            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The connection could not be opened to the arduino at %@. Please make sure no other processes are connected",automatonPath]}];

            return NO;

        }
        NSLog(@"writing erase command");
        NSString *fullCommand=[NSString stringWithFormat:@"\r\n%@\r\n",command];
        ssize_t n = write(filedesc,fullCommand.UTF8String, fullCommand.length);
        if (n < 0) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Write to automaton failed";
            alert.informativeText=@"";
            [alert runModal];
            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The automaton could not be written to at %@. Please make sure no other processes are connected.",automatonPath]}];

            return NO;

        }
        close(filedesc);
        [NSApp terminate:self];

    }
    else {
        *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"no automaton plugged in. Proceeding manually."}];
    }

    return YES;
}
-(void)reboot{

    system("/sbin/reboot");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.partitions=[NSMutableArray array];
    self.workflowNames=[NSMutableArray array];
    // Do any additional setup after loading the view.

    [self addObserver:self forKeyPath:@"isRunning" options:NSKeyValueObservingOptionNew  context:nil];

    [self addObserver:self forKeyPath:@"workflowSelectedIndex" options:NSKeyValueObservingOptionNew  context:nil];

//    self.macOSInstallerForinsertLaunch



}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{

    if ([keyPath isEqualToString:@"isRunning"]){

        if (self.isRunning==YES){

            self.runButtonTitle=@"Cancel";
        }
        else {
            self.runButtonTitle=nil;

        }


    }
    else if ([keyPath isEqualToString:@"workflowSelectedIndex"]){
        NSArray *workflowDescriptions=[TCSWorkflowManager.shared workflowDescriptions];
        if (workflowDescriptions && workflowDescriptions.count>0) {
            self.workflowDescription=[workflowDescriptions objectAtIndex:self.workflowSelectedIndex];
        }

    }
}



-(void)refresh{

    NSMutableArray *validPartitions=[NSMutableArray array];
   NSMutableArray *validPartitionNames=[NSMutableArray array];

    [self willChangeValueForKey:@"workflowNames"];
    self.workflowNames = [TCSWorkflowManager.shared workflowNames];
    [self didChangeValueForKey:@"workflowNames"];

    NSArray <TCVolume *> *attachedDisks = [TCDeviceManager.sharedDeviceManager attachedDisks];
    NSArray *sortedArray = [attachedDisks sortedArrayUsingComparator: ^(TCVolume * obj1, TCVolume* obj2) {

        return [obj1.device compare:obj2.device];
    }];

    [sortedArray enumerateObjectsUsingBlock:^(TCVolume * currVolume, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([currVolume.filePath isEqualToString:@"/"]==NO &&
            [currVolume.filePath hasPrefix:@"/Volumes"]==NO) {
            return;
        }

        if (currVolume.totalSize.longLongValue<20.*1024.*1024.*1024.){
            return;
        }
        [validPartitionNames addObject:[NSString stringWithFormat:@"%@ (%@) - %@",currVolume.diskName,currVolume.device,currVolume.humanSize]];
        [validPartitions addObject:currVolume];
    }];

    [self willChangeValueForKey:@"partitionNames"];
    self.partitionNames=[NSArray arrayWithArray:validPartitionNames];
    [self didChangeValueForKey:@"partitionNames"];

    self.partitions=[NSArray arrayWithArray:validPartitions];
    
    self.macOSInstallerForLaunch=[TCSWorkflowManager.shared macOSInstallerForLaunch];
    self.workflowSelectedIndex=0;
    if ([TCSWorkflowManager.shared.config objectForKey:@"autorun"] ){
        NSString *autorunWorkflowName=[TCSWorkflowManager.shared.config objectForKey:@"autorun"];
        NSInteger autorunTime=[[TCSWorkflowManager.shared.config objectForKey:@"autorun_time"] integerValue];
        NSString *targetVolumeName=[TCSWorkflowManager.shared.config objectForKey:@"target_volume_name"];

        if ([self.workflowNames containsObject:autorunWorkflowName]){

            self.workflowSelectedIndex=[self.workflowNames indexOfObject:autorunWorkflowName];

            __block BOOL partitionSelected=NO;
            if (targetVolumeName){
               __block BOOL hasUntitled=NO;
                __block NSInteger untitledIndex=-1;
                [self.partitions enumerateObjectsUsingBlock:^(TCVolume * currVol, NSUInteger idx, BOOL * _Nonnull stop) {

                    if ([currVol.diskName isEqualToString:targetVolumeName]) {
                        [self.partitionPopupButton selectItemAtIndex:idx];
                        partitionSelected=YES;
                        *stop=YES;
                    }
                    if ([currVol.diskName isEqualToString:@"Untitled"]){
                        hasUntitled=YES;
                        untitledIndex=idx;
                    }
                }];
                if (partitionSelected==NO && hasUntitled==YES) {
                    NSLog(@"selecting Untitled");
                    partitionSelected=YES;
                    [self.partitionPopupButton selectItemAtIndex:untitledIndex];
                }
                else {
                    NSLog(@"Untitled not found");
                }
            }
            else {
                partitionSelected=YES;
            }

            if (autorunTime<0) {
                NSLog(@"auto run time is less than zero, so exiting");
                return;
            }
            if (partitionSelected==YES){
                self.countdownSecondsRemaining=autorunTime;
                self.isRunning=YES;
                self.isAutoRunning=YES;
                self.progressText=[NSString stringWithFormat:@"autorunning in %li seconds....",self.countdownSecondsRemaining];

                self.countdownTimer=[NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    self.progressText=[NSString stringWithFormat:@"autorunning in %li seconds....",self.countdownSecondsRemaining];
                    self.countdownSecondsRemaining--;
                    if (self.countdownSecondsRemaining<0) {
                        [self.countdownTimer invalidate];
                        [self runWorkflowButtonPressed:self];
                    }

                }];
            }
            else {
                NSLog(@"The selected target volume does not exist. Cancelling autorun");
            }

        }


        
    }
}

- (void)viewWillAppear{
    NSFileManager *fm=[NSFileManager defaultManager];

    [NSApp activateIgnoringOtherApps:YES];
    [self.view.window orderFrontRegardless];

    [self refresh];
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *qrCodeScriptPath=[mainBundle pathForResource:@"qrcode" ofType:@"sh"];

    NSString *appFolder=[[mainBundle bundlePath] stringByDeletingLastPathComponent];
    NSString *deployFolderPath=[appFolder stringByDeletingLastPathComponent];
    NSString *configFolder=[deployFolderPath stringByAppendingPathComponent:@"Config"];
//    TESTCONFIGPATH
    if ([fm fileExistsAtPath:configFolder]==NO && [fm fileExistsAtPath:TESTCONFIGPATH]==YES){
        configFolder=[TESTCONFIGPATH stringByDeletingLastPathComponent];
    }
    if ([fm fileExistsAtPath:configFolder]){
        NSString *qrCodeOverride=[configFolder stringByAppendingPathComponent:@"qrcode.sh"];
        if ([fm fileExistsAtPath:qrCodeOverride]){

            qrCodeScriptPath=qrCodeOverride;
        }
    }


    TCTaskHelper *taskHelper = [TCTaskHelper sharedTaskHelper];
    NSString *text=[taskHelper runCommand:qrCodeScriptPath withOptions:@[]];
    if (text){
//        TCSQRCode *qrCodeGenerator = [[TCSQRCode alloc] init];
//
//        NSImage *qrCodeImage = [qrCodeGenerator generateQRCode:text];

        NSError* error = NULL;

    }
    NSArray *workflowDescriptions=[TCSWorkflowManager.shared workflowDescriptions];

    if (!workflowDescriptions || workflowDescriptions.count==0){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No workflows found";
        alert.informativeText=@"There are no applicable workflows for this mac.";

        [alert addButtonWithTitle:@"Quit"];
        [alert runModal];
        [NSApp terminate:self];


    }
//    if ([TCSWorkflowManager.shared  hasSupportContract]==NO){
//
//        [self performSelector:@selector(showTrial:) withObject:self afterDelay:0.1];
//    }



}
-(void)showTrial:(id)sender{


    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Support MDS";
    alert.informativeText=@"Support MDS by purchasing support. This supports development of new features, new hardware and OS versions. It also provides you with high priority feature requests and escalated support. Additionally, it makes you feel good.";

    [alert addButtonWithTitle:@"OK"];
    [alert runModal];




}
-(void)viewDidAppear{
    [self.view.window setInitialFirstResponder:self.view];

}
- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
