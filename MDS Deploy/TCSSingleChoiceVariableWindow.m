//
//  TCSSIngleChoiceVariableWindow.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/8/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSSingleChoiceVariableWindow.h"
@interface TCSSingleChoiceVariableWindow()
@property (weak) IBOutlet NSTextField *answerTextField;

@end
@implementation TCSSingleChoiceVariableWindow
-(void)setDefaultAnswer:(NSString *)inDefaultAnswer{
    self.answerTextField.stringValue=inDefaultAnswer;
}

- (void)okButtonPressed:(id)sender{

    [super okButtonPressed:self];
    self.answer=@{@"response":self.answerTextField.stringValue};
}
@end
