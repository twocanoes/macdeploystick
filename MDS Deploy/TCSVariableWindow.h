//
//  TCSVariableWindow.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/8/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSReturnWindow.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSVariableWindow : TCSReturnWindow
- (IBAction)okButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
-(void)setPrompt:(NSString *)inPrompt;
@property (strong) NSDictionary *answer;
@property (weak) IBOutlet NSTextField *promptTextField;
@end

NS_ASSUME_NONNULL_END
