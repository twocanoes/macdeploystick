//
//  TCSUsernamePasswordWindow.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/9/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSVariableWindow.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSUsernamePasswordWindow : TCSVariableWindow
-(void)setDefaultUsername:(NSString *)inDefaultAnswer;

@end

NS_ASSUME_NONNULL_END
