//
//  TCSUsernamePasswordWindow.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/9/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSUsernamePasswordWindow.h"

@interface TCSUsernamePasswordWindow ()
@property (weak) IBOutlet NSTextField *usernameTextField;
@property (weak) IBOutlet NSTextField *passwordTextField;

@end

@implementation TCSUsernamePasswordWindow

-(void)setDefaultUsername:(NSString *)inDefaultAnswer{
    self.usernameTextField.stringValue=inDefaultAnswer;
}

- (void)okButtonPressed:(id)sender{

    [super okButtonPressed:self];
    [self resignFirstResponder];
    self.answer=@{@"username":self.usernameTextField.stringValue,@"password":self.passwordTextField.stringValue};
}

@end

