//
//  NSApplication+SystemVersion.m
//  Winclone
//
//  Created by Tim Perfitt on 6/30/13.
//  Copyright (c) 2013 Twocanoes Software Inc. All rights reserved.
//

#import "NSApplication+SystemVersion.h"

@implementation NSApplication (SystemVersion)
- (void)getSystemVersionMajor:(int *)major
                        minor:(int *)minor
                       bugFix:(int *)bugFix;
{
    
    if ([[NSProcessInfo processInfo] respondsToSelector:@selector(operatingSystemVersion)]) {
        
        NSOperatingSystemVersion ver=[[NSProcessInfo processInfo] operatingSystemVersion];
        *major=(int)ver.majorVersion;
        *minor=(int)ver.minorVersion;
    }
    else {
        OSErr err;
        SInt32 systemVersion, versionMajor, versionMinor, versionBugFix;
        if ((err = Gestalt(gestaltSystemVersion, &systemVersion)) != noErr) goto fail;
        if (systemVersion < 0x1040)
        {
            if (major) *major = ((systemVersion & 0xF000) >> 12)* 10 +
                ((systemVersion & 0x0F00) >> 8);
            if (minor) *minor = (systemVersion & 0x00F0) >> 4;
            if (bugFix) *bugFix = (systemVersion & 0x000F);
        }
        else
        {
            if ((err = Gestalt(gestaltSystemVersionMajor, &versionMajor)) != noErr) goto fail;
            if ((err = Gestalt(gestaltSystemVersionMinor, &versionMinor)) != noErr) goto fail;
            if ((err = Gestalt(gestaltSystemVersionBugFix, &versionBugFix)) != noErr) goto fail;
            if (major) *major = versionMajor;
            if (minor) *minor = versionMinor;
            if (bugFix) *bugFix = versionBugFix;
        }
        
        return;
        
    fail:
        NSLog(@"Unable to obtain system version: %ld", (long)err);
        if (*major) *major = 10;
        if (*minor) *minor = 0;
        if (*bugFix) *bugFix = 0;
    }
}

@end
