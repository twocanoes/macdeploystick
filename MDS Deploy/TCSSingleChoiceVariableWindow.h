//
//  TCSSIngleChoiceVariableWindow.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/8/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSVariableWindow.h"

NS_ASSUME_NONNULL_BEGIN

@interface TCSSingleChoiceVariableWindow : TCSVariableWindow
-(void)setDefaultAnswer:(NSString *)inDefaultAnswer;
@end

NS_ASSUME_NONNULL_END
