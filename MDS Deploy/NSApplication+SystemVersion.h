//
//  NSApplication+SystemVersion.h
//  Winclone
//
//  Created by Tim Perfitt on 6/30/13.
//  Copyright (c) 2013 Twocanoes Software Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSApplication (SystemVersion)
- (void)getSystemVersionMajor:(int *)major
                        minor:(int *)minor
                       bugFix:(int *)bugFix;

@end
