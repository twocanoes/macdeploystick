//
//  TCSVariableWindow.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/8/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSVariableWindow.h"

@implementation TCSVariableWindow

-(void)setPrompt:(NSString *)inPrompt{
    self.promptTextField.stringValue=inPrompt;
}
- (void)okButtonPressed:(id)sender{

    [self close];
    [NSApp stopModalWithCode:NSModalResponseOK];

}

- (void)cancelButtonPressed:(id)sender{
    [self close];
    [NSApp stopModalWithCode:NSModalResponseCancel];
}
- (BOOL)canBecomeKeyWindow {
    return YES;
}

@end
