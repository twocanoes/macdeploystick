//
//  TCSEraseVolumeViewController.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 5/7/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSEraseVolumeViewController.h"

@interface TCSEraseVolumeViewController ()

@end

@implementation TCSEraseVolumeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (IBAction)openDiskUtilityButtonPressed:(id)sender {
    NSString *recoveryApp=@"/System/Installation/CDIS/KeyRecoveryAssistant.app";

    [[NSWorkspace sharedWorkspace] openApplicationAtURL:[NSURL fileURLWithPath:recoveryApp] configuration:NSWorkspaceOpenConfiguration.configuration completionHandler:^(NSRunningApplication * _Nullable app, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{

            [NSApp terminate:self];
        });

    }];

}
- (IBAction)cancelButtonPressed:(id)sender {
    [self.view.window close];
}

@end
