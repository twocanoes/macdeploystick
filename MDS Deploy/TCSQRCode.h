//
//  TCSQRCode.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 10/27/22.
//  Copyright © 2022 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSQRCode : NSObject
- (NSImage *)generateQRCode:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
