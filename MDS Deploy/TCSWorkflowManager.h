//
//  TCSWorkflowManager.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 3/9/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <DiskDevice/DiskDevice.h>
#import "TCVolume.h"
#import "TCSCopyFileController.h"
#define TESTAPPCONFIGPATH @"/Users/Shared/mds-test/Deploy/Applications/config.plist"
#define TESTCONFIGPATH @"/Users/Shared/mds-test/Deploy/Config/config.plist"
NS_ASSUME_NONNULL_BEGIN

@interface TCSWorkflowManager : NSObject <TCSFileCopyDelegateProtocol>
@property (strong) NSDictionary *config;

+ (instancetype)shared;
-(NSArray *)installers;
-(NSArray <NSString *> *)workflowNames;
-(NSArray <NSString *> *)workflowDescriptions;
-(void)cancelCurrentOperations:(id)sender;
-(BOOL)runWorkflowAtIndex:(NSInteger)index withVolume:(TCVolume *)volume simpleVariableBlock:(NSDictionary * (^)(NSString *query, NSString *defaultAnswer))simpleVariableBlock choiceVariableBlock:(NSDictionary * (^)(NSString *query, NSArray *selections, BOOL addOther))choiceVariableBlock usernamePasswordBlock:(NSDictionary * (^)(NSString *query, NSString *defaultAnswer))usernamePasswordBlock updateBlock:(void (^)(NSInteger percentComplete, NSString * statusMessage))update completionBlock:(void (^)( NSError *err, NSString *installerPath))completion;
-(NSString *)macOSInstallerForLaunch;
-(BOOL)hasSupportContract;
@end

NS_ASSUME_NONNULL_END
