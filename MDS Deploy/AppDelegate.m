//
//  AppDelegate.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 3/9/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "AppDelegate.h"
#import <IOKit/pwr_mgt/IOPMLib.h>
#import "TCSUtility.h"
#import "NSError+EasyError.h"
#import "TCSWorkflowManager.h"
//#import <DiskDevice/DiskDevice.h>
#import "TCSUtility.h"
#import <CoreImage/CoreImage.h>
#import "MDS-Bridging-Header.h"
@interface AppDelegate (){
    IOPMAssertionID assertionID;
}
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {


    IOPMAssertionCreateWithName(kIOPMAssertionTypePreventUserIdleSystemSleep,
                                kIOPMAssertionLevelOn, CFSTR("No sleep for MDS Deploy"), &assertionID);

    NSString *bundleID=[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleIdentifier"];
    [[NSRunningApplication runningApplicationsWithBundleIdentifier:bundleID] enumerateObjectsUsingBlock:^(NSRunningApplication * _Nonnull currApp, NSUInteger idx, BOOL * _Nonnull stop) {
        [currApp activateWithOptions:NSApplicationActivateIgnoringOtherApps];
    }];

}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
