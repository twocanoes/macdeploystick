//
//  TCSMultipleChoiceVariableWindow.h
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/8/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSVariableWindow.h"

NS_ASSUME_NONNULL_BEGIN

@interface TCSMultipleChoiceVariableWindow : TCSVariableWindow
@property (assign) BOOL showOther;
-(void)setupPopupAnswers:(NSArray *)popupAnswers;

@end

NS_ASSUME_NONNULL_END
