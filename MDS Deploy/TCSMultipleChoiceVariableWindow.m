//
//  TCSMultipleChoiceVariableWindow.m
//  MDS Deploy
//
//  Created by Timothy Perfitt on 4/8/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "TCSMultipleChoiceVariableWindow.h"

@interface TCSMultipleChoiceVariableWindow()
@property (weak) IBOutlet NSPopUpButton *answerPopupButton;
@property (weak) IBOutlet NSTextField *otherAnswerTextField;



@end
@implementation TCSMultipleChoiceVariableWindow
- (IBAction)popupButtonChanged:(id)sender {

    if (self.showOther==YES && self.answerPopupButton.indexOfSelectedItem==self.answerPopupButton.itemTitles.count-1){
        self.otherAnswerTextField.hidden=NO;
    }
    else {
        self.otherAnswerTextField.hidden=YES;
    }
}

-(void)setupPopupAnswers:(NSArray *)popupAnswers{

    [self.answerPopupButton removeAllItems];
    [popupAnswers enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.answerPopupButton insertItemWithTitle:title atIndex:self.answerPopupButton.itemArray.count];

    }];
    if (self.showOther==YES){
        [self.answerPopupButton insertItemWithTitle:@"Other..." atIndex:self.answerPopupButton.itemArray.count];

    }

}
- (void)okButtonPressed:(id)sender{

    [super okButtonPressed:self];
    if (self.showOther==YES && self.answerPopupButton.indexOfSelectedItem==self.answerPopupButton.itemTitles.count-1) {
        self.answer=@{@"response":self.otherAnswerTextField.stringValue};
    }
    else {
        self.answer=@{@"response":self.answerPopupButton.selectedItem.title};
    }
}


@end
