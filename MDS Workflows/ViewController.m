//
//  ViewController.m
//  MDS Workflows
//
//  Created by Timothy Perfitt on 10/2/24.
//  Copyright © 2024 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
