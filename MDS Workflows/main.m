//
//  main.m
//  MDS Workflows
//
//  Created by Timothy Perfitt on 10/2/24.
//  Copyright © 2024 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
