//
//  TCSXPCHelper.h
//  com.twocanoes.mdshelpertool
//
//  Created by Timothy Perfitt on 4/4/22.
//  Copyright © 2022 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSXPCHelper : NSObject
void logLine(char *line);
+(BOOL)isCorrectlySignedClientWithPID:(int)pid;

@end

NS_ASSUME_NONNULL_END
