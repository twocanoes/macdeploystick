//
//  MDSTests.swift
//  MDSTests
//
//  Created by Timothy Perfitt on 7/22/24.
//  Copyright © 2024 Twocanoes Software. All rights reserved.
//

import XCTest

final class MDSTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }
//    func testAcronameTest() throws {
//        let a = AcronameDeviceIOKit()
//        let s = a.device()
//        print(s)
//    }
    func testHubAddress() throws {

        let a = AcronameDeviceIOKit()
        let s = try? a.hubAddress(deviceAddress: 0x1120000)

        if let hubAddress = s?.hub, let port = s?.port, let vs = a.vendorSerialNumber(deviceAddress:hubAddress){
            print("Hub serial: \(vs) Port:\(port)")
        }

    }
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
