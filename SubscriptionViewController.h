//
//  SubscriptionViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/28/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubscriptionViewController : NSViewController
+ (instancetype)shared ;
-(SubscriptionViewController *)selectionSubscriptionViewController;
@property (weak) IBOutlet NSString *message;

@end

NS_ASSUME_NONNULL_END
