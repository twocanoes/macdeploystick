//
//  TCSUtility.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "TCSWebserverSetting.h"
NS_ASSUME_NONNULL_BEGIN
@interface NSFileManager (NSFileManagerDirectoryContentFullPath)
-(NSArray *)contentsOfDirectoryFullPathsAtPath:(NSString *)path error:(NSError **)error;


@end

@interface TCSUtility : NSObject
+(NSArray *)listOfMedia;
+(NSString *)apfsVolumeUUIDFromDevice:(NSString *)path;
+(NSString *)devicePathForFirstVolumeInContainer:(NSString *)containerUUID;
+(NSString *)createUserPackageAtPath:(NSString *)inPackagePath withUID:(NSString *)inUID fullname:(NSString *)inFullname shortname:(NSString *)inShortname password:(NSString *)inPassword isAdmin:(BOOL)inIsAdmin isAutologin:(BOOL)inIsAutologin isHidden:(BOOL)inIsHidden passwordHint:(NSString *)inPasswordHint jpegPhoto:(NSString *)inJpegPhoto shell:(NSString *)inShell outputPackageName:(NSString *)outputPackageName;
+(NSDictionary *)containerAndVolumesFromVolumeUUID:(NSString *)volumeUUID;
+(NSInteger)currentOSLongversionNumber;
+(NSString *)variableStringFromURL:(NSURL *)inURL;
+ (NSString *)getSerialNumber;
+ (NSString *)machineModel;
+(NSString *)getIPAddress;
+ (NSData *)calculateShadowHashData:(NSString *)pwd;
+(NSArray *)md5HashArrayFromFile:(NSString *)path;
+(NSDictionary *)manifestForURL:(NSString *)inURL fileHashArray:(NSArray *)hashArray;
+(BOOL)appendString:(NSString *)inString toEndOfFile:(NSString *)filePath;
+(void)openMunkiAdmin:(id)sender ;
//+(void)checkForServiceUpdates:(id)sender;
+(NSString *)encryptPassword:(NSString *)inPass;
+(BOOL)isProcessRunning:(NSString *)processName;
+(void)updateTLSCertIfNeed:(id)sender;
+(NSData *)pbkdf2Password:(NSString *)inPassword;
+(NSString *)mountDMGAtPath:(NSString *)path;
void logLine(char *line);
+(BOOL)isARM;
@end
NS_ASSUME_NONNULL_END
