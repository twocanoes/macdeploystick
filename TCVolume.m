//  Created by Timothy Perfitt on 1/26/12.
//  Copyright (c) 2015 Twocanoes Software Inc. All rights reserved.

#import "TCVolume.h"
#import "TCTaskWrapperWithBlocks.h"
@implementation TCVolume

+ (BOOL)supportsSecureCoding { return YES; }

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_mbr forKey:@"mbr"];
    [coder encodeObject:_partitionID forKey:@"partitionID"];
    [coder encodeObject:_filePath forKey:@"filePath"];
    [coder encodeObject:_device forKey:@"device"];
    [coder encodeObject:_drivePath forKey:@"drivePath"];
    [coder encodeObject:_info forKey:@"info"];
    [coder encodeObject:_diskName forKey:@"diskName"];
    [coder encodeObject:_usageDescription forKey:@"usageDescription"];
    [coder encodeBool:_isVM  forKey:@"isVM"];
    [coder encodeObject:_vmUsername forKey:@"vmUsername"];
    [coder encodeObject:[[_vmPassword dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0] forKey:@"vmPassword"];
    [coder encodeObject:_humanSize forKey:@"humanSize"];
    [coder encodeObject:_fileSystemType forKey:@"fileSystemType"];
    [coder encodeObject:_freeSpace forKey:@"freeSpace"];
    [coder encodeObject:_totalSize forKey:@"totalSize"];
    [coder encodeObject:[NSNumber numberWithBool:_isBootable] forKey:@"isBootable"];
    [coder encodeObject:[NSNumber numberWithBool:_isRemovable] forKey:@"isRemovable"];
    [coder encodeObject:_volumeUUID forKey:@"volumeUUID"];
    [coder encodeObject:_mediaUUID forKey:@"mediaUUID"];
    [coder encodeObject:_mediaContentUUID forKey:@"mediaContentUUID"];

}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if ((self = [super init])) {
        NSData *mbrData = [coder decodeObjectOfClass:[NSData class] forKey:@"mbr"];
        _mbr = [mbrData mutableCopy];
        NSString *partitionIDString = [coder decodeObjectOfClass:[NSString class] forKey:@"partitionID"];
        _partitionID = [partitionIDString mutableCopy];
        _filePath = [coder decodeObjectOfClass:[NSString class] forKey:@"filePath"];
        _device = [coder decodeObjectOfClass:[NSString class] forKey:@"device"];
        _drivePath = [coder decodeObjectOfClass:[NSString class] forKey:@"drivePath"];
        NSString *infoString = [coder decodeObjectOfClass:[NSString class] forKey:@"info"];
        _info = [infoString mutableCopy]; //Mutable
        _diskName = [coder decodeObjectOfClass:[NSString class] forKey:@"diskName"];
        _usageDescription = [coder decodeObjectOfClass:[NSAttributedString class] forKey:@"usageDescription"];
        _isVM = [[coder decodeObjectOfClass:[NSNumber class] forKey:@"isVM"] boolValue];
        _vmUsername = [coder decodeObjectOfClass:[NSString class] forKey:@"vmUsername"];
        NSString *base64 = [coder decodeObjectOfClass:[NSString class] forKey:_vmPassword];
        if (base64) {
            _vmPassword = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:base64 options:0]  encoding:NSUTF8StringEncoding];
        }
        _humanSize = [coder decodeObjectOfClass:[NSString class] forKey:@"humanSize"];
        _fileSystemType = [[coder decodeObjectOfClass:[NSString class] forKey:@"MVMapName"] fileSystemType];
        _freeSpace = [coder decodeObjectOfClass:[NSString class] forKey:@"freeSpace"];
        _totalSize = [coder decodeObjectOfClass:[NSString class] forKey:@"totalSize"];
        _isBootable=[[coder decodeObjectOfClass:[NSNumber class] forKey:@"isBootable"] boolValue];
        _isRemovable=[[coder decodeObjectOfClass:[NSNumber class] forKey:@"isRemovable"] boolValue];

        _volumeType = [coder decodeObjectOfClass:[NSString class] forKey:@"volumeType"];
        _volumeUUID = [coder decodeObjectOfClass:[NSUUID class] forKey:@"volumeUUID"];
        _mediaUUID = [coder decodeObjectOfClass:[NSUUID class] forKey:@"mediaUUID"];
        _mediaContentUUID = [coder decodeObjectOfClass:[NSUUID class] forKey:@"mediaContentUUID"];

        _showLine=NO;
    }
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        _showLine = NO;
        _mbr = [NSMutableData new];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    TCVolume *copy = [[[self class] allocWithZone: zone] init];
    copy->_mbr = [_mbr copyWithZone:zone];
    copy->_partitionID = [_partitionID copyWithZone:zone];
    copy.filePath = _filePath;
    copy.device = _device;
    copy.drivePath = _drivePath;
    copy.info = _info;
    copy.diskName = _diskName;
    copy.usageDescription = _usageDescription;
    copy.isVM = _isVM;
    copy.vmUsername = _vmUsername;
    copy.vmPassword = _vmPassword;
    copy.humanSize = _humanSize;
    copy.fileSystemType = _fileSystemType;
    copy.freeSpace = _freeSpace;
    copy.totalSize = _totalSize;
    copy.volumeType = _volumeType;
    copy.showLine = _showLine;
    copy.isBootable = _isBootable;
    copy.isRemovable = _isRemovable;
    copy.mediaSizeRaw = _mediaSizeRaw;
    copy.mediaBlockSize=_mediaBlockSize;
    copy.volumeUUID = _volumeUUID;
    copy.mediaUUID = _mediaUUID;
    copy.mediaContentUUID = _mediaContentUUID;

    return copy;
}

-(void)appendMBRData:(NSData *)inData{
    [self.mbr appendData:inData];
}

-(void)appendpartitionIDData:(NSString *)inString{
    if (self.partitionID == nil) {
        self.partitionID = [NSMutableString string];
    }
    [self.partitionID appendString:inString];
}

-(void)appendInfo:(NSString *)inString{
    if (self.info == nil) {
        self.info=[NSMutableString string];
    }
    [self.info appendString:inString];
}

-(NSString *)description{

    NSMutableString *retValue=[NSMutableString string];
    if (self.filePath) [retValue appendString:[NSString stringWithFormat:@"File Path: %@\n",self.filePath]];
    if (self.mbr) [retValue appendString:[NSString stringWithFormat:@"     MBR: %@\n",[self.mbr description]]];
    if (self.partitionID) [retValue appendString:[NSString stringWithFormat:@"     Partition ID:%@\n",self.partitionID]];
    [retValue appendString:[NSString stringWithFormat:@"     File System Size:%li\n",self.filesystemSize]];
    if (self.drivePath) [retValue appendString:[NSString stringWithFormat:@"     Drive Path: %@\n", self.drivePath]];
    if (self.device) [retValue appendString:[NSString stringWithFormat:@"     Device: %@\n",self.device]];
    if (self.info) [retValue appendString:[NSString stringWithFormat:@"     Info: %@\n",self.info]];
    if (self.diskName) [retValue appendString:[NSString stringWithFormat:@"     Disk Name: %@\n",self.diskName]];
    if (self.usageDescription) [retValue appendString:[NSString stringWithFormat:@"     Usage: %@\n",[(NSAttributedString *)self.usageDescription string]]];


    if (self.humanSize) [retValue appendString:[NSString stringWithFormat:@"     Readble Size:%@\n",self.humanSize]];
    if (self.fileSystemType) [retValue appendString:[NSString stringWithFormat:@"     File System Type: %@\n",self.fileSystemType]];
    if (self.freeSpace) [retValue appendString:[NSString stringWithFormat:@"     Free Space: %@\n",self.freeSpace]];
    if (self.totalSize) [retValue appendString:[NSString stringWithFormat:@"     Total Size: %@\n",self.totalSize]];

    return [NSString stringWithString:retValue];


}

-(NSString *)diskLabelWithSize{
    return [NSString localizedStringWithFormat:@"%@ (%@ bytes)", self.diskName, self.humanSize];
}

-(void)updateSize{

    NSString *pathExtension=[self.filePath pathExtension];
    if (
        ([pathExtension isEqualToString:@"zip"] ||
         [pathExtension isEqualToString:@"gz"] ) &&
        [self.totalSize integerValue]<=0

        ) {


        if (!self.wrapperArray) self.wrapperArray=[NSMutableArray array];
        NSArray *arguments;

        NSString *command;
        NSString *unzipPath=[[NSBundle mainBundle] pathForResource:@"unzip" ofType:@""];
        //        command=[NSString stringWithFormat:@"\"%@\" -d -c  \"%@\" | /usr/bin/wc -c",pigzPath,self.filePath];

        //
        if ([pathExtension isEqualToString:@"gz"]) {
            command=[NSString stringWithFormat:@"/usr/bin/gunzip -d -c  \"%@\" | /usr/bin/wc -c",self.filePath];

        }
        else {

            command=[NSString stringWithFormat:@"\"%@\" -p  \"%@\" | /usr/bin/wc -c",unzipPath,self.filePath];
        }
        arguments = [NSArray arrayWithObjects:  @"/bin/sh",@"-c",
                     command, nil];

        __block TCTaskWrapperWithBlocks *taskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

            self.totalSize=@"-1";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSDisksChanged" object:self];

        } endBlock:^{

            [self.wrapperArray removeObject:taskWrapper];
            //            [self saveImagesLinks];
        } outputBlock:^(NSString *output) {


            self.totalSize=0;
            if (output) {

                self.totalSize=output;
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSDisksChanged" object:self];
        } errorOutputBlock:^(NSString *errorOutput) {

        } arguments:arguments];
        [self.wrapperArray addObject:taskWrapper];
        [taskWrapper startProcess];
    }
    else   if ([[self.filePath pathExtension] isEqualToString:@"gz"] && [self.totalSize integerValue]<=0)  {


    }


}

@end

