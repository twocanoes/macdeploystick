//
//  TCDeviceManager.h
//  Winclone 3
//
//  Created by Timothy Perfitt on 1/27/12.
//  Copyright (c) 2015 Twocanoes Software Inc. All rights reserved.
//

@import Foundation;

@class TCVolume;

#define DISKTYPELEGACY 0
#define DISKTYPEEFI 1
#define DISKTYPEMAC 2
#define DISKTYPECURRENTDISK 3
#define DISKTYPEUNKNOWN 4
#define DISKTYPEVOLUMEEFI 5

#define TCSLogWithFormat(fmt,...) [[TCDeviceManager sharedDeviceManager] logString:[NSString stringWithFormat:fmt,__VA_ARGS__]]
#define TCSLog(string) [[TCDeviceManager sharedDeviceManager] logString:string]
NS_ASSUME_NONNULL_BEGIN
@protocol TCSLoggingDelegate <NSObject>

@optional
-(void)log:(NSString *)logString;

@end
@interface TCDeviceManager : NSObject

@property (nonnull, readwrite, strong) NSFileManager *fileManager;
@property (nonnull, readwrite, strong) id log;
@property (assign) id delegate;
+ (instancetype)sharedDeviceManager;


- (void)diskUnmountStatus:(BOOL)stat;
- (void)diskMountStatus:(BOOL)status;


-(NSArray *)attachedDisks;
@end

NS_ASSUME_NONNULL_END
