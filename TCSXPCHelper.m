//
//  TCSXPCHelper.m
//  com.twocanoes.mdshelpertool
//
//  Created by Timothy Perfitt on 4/4/22.
//  Copyright © 2022 Twocanoes Software. All rights reserved.
//

#import "TCSXPCHelper.h"
#define LOGFILE "/Library/Logs/mdshelper.log"

@implementation TCSXPCHelper
+(BOOL)isCorrectlySignedClientWithPID:(int)pid{
    CFNumberRef value = NULL;
    CFDictionaryRef attributes = NULL;

    SecCodeRef code = NULL;
    OSStatus status;


    value = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &pid);
    if (!value) {

        logLine("Error creating number");
        return NO;

    }
    attributes = CFDictionaryCreate(kCFAllocatorDefault, (const void **)&kSecGuestAttributePid, (const void **)&value, 1, NULL, NULL);
    if (!attributes) {

        logLine("Error creating attributes");
        return NO;

    }
    status = SecCodeCopyGuestWithAttributes(NULL, attributes, kSecCSDefaultFlags, &code);
    if (status!=0) {

        logLine("Error creating SecCodeCopyGuestWithAttributes");
        return NO;

    }

    NSString *entitlement = @"anchor apple generic and certificate leaf[subject.OU] = UXP6YEHSPW";
    SecRequirementRef requirement = NULL;
    CFErrorRef errors = NULL;
    status = SecRequirementCreateWithStringAndErrors((__bridge CFStringRef)entitlement, kSecCSDefaultFlags, &errors, &requirement);
    if (status!=0) {

        logLine("Error creating SecRequirementCreateWithStringAndErrors");
        return NO;

    }
    status = SecCodeCheckValidity(code, kSecCSDefaultFlags, requirement);
    if (status!=0) {

        logLine("Request from invalid client. Refusing");
        return NO;

    }

    return YES;

}
void logLine(char *line){

    FILE *f_log;
    if((f_log=fopen(LOGFILE, "a"))==NULL){
        fprintf(stderr, "error opening log file");
        return;
    }

    size_t len=strnlen(line, 1024);
    fwrite(line,len, 1, f_log);
    fclose(f_log);

}

@end
