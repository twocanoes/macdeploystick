//
//  SubscriptionManager.h
//  MDS
//
//  Created by Timothy Perfitt on 10/28/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//struct TCSEditions {
//    NSInteger EDITION_FREE;
//    NSInteger EDITION_PRO;
//    NSInteger EDITION_ENTERPRISE;
//
//
//} edition;

typedef NS_ENUM(NSUInteger, Edition) {
    EDITION_FREE,
    EDITION_PRO,
    EDITION_ENTERPRISE
};


@interface SubscriptionManager : NSObject
+ (instancetype)shared;
-(BOOL)savePrefs;
-(void)showSubcribeWithMessage:(NSString *)message;
@property (assign) BOOL isSubscribed;
@property (assign)  Edition edition;

@end

NS_ASSUME_NONNULL_END
