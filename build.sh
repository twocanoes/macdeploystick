#!/bin/bash 
set -e 

PRODUCT_NAME="MDS"

#export SKIP_PACKAGE=1
###########################
build_avrdude="n"
echo "build arvdude? (y/N)"
read build_avrdude


get_new_mkuser="n"
echo "download mkuser? (y/N)"
read get_new_mkuser

if [ "${get_new_mkuser}" = "y" ]; then
	if  ! curl -o ./MacDeployStick/mkuser.sh https://raw.githubusercontent.com/freegeek-pdx/mkuser/main/mkuser.sh ; then
		exit -1
	fi
chmod +x ./MacDeployStick/mkuser.sh
fi


#https://raw.githubusercontent.com/freegeek-pdx/mkuser/main/mkuser.sh
build_loginlogng="y"
#echo "build loginlogng package? (y/N)"
#read build_loginlogng

agvtool bump
CURRENT_PATH=$(dirname $0)
buildNumber=$(agvtool what-version -terse)
temp_folder=$(mktemp -d "/tmp/${PRODUCT_NAME}.XXXXXXXX")




if [ -e "${CURRENT_PATH}/build/Documentation/README_Resources.txt" ]; then 
	rm -r "${CURRENT_PATH}/build/Documentation/README_Resources.txt"
fi

cp -Rv "${CURRENT_PATH}/README_Resources.txt" "${CURRENT_PATH}/build/Documentation/"



if [ "$build_avrdude" = "y" ]; then
	if  ! ./build_avrdude  ; then
		exit -1
	fi
fi

if [ "${build_loginlogng}" = "y" ]; then
	
	xcodebuild archive SKIP_INSTALL=NO -project MDS.xcodeproj -scheme LoginLog -archivePath "${temp_folder}"/LoginLog.xcarchive
	
	xcodebuild -exportArchive -archivePath "${temp_folder}"/LoginLog.xcarchive  -exportOptionsPlist "${CURRENT_PATH}/build/exportOptions.plist" -exportPath "${temp_folder}/build"
	
	if [ -e "first-run-install/Scripts/com.twocanoes.mds/LoginLog.app" ]; then
		echo "removing old loginlog.app"
		rm -rf "first-run-install/Scripts/com.twocanoes.mds/LoginLog.app"

	fi	
	cp -Rvp "${temp_folder}/build/LoginLog.app" first-run-install/Scripts/com.twocanoes.mds/
	#/usr/local/bin/packagesbuild first-run-install/"MDS First Login Installer"/"MDS First Login Installer.pkgproj"
	
fi
#if [ ! -e "${CURRENT_PATH}/php_build/mds-php-universal" ]; then
#	
#		echo "no mds-php folder! at ${CURRENT_PATH}/php_build/mds-php-universal"
#	exit -1
#fi

#echo "copying php folder for installer"

#if [ -e "${CURRENT_PATH}/build/AdditionalPackagesResources/mds-php" ]; then
#	rm -rf "${CURRENT_PATH}/build/AdditionalPackagesResources/mds-php"
#
#fi

#mkdir "${CURRENT_PATH}/build/AdditionalPackagesResources/mds-php" 
#cp -Rpfv "${CURRENT_PATH}"/php_build/mds-php-universal/* "${CURRENT_PATH}/build/AdditionalPackagesResources/mds-php/"


xcodebuild archive -project "${PRODUCT_NAME}.xcodeproj" -scheme "${PRODUCT_NAME}" -archivePath  "${temp_folder}/${PRODUCT_NAME}.xcarchive"


xcodebuild -exportArchive -archivePath "${temp_folder}/${PRODUCT_NAME}.xcarchive"  -exportOptionsPlist "${CURRENT_PATH}/build/exportOptions.plist" -exportPath "${temp_folder}/build"

echo saving symbols
mkdir -p "${CURRENT_PATH}/build/symbols/${buildNumber}"


cp -R "${temp_folder}/${PRODUCT_NAME}.xcarchive/dSYMs"/*.dSYM "${CURRENT_PATH}/build/symbols/${buildNumber}/"

open "${temp_folder}/build" 
if [ $? -ne 0 ]; then 

exit 
fi

pwd 

~/Documents/Projects/build/build.sh  "${CURRENT_PATH}/build" "${CURRENT_PATH}" "${PRODUCT_NAME}" "${temp_folder}/build/${PRODUCT_NAME}.app"

echo Please commit with git commit -a -m 'bumped version'
	
	
