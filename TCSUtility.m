//
//  TCSUtility.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSUtility.h"
#include <sys/mount.h>
#include <IOKit/IOKitLib.h>
#import "TCSDefaultsManager.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import <Security/Security.h>
#include <CommonCrypto/CommonHMAC.h>
#import "TCSConstants.h"
#include <sys/utsname.h>
#import "NSApplication+SystemVersion.h"
#import "TCSWebserverSetting.h"
#import "TCWebConnection.h"
#include <string.h>
#import "GetBSDProcessList.h"
#import "TCTaskHelper.h"
static const char ascii_dollar[] = { 0x24, 0x00 };
static unsigned const char cov_2char[64] = {
    /* from crypto/des/fcrypt.c */
    0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35,
    0x36, 0x37, 0x38, 0x39, 0x41, 0x42, 0x43, 0x44,
    0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C,
    0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 0x54,
    0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x61, 0x62,
    0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A,
    0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72,
    0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A
};

@implementation NSFileManager (NSFileManagerDirectoryContentFullPath)

- (NSArray *)contentsOfDirectoryFullPathsAtPath:(NSString *)path error:(NSError *__autoreleasing *)error{

    NSArray *contents=[self contentsOfDirectoryAtPath:path error:error];
    if (!contents) return nil;

    NSMutableArray *fullPathArray=[NSMutableArray arrayWithCapacity:contents.count];

    [contents enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL * _Nonnull stop) {
        [fullPathArray addObject:[path stringByAppendingPathComponent:filename]];
    }];
    return [NSArray arrayWithArray:fullPathArray];

}
@end
@implementation NSAttributedString (Hyperlink)

+(id)hyperlinkFromString:(NSString*)inString withURL:(NSURL*)aURL
{
    NSFont *systemFont = [NSFont systemFontOfSize:14.0f];
    NSDictionary *defaultAttrs = @{NSFontAttributeName :systemFont};

    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString: inString attributes:defaultAttrs];
    NSRange range = NSMakeRange(0, [attrString length]);

    [attrString beginEditing];
    [attrString addAttribute:NSLinkAttributeName value:[aURL absoluteString] range:range];

    // make the text appear in blue
    [attrString addAttribute:NSForegroundColorAttributeName value:[NSColor blueColor] range:range];

    // next make the text appear with an underline
    [attrString addAttribute:
     NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSSingleUnderlineStyle] range:range];

    [attrString endEditing];

    return attrString;
}

@end

@implementation TCSUtility
size_t OPENSSL_strlcpy(char *dst, const char *src, size_t size)    
{
    size_t l = 0;
    for (; size > 1 && *src; size--) {

        *dst++ = *src++;
        l++;
    }

    if (size)

        *dst = '\0';

    return l + strlen(src);

}
size_t OPENSSL_strlcat(char *dst, const char *src, size_t size)

{
    size_t l = 0;
    for (; size > 0 && *dst; size--, dst++)

        l++;

    return l + OPENSSL_strlcpy(dst, src, size);

}

//static char *md5crypt(const char *passwd, const char *magic, const char *salt)
//{
//    /* "$apr1$..salt..$.......md5hash..........\0" */
//    static char out_buf[6 + 9 + 24 + 2];
//    unsigned char buf[MD5_DIGEST_LENGTH];
//    char ascii_magic[5];         /* "apr1" plus '\0' */
//    char ascii_salt[9];          /* Max 8 chars plus '\0' */
//    char *ascii_passwd = NULL;
//    char *salt_out;
//    int n;
//    unsigned int i;
//    EVP_MD_CTX *md = NULL, *md2 = NULL;
//    size_t passwd_len, salt_len, magic_len;
//
//    passwd_len = strlen(passwd);
//
//    out_buf[0] = 0;
//    magic_len = strlen(magic);
//    OPENSSL_strlcpy(ascii_magic, magic, sizeof(ascii_magic));
//#ifdef CHARSET_EBCDIC
//    if ((magic[0] & 0x80) != 0)    /* High bit is 1 in EBCDIC alnums */
//        ebcdic2ascii(ascii_magic, ascii_magic, magic_len);
//#endif
//
//    /* The salt gets truncated to 8 chars */
//    OPENSSL_strlcpy(ascii_salt, salt, sizeof(ascii_salt));
//    salt_len = strlen(ascii_salt);
//#ifdef CHARSET_EBCDIC
//    ebcdic2ascii(ascii_salt, ascii_salt, salt_len);
//#endif
//
//#ifdef CHARSET_EBCDIC
//    ascii_passwd = OPENSSL_strdup(passwd);
//    if (ascii_passwd == NULL)
//        return NULL;
//    ebcdic2ascii(ascii_passwd, ascii_passwd, passwd_len);
//    passwd = ascii_passwd;
//#endif
//
//    if (magic_len > 0) {
//        OPENSSL_strlcat(out_buf, ascii_dollar, sizeof(out_buf));
//
//        if (magic_len > 4)    /* assert it's  "1" or "apr1" */
//            goto err;
//
//        OPENSSL_strlcat(out_buf, ascii_magic, sizeof(out_buf));
//        OPENSSL_strlcat(out_buf, ascii_dollar, sizeof(out_buf));
//    }
//
//    OPENSSL_strlcat(out_buf, ascii_salt, sizeof(out_buf));
//
//    if (strlen(out_buf) > 6 + 8) /* assert "$apr1$..salt.." */
//        goto err;
//
//    salt_out = out_buf;
//    if (magic_len > 0)
//        salt_out += 2 + magic_len;
//
//    if (salt_len > 8)
//        goto err;
//
//    md = EVP_MD_CTX_create();
//    if (md == NULL
//        || !EVP_DigestInit_ex(md, EVP_md5(), NULL)
//        || !EVP_DigestUpdate(md, passwd, passwd_len))
//        goto err;
//
//    if (magic_len > 0)
//        if (!EVP_DigestUpdate(md, ascii_dollar, 1)
//            || !EVP_DigestUpdate(md, ascii_magic, magic_len)
//            || !EVP_DigestUpdate(md, ascii_dollar, 1))
//          goto err;
//
//    if (!EVP_DigestUpdate(md, ascii_salt, salt_len))
//        goto err;
//
//    md2 = EVP_MD_CTX_create();
//    if (md2 == NULL
//        || !EVP_DigestInit_ex(md2, EVP_md5(), NULL)
//        || !EVP_DigestUpdate(md2, passwd, passwd_len)
//        || !EVP_DigestUpdate(md2, ascii_salt, salt_len)
//        || !EVP_DigestUpdate(md2, passwd, passwd_len)
//        || !EVP_DigestFinal_ex(md2, buf, NULL))
//        goto err;
//
//    for (i = (int)passwd_len; i > sizeof(buf); i -= sizeof(buf)) {
//        if (!EVP_DigestUpdate(md, buf, sizeof(buf)))
//            goto err;
//    }
//    if (!EVP_DigestUpdate(md, buf, i))
//        goto err;
//
//    n = (int)passwd_len;
//    while (n) {
//        if (!EVP_DigestUpdate(md, (n & 1) ? "\0" : passwd, 1))
//            goto err;
//        n >>= 1;
//    }
//    if (!EVP_DigestFinal_ex(md, buf, NULL))
//        return NULL;
//
//    for (i = 0; i < 1000; i++) {
//        if (!EVP_DigestInit_ex(md2, EVP_md5(), NULL))
//            goto err;
//        if (!EVP_DigestUpdate(md2,
//                              (i & 1) ? (unsigned const char *)passwd : buf,
//                              (i & 1) ? passwd_len : sizeof(buf)))
//            goto err;
//        if (i % 3) {
//            if (!EVP_DigestUpdate(md2, ascii_salt, salt_len))
//                goto err;
//        }
//        if (i % 7) {
//            if (!EVP_DigestUpdate(md2, passwd, passwd_len))
//                goto err;
//        }
//        if (!EVP_DigestUpdate(md2,
//                              (i & 1) ? buf : (unsigned const char *)passwd,
//                              (i & 1) ? sizeof(buf) : passwd_len))
//                goto err;
//        if (!EVP_DigestFinal_ex(md2, buf, NULL))
//                goto err;
//    }
//    EVP_MD_CTX_destroy(md2);
//    EVP_MD_CTX_destroy(md);
//    md2 = NULL;
//    md = NULL;
//
//    {
//        /* transform buf into output string */
//        unsigned char buf_perm[sizeof(buf)];
//        int dest, source;
//        char *output;
//
//        /* silly output permutation */
//        for (dest = 0, source = 0; dest < 14;
//             dest++, source = (source + 6) % 17)
//            buf_perm[dest] = buf[source];
//        buf_perm[14] = buf[5];
//        buf_perm[15] = buf[11];
//# ifndef PEDANTIC              /* Unfortunately, this generates a "no
//                                 * effect" warning */
//        assert(16 == sizeof(buf_perm));
//# endif
//
//        output = salt_out + salt_len;
//        assert(output == out_buf + strlen(out_buf));
//
//        *output++ = ascii_dollar[0];
//
//        for (i = 0; i < 15; i += 3) {
//            *output++ = cov_2char[buf_perm[i + 2] & 0x3f];
//            *output++ = cov_2char[((buf_perm[i + 1] & 0xf) << 2) |
//                                  (buf_perm[i + 2] >> 6)];
//            *output++ = cov_2char[((buf_perm[i] & 3) << 4) |
//                                  (buf_perm[i + 1] >> 4)];
//            *output++ = cov_2char[buf_perm[i] >> 2];
//        }
//        assert(i == 15);
//        *output++ = cov_2char[buf_perm[i] & 0x3f];
//        *output++ = cov_2char[buf_perm[i] >> 6];
//        *output = 0;
//        assert(strlen(out_buf) < sizeof(out_buf));
//#ifdef CHARSET_EBCDIC
//        ascii2ebcdic(out_buf, out_buf, strlen(out_buf));
//#endif
//    }
//
//    return out_buf;
//
// err:
//    OPENSSL_free(ascii_passwd);
//    EVP_MD_CTX_destroy(md2);
//    EVP_MD_CTX_destroy(md);
//    return NULL;
//}
//
+(BOOL)isARM{
    struct utsname sysinfo;
    int retVal = uname(&sysinfo);
    if (EXIT_SUCCESS != retVal) return NO;
    NSString *processorString=[NSString stringWithUTF8String:sysinfo.machine];
    if ([processorString.lowercaseString isEqualToString:@"arm64"]){
        return YES;
    }
    return NO;

}



+(NSString *)devicePathForFirstVolumeInContainer:(NSString *)containerUUID{
    NSString *plistOutputString=[[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/sbin/diskutil" withOptions:@[@"apfs",@"list",@"-plist",containerUUID]];

    if (!plistOutputString) {
        return nil;
    }

    NSData *plistOutputData=[plistOutputString dataUsingEncoding:NSUTF8StringEncoding];
    if (!plistOutputData) {
        return nil;
    }
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSDictionary * dict = (NSDictionary*)[NSPropertyListSerialization
                                          propertyListFromData:plistOutputData
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];

    //    new_device=plist[u"Containers"][0][u"Volumes"][0][u"DeviceIdentifier"]

    if (!dict) { return nil; }

    NSArray *containers = dict[@"Containers"];

    if (!containers || containers.count==0) { return nil; }

    NSDictionary *currContainer = [containers objectAtIndex:0];
    if (!currContainer) {return nil;}
    NSArray *volumes = currContainer[@"Volumes"];
    if (!volumes || volumes.count==0){ return nil; }

    NSDictionary *volumeInfo=[volumes objectAtIndex:0];
    NSString *deviceIdentifier=[volumeInfo objectForKey:@"DeviceIdentifier"];

    if (!deviceIdentifier) {return nil;}

    return deviceIdentifier;

}

+(NSString *)apfsVolumeUUIDFromDevice:(NSString *)path{

    NSString *plistOutputString=[[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/sbin/diskutil" withOptions:@[@"info",@"-plist",path]];

    if (!plistOutputString) {
        return nil;
    }
    NSData *plistOutputData=[plistOutputString dataUsingEncoding:NSUTF8StringEncoding];
    if (!plistOutputData) {
        return nil;
    }
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSDictionary * dict = (NSDictionary*)[NSPropertyListSerialization
                                          propertyListFromData:plistOutputData
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];

    if (!dict) {
        return nil;
    }
    return dict[@"VolumeUUID"];
}
+(NSDictionary *)containerAndVolumesFromVolumeUUID:(NSString *)volumeUUID{
    __block BOOL foundVolume=NO;
    __block NSString *containerUUID=nil;
    __block NSMutableArray *apfsVolumesInContainer=[NSMutableArray array];
    NSString *plistOutputString=[[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/sbin/diskutil" withOptions:@[@"apfs",@"list",@"-plist"]];

    if (!plistOutputString) {
        return nil;
    }

    NSData *plistOutputData=[plistOutputString dataUsingEncoding:NSUTF8StringEncoding];
    if (!plistOutputData) {
        return nil;
    }
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSDictionary * dict = (NSDictionary*)[NSPropertyListSerialization
                                          propertyListFromData:plistOutputData
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];


    NSArray *containers=[dict objectForKey:@"Containers"];
    if (!containers){
        return nil;
    }
    [containers enumerateObjectsUsingBlock:^(NSDictionary *currentContainer, NSUInteger idx, BOOL * _Nonnull stop) {
        if (foundVolume==YES){
            *stop=YES;
            return;
        }
        containerUUID=[currentContainer objectForKey:@"APFSContainerUUID"];
        [apfsVolumesInContainer removeAllObjects];

        NSArray *volumes = [currentContainer objectForKey:@"Volumes"];
        if (!volumes){
            *stop=YES;
            return;
        }
        [volumes enumerateObjectsUsingBlock:^(NSDictionary *currentVolume, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *currVolumeUUID = [currentVolume objectForKey:@"APFSVolumeUUID"];
            if (!currVolumeUUID)  {
                *stop=YES;
                return;
            }
            [apfsVolumesInContainer addObject:currVolumeUUID];
            if ([currVolumeUUID.lowercaseString isEqualToString:volumeUUID.lowercaseString]){
                foundVolume=YES;
            }

        }];

    }];
    if (foundVolume==YES){
        NSLog(@"found volumes: %@ %@",containerUUID,[apfsVolumesInContainer componentsJoinedByString:@" "]);

        return @{@"container":containerUUID,@"volumes":apfsVolumesInContainer};
    }

    return nil;
}

+(NSString *)mountDMGAtPath:(NSString *)path{

    NSTask *task=[[NSTask alloc] init];
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:path]==NO && ![path.lowercaseString hasPrefix:@"http"] ){

        return nil;
    }
    task.launchPath=@"/usr/bin/hdiutil";

    task.arguments=@[@"mount",path,@"-plist"];

    NSPipe *outPipe=[NSPipe pipe];
    task.standardOutput=outPipe;

    [task launch];

    NSData *outputData=[[outPipe fileHandleForReading] readDataToEndOfFile];
    NSError *err;

    NSPropertyListFormat format;

    NSDictionary *mountedImageDict=[NSPropertyListSerialization propertyListWithData:outputData options:0 format:&format error:&err];


    if (!mountedImageDict) {
        NSLog(@"%@",err.localizedDescription);
        return nil;
    }
    NSArray *identities=[mountedImageDict objectForKey:@"system-entities"];
    if (!identities){
        return nil;
    }
    __block NSString *returnMountPoint=nil;
    [identities enumerateObjectsUsingBlock:^(NSDictionary *currPartInfo, NSUInteger idx, BOOL * _Nonnull stop) {

        NSString *mountPoint;
        if ((mountPoint=[currPartInfo objectForKey:@"mount-point"])){

            *stop=YES;
            returnMountPoint=mountPoint;
        }
    }];
    NSLog(@"%@",returnMountPoint);
    return returnMountPoint;
}

//+(NSString *)encryptPassword:(NSString *)inPass{
//    srandomdev();
//    char s[8];
//    for (int i=0;i<8;i++){
//        int r=random()%54+65;
//        s[i]=r;
//    }
//
//    char *pass=md5crypt(inPass.cString, "1",s );
//    return [NSString stringWithCString:pass encoding:NSUTF8StringEncoding];
//}

+(NSData *)pbkdf2Password:(NSString *)inPassword{
    #define RANDOM_BYTES_LENGTH 16
    #define SALT_LENGTH 4

    int iterations=30202;
    if (inPassword.length>255) return nil;

    const uint8_t salt[32];
    int status = SecRandomCopyBytes(kSecRandomDefault, (sizeof salt)/(sizeof salt[0]), (void *)salt);

    if (status != errSecSuccess) { // Always test the status.

        return nil;
    }
    uint8_t hash[CC_SHA512_BLOCK_BYTES];
    status = CCKeyDerivationPBKDF(kCCPBKDF2, [inPassword UTF8String], [inPassword lengthOfBytesUsingEncoding:NSUTF8StringEncoding], salt, sizeof(salt),
                         kCCPRFHmacAlgSHA512, iterations, hash, CC_SHA512_BLOCK_BYTES);


    if (status != errSecSuccess) { // Always test the status.

        return nil;
    }

    NSDictionary *returnDict=@{@"SALTED-SHA512-PBKDF2":@{@"entropy":[NSData dataWithBytes:hash length:sizeof(hash)],@"salt":[NSData dataWithBytes:salt length:sizeof(salt)],@"iterations":@(iterations)}};

    NSString *err;
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:returnDict
            format:NSPropertyListBinaryFormat_v1_0
            errorDescription:&err];


    if (err!=nil){
        NSLog(@"%@",err);
    }

    return plistData;
}

+(NSInteger)currentOSLongversionNumber{
    int major, minor, bugFix;
    [[NSApplication sharedApplication] getSystemVersionMajor:&major minor:&minor bugFix:&bugFix];

    return major*100+minor;

}

+(void)openMunkiAdmin:(id)sender {
    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:@"/Applications/MunkiAdmin.app"]){

        [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/MunkiAdmin.app"];

    }
    else {

        NSAlert *bigAlert=[[NSAlert alloc] init];
        bigAlert.messageText=@"Install MunkiAdmin";

        bigAlert.informativeText=@"MunkiAdmin was not found in the Applications folder. Do you want to install it now?";



        [bigAlert addButtonWithTitle:@"Install"];
        [bigAlert addButtonWithTitle:@"Cancel"];

        NSInteger res=[bigAlert runModal];

        if (res==NSAlertFirstButtonReturn){
            [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://github.com/hjuutilainen/munkiadmin/releases/latest"]];

        }
        return;


    }

}



+(BOOL)appendString:(NSString *)inString toEndOfFile:(NSString *)filePath{


    NSFileManager *fm=[NSFileManager defaultManager];

    if([fm fileExistsAtPath:filePath]==NO){

        if ([fm createFileAtPath:filePath contents:nil attributes:nil]==NO){
            return NO;
        }
    }

    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:[inString dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandle closeFile];



    return YES;

}

+(NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;

    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Get NSString from C String
                address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
            }
            temp_addr = temp_addr->ifa_next;
        }
    }

    // Free memory
    freeifaddrs(interfaces);
    return address;
}


+ (NSString *)getSerialNumber
{
    NSString *serial = nil;
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,
                                                              IOServiceMatching("IOPlatformExpertDevice"));
    if (platformExpert) {
        CFTypeRef serialNumberAsCFString =
        IORegistryEntryCreateCFProperty(platformExpert,
                                        CFSTR(kIOPlatformSerialNumberKey),
                                        kCFAllocatorDefault, 0);
        if (serialNumberAsCFString) {
            serial = CFBridgingRelease(serialNumberAsCFString);
        }

        IOObjectRelease(platformExpert);
    }
    return serial;
}

+ (NSString *)machineModel
{
    NSData *machineModel = nil;
    NSString *machineModelString= nil;
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,
                                                              IOServiceMatching("IOPlatformExpertDevice"));
    if (platformExpert) {
        CFTypeRef machineModelAsData =
        IORegistryEntryCreateCFProperty(platformExpert,
                                        CFSTR("model"),
                                        kCFAllocatorDefault, 0);
        if (machineModelAsData) {
            machineModel = CFBridgingRelease(machineModelAsData);
        }

        IOObjectRelease(platformExpert);

        const char *modelCString=[machineModel bytes];
        machineModelString=[[NSString alloc] initWithCString:modelCString encoding:NSUTF8StringEncoding];
    }
    return machineModelString;
}

+(NSArray *)listOfMedia{
    NSArray *mediaURLS= [[NSFileManager defaultManager]  mountedVolumeURLsIncludingResourceValuesForKeys:nil options:NSVolumeEnumerationSkipHiddenVolumes];

    NSMutableArray *paths=[NSMutableArray arrayWithCapacity:mediaURLS.count];
    [mediaURLS enumerateObjectsUsingBlock:^(NSURL *currFilesystemPath, NSUInteger idx, BOOL * _Nonnull stop) {
        struct statfs output;
        statfs([[currFilesystemPath path] UTF8String], &output);
        NSString *filesystemType=[NSString stringWithUTF8String:output.f_fstypename];
        if ([[currFilesystemPath path] isEqualToString: @"/"]==NO && [[filesystemType lowercaseString] containsString:@"hfs"]==YES) { //skip root
            [paths addObject:[currFilesystemPath path]];
        }
    }];
    return [NSArray arrayWithArray:paths];
}
+(NSString *)createUserPackageAtPath:(NSString *)inPackagePath withUID:(NSString *)inUID fullname:(NSString *)inFullname shortname:(NSString *)inShortname password:(NSString *)inPassword isAdmin:(BOOL)inIsAdmin isAutologin:(BOOL)inIsAutologin isHidden:(BOOL)inIsHidden passwordHint:(NSString *)inPasswordHint jpegPhoto:(NSString *)inJpegPhoto shell:(NSString *)inShell outputPackageName:(NSString *)outputPackageName{
    NSLog(@"Creating user %@",inShortname);
    if (!inPackagePath || inPackagePath.length==0 || !inShortname || inShortname.length==0 ) return @"No short name specified.";
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *createUserBin=[[NSBundle mainBundle] pathForResource:@"mkuser" ofType:@"sh"];

    if (![fm fileExistsAtPath:createUserBin] || ![fm isExecutableFileAtPath:createUserBin]) {
        NSString *fileError = [NSString stringWithFormat: @"File at \"%@\" does not exist or is not executable.", createUserBin];
        NSLog(@"%@", fileError);
        return fileError;
    }

    NSMutableArray *argArray=[@[@"--short-name",inShortname,
                                @"--uid",inUID,
                                @"--full-name",inFullname,
                                @"--pkg-id",[NSString stringWithFormat:@"com.twocanoes-createuser-%@",inShortname]] mutableCopy];

    if (inPassword && inPassword.length>0) [argArray addObject:@"--stdin-password"];
    if (inIsAdmin) [argArray addObject:@"--administrator"];
    if (inIsAutologin) [argArray addObject:@"--automatic-login"];
    if (inIsHidden) [argArray addObject:@"--hide"];
    if (inPasswordHint) [argArray addObjectsFromArray:@[@"--password-hint",inPasswordHint]];

    if (inShell) [argArray addObjectsFromArray:@[@"--shell",inShell]];
    if (inJpegPhoto && [fm fileExistsAtPath:inJpegPhoto]) [argArray addObjectsFromArray:@[@"--photo",inJpegPhoto]];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    BOOL shouldSign=[ud boolForKey:@"shouldSignPackages"];
    NSString *signingIdentity=[ud objectForKey:@"signingIdentity"];

    if (shouldSign==YES && signingIdentity){
        [argArray addObjectsFromArray:@[@"--pkg-sign",signingIdentity]];
    }

    NSString *destination=[inPackagePath stringByAppendingPathComponent:outputPackageName];
    [argArray addObjectsFromArray:@[@"--pkg-path",destination]];

    NSTask *createUserTask=[[NSTask alloc] init];
    createUserTask.launchPath=createUserBin;
    createUserTask.arguments=[NSArray arrayWithArray:argArray];

    NSLog(@"creating package : %@ %@",createUserTask.launchPath,[argArray componentsJoinedByString:@" "]);

    if ([argArray containsObject: @"--stdin-password"]) {
        createUserTask.standardInput = [NSPipe pipe];

        NSFileHandle *stdInPasswordHandle = [createUserTask.standardInput fileHandleForWriting];
        [stdInPasswordHandle writeData: [inPassword dataUsingEncoding: NSUTF8StringEncoding]];
        [stdInPasswordHandle closeFile];
    }
    
    // About NSTask retrieving stdout and stderr: https://stackoverflow.com/a/55349726 & https://stackoverflow.com/a/16274629

    NSMutableString *createUserTaskStdErr = [[NSMutableString alloc] init];

    createUserTask.standardOutput = [NSPipe pipe];
    [[createUserTask.standardOutput fileHandleForReading] setReadabilityHandler: ^(NSFileHandle *stdOutFileHandle) {
        NSString *thisStdOutString = [[[NSString alloc] initWithData: [stdOutFileHandle availableData] encoding: NSUTF8StringEncoding] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (![thisStdOutString isEqualToString:@""]) {
            NSLog(@"%@", thisStdOutString);
        }
    }];

    createUserTask.standardError = [NSPipe pipe];
    [[createUserTask.standardError fileHandleForReading] setReadabilityHandler: ^(NSFileHandle *stdErrFileHandle) {
        NSString *thisStdErrString = [[[NSString alloc] initWithData: [stdErrFileHandle availableData] encoding: NSUTF8StringEncoding] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (![thisStdErrString isEqualToString: @""] && (![thisStdErrString containsString: @"base64["] || ![thisStdErrString hasSuffix: @"bytes."])) { // Ignore base64 stderr messages which I think happen from passing the password via stdin.
            [createUserTaskStdErr appendFormat: @"\n\n%@", thisStdErrString];
            NSLog(@"%@", thisStdErrString);
        }
    }];

    [createUserTask setTerminationHandler: ^(NSTask *completedTask) {
        [completedTask.standardOutput fileHandleForReading].readabilityHandler = nil;
        [completedTask.standardError fileHandleForReading].readabilityHandler = nil;
    }];
    
    [createUserTask launch];
    [createUserTask waitUntilExit];

    if (createUserTask.terminationStatus!=0) {
        NSLog(@"Could not createUserTask package: %i",createUserTask.terminationStatus);
        return [createUserTaskStdErr stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }

    return @"SUCCESS";
}
+(NSString *)variableStringFromURL:(NSURL *)inURL{
    NSString *urlString=[NSString stringWithFormat:@"file://{{current_volume_path}}%@",inURL.path];
    if ([[urlString substringFromIndex:urlString.length-1] isEqualToString:@"/"]){
        urlString=[urlString substringToIndex:urlString.length-1];
    }

    return urlString;
}
//+(BOOL)convertPackageToMetaDistribution:(NSString *)inPath destination:(NSString *)inDestination error:(NSError **)error{
//    NSTask *productBuildTask=[[NSTask alloc] init];
//    productBuildTask.launchPath=@"/usr/bin/productbuild";
//    productBuildTask.arguments=@[@"--package",inPath,inDestination];
//    [productBuildTask launch];
//    [productBuildTask waitUntilExit];
//
//    if (productBuildTask.terminationStatus!=0){
//        *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not convert package %@",inPath.lastPathComponent]}];
//        return NO;
//    }
//    return YES;
//}
#define PBKDF2_SALT_LEN 32
#define PBKDF2_ENTROPY_LEN 128

+ (NSData *)calculateShadowHashData:(NSString *)pwd
{
    unsigned char salt[PBKDF2_SALT_LEN];
    int status = SecRandomCopyBytes(kSecRandomDefault, PBKDF2_SALT_LEN, salt);
    if (status == 0) {
        unsigned char key[PBKDF2_ENTROPY_LEN];
        // calculate the number of iterations to use
        unsigned int rounds = CCCalibratePBKDF(kCCPBKDF2,
                                               [pwd lengthOfBytesUsingEncoding:NSUTF8StringEncoding],
                                               PBKDF2_SALT_LEN, kCCPRFHmacAlgSHA512, PBKDF2_ENTROPY_LEN, 100);
        // derive our SALTED-SHA512-PBKDF2 key
        CCKeyDerivationPBKDF(kCCPBKDF2,
                             [pwd UTF8String],
                             (CC_LONG)[pwd lengthOfBytesUsingEncoding:NSUTF8StringEncoding],
                             salt, PBKDF2_SALT_LEN,
                             kCCPRFHmacAlgSHA512, rounds, key, PBKDF2_ENTROPY_LEN);
        // Make a dictionary containing the needed fields
        NSDictionary *dictionary = @{
                                     @"SALTED-SHA512-PBKDF2" : @{
                                             @"entropy" : [NSData dataWithBytes: key length: PBKDF2_ENTROPY_LEN],
                                             @"iterations" : [NSNumber numberWithUnsignedInt: rounds],
                                             @"salt" : [NSData dataWithBytes: salt length: PBKDF2_SALT_LEN]
                                             }
                                     };
        // convert to binary plist data
        NSError *error = NULL;
        NSData *plistData = [NSPropertyListSerialization dataWithPropertyList: dictionary
                                                                       format: NSPropertyListBinaryFormat_v1_0
                                                                      options: 0
                                                                        error: &error];
        return plistData;
    }
    return nil;
}
+(NSArray *)md5HashArrayFromFile:(NSString *)path{


    if ([[NSFileManager defaultManager] fileExistsAtPath:path]==NO){
        return nil;
    }

    NSFileHandle *fh=[NSFileHandle fileHandleForReadingAtPath:path];

    if (!fh){
        return nil;
    }
    NSData *data=[fh readDataOfLength:10485760];

    NSMutableArray *hashArray=[NSMutableArray array];
    while (data.length>0){
        unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
        CC_MD5(( const void*)([data bytes]), (CC_LONG)data.length, md5Buffer);
        NSMutableString* output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
        for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
            [output appendFormat:@"%02x", md5Buffer[i]];
        }

        [hashArray addObject:output];
        data=[fh readDataOfLength:10485760];
    }
    return [NSArray arrayWithArray:hashArray];
}
+(NSDictionary *)manifestForURL:(NSString *)inURL fileHashArray:(NSArray *)hashArray{

    NSMutableDictionary *details=[NSMutableDictionary dictionary];
    [details setObject:@"software-package" forKey:@"kind"];
    [details setObject:@(10485760) forKey:@"md5-size"];
    [details setObject:hashArray forKey:@"md5s"];
    [details setObject:inURL forKey:@"url"];


    NSMutableDictionary *assets=[NSMutableDictionary dictionary];
    [assets setObject:@[details] forKey:@"assets"];

    NSMutableDictionary *items=[NSMutableDictionary dictionary];
    [items setObject:@[assets] forKey:@"items"];

    return [NSDictionary dictionaryWithDictionary:items];
}
+(BOOL)isProcessRunning:(NSString *)processName{
    kinfo_proc *list = {0};
    size_t count;
    BOOL processFound = NO;

    GetBSDProcessList(&list, &count);

    for (unsigned long i = 0; i < count; i++) {
        kinfo_proc proc = list[i];

        if (strcmp(processName.UTF8String, proc.kp_proc.p_comm) == 0) {
            processFound = YES;
            break;
        }
    }

    if (count > 0) {
        free(list);
    }
    return processFound;
}
+(void)updateTLSCertIfNeed:(id)sender{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *tlsCertFolder=[ud objectForKey:TLSCERTIFICATEFOLDER];

    NSString *certPathInUserDefaults=[ud objectForKey:CERTIFICATEPATH] ;
    NSString *keyPathInUserDefaults=[ud objectForKey:INDENTITYKEYPATH] ;

    NSString *certPath=[tlsCertFolder stringByAppendingPathComponent:@"mds_identity.cer"];
    NSString *keyPath=[tlsCertFolder stringByAppendingPathComponent:@"mds_identity.key"];

    if (certPathInUserDefaults && [fm fileExistsAtPath:certPathInUserDefaults]==NO){

        [ud removeObjectForKey:CERTIFICATEPATH];

    }
    if (keyPathInUserDefaults && [fm fileExistsAtPath:keyPathInUserDefaults]==NO){

          [ud removeObjectForKey:INDENTITYKEYPATH];

    }
    if (certPath && keyPath && ([fm fileExistsAtPath:certPath]==YES && [fm fileExistsAtPath:keyPath]==YES) &&
        (!certPathInUserDefaults || !keyPathInUserDefaults)){

        [ud setObject:certPath forKey:CERTIFICATEPATH];
        [ud setObject:keyPath forKey:INDENTITYKEYPATH];
    }

}


@end
