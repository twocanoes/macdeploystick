#include "EEPROMAnything.h"
#include <avr/wdt.h>
#include <avr/wdt.h>
//#include <Mouse.h>
//#include <Keyboard.h>
#include "HID-Project.h"
#include <string.h>
#define LAST_INSTALLED_RECOVERY 0
#define LATEST_RECOVERY 1
#define EARLIEST_RECOVERY 2

#define PRESSDELAY 50
//#define SHOWFREEMEM
#define CURRENTVERSION 54
#define BLOCK_SIZE 256 
#define DEFAULTCOMMAND "/Volumes/mds/run"
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__er

#ifdef SHOWFREEMEM
int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}
#endif
enum mode_types {RECOVERY, DEP, CLI, UAMDM} current_mode;


struct settings_t
{
  char command[512];
  char firmware_password[32];
  long startup_delay;
  long pre_command_delay;
  bool autorun;
  bool erase_volume;
  int version;
  int recovery_mode;
  bool language_english;
  int startup_mode;
  bool use_wifi;
  char wifi_ssid[32];
  char wifi_password[32];
  long admin_startup_delay;

  
} __attribute__((packed)) settings;

struct runtimevalues_t
{
  int activate_on_startup;
} __attribute__((packed)) runtimevalues;

int settings_pos=0;
int runtime_pos=sizeof(settings);



void reboot() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

void setup() {
  Mouse.begin();
  pinMode(13, OUTPUT);
   pinMode(button1Pin, INPUT_PULLUP);
  pinMode(button2Pin, INPUT_PULLUP);
  pinMode(button3Pin, INPUT_PULLUP);
  BootKeyboard.begin();
  delay(1000);
}


void flash_led(int count) {
  
  for (int i = 0; i < count; i++) {
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(100);
  }
}
void presskey(int key, int times, int delayms) {
  presskey_internal(key,times,delayms, false);
}
void presskey_voice_over(int key, int times, int delayms) {
  presskey_internal(key,times,delayms, true);
}
void presskey_internal(int key, int times, int delayms, bool voiceover) {
  
  for (int i = 0; i < times; i++) {
    flash_led(1);

    if (voiceover) {
      BootKeyboard.press(KEY_LEFT_CTRL);  
      BootKeyboard.press(KEY_LEFT_ALT);

    }
    switch (key) {
        case KEY_F1:
        BootKeyboard.press(KEY_F1);
        break;
      case KEY_TAB:
        BootKeyboard.press(KEY_TAB);
        break;
      case KEY_DOWN_ARROW:
        BootKeyboard.press(KEY_DOWN_ARROW);
        break;
      case KEY_UP_ARROW:
        BootKeyboard.press(KEY_UP_ARROW);
        break;
      case KEY_RIGHT_ARROW:
        BootKeyboard.press(KEY_RIGHT_ARROW);
        break;
      case KEY_LEFT_ARROW:
        BootKeyboard.press(KEY_LEFT_ARROW);
        break;
      case KEY_RETURN:
        BootKeyboard.press(KEY_RETURN);
        break;
      case KEY_ESC:
        BootKeyboard.press(KEY_ESC);
        break;
      default:
        BootKeyboard.press(key);
        break;
    }
    delay(delayms);
    BootKeyboard.releaseAll();
    delay(100);
  
  }
}

void showusage() {
  
  Serial.println(F("\nCopyright 2019 Twocanoes Software, Inc."));
  Serial.println(F("help: this message"));
  Serial.println(F("show: show current settings"));
  Serial.println(F("reset: reset settings to defaults"));
  Serial.println(F("reboot: reboot the device"));
  Serial.println(F("erasem1: m1 erase"));
  Serial.println(F("installerm1: walk through the m1 installer"));
  Serial.println(F("set_command <command>: set command to run in recovery."));
  Serial.println(F("set_firmware_password <password>: set firmware password to enter prior to booting to recovery."));
  Serial.println(F("set_startup_delay <seconds>: how many seconds to wait from booting in recovery to launching terminal."));
  Serial.println(F("set_pre_command_delay <seconds>: how many seconds to wait after launching terminal until typing command."));
  Serial.println(F("set_settings <binary>: send packed structure"));
  Serial.println(F("get_settings: Get all settings in binary hex format."));
  Serial.println(F("dep: Automatically configure DEP in setup assistant"));
  Serial.println(F("uamdm: Open System Preferences and click Approve for UAMDM in Profiles pane. Must be logged in."));
  Serial.println(F("profile <path_to_profile> <admin name> <admin password>: install profile."));
  Serial.println(F("set_wifi <SSID> <Password>: wifi ssid and password used when activating M1 Mac. Both must be less than 32 characters."));
  Serial.println(F("recovery: provide keyboard commands to boot into recovery and run resources from external volume or remote disk image."));
  Serial.println(F("set_autorun <on|off>: automatically enter recovery mode after admin time."));
  Serial.println(F("set_erase_volume <on|off>: open up disk utility and erase first volume."));
  Serial.println(F("set_recovery_mode: 0|1|2: recovery mode - command-r (0), option-command-r (1), shift-option-command-r (2)"));
  Serial.println(F("set_language_english <on|off>: turn on or off setting the language to English before running workflow."));
  Serial.println(F("set_startup_mode 0|1: startup in recovery (0) or Apple Silicon (1)"));
  Serial.println(F("set_activate_startup: continue from erase"));
  Serial.println(F("set_admin_startup_delay <seconds>: how many seconds to wait for <return> to go into CLI, or until recovery keystroke is pressed."));


  
}

void writeSettings() {
  

}
int setdefaults() {
  strcpy(settings.command, DEFAULTCOMMAND);
  strcpy(settings.firmware_password, "");
  settings.version = CURRENTVERSION;
  settings.startup_delay = 180;
  settings.autorun = true;
  settings.erase_volume = false;
  settings.pre_command_delay = 6;
  settings.recovery_mode=0;
  settings.startup_mode=0;
  settings.language_english=false;
  settings.use_wifi=false;
  settings.admin_startup_delay=10;
  if (EEPROM_writeAnything(settings_pos, settings)<0) {
    if (EEPROM_writeAnything(settings_pos, settings)<0) {
      return -1;
    }
  }
  runtimevalues.activate_on_startup=0;
  
  if (EEPROM_writeAnything(runtime_pos, runtimevalues)<0) {
    if (EEPROM_writeAnything(runtime_pos, runtimevalues)<0) {
      return -1;
    }
  }



  EEPROM_readAnything(settings_pos, settings);
  return 0;
  
}

void enter_cli() {
  
  Serial.begin(9600);
  
  Serial.println(F("Configuration Mode. Enter help for assistance. Copyright 2018-2019 Twocanoes Software, Inc."));
  while (current_mode == CLI) {
    Serial.setTimeout(1000000);
    flash_led(1);
    Serial.print(">");
    String s = Serial.readStringUntil('\n');
    s.trim();
    Serial.println(s);
#ifdef SHOWFREEMEM
    Serial.print("Free memory: ");
    Serial.println(freeMemory());
#endif
    
    
    if (s.startsWith("show") == true) {
      Serial.print("Version: ");
      Serial.println(settings.version, DEC);
      Serial.print(F("Command:"));
      Serial.println(settings.command);
      Serial.print(F("Startup Delay:"));
      Serial.println(settings.startup_delay, DEC);
       Serial.print(F("Admin Startup Delay:"));
      Serial.println(settings.admin_startup_delay, DEC);
      Serial.print(F("Pre Commmand Delay:"));
      Serial.println(settings.pre_command_delay, DEC);
      Serial.print(F("Firmware is Set:"));
      if (strlen(settings.firmware_password) > 0) Serial.println("YES");
      else Serial.println(F("NO"));
      Serial.print(F("Autorun:"));
      if (settings.autorun == 1) {
        Serial.println("on");
        Serial.print(F("Recovery mode: "));
        Serial.println(settings.recovery_mode, DEC);
        Serial.print(F("Startup mode: "));
        Serial.println(settings.startup_mode, DEC);

      }
      else Serial.println("off");
      Serial.print(F("Erase Volume:"));
      if (settings.erase_volume == 1) Serial.println("on");
      else Serial.println("off");
      
      Serial.print(F("Set Language to English:"));
      if (settings.language_english == 1) Serial.println("on");
      else Serial.println("off");
    }
    else if (s.startsWith("set_command") == true) {
      
      String new_command = s.substring(12);
      strncpy(settings.command, new_command.c_str(), 255);
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith("set_firmware_password") == true) {
      
      String new_firmware_password = s.substring(22);
      strncpy(settings.firmware_password, new_firmware_password.c_str(), 32);
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith(F("help")) == true) {
      showusage();
      
    }
    else if (s.startsWith(F("set_startup_delay")) == true) {
      String new_command = s.substring(18);
      settings.startup_delay = new_command.toInt();
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith(F("set_admin_startup_delay")) == true) {
      String new_command = s.substring(24);
      settings.admin_startup_delay = new_command.toInt();
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith(F("set_pre_command_delay")) == true) {
      String new_command = s.substring(22);
      settings.pre_command_delay = new_command.toInt();
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith(F("set_recovery_mode")) == true) {
      String new_recovery_mode = s.substring(18);
      settings.recovery_mode = new_recovery_mode.toInt();
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith(F("set_startup_mode")) == true) {
      String new_startup_mode = s.substring(18);
      settings.startup_mode = new_startup_mode.toInt();
      EEPROM_writeAnything(settings_pos, settings);
      
    }
    else if (s.startsWith(F("set_activate_startup")) == true) {
      activate_on_startup();
      
    }
    

    
    else if (s.startsWith(F("reset")) == true) {
      reset();
    }
    else if (s.startsWith(F("erasem1")) == true) {
      Serial.println(F("m1_erase..."));
      Serial.end();
      BootKeyboard.begin();
      m1_erase();
    }
    else if (s.startsWith(F("installerm1")) == true) {
      m1_installer();
    }
    else if (s.startsWith(F("recovery")) == true) {
      current_mode = RECOVERY;
    }
    else if (s.startsWith(F("bootloader")) == true) {
      enter_bootloader();
    }
    else if (s.startsWith(F("dep")) == true) {
      current_mode = DEP;
    }
    else if (s.startsWith(F("uamdm")) == true) {
      current_mode = UAMDM;
    }
    else if (s.startsWith(F("set_autorun")) == true) {
      char *line = (char *)s.c_str();
      char *ptr = NULL;
      byte index = 0;
      char *tokens[2];
      ptr = strtok(line, " ");  // takes a list of delimiters
      while (ptr != NULL && index < 2) {
        tokens[index] = ptr;
        index++;
        ptr = strtok(NULL, " ");  // takes a list of delimiters
      }
      if (index != 2) {
        
        Serial.println(F("Invalid commmand. Please provide on or off for the set_autorun command"));
      }
      else {
        if (strncmp(tokens[1], "on", 3) == 0) {
          
          settings.autorun = true;
          Serial.println(F("Autorun turned on"));
          EEPROM_writeAnything(settings_pos, settings);
        }
        else if (strncmp(tokens[1], "off", 3) == 0) {
          settings.autorun = false;
          Serial.println(F("Autorun turned off"));
          EEPROM_writeAnything(settings_pos, settings);
        }
        else {
          Serial.println(F("Invalid value. Please specify on or off"));
        }
      }
    }
    else if (s.startsWith(F("profile")) == true) {
      char *line = (char *)s.c_str();
      char *ptr = NULL;
      byte index = 0;
      char *tokens[3];
      ptr = strtok(line, " ");  // takes a list of delimiters
      while (ptr != NULL && index < 3) {
        tokens[index] = ptr;
        index++;
        ptr = strtok(NULL, " ");  // takes a list of delimiters
      }
      if (index != 3) {
        
        Serial.println(F("Invalid commmand. Provide path to profile and a admin password separated by a space. Path and password cannot contain the space character"));
        
      }
      else {
        
        
        enter_install_profile(tokens[1], tokens[2]);
      }
    }

    else if (s.startsWith(F("set_erase_volume")) == true) {
      char *line = (char *)s.c_str();
      char *ptr = NULL;
      byte index = 0;
      char *tokens[2];
      ptr = strtok(line, " ");  // takes a list of delimiters
      while (ptr != NULL && index < 2) {
        tokens[index] = ptr;
        index++;
        ptr = strtok(NULL, " ");  // takes a list of delimiters
      }
      if (index != 2) {
        
        Serial.println(F("Invalid commmand. Please provide on or off for the set_erase_volume command"));
      }
      else {
        if (strncmp(tokens[1], "on", 3) == 0) {
          
          settings.erase_volume = true;
          Serial.println(F("Erase Volume turned on"));
          EEPROM_writeAnything(settings_pos, settings);
        }
        else if (strncmp(tokens[1], "off", 3) == 0) {
          settings.erase_volume = false;
          Serial.println(F("Erase Volume turned off"));
          EEPROM_writeAnything(settings_pos, settings);
        }
        else {
          Serial.println(F("Invalid value. Please specify on or off"));
        }
      }
    }
    else if (s.startsWith(F("set_language_english")) == true) {
      char *line = (char *)s.c_str();
      char *ptr = NULL;
      byte index = 0;
      char *tokens[2];
      ptr = strtok(line, " ");  // takes a list of delimiters
      while (ptr != NULL && index < 2) {
        tokens[index] = ptr;
        index++;
        ptr = strtok(NULL, " ");  // takes a list of delimiters
      }
      if (index != 2) {
        
        Serial.println(F("Invalid commmand. Please provide on or off for the set_erase_volume command"));
      }
      else {
        if (strncmp(tokens[1], "on", 3) == 0) {
          
          settings.language_english = true;
          Serial.println(F("Set English Language turned on"));
          EEPROM_writeAnything(settings_pos, settings);
        }
        else if (strncmp(tokens[1], "off", 3) == 0) {
          settings.language_english = false;
          Serial.println(F("Set English Language turned off"));
          EEPROM_writeAnything(settings_pos, settings);
        }
        else {
          Serial.println(F("Invalid value. Please specify on or off"));
        }
      }
      
    }
    else if (s.startsWith(F("reboot")) == true) {
      
      reboot();
    }
    else if (s.length() == 0) {
      
      
    }
    else if (s.startsWith(F("set_settings")) == true){
      static unsigned int block_number;
      
      //save old password in case we need to use it later
      char temp_wifi_password[sizeof(settings.wifi_password)];
      strncpy(temp_wifi_password,settings.wifi_password,sizeof(settings.wifi_password));

      
      int string_length=s.length()-16;
      s.c_str();  //convert to c string
      
      char *pos=&s[13]; //jump past header
      block_number=3;
      sscanf(pos,"%d",&block_number);
      pos=&s[16];
      
      byte b;
      int start_byte=block_number*BLOCK_SIZE/2;
      unsigned int end_byte=start_byte+string_length/2-1;
      if ((end_byte>sizeof(settings)-1)|| (string_length>BLOCK_SIZE) ){
        Serial.println(F("Invalid block or length."));
        
        continue;
        
      }
      
      
      for(long i = 0; i<string_length/2; i++){
        byte *ptr=(byte *)(void *)&settings;
        sscanf(&pos[i*2],"%02x",&b);
        
        memcpy(&ptr[start_byte+i],&b,1); 
      }
      
      
      if (block_number>=sizeof(settings)/(BLOCK_SIZE/2)){
        //67BB5934E2ED4DBFA33C is just a random value
        if (strncmp(settings.wifi_password, "67BB5934E2ED4DBFA33C", 32)==0){
          //marker found so copy old password to this new one.
          strncpy(settings.wifi_password, temp_wifi_password, 32);
        
        }
        EEPROM_writeAnything(settings_pos, settings);
      }
    }
    
    else if (s.startsWith("get_settings") == true) {
      
      char temp_firmware_password[sizeof(settings.firmware_password)];
      strncpy(temp_firmware_password,settings.firmware_password,sizeof(settings.firmware_password));
      
      memset(settings.firmware_password,0,sizeof(settings.firmware_password));
      int firmware_pw_len = strlen(temp_firmware_password);
      
      if (firmware_pw_len>0) {
        
        settings.firmware_password[0]=0x01;
        
      }
      
      char temp_wifi_password[sizeof(settings.wifi_password)];
      strncpy(temp_wifi_password,settings.wifi_password,sizeof(settings.wifi_password));
      
      memset(settings.wifi_password,0,sizeof(settings.wifi_password));
      int wifi_pw_len = strlen(temp_wifi_password);
      
      if (wifi_pw_len>0) {
        
        settings.wifi_password[0]=0x01;
        
      }


      const byte* p = (const byte*)(const void*)&settings;
      unsigned int i;
      char curr_bytes[3];
      Serial.print(F("DATA:"));
      for (i = 0; i < sizeof(settings); i++){
        sprintf(curr_bytes,"%02x",p[i]);
        Serial.print(curr_bytes);
      }
      Serial.println();  
      
      //restore passwords
      strncpy(settings.firmware_password,temp_firmware_password,sizeof(settings.firmware_password));
      strncpy(settings.wifi_password,temp_wifi_password,sizeof(settings.wifi_password));

    }
    else {
      Serial.println(F("Invalid command. Enter help for usage."));
      Serial.println(s);
      
    }
  }
  Serial.end();
}
void hold_recovery_key(int seconds) {
  int j;
  int led_flash_delay = 200; // ms
  int key_press_interval = 500; // ms
  BootKeyboard.begin();
  for (j = 0; j < (seconds * 1000) / key_press_interval; j++) {
    
    switch (settings.recovery_mode) {
      case LAST_INSTALLED_RECOVERY: 
        BootKeyboard.press(KEY_LEFT_GUI);
        BootKeyboard.press('r');
        break;
      case LATEST_RECOVERY:
        BootKeyboard.press(KEY_LEFT_ALT);
        BootKeyboard.press(KEY_LEFT_GUI);
        BootKeyboard.press('r');
        break;
      case EARLIEST_RECOVERY:
        BootKeyboard.press(KEY_LEFT_SHIFT);
        BootKeyboard.press(KEY_LEFT_ALT);
        BootKeyboard.press(KEY_LEFT_GUI);
        BootKeyboard.press('r');
        break;
      default:
        break;
    }
    
    delay(key_press_interval - led_flash_delay);
    flash_led(1);
    BootKeyboard.releaseAll();
  }
}
void enter_dep() {
  // Enter setup assistant. Assumes all screens that can be are skipped. 
  flash_led(3);
  BootKeyboard.begin();
  // Country picker - tab once, Continue
  presskey(KEY_TAB, 1, 40);
  presskey(' ', 1, 40);
  // Keyboard layout picker - tab x3, Continue
  presskey(KEY_TAB, 3, 40);
  presskey(' ', 1, 40);
  // Remote Management screen - tab x3, Continue
  presskey(KEY_TAB, 3, 40);
  presskey(' ', 1, 40);
  
  BootKeyboard.end();
  
  current_mode = CLI;
  
}
void enter_install_profile(char *profile_path, char*password){
  Serial.println(F("installing profile..."));
  // Enter setup assistant. Assumes all screens that can be are skipped. 
  flash_led(4);
  BootKeyboard.begin();
  
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press(' ');
  delay(PRESSDELAY);
  BootKeyboard.releaseAll();
  BootKeyboard.print(F("Finder"));
  delay(500);
  BootKeyboard.println();
  delay(500);
  
  //press command-shift-g to go to folder
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press(KEY_LEFT_SHIFT);
  BootKeyboard.press('g');
  delay(50);
  BootKeyboard.releaseAll();
  delay(1000);
  BootKeyboard.println(profile_path);
  delay(1000);
  
  
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press('o');
  delay(50);
  BootKeyboard.releaseAll();
  delay(3000);
  
  
  
  presskey(KEY_TAB, 2, 50);
  
  BootKeyboard.print(' ');
  
  delay(2000);
  
  BootKeyboard.print(password);
  delay(1000);
  BootKeyboard.println("");
  BootKeyboard.releaseAll();
  BootKeyboard.end();
  
  current_mode = CLI;
  
}
void enter_uamdm() {
  Serial.println(F("Running UAMDM..."));
  // Enter setup assistant. Assumes all screens that can be are skipped. 
  flash_led(3);
  //delay for a bit since it is freaky to start so fast
  delay(2000);
  
  BootKeyboard.begin();
  
  //Apple menu
  //turn on menu navigation
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  BootKeyboard.releaseAll();
  
  
  //move to System Preferences
  presskey(KEY_DOWN_ARROW, 3, 50);
  BootKeyboard.println("");
  
  //Go to Profiles Pane
  delay(5000);
  BootKeyboard.print("Profiles");
  delay(1000);
  BootKeyboard.println("");
  delay(5000);
  
  //Press control F7 to turn on keboard nav
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F7);
  BootKeyboard.releaseAll();
  
  //tab to button
  presskey(KEY_TAB, 2, 40);
  
  //press button
  presskey(' ', 1, 40);
  
  //tab to Approve
  presskey(KEY_TAB, 1, 40);
  
  //Approve it!
   BootKeyboard.releaseAll();
  
  //exit
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press('q');
  delay(50);
  BootKeyboard.releaseAll();
  
  
  BootKeyboard.end();
  
  current_mode = CLI;
  
}
void erase_volume(){
  
  // select disk utility
  presskey(KEY_DOWN_ARROW, 4, 50);
  
  //select continue
  presskey(KEY_TAB, 1, 50); 
  // press continue
  presskey(' ', 1, 50);  
  //wait 20 seconds to open Disk Utility. was 5, now it is 20 from a request.
  delay(20000);
  //hold up arrow for 3 seconds
  presskey(KEY_UP_ARROW,1,3000);
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press(KEY_LEFT_SHIFT);
  BootKeyboard.press('e');
  delay(50);
  BootKeyboard.releaseAll();
  
  // move to erase button
  presskey(KEY_TAB,3,50);
  // select erase button
  presskey(' ',1,50);
  // wait 30 seconds to erase the disk. was at 15 seconds, then a request for 30.
  delay(30000);
  // Press return to complete
  presskey(KEY_RETURN,1,50);
  
  // quit with command q and wait a bit
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press('q');
  delay(50);
  BootKeyboard.releaseAll();
  delay(3000);
  
}
void enter_bootloader(){
  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = 0x0800;
  
  // Stash the magic key
  *bootKeyPtr = bootKey;
  
  // Set a watchdog timer
  wdt_enable(WDTO_120MS);
  
  while(1) {} // This infinite loop ensures nothing else
  // happens before the watchdog reboots us
  
}
void m1_setup(){
  Serial.println(F("m1_recovery..."));
  Serial.end();
  BootKeyboard.begin();
  // turning on mouse to dismiss flashing trackpad / mouse screen
  Mouse.begin();
  //wait 30 seconds to make Options selector settle down
  delay(30000);
  //initial screen show drives and options at far right. press right arrow a bunch of times and then return to select options
  
  presskey(KEY_RIGHT_ARROW, 10, 50);
  delay(500);
  BootKeyboard.println("");

  //wait for 1TR to boot
  delay(15000);
  
  //dismiss language if shown. if not, it is ignored
  BootKeyboard.println("");
  delay(10000);
  
}
void m1_recovery(){
  m1_setup();
  
}
void open_terminal_and_run(){
  
  //go to utilities menu
  presskey('u', 1, 500);

  delay(50);
  presskey(KEY_DOWN_ARROW, 1, 50);

  //go to terminal menu
  presskey('t', 1, 500);
  //go go activate!
  BootKeyboard.println("");
  
  //run command
  delay(settings.pre_command_delay * 1000);
  BootKeyboard.println(settings.command);
  BootKeyboard.end();
  
  
  
}
void turn_on_menu_nav(){
  
  //turn on menu navigation
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  BootKeyboard.releaseAll();
  
  
}
void enter_recovery() {
  Serial.println(F("Running..."));
  Serial.end();
  BootKeyboard.begin();
  delay(500);
  
  
  int firmware_pw_len = strlen(settings.firmware_password);
  if (firmware_pw_len > 0) {
    int i;
    delay(500);
    for (i = 0; i < firmware_pw_len; i++) {
      
      BootKeyboard.press(settings.firmware_password[i]);
      delay(20);
      BootKeyboard.releaseAll();
      delay(500);
    }
    BootKeyboard.press(KEY_RETURN);
    delay(500);
    BootKeyboard.releaseAll();
  }

  delay(500);
  
  hold_recovery_key(30);
  long delay_completed=0;
  if (settings.startup_delay<1){
    settings.startup_delay=1;
  }
  long total_delay = settings.startup_delay * 1000;
  int led_flash_delay = 200; // ms
  int led_flash_interval = 1000; // ms
  
  while(delay_completed<total_delay) {
    flash_led(2);
    delay(led_flash_interval - led_flash_delay * 2);
    delay_completed += led_flash_interval;
  }
  
  //not sure why theses escapes are here
  presskey(KEY_ESC, 1, 400);
  presskey(KEY_ESC, 1, 400);
  delay(500);

  // Thanks for Tim Sutton for this fix:
  // turning on mouse to dismiss flashing trackpad / mouse screen
  // (this is on Intel as of Big Sur)
  BootMouse.begin();
  // ..and add an additional 10 seconds delay
  delay(10000);

  //skip language
  BootKeyboard.println("");
  delay(30000);
  
  if (settings.erase_volume==true){
    erase_volume();
  }
  if (settings.language_english==true) {
    //turn on menu navigation
    BootKeyboard.press(KEY_LEFT_CTRL);
    BootKeyboard.press(KEY_F2);
    BootKeyboard.releaseAll();
    
    //move to language menu
    presskey(KEY_RIGHT_ARROW, 2, 50);
    //move to terminal menu
    presskey(KEY_DOWN_ARROW, 2, 400);
    BootKeyboard.println("");
    
    presskey(KEY_UP_ARROW, 2, 5000);
    BootKeyboard.println("");
    delay(10000);
  }
  //turn on menu navigation
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  delay(50);
  BootKeyboard.releaseAll();
  
  //move to utilities menu
  presskey(KEY_RIGHT_ARROW, 4, 50);
  //move to terminal menu
  presskey(KEY_DOWN_ARROW, 4, 400);
  //go go activate!
  BootKeyboard.println("");
  
  //run command
  delay(settings.pre_command_delay * 1000);
  BootKeyboard.println(settings.command);
  BootKeyboard.end();

  //done, so go back to CLI
  Serial.begin(9600);
  delay(2000);
  current_mode = CLI;
}
void enter_m1_recovery() {
  
  m1_setup();
  
  
  
  turn_on_menu_nav();
  open_terminal_and_run();
  //done, so go back to CLI
  BootKeyboard.end();
  Serial.begin(9600);
  delay(2000);
  current_mode = CLI;
}


void reset(){
  if(setdefaults()!=0){
    Serial.println("error writing defaults!");
    while(1) {
      digitalWrite(13, HIGH);
      delay(50);
      digitalWrite(13, LOW);
      delay(50);
    }
  }
}
void join_wifi(char *ssid, char *password){
 
//
//  toggle_voice_over();
//  presskey_voice_over('m', 2, 50);
//  presskey_voice_over(' ', 1,1000);
//  delay(250);
    for (int i=0;i<1000;i++) {
      Mouse.move(-1000,1000, 0);
      
    }
    Mouse.click();
  delay(500);
  BootKeyboard.println("join");
  
  delay(500);
  BootKeyboard.println(ssid);
    presskey(KEY_TAB,2,50);

  delay(500);
  
  BootKeyboard.println(password);
  delay(500);
 
  delay(30000);

}
 
void m1_erase(){
  
  delay(5000);
  
  Serial.println("m1_erase running");

  //select erase Mac
  turn_on_menu_nav();
  presskey('r', 1, 50);
  BootKeyboard.println("");
  presskey('e', 1, 50);
  BootKeyboard.println("");
  
  delay(2000);
  
  //nav to erase mac in window  
  toggle_voice_over();
  //there is an extra field depending how it is run. to accommidate this, go all the way to the
  //bottom and then go up one.
  presskey_voice_over(KEY_DOWN_ARROW, 5, 50);
  presskey_voice_over(KEY_UP_ARROW, 1, 50);
  toggle_voice_over();
  delay(500);
  presskey(' ', 1, 50);
  
  //confirm
  presskey(KEY_TAB, 1, 50);
  delay(500);
  presskey(' ', 1, 50);

  Serial.begin(9600);
  delay(2000);
  Serial.println("done. marking for activation on next boot and dropping to cli after a minute");

  activate_on_startup();
  
  current_mode = CLI;
}
void activate_on_startup(){
  runtimevalues.activate_on_startup=1;
  EEPROM_writeAnything(runtime_pos,runtimevalues);
}
void toggle_voice_over(){
  delay(1000);
  BootKeyboard.press(KEY_LEFT_GUI);  
  BootKeyboard.press(KEY_F5);
  delay(50);
  BootKeyboard.releaseAll();
  //sometimes the next keystroke gets ignored, so we pause for a bit
  delay(1000);
}
void m1_installer(){

  //dismiss continue button
  BootKeyboard.println();
  //continue button sometimes freezes up UI for 5 to 7 seconds. Using 15 seconds to be safe.
  delay(15000);
  presskey(KEY_TAB, 1, 50);
  presskey(' ', 1, 50);
  presskey(KEY_TAB, 1, 50);
  presskey(' ', 1, 50);
  
  toggle_voice_over();
  presskey_voice_over(KEY_RIGHT_ARROW, 3, 50);

  
  BootKeyboard.press(KEY_LEFT_CTRL);  
  delay(100);
  BootKeyboard.press(KEY_LEFT_GUI);
  delay(100);
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(50);
  BootKeyboard.releaseAll();
  delay(100);
  
  presskey_voice_over(KEY_DOWN_ARROW, 1, 50);
  presskey_voice_over(KEY_RIGHT_ARROW, 1, 50);
  presskey(' ', 1, 50);
  //This is only needed if there is no power adapter inserted. 
  presskey(KEY_TAB, 1, 50);
  presskey(' '  , 1, 50);
  toggle_voice_over();
  current_mode = CLI;

}
void loop() {

  digitalWrite(13, HIGH);
  Serial.begin(9600);
  delay(2000);
  digitalWrite(13, LOW);
  digitalWrite(13, HIGH);
  EEPROM_readAnything(settings_pos, settings);
  if (settings.version != CURRENTVERSION) {
    
    reset();
    
  }
  EEPROM_readAnything(runtime_pos, runtimevalues);

  if (runtimevalues.activate_on_startup==1){
    Serial.println(F("continuing from mac erased"));
    
    //since it gets rebooted multiple times, we wait five seconds to clear
    //the activate_on_startup setting. otherwise it gets after a reboot.
    delay(5000);
    runtimevalues.activate_on_startup=0;
    EEPROM_writeAnything(runtime_pos,runtimevalues);
    delay(20000);

    //wait for activation screen
  
    //seems that 11.2.x may have a language selection screen that pops
    //up before activation. Need to zero get pass that.
    
    presskey(KEY_TAB, 1, 50);
    delay(500);
    presskey(KEY_RETURN,1,50);
    delay(10000);

    if (settings.use_wifi){
      join_wifi(settings.wifi_ssid,settings.wifi_password);
      //joining wifi makes the main activation window lose focus. This brings it back.
      //if we are not joining wifi, we don't need to do it.
      toggle_voice_over();
      delay(1000);
      presskey_voice_over(KEY_F1, 2, 50);
       BootKeyboard.println("recovery asssistant");
       delay(500);
        BootKeyboard.println("");
        delay(500);
        BootKeyboard.println("");
      BootKeyboard.releaseAll();
//      presskey(KEY_DOWN_ARROW, 3, 50);
//      presskey_voice_over(' ', 2, 50);
      toggle_voice_over();
    }
    //wait for activation. this is a long time but sometimes wifi takes a while
    //and who knows how long activation can take.
    delay(60000);

    //hopefully activation is complete. contnuing onwards.
    toggle_voice_over();
   
    presskey_voice_over(KEY_RIGHT_ARROW, 4, 50);
    presskey_voice_over(' ', 1,1000);
    delay(5000);
    toggle_voice_over();

    turn_on_menu_nav();
    open_terminal_and_run();

    current_mode = CLI;

  }
  else {
    Serial.println(F("Copyright 2018-2021 Twocanoes Software, Inc."));
    Serial.println(F("Press <return> to enter configuration mode..."));
    int i;
    //we used to start up in RECOVERY by default. Now it is defined by startup_mode
    //so we can go into erase filevault mode
    if (settings.autorun == true) {
      for (i = 0; i < settings.admin_startup_delay*2; i++) {
        if (Serial.available()) {
          while (Serial.available()) {
            Serial.read();
          }
          current_mode = CLI;
          break;
        }
        else {
          delay(500);
        }
      }
      
    }
    else {
      current_mode = CLI;
    }
    digitalWrite(13, LOW);
    if (current_mode != CLI) {  
      Serial.end();
    }
  }
  while (1) {
    switch (current_mode) {
      
      case CLI:
        enter_cli();
        break;
      case DEP:
        enter_dep();
        break;
      case UAMDM:
        enter_uamdm();
        break;
      case RECOVERY:
        if (settings.startup_mode==1 && !settings.erase_volume){
          enter_m1_recovery();

        }
        else if (settings.startup_mode==1 && settings.erase_volume){
            Serial.println(F("m1_erase..."));
            Serial.end();
            BootKeyboard.begin();
            m1_setup();
            m1_erase();
        }
        else {
          enter_recovery();
        }
        break;

    }
  }
}
