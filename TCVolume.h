//  Created by Timothy Perfitt on 1/26/12.
//  Copyright (c) 2015 Twocanoes Software Inc. All rights reserved.

@import Foundation;

@interface TCVolume : NSObject <NSCopying, NSSecureCoding>

@property (readwrite, strong) NSMutableData *mbr;
@property (readwrite, strong) NSMutableString *partitionID;
@property (assign) NSInteger filesystemSize;
@property (assign) BOOL isBootable;
@property (assign) BOOL isRemovable;
@property (readwrite, copy) NSString *filePath;
@property (readwrite, copy) NSString *drivePath;
@property (readwrite, copy) NSString *device;
@property (readwrite, strong) NSMutableString *info;
@property (readwrite, copy) NSString *diskName;
@property (readwrite, copy) NSAttributedString *usageDescription;
@property (assign) BOOL isVM;

@property (readwrite, copy) NSString *vmUsername;

@property (readwrite, copy) NSString *vmPassword;

@property (readwrite, copy) NSString *humanSize;
@property (readwrite, copy) NSString *fileSystemType;
@property (readwrite, copy) NSString *freeSpace;
@property (readwrite, copy) NSString *totalSize;
@property (readwrite, copy) NSString *volumeType;
@property (assign) BOOL showLine;

@property (readwrite, copy) NSString *mediaSize;
@property (readwrite, copy) NSNumber *foundWindowsVersion;
@property (readwrite, copy) NSNumber *mediaSizeRaw;
@property (readwrite, copy) NSNumber *mediaBlockSize;

@property (readwrite, assign) BOOL hasWindows;
@property (readwrite, assign) BOOL hasWindows8orGreater;
@property (readwrite, assign) BOOL isWritable;
@property (readwrite, copy) NSUUID *volumeUUID;
@property (readwrite, copy) NSUUID *mediaUUID;
@property (readwrite, copy) NSUUID *mediaContentUUID;

@property (readwrite, copy) NSString *installedOS;
@property (readwrite, copy) NSString *networkShare;
@property (readwrite, copy) NSString *bootResourcePath;
@property (readwrite, copy) NSString *bootMgrPath;
@property (strong,nonatomic) NSMutableArray *wrapperArray;

-(void)appendMBRData:(NSData *)inData;
-(void)appendpartitionIDData:(NSString *)inString;
-(void)appendInfo:(NSString *)inString;

- (void)updateSize;

@end


