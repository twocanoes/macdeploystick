//
//  CloneFeedbackProtocol.h
//  winclone_helper_tool
//
//  Created by Tim Perfitt on 4/29/17.
//  Copyright © 2017 Twocanoes Software, Inc. All rights reserved.
//
#import <Foundation/Foundation.h>


@protocol TCSMDSHelperReplyProtocol <NSObject>

@required

- (void)helperToolDidUpdatePercentComplete:(float)percentComplete statusMessage:(NSString *)statusMessage;
- (void)helperToolDidFailWithError:(NSError *)error;
- (void)helperToolDidComplete;

@optional

-(void)helperToolDidReportStandardError:(NSString *)msg;
-(void)helperToolDidReportStandardOut:(NSString *)msg;


@end
