//
//  mdshelperProtocol.h
//  mdshelper
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

// The protocol that this service will vend as its API. This header file will also need to be visible to the process hosting the service.
@protocol mdshelperProtocol
NS_ASSUME_NONNULL_BEGIN
- (void)setFeedbackListenerEndpoint:(NSXPCListenerEndpoint * )endpoint;

- (void)getVersionWithCallingAppPath:(NSString *)path callback:(void (^)(NSInteger))callback;


- (void)installInstallMacOSWithProduct:(NSDictionary *)product catalog:(NSString *)catalog addToDiskImage:(BOOL)shouldAddToDiskImage workingPath:(NSString *)workingPath auth:(NSData *)token withCallback:(void (^)(BOOL success))callback;
-(void)stopRunningProcessesWithAuth:(NSData *)token callback:(void (^)(BOOL success))callback;
-(void)updateWebserverWithAuth:(NSData *)token configurations:(NSDictionary *)configurations withCallback:(void (^)(BOOL success))callback;
-(void)stopWebserverWithAuth:(NSData *)token callback:(void (^)(BOOL success))callback;
-(void)startWebserverWithAuth:(NSData *)token callback:(void (^)(BOOL success))callback;
-(void)restartWebserverWithAuth:(NSData *)token callback:(void (^)(BOOL success))callback;
-(void)setupMunkiReportWithAuth:(NSData *)token source:(NSString *)source destination:(NSString *)destination withCallback:(void (^)(NSError *err))callback;
-(void)addMunkiReportWithAuth:(NSData *)token userFile:(NSString *)path contents:(NSString *)contents withCallback:(void (^)(NSError *err))callback;
-(void)migrateFiles:(NSArray *)files withAuth:(NSData *)token withCallback:(void (^)(NSError *err))callback;

-(void)removeUsers:(NSArray *)users fromFolder:(NSString *)path withAuth:(NSData *)token  withCallback:(void (^)(NSError *err))callback;
-(void)startMicroMDMWithAuth:(NSData *)token settings:(NSString *)settings callback:(void (^)(NSError * _Nullable err))callback;
-(void)stopMicroMDMWithAuth:(NSData *)token callback:(void (^)(NSError * _Nullable err))callback;
-(void)restartMicroMDMWithAuth:(NSData *)token callback:(void (^)(NSError * _Nullable err))callback;
-(void)createMunkiRepoWithAuth:(NSData *)token path:(NSString *)munkiRepoPath callback:(void (^)(NSError * _Nullable))callback;
-(void)createMacOSInstallWithAuth:(NSData *)token volume:(NSString *)volumePath withInstaller:(NSString *)installerPath callback:(void (^)(BOOL isDone, NSString *statusMsg,NSError * _Nullable))callback;
//-(void)updatePermissions:(NSString *)workingPath withCallback:(void (^)(BOOL success))callback;
-(void)addCertificateAtPath:(NSString *)certPath withCallback:(void (^)(NSError *err))callback;
-(void)enterDFUMode:(void (^)(NSInteger))callback;
-(void)reboot:(void (^)(NSInteger))callback;

NS_ASSUME_NONNULL_END
@end

