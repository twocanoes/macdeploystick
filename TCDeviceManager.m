//
//  TCDeviceManager.m
//  Winclone 3
//
//  Created by Timothy Perfitt on 1/27/12.
//  Copyright (c) 2015 Twocanoes Software Inc. All rights reserved.
//

@import IOKit;
@import DiskArbitration;

#import "TCDeviceManager.h"

#import <sys/mount.h>
#import <paths.h>
#import <IOKit/storage/IOMedia.h>

#import "TCVolume.h"
#import "NSNumber+HumanReadable.h"
#import "TCTaskHelper.h"


static TCDeviceManager *myObj;

kern_return_t MyFindEjectableMedia( io_iterator_t * );

kern_return_t MyFindEjectableMedia( io_iterator_t *mediaIterator )
{
    mach_port_t         masterPort;
    kern_return_t       kernResult;
    CFMutableDictionaryRef   classesToMatch;

    kernResult = IOMasterPort( MACH_PORT_NULL, &masterPort );
    if ( kernResult != KERN_SUCCESS )
    {
        printf( "IOMasterPort returned %d\n", kernResult );
        return kernResult;
    }
    // CD media are instances of class kIOCDMediaClass.
    classesToMatch = IOServiceMatching(kIOUserClientClassKey );
    kernResult = IOServiceGetMatchingServices( masterPort,
                                              classesToMatch, mediaIterator );
    if ( (kernResult != KERN_SUCCESS) || (mediaIterator == NULL) )
        printf( "No ejectable CD media found.\n kernResult = %d\n",
               kernResult );

    return kernResult;
}

kern_return_t MyGetDeviceFilePath( io_object_t,
                                  char *, CFIndex );

kern_return_t MyGetDeviceFilePath( io_object_t nextMedia,
                                  char *deviceFilePath, CFIndex maxPathSize )
{
    //    io_object_t nextMedia;
    kern_return_t kernResult = KERN_FAILURE;

    *deviceFilePath = '\0';
    //    nextMedia = IOIteratorNext( mediaIterator );
    if ( nextMedia )
    {
        CFTypeRef   deviceFilePathAsCFString;
        deviceFilePathAsCFString = IORegistryEntryCreateCFProperty(
                                                                   nextMedia, CFSTR( kIOBSDNameKey ),
                                                                   kCFAllocatorDefault, 0 );
        *deviceFilePath = '\0';
        if ( deviceFilePathAsCFString )
        {
            size_t devPathLength;
            strcpy( deviceFilePath, _PATH_DEV );
            // Add "r" before the BSD node name from the I/O Registry
            // to specify the raw disk node. The raw disk node receives
            // I/O requests directly and does not go through the
            // buffer cache.
            // strcat( deviceFilePath, "r");
            devPathLength = strlen( deviceFilePath );
            if ( CFStringGetCString( deviceFilePathAsCFString,
                                    deviceFilePath + devPathLength,
                                    maxPathSize - devPathLength,
                                    kCFStringEncodingASCII ) )
            {
                kernResult = KERN_SUCCESS;
            }
            CFRelease( deviceFilePathAsCFString );
        }
    }
    IOObjectRelease( nextMedia );

    return kernResult;
}




@interface TCDeviceManager()

@property (readwrite, assign) int mountStatus;
@property (readwrite, assign) int unmountStatus;
@property (readwrite, assign) BOOL didUnmount;
@property (readwrite, assign) BOOL didMount;

-(NSDictionary *)diskInfoFromMountedPath:(NSString *)inPath;

@end

#if TEST
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"
#endif
@implementation TCDeviceManager


+ (instancetype)sharedDeviceManager {
    static id sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        TCDeviceManager *instance = [[[self class] alloc] init];
        instance->_fileManager = [NSFileManager defaultManager];
        sharedManager = instance;
        myObj = sharedManager;
    });
    return sharedManager;
}
-(void)diskMountStatus:(BOOL)mountStat{

    if (mountStat==YES) self.didMount=YES;
    if (mountStat==YES) self.mountStatus=0;
    else self.mountStatus=1;
}

-(void)logString:(NSString *)logString{

    if (self.delegate && [self.delegate respondsToSelector:@selector(log:)]){


        [self.delegate log:logString];
    }


}
-(void)diskUnmountStatus:(BOOL)stat{

    _didUnmount=stat;
    if (stat==YES) _unmountStatus=0;
    else _unmountStatus=1;
}
-(BOOL)selectStartupDisk:(TCVolume *)disk withType:(int)inType nextOnly:(BOOL)nextOnly{
    
    
    TCSLogWithFormat(@"Setting startup disk to device %@", disk.device);
//    char *args[6];
    
    int type=inType;
    
    NSString *wholeDisk=[self diskFromSlice:disk.device];
    
    NSString *efiPartition=[NSString stringWithFormat:@"%@s1",wholeDisk];
    
    NSString *command=@"/usr/sbin/bless";
    NSString *systemCommand;
    if (type==DISKTYPEUNKNOWN) {
        TCSLog(@"Disk type unknown so selecting EFI style booting.");
        type=DISKTYPEEFI;
        
    }
    switch (type) {
            case DISKTYPECURRENTDISK:
            return NO;
            break;
            
            case DISKTYPEMAC:
                TCSLog(@"Setting boot disk to Mac volume.");
                systemCommand=[NSString stringWithFormat:@"%@ -device %@ -setBoot",command,disk.device];
                break;
            case DISKTYPEEFI:
                TCSLog(@"Setting boot disk to EFI volume.");
                systemCommand=[NSString stringWithFormat:@"%@ -device %@ -setBoot",command,efiPartition];
                break;
            
            case DISKTYPELEGACY:
                TCSLog(@"Setting boot disk to legacy volume.");

            systemCommand=[NSString stringWithFormat:@"%@ -device %@ -setBoot",command,disk.device];

            break;
        case DISKTYPEVOLUMEEFI:
            systemCommand=[NSString stringWithFormat:@"%@ -device %@ -setBoot -nextonly",command,disk.device];

            break;

        default:
            TCSLog(@"Unknown disktype. Aborting set boot disk.");
            return NO;
            break;
    }
    int currUid=getuid();
    
   if (nextOnly==YES && disk.isRemovable==NO) systemCommand=[systemCommand stringByAppendingString:@" -nextonly"];

    if (currUid!=0){
        TCSLog(@"Process not running as root so not attempting to set startup disk!");
    }
    else {
        TCSLogWithFormat(@"Running command \"%@\"", systemCommand);
        
        system([systemCommand UTF8String]);
    }
    
    
    return YES;

}


-(NSDictionary *)attachedDisksFromIORegistry{
    kern_return_t       kernResult;
    CFMutableDictionaryRef matchingDict=IOServiceMatching(kIOMediaClass);

    CFMutableDictionaryRef propertyMatchDict = CFDictionaryCreateMutable(kCFAllocatorDefault, 0,
                                                                         &kCFTypeDictionaryKeyCallBacks,
                                                                         &kCFTypeDictionaryValueCallBacks);

    if (NULL == propertyMatchDict) {
        printf("CFDictionaryCreateMutable returned a NULL dictionary.\n");
    }
    else {
        // Set the value in the dictionary of the property with the given key, or add the key
        // to the dictionary if it doesn't exist. This call retains the value object passed in.
        CFDictionarySetValue(propertyMatchDict, CFSTR(kIOMediaContentHintKey), CFSTR("C12A7328-F81F-11D2-BA4B-00A0C93EC93B"));

        // Now add the dictionary containing the matching value for kIOPrimaryInterface to our main
        // matching dictionary. This call will retain propertyMatchDict, so we can release our reference
        // on propertyMatchDict after adding it to matchingDict.
        CFDictionarySetValue(matchingDict, CFSTR(kIOPropertyMatchKey), propertyMatchDict);
        CFRelease(propertyMatchDict);
    }


    io_iterator_t iter;
    kernResult = IOServiceGetMatchingServices(kIOMasterPortDefault, matchingDict,&iter);
    if (KERN_SUCCESS != kernResult) {
        printf("IOServiceGetMatchingServices returned 0x%08x\n", kernResult);
    }
    io_service_t                service;
    kern_return_t               kr;
    CFMutableDictionaryRef      properties;
    CFStringRef                 cfStr;

    for( ; (service = IOIteratorNext(iter)); IOObjectRelease(service)) {

        io_string_t path;
        kr = IORegistryEntryGetPath(service, kIOServicePlane, path);
        assert( KERN_SUCCESS == kr );
        TCSLogWithFormat(@"Found a device of class kAppleSamplePCIClassName: %s", path);

        // print the value of kIONameMatchedKey property, as an example of
        // getting properties from the registry. Property based access
        // doesn't require a user client connection.

        // grab a copy of the properties
        kr = IORegistryEntryCreateCFProperties( service, &properties,
                                               kCFAllocatorDefault, kNilOptions );
        assert( KERN_SUCCESS == kr );

        cfStr = CFDictionaryGetValue( properties, CFSTR(kIONameMatchedKey) );
        if( cfStr) {
            const char * c = NULL;
            char * buffer = NULL;
            c = CFStringGetCStringPtr(cfStr, kCFStringEncodingMacRoman);
            if(!c) {
                CFIndex bufferSize = CFStringGetLength(cfStr) + 1;
                buffer = malloc(bufferSize);
                if(buffer) {
                    if(CFStringGetCString(cfStr, buffer, bufferSize, kCFStringEncodingMacRoman))
                        c = buffer;
                }
            }
            if(c)
                TCSLogWithFormat(@"Device matched on name: %s", c);

            if(buffer)
                free(buffer);
        }
        CFRelease( properties );

        // test out the user client
        //Test( masterPort, service );
    }
    IOObjectRelease(iter);
    
    return nil;
}
-(NSArray <NSDictionary *> *)attachedAPFSVolumesFromIORegistry{
    kern_return_t       kernResult;
    CFMutableDictionaryRef matchingDict=IOServiceMatching("AppleAPFSVolume");

    CFMutableDictionaryRef propertyMatchDict = CFDictionaryCreateMutable(kCFAllocatorDefault, 0,
                                                                         &kCFTypeDictionaryKeyCallBacks,
                                                                         &kCFTypeDictionaryValueCallBacks);

    NSMutableArray *returnArray=[NSMutableArray array];
    if (NULL == propertyMatchDict) {
        printf("CFDictionaryCreateMutable returned a NULL dictionary.\n");
    }
    else {
        // Set the value in the dictionary of the property with the given key, or add the key
        // to the dictionary if it doesn't exist. This call retains the value object passed in.
//        CFDictionarySetValue(propertyMatchDict, CFSTR(kIOMediaContentHintKey), CFSTR("C12A7328-F81F-11D2-BA4B-00A0C93EC93B"));

        // Now add the dictionary containing the matching value for kIOPrimaryInterface to our main
        // matching dictionary. This call will retain propertyMatchDict, so we can release our reference
        // on propertyMatchDict after adding it to matchingDict.
//        CFDictionarySetValue(matchingDict, CFSTR(kIOPropertyMatchKey), propertyMatchDict);
//        CFRelease(propertyMatchDict);
    }


    io_iterator_t iter;
    kernResult = IOServiceGetMatchingServices(kIOMasterPortDefault, matchingDict,&iter);
    if (KERN_SUCCESS != kernResult) {
        printf("IOServiceGetMatchingServices returned 0x%08x\n", kernResult);
    }
    io_service_t                service;
    kern_return_t               kr;
    CFMutableDictionaryRef      properties;
    CFStringRef                 cfStr;

    for( ; (service = IOIteratorNext(iter)); IOObjectRelease(service)) {

        io_string_t path;
        kr = IORegistryEntryGetPath(service, kIOServicePlane, path);
        assert( KERN_SUCCESS == kr );
        TCSLogWithFormat(@"Found a device of class kAppleSamplePCIClassName: %s", path);

        // print the value of kIONameMatchedKey property, as an example of
        // getting properties from the registry. Property based access
        // doesn't require a user client connection.

        // grab a copy of the properties
        kr = IORegistryEntryCreateCFProperties( service, &properties,
                                               kCFAllocatorDefault, kNilOptions );
        assert( KERN_SUCCESS == kr );

        [returnArray addObject:(NSDictionary *)CFBridgingRelease(properties)];
//        cfStr = CFDictionaryGetValue( properties, CFSTR(kIONameMatchedKey) );
//        if( cfStr) {
//            const char * c = NULL;
//            char * buffer = NULL;
//            c = CFStringGetCStringPtr(cfStr, kCFStringEncodingMacRoman);
//            if(!c) {
//                CFIndex bufferSize = CFStringGetLength(cfStr) + 1;
//                buffer = malloc(bufferSize);
//                if(buffer) {
//                    if(CFStringGetCString(cfStr, buffer, bufferSize, kCFStringEncodingMacRoman))
//                        c = buffer;
//                }
//            }
//            if(c)
//                TCSLogWithFormat(@"Device matched on name: %s", c);
//
//            if(buffer)
//                free(buffer);
//        }
//        CFRelease( properties );

        // test out the user client
        //Test( masterPort, service );
    }
    IOObjectRelease(iter);

    return returnArray;
}




-(NSString *)diskFromSlice:(NSString *)slice{
    
    DADiskRef		disk		= NULL;
	DASessionRef	session		= NULL;
    session = DASessionCreate ( kCFAllocatorDefault );
    if(session == NULL)
    {
        printf("session NULL\n");
        return 0;
    }
    disk = DADiskCreateFromBSDName ( kCFAllocatorDefault, session, [slice UTF8String]);
    
    if(disk == NULL)
    {
        CFRelease ( session );
        printf("disk NULL\n");
        return 0;
    }
    
    DADiskRef whole;
    char *diskNumber;
	whole = DADiskCopyWholeDisk( disk );

	if(whole)
	{
        diskNumber=(char *)DADiskGetBSDName( whole );
	}
	else
	{
        CFRelease(disk);
        CFRelease ( session );
        return nil;
	}

    NSString *ret=[NSString stringWithFormat:@"/dev/%@",[NSString stringWithUTF8String:diskNumber]];
    CFRelease(disk);
    CFRelease(whole);
    CFRelease ( session );
    return ret;
    
    
    
}






-(NSArray *)attachedDisks{
    return [self attachedDisksShowingWindowsOnly:NO showAllDisks:YES];

}

-(NSArray *)attachedDisksShowingWindowsOnly:(BOOL)onlyWindows{

    return [self attachedDisksShowingWindowsOnly:onlyWindows showAllDisks:NO];
}
-(NSArray *)attachedDisksShowingWindowsOnly:(BOOL)onlyWindows showAllDisks:(BOOL)shouldShowAll{
    NSMutableArray * returnArray=[NSMutableArray arrayWithCapacity:0];
    NSMutableArray * fullList=[NSMutableArray arrayWithCapacity:0];
    
    NSFileManager *fm=[NSFileManager defaultManager];
    NSDictionary *diskAttributes;
    
    HFSUniStr255 t_volumeName;
    FSRef volumeFSRef;
    unsigned int volumeIndex = 1;
    FSVolumeInfo info;
    FSVolumeRefNum actualVolume;
    NSString *fileSystemType;
    
    while (FSGetVolumeInfo(kFSInvalidVolumeRefNum, volumeIndex++, &actualVolume, kFSVolInfoGettableInfo, &info, &t_volumeName, &volumeFSRef) == noErr) {
        
        NSString *volumeName = ( NSString *)CFBridgingRelease(CFStringCreateWithCharacters(kCFAllocatorDefault, t_volumeName.unicode, t_volumeName.length));
        NSURL *url = ( NSURL *)CFBridgingRelease(CFURLCreateFromFSRef(NULL, &volumeFSRef));
        if (!url) {
            continue;
        }
        fileSystemType=[self fileSystemTypeAtPath:[url path]];
        diskAttributes=[fm attributesOfFileSystemForPath:[url path] error:nil];
        struct statfs fs;
        
        statfs([[url path] fileSystemRepresentation], &fs);
        NSString *deviceName = [fm stringWithFileSystemRepresentation:fs.f_mntfromname length:strlen(fs.f_mntfromname)];
        if ([deviceName hasPrefix:@"ntfs:/"]){
            
            NSArray *components=[deviceName componentsSeparatedByString:@"/"];
            if (components.count>2){
        
                deviceName=[NSString stringWithFormat:@"/dev/%@",[components objectAtIndex:2]];
                
                fileSystemType=@"ntfs";
            }
            
        }
        NSString *wholeDisk=[self diskFromSlice:deviceName];
        
        if (wholeDisk==nil) {
            continue;
        }
        
        NSString *humanSize=[[diskAttributes objectForKey:@"NSFileSystemSize"] stringWithHumanReadableInt];
        
        NSDictionary *daInfo=[self diskInfoForDeviceNodeAtURL:[NSURL URLWithString:deviceName]];
        NSString *descr = [daInfo objectForKey:(NSString *)kDADiskDescriptionVolumeKindKey];

        NSString *mediaContentUUID = (NSString *)([daInfo objectForKey:(NSString *)kDADiskDescriptionMediaContentKey]);
        CFUUIDRef mediaUUID = (__bridge CFUUIDRef)([daInfo objectForKey:(NSString *)kDADiskDescriptionMediaUUIDKey]);
        CFUUIDRef volumeUUID = (__bridge CFUUIDRef)([daInfo objectForKey:(NSString *)kDADiskDescriptionVolumeUUIDKey]);

        if (shouldShowAll==NO) {
           __block NSRange textRange;

            [@[@"ntfs",@"msdos",@"exfat",@"MS-DOS"] enumerateObjectsUsingBlock:^(NSString *currType, NSUInteger idx, BOOL * _Nonnull stop) {
                textRange =[[descr lowercaseString] rangeOfString:currType];
                if (textRange.location != NSNotFound) {
                    *stop=YES;
                }
            }];
            if (textRange.location == NSNotFound) continue;

        }

        
        TCVolume *volume=[[TCVolume alloc] init];
        
        volume.filePath=[url path];
        volume.diskName=volumeName;
        volume.humanSize=humanSize;
        volume.device=deviceName;
        volume.drivePath=wholeDisk;
        volume.fileSystemType=fileSystemType;
        if (mediaUUID) {
            volume.mediaUUID = [[NSUUID alloc] initWithUUIDString:CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, mediaUUID))];
        }
        if (volumeUUID) {
            volume.volumeUUID = [[NSUUID alloc] initWithUUIDString:CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, volumeUUID))];
        }
        if (mediaContentUUID) {
            volume.mediaContentUUID = [[NSUUID alloc] initWithUUIDString:mediaContentUUID];
        }
        volume.freeSpace=[NSString stringWithFormat:@"%lli",[[diskAttributes objectForKey:NSFileSystemFreeSize] longLongValue]];
        volume.totalSize=[NSString stringWithFormat:@"%lli",[[diskAttributes objectForKey:NSFileSystemSize] longLongValue]];
        
        
        
        BOOL isDir;
        
        NSString *windowsFolder=[TCDeviceManager windowsFolderFromVolume:volume.filePath];
        NSString *system32Folder=[TCDeviceManager system32FolderFromVolume:volume.filePath];
        
        NSString *bootWimFilePath=[TCDeviceManager bootWIMFileFromVolume:volume.filePath];
        
        if (([fm fileExistsAtPath:windowsFolder isDirectory:&isDir] && isDir==YES) &&
            (([fm fileExistsAtPath:system32Folder isDirectory:&isDir] && isDir==YES) ) ){
            volume.hasWindows=YES;
        }
        else if ([fm fileExistsAtPath:bootWimFilePath isDirectory:&isDir] && isDir==NO){
            volume.hasWindows=YES;
        }
        
        
        NSNumber *mediaSizeNumber,*mediaBlockSizeNumber;
        
        if ((mediaSizeNumber=[daInfo objectForKey:@"DAMediaSize"])) {
            volume.mediaSize=[mediaSizeNumber stringWithHumanReadableInt];
            volume.mediaSizeRaw=mediaSizeNumber;
            
        }
        
        if ((mediaBlockSizeNumber=[daInfo objectForKey:@"DAMediaBlockSize"])) {
            volume.mediaBlockSize=mediaBlockSizeNumber;
            
        }
        if ([daInfo objectForKey:@"DAVolumeUUID"]){
            CFUUIDRef uuidRef= (__bridge CFUUIDRef)[daInfo objectForKey:@"DAVolumeUUID"];
            NSString *uuidString=(NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, uuidRef));

            volume.volumeUUID=[[NSUUID alloc] initWithUUIDString:uuidString];
            
            
        }
        else {
            volume.volumeUUID=[NSUUID UUID];
            
        }
        
        volume.volumeType=@"disk";
        
        //#ifdef PI_CLONE
        
        //        [returnArray addObject:volume];
        //#else
        if ( (onlyWindows==NO) || (onlyWindows==YES&& volume.hasWindows==YES)) [returnArray addObject:volume];
        //#endif
        [fullList addObject:volume];
    }
    
    
    for (TCVolume *currVol in returnArray) {
        if (currVol.hasWindows==NO) continue;
        
        
        NSFileManager *fm=[NSFileManager defaultManager];
        if (![fm fileExistsAtPath:[currVol.filePath stringByAppendingPathComponent:@"BOOT"]]) {
            //    return @{@"bootFolderPath":bootFolderPath,@"bootmgrPath":bootmgrPath};
            NSDictionary *infoDict=[self splitBootInfo:currVol.device fullList:fullList];
            currVol.bootResourcePath=[infoDict objectForKey:@"bootFolderPath"];
            currVol.bootMgrPath=[infoDict objectForKey:@"bootmgrPath"];
        }
        
    }
    
    return returnArray;
}

- (TCVolume *)volumeWithMediaUUID:(NSUUID *)UUID deviceNode:(NSURL *)deviceNode {
    NSArray<TCVolume *> *volumes = [self attachedDisksShowingWindowsOnly:NO];
    TCVolume *foundVolume;
    for (TCVolume *volume in volumes) {
        if ([volume.mediaUUID isEqualTo:UUID] && [volume.device isEqualToString:deviceNode.path]) {
            foundVolume = volume;
            break;
        }
    }
    return foundVolume;
}

- (TCVolume *)volumeWithMediaUUID:(NSUUID *)UUID {
    NSArray<TCVolume *> *volumes = [self attachedDisksShowingWindowsOnly:NO];
    TCVolume *foundVolume;
    for (TCVolume *volume in volumes) {
        if ([volume.mediaUUID isEqualTo:UUID]) {
            foundVolume = volume;
            break;
        }
    }
    return foundVolume;
}
- (NSString *)fileSystemTypeAtPath:(NSString *)inPath {
    NSURL *volumePath = [NSURL fileURLWithPath:inPath];
    NSDictionary *info = [self diskInfoForVolumeMountedAtURL:volumePath];
    return info[(NSString *)kDADiskDescriptionVolumeTypeKey];
}

+ (NSString *)windowsFolderFromVolume:(NSString *)inVolumePath{
    __block NSString *winPath=nil;
    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    NSArray *contentsOfWindowsVolume=[fm contentsOfDirectoryAtPath:inVolumePath error:&err];

    if (contentsOfWindowsVolume){
        [contentsOfWindowsVolume enumerateObjectsUsingBlock:^(NSString *currFile, __unused NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[currFile lowercaseString] isEqualToString:@"windows"]) {
                winPath=[inVolumePath stringByAppendingPathComponent:currFile];
                *stop=YES;


            }
        }];


    }
    return winPath;

}

+ (NSString *)system32FolderFromVolume:(NSString *)inVolumePath{

    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *winPath=[TCDeviceManager windowsFolderFromVolume:inVolumePath];

    NSError *err;
    __block NSString *system32Path=nil;

    if (winPath) {
        NSArray *contentsOfWindowsFolder=[fm contentsOfDirectoryAtPath:winPath error:&err];

        [contentsOfWindowsFolder enumerateObjectsUsingBlock:^(NSString *currFile, __unused NSUInteger idx, BOOL * _Nonnull stop) {

            if ([[currFile lowercaseString] isEqualToString:@"system32"]) {
                system32Path=[winPath stringByAppendingPathComponent:currFile];
                *stop=YES;

            }

        }];
    }

    return system32Path;

}
+ (NSString *)windowsSourcesFolderFromVolume:(NSString *)inVolumePath{
    __block NSString *sourcesPath=nil;
    NSFileManager *fm=[NSFileManager defaultManager];
    
    NSError *err;
    NSArray *contentsOfWindowsSourcesVolume=[fm contentsOfDirectoryAtPath:inVolumePath error:&err];
    
    if (contentsOfWindowsSourcesVolume){
        [contentsOfWindowsSourcesVolume enumerateObjectsUsingBlock:^(NSString *currFile, __unused NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[currFile lowercaseString] isEqualToString:@"sources"]) {
                sourcesPath=[inVolumePath stringByAppendingPathComponent:currFile];
                *stop=YES;
                
                
            }
        }];
        
        
    }
    return sourcesPath;
    
}

+ (NSString *)bootWIMFileFromVolume:(NSString *)inVolumePath{
    
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *windowsSourcesFolder=[TCDeviceManager windowsSourcesFolderFromVolume:inVolumePath];
    
    NSError *err;
    __block NSString *bootWimFilePath=nil;
    
    if (windowsSourcesFolder) {
        NSArray *contentsOfWindowsSourcesFolder=[fm contentsOfDirectoryAtPath:windowsSourcesFolder error:&err];
        
        [contentsOfWindowsSourcesFolder enumerateObjectsUsingBlock:^(NSString *currFile, __unused NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([[currFile lowercaseString] isEqualToString:@"boot.wim"]) {
                bootWimFilePath=[windowsSourcesFolder stringByAppendingPathComponent:currFile];
                *stop=YES;
                
            }
            
        }];
    }
    
    return bootWimFilePath;
    
}
- (NSDictionary *)splitBootInfo:(NSString *)inDevice fullList:(NSArray *)attachedDisks{

    BOOL isDir;
    NSString *bootFolderPath=@"";
    NSString *bootmgrPath=@"";
    NSFileManager *fm=self.fileManager;
    //now we search for the boot volume since we don't have the boot folder.
    for (TCVolume *vol in attachedDisks) {
        bootFolderPath=[vol.filePath stringByAppendingPathComponent:@"BOOT"];

        NSString *currDevice=[[TCDeviceManager sharedDeviceManager] diskFromSlice:vol.device];
        NSString *currSelectedDevice=[[TCDeviceManager sharedDeviceManager] diskFromSlice:inDevice];


        bootmgrPath=[vol.filePath stringByAppendingPathComponent:@"bootmgr"];
        if (!bootmgrPath) bootmgrPath=@"";
        if (([vol.diskName isEqualToString:@"System Reserved"])&&
            ([fm fileExistsAtPath:bootFolderPath isDirectory:&isDir] && isDir==YES) &&
            ([currDevice isEqualToString:currSelectedDevice]) &&
            (vol.hasWindows==NO)
            )
        {

            break;
        }
    }
    if (!bootmgrPath) bootmgrPath=@"";
    if (!bootFolderPath) bootFolderPath=@"";
    return @{@"bootFolderPath":bootFolderPath,@"bootmgrPath":bootmgrPath};

}


// The methods diskInfoFromDeviceAtPath: and diskInfoFromMountedPath: are nearly identical
// Their main bodies should be consolidated
- (NSDictionary *)diskInfoForDeviceNodeAtURL:(NSURL *)nodeURL {

    NSDictionary *diskInfo = nil;
    DADiskRef disk = NULL;
    DASessionRef session = NULL;
    _unmountStatus = -1; //TODO: What is the unmount status?
    session = DASessionCreate ( kCFAllocatorDefault );

    disk = DADiskCreateFromBSDName ( kCFAllocatorDefault, session, nodeURL.path.fileSystemRepresentation);
    if ( disk != NULL ) {
        diskInfo = (NSDictionary *)CFBridgingRelease(DADiskCopyDescription(disk));
        CFRelease(disk);
    }
    CFRelease(session);
    return diskInfo;
}

- (NSDictionary *)diskInfoFromMountedPath:(NSString *)path {
    NSURL *volumeURL = [NSURL fileURLWithPath:path];
    return [self diskInfoForVolumeMountedAtURL:volumeURL];
}
- (NSDictionary *)diskInfoForVolumeMountedAtURL:(NSURL *)volumePath {

    NSDictionary *retDict=nil;
    DADiskRef disk = NULL;
    DASessionRef session = NULL;
    self.unmountStatus = -1;
    session = DASessionCreate ( kCFAllocatorDefault );
    disk =  DADiskCreateFromVolumePath( kCFAllocatorDefault, session, (CFURLRef)volumePath );

    if ( disk != NULL ) {
        retDict=(NSDictionary *)CFBridgingRelease(DADiskCopyDescription(disk));
        CFRelease(disk);
    }
    CFRelease(session);
    return retDict;
}


@end
